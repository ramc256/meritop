@extends('layouts.web.blank')

@section('content')

    <!-- Login -->
    <section class="g-height-100vh g-flex-centered g-bg-size-cover g-bg-pos-top-center" style="background-image: url(@if (empty($bg -> image)) {{ Storage::disk('images')->url('bg.jpg') }} @else {{ $_ENV['STORAGE_PATH'].$bg -> image }} @endif);">
        <div class="container g-py-100 g-pos-rel g-z-index-1">
            <div class="row justify-content-center">
                <div class="col-sm-8 col-lg-5">
                    <div class="g-bg-white rounded g-py-40 g-px-30">
                        <header class="text-center mb-4">
                            @if (empty($image))
                                <h4 class="text-capitalize">{{ $query_club -> name }}</h4>
                            @else
                                <img class="m-w-60" src="{{ $_ENV['STORAGE_PATH'].$image }}" alt="{{ $query_club -> name }}">
                            @endif
                            <p class="g-color-black g-my-20 text-center">Ingrese su correo para re-establecer su contraseña</p>                            
                        </header>

                        <!-- Form -->
                        <form action="{{ route('password.request') }}" method="post">
                            {{ csrf_field() }}
                            @include('flash::message')
                            <input type="hidden" name="token" value="{{ $token }}">
                            <input type="hidden" name="club" value="{{ $club }}">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="email" name="email" placeholder="Correo electronico" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" name="password" placeholder="Contraseña" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <input id="password-confirm" type="password" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" name="password_confirmation" placeholder="Confirmar Contraseña" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="mb-4">
                                <button class="btn btn-md btn-block u-btn-primary rounded g-py-13" type="submit">Recuperar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Login -->

@endsection
