@extends('layouts.web.blank')

@section('content')
    <!-- Login -->
    <section class="g-height-100vh g-flex-centered g-bg-size-cover g-bg-pos-top-center" style="background-image: url(@if (empty($bg -> image)) {{ Storage::disk('images')->url('bg.jpg') }} @else {{ $_ENV['STORAGE_PATH'].$bg -> image }} @endif);">
        <div class="container g-py-100 g-pos-rel g-z-index-1">
            <div class="row justify-content-center">
                <div class="col-sm-8 col-lg-5">
                    <div class="g-bg-white rounded g-py-40 g-px-30">
                        <header class="text-center mb-4">
                            @if (empty($image))
                                <h4 class="text-capitalize">{{ $club -> name }}</h4>
                            @else
                                <img class="m-w-60" src="{{ $_ENV['STORAGE_PATH'].$image }}" alt="{{ $club -> name }}">
                            @endif
                            
                        </header>
                        <!-- Form -->
                        <form action="{{ route('password.email') }}" method="post">
                            {{ csrf_field() }}
                            @if (session('status'))
                                <div class="alert alert-success">
                                {{ session('status') }}
                                </div>
                            @elseif($errors->has('email'))
                                <div class="alert alert-success">
                                    {{ $errors->first('email') }}
                                </div>
                            @else
                                <p class="g-color-black g-my-20 text-center">Ingrese su correo para re-establecer su contraseña</p>
                            @endif  
                            <div class="mb-4">
                                <input id="email" name="email" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="email" placeholder="Correo electronico" value="{{ old('email') }}">
                                <input type="hidden" name="gateway" value="{{ $club -> slug }}">  
                            </div>
                            <div class="mb-4">
                                <button class="btn btn-md btn-block u-btn-primary rounded g-py-13" type="submit">Recuperar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Login -->

@endsection
