@extends('layouts.web.blank')

@section('title', 'Iniciar sesion')
@section('content')
	<!-- Login -->
	<section class="g-height-100vh g-flex-centered g-bg-size-cover g-bg-pos-top-center" style="background-image: url(@if (empty($bg -> image)) {{ Storage::disk('images')->url('bg.jpg') }} @else {{ $_ENV['STORAGE_PATH'].$bg -> image }} @endif);">
		<div class="container g-py-100 g-pos-rel g-z-index-1">
			<div class="row justify-content-center">
				<div class="col-sm-8 col-lg-5">
					<div class="g-bg-white rounded g-py-40 g-px-30">
						<header class="text-center mb-4">
							@if (empty($image))
								<h4 class="text-capitalize">{{ $club -> name }}</h4>
							@else
								<img class="m-w-60" src="{{ $_ENV['STORAGE_PATH'].$image }}" alt="{{ $club -> name }}">
							@endif
						</header>

						<!-- Form -->
						<form class="g-py-15" action="{{ route('login') }}" method="post">
							{{ csrf_field() }}
							@include('flash::message')
							<input type="hidden" name="club" value="{{ $club -> id }}">
							<div class="mb-4">
								<input id="email" name="email" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="email" placeholder="Correo electronico">
							</div>

							<div class="g-mb-35">
								<input id="password" name="password" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15 mb-3" type="password" placeholder="Contraseña">
								<div class="row justify-content-between">
									<div class="col align-self-center">
										<label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-12 g-pl-25 mb-0">
											<input class="hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
											<div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
												<i class="fa" data-check-icon="&#xf00c"></i>
											</div>
											Recordarme
										</label>
									</div>
									<div class="col align-self-center text-right">
										<a href="{{ route('member.password.email', $club->slug) }}" id="forgot" class="g-font-size-12">¿Olvidó su contraseña?</a>
									</div>
								</div>
							</div>

							<div class="mb-4">
								<button class="btn btn-md btn-block u-btn-primary rounded g-py-13" type="submit">Entrar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Login -->
@endsection
