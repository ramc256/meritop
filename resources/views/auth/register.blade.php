@extends('layouts.web.blank')

@section('title', 'Recuperar contraseña')
@section('content')
	<!-- Login -->
	<section class="g-height-100vh g-flex-centered g-bg-size-cover g-bg-pos-top-center" style="background-image: url({{ asset('images/login/bg.jpg') }});">
		<div class="container g-py-100 g-pos-rel g-z-index-1">
			<div class="row justify-content-center">
				<div class="col-sm-8 col-lg-5">
					<div class="g-bg-white rounded g-py-40 g-px-30">
						<header class="text-center mb-4">
							<h2 class="h2 g-color-black g-font-weight-600">Login</h2>
						</header>

						<!-- Form -->
						<form class="g-py-15" action="{{ route('login') }}" method="post">
							{{ csrf_field() }}
							@if (empty($club))
								<input type="hidden" name="club" value="1">
							@else
								<input type="hidden" name="club" value="{{ $club -> id }}">
							@endif
							@include('flash::message')
							<div class="mb-4">
								<input id="email" name="email" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="email" placeholder="Correo electronico">
							</div>

							<div class="mb-4">
								<button class="btn btn-md btn-block u-btn-primary rounded g-py-13" type="submit">Recuperar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Login -->
@endsection
