@extends('layouts.web.blank')
@section('styles')
    <script src="https://use.fontawesome.com/3b647176ae.js"></script>
@endsection
@section('content')
    <!-- Login -->
    <section class="g-height-100vh g-flex-centered g-bg-size-cover g-bg-pos-top-center" style="background-image: url(@if (empty($bg -> image)) {{ Storage::disk('images')->url('bg.jpg') }} @else {{ $_ENV['STORAGE_PATH'].$bg -> image }} @endif);">
        <div class="container g-py-100 g-pos-rel g-z-index-1">
            <div class="row justify-content-center">
                <div class="col-sm-8 col-lg-6">
                    <div class="g-bg-white rounded g-py-40 g-px-30">
                        <div class="text-center">                          
                            @include('flash::message')  
                            <i style="display: block; margin-bottom: 30px; font-size: 80pt; color: #D9534F;" class="fa fa-times-circle-o"></i>
                            
                            <p>Para acceder a su cuenta dirijase al portal de su club como se muestra a continuación reemplazando el texto en azul por el nombre de su club.</p>
                            <img src="{{ asset('images/urlexample.jpg') }}" alt="urlexample">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Login -->

@endsection
