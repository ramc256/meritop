<nav class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar-->
    <section class="sidebar">
        <div id="menu" role="navigation">
            <div class="nav_profile">
                <div class="media profile-left">
                    <a class="pull-left profile-thumb" href="{{ url('admin/profile') }}">
                        @if (empty(Auth::guard('user')->user()-> image))
                            <div class="img-circle img-strech" style=" background-image: url( {{ Storage::disk('images') -> url('img-default-male.jpg') }} );"></div>
                        @else
                            <div class="img-circle img-strech" style=" background-image: url( {{ $_ENV['STORAGE_PATH'].Auth::guard('user')-> user() -> image }} );"></div>
                        @endif
                    </a>
                    <div class="content-profile">
                        <h4 class="media-heading"> {{ ucwords(Auth::guard('user')->user()->first_name . ' ' . Auth::guard('user')->user()->last_name) }}  </h4>
                    </div>
                </div>
            </div>
            <ul class="navigation">
                <li {!! (Request::is('admin')? 'class="active"':"") !!}>
                    <a href="{{ url('admin') }} ">
                        <i class="menu-icon fa fa-fw fa-tachometer"></i>
                        <span class="mm-text">Inicio</span>
                    </a>
                </li>
                <li  class="menu-dropdown {!! ( Request::is('admin/transactions') || Request::is('admin/transactions/*') || Request::is('admin/accounts') || Request::is('admin/accounts/*') || Request::is('admin/payload') || Request::is('admin/payload/*')  ? 'active' : '') !!}">
                    <a href="#">
                        <i class="menu-icon fa fa-line-chart"></i>
                        <span>Cuentas</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li {!! (Request::is('admin/transactions') || Request::is('admin/transactions/*') ? 'class="active"' : '') !!}>
                            <a href="{{ url('admin/transactions') }}">
                                <i class="fa fa-fw fa-file-text"></i> Consolidado
                            </a>
                        </li>
                        <li {!! (Request::is('admin/accounts') || Request::is('admin/accounts/*') ? 'class="active"' : '') !!}>
                            <a href="{{ url('admin/accounts') }}">
                                <i class="fa fa-fw fa-list-ul"></i> Estado de cuentas
                            </a>
                        </li>
                        <li {!! (Request::is('admin/payload') || Request::is('admin/payload/*') ? 'class="active"' : '') !!}>
                            <a href="{{ url('admin/payload') }}">
                                <i class="fa fa-fw fa-money "></i> Aporte de puntos
                            </a>
                        </li>
                    </ul>
                </li>
                <li  class="menu-dropdown {!! ( Request::is('admin/members') || Request::is('admin/members/*') || Request::is('admin/configuration') || Request::is('admin/configuration/*') || Request::is('admin/groups') || Request::is('admin/groups/*') ? 'active' : '') !!}">
                    <a href="#">
                        <i class="menu-icon fa fa-fw fa-user"></i>
                        <span>Miembros & Grupos </span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li {!! (Request::is('admin/members') || Request::is('admin/members/*') ? 'class="active"' : '') !!}>
                            <a href="{{ url('admin/members') }}">
                                <i class="fa fa-fw fa-male "></i> Miembros
                            </a>
                        </li>
                        <li {!! (Request::is('admin/groups') || Request::is('admin/groups/*') ? 'class="active"' : '') !!}>
                            <a href="{{ url('admin/groups') }}">
                                <i class="fa fa-fw fa-circle-o-notch"></i> Grupos
                            </a>
                        </li>
                    </ul>
                </li>
                <li  class="menu-dropdown {!! ( Request::is('admin/programs') || Request::is('admin/programs/*') || Request::is('admin/objetives') || Request::is('admin/objetives/*') || Request::is('admin/objetives') || Request::is('admin/objetives/*') ? 'active' : '') !!}">
                    <a href="{{ url('admin/programs') }}">
                        <i class="fa fa-fw fa-calendar"></i> Programas
                    </a>
                </li>
                @if(Auth::user() -> kind == 1)
                    <li  class="menu-dropdown {!! ( Request::is('admin/parents-products') || Request::is('admin/parents-products/*') || Request::is('admin/products') || Request::is('admin/products/*') || Request::is('admin/coupons') || Request::is('admin/coupons/*') || Request::is('admin/brands') || Request::is('admin/brands/*')|| Request::is('admin/markets') || Request::is('admin/markets/*') || Request::is('admin/categories') || Request::is('admin/categories/*') || Request::is('admin/inventory') || Request::is('admin/inventory/*') || Request::is('admin/subcategories') || Request::is('admin/subcategories/*') ? 'active' : '') !!}">
                        <a href="#">
                            <i class="menu-icon fa fa-fw fa-shopping-bag"></i>
                            <span>Productos</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! (Request::is('admin/parents-products') || Request::is('admin/parents-products/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/parents-products') }}">
                                    <i class="fa fa-fw fa-gift"></i> Items
                                </a>
                            </li>
                            <li {!! (Request::is('admin/brands') || Request::is('admin/brands/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/brands') }}">
                                    <i class="fa fa-fw fa-trademark "></i> Marcas
                                </a>
                            </li>
                             <li {!! (Request::is('admin/markets') || Request::is('admin/markets/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/markets') }}">
                                    <i class="fa fa-fw fa-bullseye"></i> Mercados
                                </a>
                            </li>
                            <li {!! (Request::is('admin/inventory') || Request::is('admin/inventory/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/inventory') }}">
                                    <i class="fa fa-fw fa-list-ol"></i> Inventario
                                </a>
                            </li>
                            <li {!! (Request::is('admin/categories') || Request::is('admin/categories/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/categories') }}">
                                    <i class="fa fa-fw fa-align-justify"></i> Categorías
                                </a>
                            </li>
                            <li {!! (Request::is('admin/subcategories') || Request::is('admin/subcategories/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/subcategories') }}">
                                    <i class="fa fa-fw fa-indent "></i> Sub-Categorías
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown {!! (Request::is('admin/orders') || Request::is('admin/orders/* ') || Request::is('admin/picking') || Request::is('admin/picking/* ') ? 'active' : '') !!}">
                        <a href="#">
                            <i class="menu-icon fa fa-fw fa-shopping-cart"></i>
                            <span>Ordenes</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                             <li {!! (Request::is('admin/orders') || Request::is('admin/orders/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/orders') }}">
                                    <i class="fa fa-fw fa-th-list"></i> Ordenes
                                </a>
                            </li>
                            <li {!! (Request::is('admin/picking') || Request::is('admin/picking/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/picking') }}">
                                    <i class="fa fa-fw fa-check-square-o"></i> Picking List
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li {!! (Request::is('admin/clubs') || Request::is('admin/clubs/*') ? 'class="active"' : '') !!}>
                        <a href="{{ url('admin/clubs') }} ">
                            <i class="menu-icon fa fa-fw fa-institution"></i>
                            <span class="mm-text">Clubs</span>
                        </a>
                    </li>
                    <li class="menu-dropdown {!! ( Request::is('admin/roles') || Request::is('admin/users') || Request::is('admin/roles/*') || Request::is('admin/users/*') ? 'active' : '') !!}">
                        <a href="#">
                            <i class="menu-icon fa fa-fw fa-users"></i>
                            <span>Usuarios y Roles</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! ( Request::is('admin/users') || Request::is('admin/users/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/users') }}">
                                    <i class="fa fa-fw fa-user"></i> Usuarios
                                </a>
                            </li>
                            <li {!! ( Request::is('admin/roles') || Request::is('admin/roles/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/roles') }}">
                                    <i class="fa fa-fw fa-share"></i> Roles
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li {!! (Request::is('admin/logs')? 'class="active"':"") !!}>
                        <a href="{{ url('admin/logs') }} ">
                            <i class="menu-icon fa fa-fw fa-database"></i>
                            <span class="mm-text ">Logs</span>
                        </a>
                    </li>
                    <li {!! (Request::is('admin/messages') || Request::is('admin/messages/*') ? 'class="active"' : '') !!}>
                        <a href="{{ url('admin/messages') }} ">
                            <i class="menu-icon fa fa-fw fa-envelope"></i>
                            <span class="mm-text ">Mensajes</span>
                        </a>
                    </li>
                @endif
                @if(Auth::user() -> kind == 0)
                    <li {!! (Request::is('admin/messages') || Request::is('admin/messages/*') ? 'class="active"' : '') !!}>
                        <a href="{{ url('admin/messages') }} ">
                            <i class="menu-icon fa fa-fw fa-envelope"></i>
                            <span class="mm-text ">Mensajes</span>
                        </a>
                    </li>
                    <li  class="menu-dropdown {!! ( Request::is('admin/banners')  || Request::is('admin/branches') || Request::is('admin/branches/*') || Request::is('admin/departments') || Request::is('admin/departments/*') ? 'active':"") !!}">
                        <a href="#">
                            <i class="menu-icon fa fa-fw fa-cog"></i>
                            <span class="">Configuración</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li {!! ( Request::is('admin/banners') || Request::is('admin/banners/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/banners') }} ">
                                    <i class="fa fa-fw fa-bookmark"></i>Banners
                                </a>
                            </li>
                            <li {!! ( Request::is('admin/branches') || Request::is('admin/branches/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/branches') }} ">
                                    <i class="fa fa-fw fa-home"></i> Sedes
                                </a>
                            </li>
                            <li {!! ( Request::is('admin/departments') || Request::is('admin/departments/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/departments') }} ">
                                    <i class="fa fa-fw fa-home "></i> Departamentos
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                 <li  class="menu-dropdown {!! ( Request::is('admin/reports/exchange') || Request::is('admin/reports/exchange/*') || Request::is('admin/reports/inventory-output') || Request::is('admin/reports/inventory-output/*') || Request::is('admin/reports/exchange-item') || Request::is('admin/reports/exchange-item/*' ) || Request::is('admin/reports/contribution-exchange') || Request::is('admin/reports/contribution-exchange/*' ) || Request::is('admin/reports/exchange-user') || Request::is('admin/reports/exchange-user/*' )? 'active' : '') !!}">
                    <a href="#">
                        <i class="menu-icon fa fa-line-chart"></i>
                        <span>Reportes</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li {!! (Request::is('admin/reports/exchange-user') || Request::is('admin/reports/exchange-user/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/reports/exchange-user') }}">
                                    <i class="fa fa-fw fa-check-square-o"></i> Reporte de usuario
                                </a>
                        </li>
                        <li {!! (Request::is('admin/reports/inventory-output') || Request::is('admin/reports/inventory-output/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/reports/inventory-output') }}">
                                    <i class="fa fa-fw fa-check-square-o"></i> Salida de inventario
                                </a>
                        </li>
                        <li {!! (Request::is('admin/reports/exchange-item') || Request::is('admin/reports/exchange-item/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/reports/exchange-item') }}">
                                    <i class="fa fa-fw fa-check-square-o"></i> Canjes de Items
                                </a>
                        </li>
                         <li {!! (Request::is('admin/reports/contribution-exchange/') || Request::is('admin/reports/contribution-exchange/*') ? 'class="active"' : '') !!}>
                                <a href="{{ url('admin/reports/contribution-exchange') }}">
                                    <i class="fa fa-fw fa-check-square-o"></i> Aporte & Canjes
                                </a>
                        </li>
                    </ul>
                </li>
            </ul>
        <!-- / .navigation -->
        </div>
    <!-- menu -->
    </section>
    <!-- /.sidebar -->
</nav>
