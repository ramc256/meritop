<header class="header">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="{{ url('/') }} " class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <img width="90" src="{{asset('images/logo-white.svg')}}" alt="logo"/>
        </a>
        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <i class="fa fa-fw fa-bars"></i>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle padding-user" data-toggle="dropdown">
                        <div class="riot">
                           {{ ucwords(Auth::user()->first_name . ' ' . Auth::user()->last_name) }}
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if (empty(Auth::user()-> image) )
                                <div class="img-circle img-strech" style=" background-image: url( {{ Storage::disk('images') -> url('img-default-male.jpg') }} );"></div>
                            @else 
                                <div class="img-circle img-strech" style=" background-image: url( {{ Storage::disk('users') -> url( Auth::guard('user')->user()-> image ) }} );"></div>
                            @endif
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('admin/profile') }}"><i class="fa fa-fw fa-user"></i> Mi perfil </a>
                            </div>
                            <div class="pull-right">
                                <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>Salir</a>
                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
