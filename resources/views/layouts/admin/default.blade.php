<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Meritop | @yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}"/>
    <!-- global css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/app.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/custom.css') }}"/>

    <!-- Font Awesome css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}"/>

    <!-- Pnotify plugin css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/pnotify/css/pnotify.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/pnotify/css/pnotify.buttons.css') }}"/>

    @yield('styles')
    <!-- end of global css -->
</head>

<body class="skin-coreplus">
    {{-- <div class="preloader">
        <div class="loader_img"><img src="{{ asset('core-plus-theme/images/loader.gif') }}" alt="loading..." height="64" width="64"></div>
    </div> --}}
<!-- header logo: style can be found in header-->
@include('layouts.admin.header')

<!-- page wrapper-->
<div class="wrapper row-offcanvas row-offcanvas-left ">
    <!-- navbar left -->
    @include('layouts.admin.nav')
    <!-- end navbar left -->
    <main id="main" class="right-side" role="main">
        <!-- Content -->
        @yield('content')
        <!-- end Content -->
    </main>
</div>
@yield('modals')
<!-- JQuery JS -->
<script type="text/javascript" src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<!-- Global js -->
<script type="text/javascript" src="{{ asset('core-plus-theme/js/app.js') }}"></script>

<!-- end of global js -->
@yield('scripts')
</body>

</html>
