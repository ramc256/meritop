<!DOCTYPE html>
<html lang="es">

<head>
    <title>Meritop - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" />
    <!-- Global CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/app.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/custom.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/login.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/iCheck/css/all.css') }}"/>
</head>

<body class="bg-slider">
    <div class="container">
        @yield('content')
    </div>
    <!-- JQuery JS -->
    <script type="text/javascript" src="{{ asset('vendor/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- Core Plus Theme JS -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/backstretch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/login.js') }}"></script>
    <!-- iCheck JS -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>

</body>

</html>
