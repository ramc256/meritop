<!DOCTYPE html>
<html lang="es">
	<head>
		<!-- Title -->
		<title>Meritop | @yield('title')</title>
		<!-- Required Meta Tags Always Come First -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<script>
	        window.Laravel = <?php echo json_encode([
	            'csrfToken' => csrf_token(),
	        ]); ?>
	    </script>
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<!-- Favicon -->
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
		<!-- CSS Global Compulsory -->
		<link rel="stylesheet" href="{{ asset('vendor/bootstrap/bootstrap.css') }}">
		<!-- CSS Unify -->
		<link rel="stylesheet" href="{{ asset('css/unify.css') }}">
		<!-- CSS Unify Theme -->
		<link rel="stylesheet" href="{{ asset('css/styles.css') }}">
		<!-- CSS Customization -->
		<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
		<!-- CSS Dinamics -->
		<style>
			.g-color-primary, .g-color-primary--hover:hover{color: {{ session('club') -> color}} !important;}
			.bg-color, .u-btn-primary, .g-bg-primary, .g-bg-primary--active.active, .active .g-bg-primary--active {background-color: {{ session('club') -> color}} !important;}
			.g-brd-primary{border-color: {{ session('club') -> color}} !important;}
			.pagination .active span{
    			border-color: {{ session('club') -> color}} !important;
    			background-color: {{ session('club') -> color}} !important;
    		}
		</style>

		<!-- ============== Web Styles ============== -->

		<!-- CSS Implementing Plugins -->
		<link rel="stylesheet" href="{{ asset('vendor/icon-awesome/css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/icon-line-pro/style.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/slick-carousel/slick/slick.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/icon-hs/style.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/icon-line/css/simple-line-icons.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/animate.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/hamburgers/hamburgers.min.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/hs-megamenu/src/hs.megamenu.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/dzsparallaxer/dzsparallaxer.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/dzsparallaxer/dzsscroller/scroller.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/dzsparallaxer/advancedscroller/plugin.css') }}">
		<!-- Revolution Slider -->
		<link rel="stylesheet" href="{{ asset('vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/revolution-slider/revolution/css/settings.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/revolution-slider/revolution/css/layers.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/revolution-slider/revolution/css/navigation.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/revolution-slider/revolution-addons/typewriter/css/typewriter.css') }}">
			@yield('styles')

	</head>

	<body>
		@include('layouts.web.nav')
		@include('layouts.web.slider')
		<main>
			@yield('content')
		</main>
		@include('layouts.web.footer')
		@include('layouts.web.modals')
		<!-- ============== End Content ============== -->
			
		<!-- ============== Scripts WebSite ============== -->

		<!-- JS Global Compulsory -->
		<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
		<script src="{{ asset('vendor/tether.min.js') }}"></script>
		<script src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>	
	 
		<!-- HS Components -->
		<script src="{{ asset('js/hs.core.js') }}"></script>
		<script src="{{ asset('js/components/hs.header.js') }}"></script>
		<script src="{{ asset('js/helpers/hs.hamburgers.js') }}"></script>
		<script src="{{ asset('js/components/hs.dropdown.js') }}"></script>
		<script src="{{ asset('js/components/hs.scrollbar.js') }}"></script>
		<script src="{{ asset('js/components/hs.countdown.js') }}"></script>
		<script src="{{ asset('js/components/hs.tabs.js') }}"></script>
		<script src="{{ asset('js/components/hs.count-qty.js') }}"></script>
		<script src="{{ asset('js/components/hs.carousel.js')}}"></script>
		<!-- End HS Components -->

		
		<!-- JS Implementing Plugins -->
		<script src="{{ asset('vendor/jquery.countdown.min.js') }}"></script>
		<script src="{{ asset('vendor/slick-carousel/slick/slick.js') }}"></script>
		<script src="{{ asset('vendor/hs-megamenu/src/hs.megamenu.js') }}"></script>
		<script src="{{ asset('vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
		<script src="{{ asset('vendor/dzsparallaxer/dzsparallaxer.js') }}"></script>
		<script src="{{ asset('vendor/dzsparallaxer/dzsscroller/scroller.js') }}"></script>

		<!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-86243421-2"></script>
		<script>
			 window.dataLayer = window.dataLayer || [];
			 function gtag(){dataLayer.push(arguments)};
			 gtag('js', new Date());

			 gtag('config', 'UA-86243421-2');
		</script>
	
		<!-- ============== Scripts Page ============== -->
		@yield('scripts')
		<script>
			;(function ($) {
				'use strict';
				$(document).on('ready', function () {
					// Header
					// $.HSCore.components.HSHeader.init($('#js-header'));
					$.HSCore.helpers.HSHamburgers.init('.hamburger');
					// $.HSCore.components.HSCountQty.init('.js-quantity');

					// // Initialization of HSDropdown component
					$.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
						afterOpen: function(){
							$(this).find('input[type="search"]').focus();
						}
					});

					// Initialization of HSScrollBar component
					$.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

					// Initialization of HSMegaMenu plugin
					$('.js-mega-menu').HSMegaMenu({
						event: 'hover',
						pageContainer: $('.container'),
						breakpoint: 991
					});
					// $.HSCore.components.HSTabs.init('[data-tabs-mobile-type]');
				});
			})(jQuery);
			
		</script>

		{{-- <script>
			$(document).on('ready', function () {
				$('[data-toggle="tooltip"]').tooltip();
			});

			;(function ($) {
				'use strict';
				$(document).on('ready', function () {
					// Carousel
					$.HSCore.components.HSCarousel.init('[class*="js-carousel"]');

					$('#js-carousel-1').slick('setOption', 'responsive', [{
						breakpoint: 992,
						settings: {
							slidesToShow: 3
						}
					}, {
						breakpoint: 768,
						settings: {
							slidesToShow: 2
						}
					}, {
						breakpoint: 554,
						settings: {
							slidesToShow: 2
						}
					}], true);

				});

			})(jQuery);
		</script> --}}
	</body>
</html>
