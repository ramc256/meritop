<!DOCTYPE html>
<html lang="es">
		<head>
			<!-- Title -->
			<title>Meritop | Mi cuenta - @yield('title')</title>
			<!-- Required Meta Tags Always Come First -->
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<!-- Favicon -->
			<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
			<!-- Google Fonts -->
			<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
			<!-- CSS Global Compulsory -->
			<link rel="stylesheet" href="{{ asset('vendor/bootstrap/bootstrap.min.css') }}">
			<!-- CSS Unify -->
			<link rel="stylesheet" href="{{ asset('css/unify.css') }}">
			<!-- CSS Styles -->
			<link rel="stylesheet" href="{{ asset('css/styles.css') }}">
			<!-- CSS Customization -->
			<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
			<!-- CSS Implementing Plugins -->
			<link rel="stylesheet" href="{{ asset('vendor/icon-awesome/css/font-awesome.min.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/icon-line-pro/style.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/slick-carousel/slick/slick.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/icon-hs/style.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/icon-line/css/simple-line-icons.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/animate.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/hamburgers/hamburgers.min.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/hs-megamenu/src/hs.megamenu.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css') }}">
			<!-- CSS Parallax -->
			<link rel="stylesheet" href="{{ asset('vendor/dzsparallaxer/dzsparallaxer.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/dzsparallaxer/dzsscroller/scroller.css') }}">
			<link rel="stylesheet" href="{{ asset('vendor/dzsparallaxer/advancedscroller/plugin.css') }}">
			
			<!-- CSS Dinamics -->
			<style>
				.color-primary{color: {{ session('club') -> color}} !important;}
				.bg-color, .u-btn-primary, .g-bg-primary, .list-group-item.active, .u-nav-v1-1.u-nav-primary .nav-link.active{background-color: {{ session('club') -> color}} !important;}
				.g-brd-primary,.list-group-item.active{border-color: {{ session('club') -> color}} !important;}
			.pagination .active span{
    			border-color: {{ session('club') -> color}} !important;
    			background-color: {{ session('club') -> color}} !important;
    		}
			</style>
			<!-- ============== Page Styles ============== -->

			@yield('styles')
		</head>
		<body>

			<!-- ============== Navegator ============== -->
			@include('layouts.web.nav')
			<!-- ============== Breadcrum ============== -->
			<header class="shortcode-html">
				<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image}})">
				  	<div class="container g-bg-cover__inner">
					    <header class="g-mb-20">
				      		<h2 class="h1 g-font-weight-300 text-uppercase">@yield('title')</h2>
					    </header>

					    <ul class="u-list-inline">
					      	<li class="list-inline-item g-mr-7">
						        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Inicio</a>
						        <i class="fa fa-angle-right g-ml-7"></i>
					      	</li>
					      	<li class="list-inline-item g-mr-7">
						        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ route('my-account.index') }}">Mi cuenta</a>
						        <i class="fa fa-angle-right g-ml-7"></i>
					      	</li>
					      	<li class="list-inline-item g-color-white">
					        	<span>@yield('title')</span>
					      	</li>
					    </ul>
				  	</div>
				</section>
			</header>
			<!-- ============== Main ============== -->
			
			<main class="g-mb-100 container">
				<div class="row">
					<!-- Profile Sidebar -->
					<aside id="aside-page" class="col-lg-3 g-mb-50 g-mb-0--lg">
						@include('layouts.web.myaccount-aside')
					</aside>
					<!-- Profile Content -->
					<section id="content-page" class="col-lg-9">
						@include('flash::message')
						@yield('content')
					</section>
				</div>
			</main>
			<!-- ============== Footer ============== -->
			@include('layouts.web.footer')

			<!-- ============== Modals ============== -->
			
			@include('layouts.web.modals')
			@yield('modals')

			<!-- ============== Scripts WebSite ============== -->

			<!-- JS Global Compulsory -->
			<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
			<script src="{{ asset('vendor/tether.min.js') }}"></script>
			<script src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>
			<!-- JS Customization -->
			<script src="{{ asset('js/custom.js') }}"></script>
			<!-- JS Implementing Plugins -->
			<script src="{{ asset('vendor/jquery.countdown.min.js') }}"></script>
			<script src="{{ asset('vendor/slick-carousel/slick/slick.js') }}"></script>
			<script src="{{ asset('vendor/hs-megamenu/src/hs.megamenu.js') }}"></script>
			<script src="{{ asset('vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
			<!-- JS Parallax Plugins -->
			<script src="{{ asset('vendor/dzsparallaxer/dzsparallaxer.js') }}"></script>
			<script src="{{ asset('vendor/dzsparallaxer/dzsscroller/scroller.js') }}"></script>
			<!-- JS Unify -->
			<script src="{{ asset('js/hs.core.js') }}"></script>
			<script src="{{ asset('js/components/hs.header.js') }}"></script>
			<script src="{{ asset('js/helpers/hs.hamburgers.js') }}"></script>
			<script src="{{ asset('js/components/hs.dropdown.js') }}"></script>
			<script src="{{ asset('js/components/hs.scrollbar.js') }}"></script>
			<script src="{{ asset('js/components/hs.countdown.js') }}"></script>
			<script src="{{ asset('js/components/hs.carousel.js') }}"></script>
			<script src="{{ asset('js/components/hs.tabs.js') }}"></script>
			<!-- JS Plugins Init. -->
			<script>
				;(function ($) {
					'use strict';
					$(document).on('ready', function () {
						// Header
						$.HSCore.components.HSHeader.init($('#js-header'));
						$.HSCore.helpers.HSHamburgers.init('.hamburger');

						// Initialization of HSDropdown component
						$.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
							afterOpen: function(){
								$(this).find('input[type="search"]').focus();
							}
						});

						// Initialization of HSScrollBar component
						$.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

						// Initialization of HSMegaMenu plugin
						$('.js-mega-menu').HSMegaMenu({
							event: 'hover',
							pageContainer: $('.container'),
							breakpoint: 991
						});
						$.HSCore.components.HSTabs.init('[data-tabs-mobile-type]');
					});
				})(jQuery);
			</script>

			<script>
				$(document).on('ready', function () {
					$('[data-toggle="tooltip"]').tooltip();
				});

				;(function ($) {
					'use strict';
					$(document).on('ready', function () {
						// Carousel
						$.HSCore.components.HSCarousel.init('[class*="js-carousel"]');

						$('#js-carousel-1').slick('setOption', 'responsive', [{
							breakpoint: 992,
							settings: {
								slidesToShow: 3
							}
						}, {
							breakpoint: 768,
							settings: {
								slidesToShow: 2
							}
						}, {
							breakpoint: 554,
							settings: {
								slidesToShow: 2
							}
						}], true);
					});

					// initialiation of countdowns
					var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
						yearsElSelector: '.js-cd-years',
						monthElSelector: '.js-cd-month',
						daysElSelector: '.js-cd-days',
						hoursElSelector: '.js-cd-hours',
						minutesElSelector: '.js-cd-minutes',
						secondsElSelector: '.js-cd-seconds'
					});
				})(jQuery);
			</script>
			<!-- ============== Scripts Page ============== -->
			@yield('scripts')
		</body>
</html>
