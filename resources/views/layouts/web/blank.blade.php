<!DOCTYPE html>
<html lang="es">
	<head>
		<!-- Title -->
		<title>Meritop | @yield('title')</title>

		<!-- Required Meta Tags Always Come First -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		<!-- Favicon -->
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

		<!-- CSS Global Compulsory -->
		<link rel="stylesheet" href="{{ asset('vendor/bootstrap/bootstrap.min.css') }}">

		<!-- CSS Unify -->
		<link rel="stylesheet" href="{{ asset('css/unify.css') }}">

		<!-- CSS Unify Theme -->
		<link rel="stylesheet" href="{{ asset('css/styles.css') }}">

		<!-- CSS Customization -->
		<link rel="stylesheet" href="{{ asset('css/custom.css') }}">

		@if (!empty($club -> color))
			<!-- CSS Dinamics -->
			<style>
				.color-primary, .g-color-primary--hover:hover{color: {{ $club -> color}} !important;}
				.bg-color, .u-btn-primary, .g-bg-primary{background-color: {{ $club -> color}} !important;}
				.g-brd-primary{border-color: {{ $club -> color}} !important;}
			</style>
		@endif

		@yield('styles')

	</head>

	<body>
		<main>
			@yield('content')
		</main>
	
		<!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-86243421-2"></script>
		<script>
			 window.dataLayer = window.dataLayer || [];
			 function gtag(){dataLayer.push(arguments)};
			 gtag('js', new Date());

			 gtag('config', 'UA-86243421-2');
		</script>
		<!-- ============== Scripts Page ============== -->
		@yield('scripts')
	</body>
</html>
