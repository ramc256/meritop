
			<!-- ===================== Footer ===================== -->
			<section id="footer-page" class="g-pt-100">
				<div id="shortcode1">
					<!-- Footer -->
					<div class="g-bg-black-opacity-0_9 g-color-white-opacity-0_8 g-py-60">
						<div class="container">
							<div class="row">
								<!-- Footer Content -->
								<div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
										<span class="g-color-white g-font-size-10 g-font-weith-100">powered by</span><br>
									<a class="d-block g-mb-20" href="index.html">
										<img width="50%" class="img-fluid" src="{{ asset('images/logo-white.svg') }}" alt="Logo">
									</a>
								</div>
								<!-- End Footer Content -->

								
								<!-- Footer Content -->
								<div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
									<div class="u-heading-v3-1 g-brd-white-opacity-0_3 g-mb-25">
										<h2 class="u-heading-v3__title h6 text-uppercase g-brd-primary">Portal</h2>
									</div>

									<nav class="text-uppercase1">
										<ul class="list-unstyled g-mt-minus-10 mb-0">
											<li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
												<h4 class="h6 g-pr-20 mb-0">
													<a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ url('programs') }}">Programas</a>
													<i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
												</h4>
											</li>
											<li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
												<h4 class="h6 g-pr-20 mb-0">
													<a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ url('directory') }}">Directorio</a>
													<i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
												</h4>
											</li>
											<li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
												<h4 class="h6 g-pr-20 mb-0">
													<a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ url('mall') }}">Mall</a>
													<i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
												</h4>
											</li>
										</ul>
									</nav>
								</div>
								<!-- End Footer Content -->

								<!-- Footer Content -->
								<div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
									<div class="u-heading-v3-1 g-brd-white-opacity-0_3 g-mb-25">
										<h2 class="u-heading-v3__title h6 text-uppercase g-brd-primary">Mi cuenta</h2>
									</div>

									<nav class="text-uppercase1">
										<ul class="list-unstyled g-mt-minus-10 mb-0">
											<li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
												<h4 class="h6 g-pr-20 mb-0">
													<a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('my-account.index') }}">Transacciones</a>
													<i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
												</h4>
											</li>
											<li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
												<h4 class="h6 g-pr-20 mb-0">
													<a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('my-account.orders') }}">Ordenes</a>
													<i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
												</h4>
											</li>
											<li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
												<h4 class="h6 g-pr-20 mb-0">
													<a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('imessages.index') }}">Mensajes</a>
													<i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
												</h4>
											</li>
											<li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
												<h4 class="h6 g-pr-20 mb-0">
													<a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('my-account.configuration') }}">Configuración</a>
													<i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
												</h4>
											</li>
										</ul>
									</nav>
								</div>
								<!-- End Footer Content -->

								<!-- Footer Content -->
								<div class="col-lg-3 col-md-6">
									<div class="u-heading-v3-1 g-brd-white-opacity-0_3 g-mb-25">
										<h2 class="u-heading-v3__title h6 text-uppercase g-brd-primary">Contactanos</h2>
									</div>

									<address class="g-bg-no-repeat g-font-size-12 mb-0" >
										<!-- Location -->
										<div class="d-flex g-mb-20">
											<div class="g-mr-10">
												<span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
													<i class="fa fa-map-marker"></i>
												</span>
											</div>
											<p class="mb-0">Urbanización Boleita Sur. <br> Caracas, Venezuela.</p>
										</div>
										<!-- End Location -->

										<!-- Phone -->
										<div class="d-flex g-mb-20">
											<div class="g-mr-10">
												<span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
													<i class="fa fa-phone"></i>
												</span>
											</div>
											<p class="mb-0">+58 (212) 239 8820 <br> +58 (212) 232 1063</p>
										</div>
										<!-- End Phone -->

										<!-- Email and Website -->
										<div class="d-flex g-mb-20">
											<div class="g-mr-10">
												<span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
													<i class="fa fa-globe"></i>
												</span>
											</div>
											<p class="mb-0">
												<a class="g-color-white-opacity-0_8 g-color-white--hover" href="mailto:info@meritop.com">info@meritop.com</a>
												<br>
												<a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">www.meritop.com</a>
											</p>
										</div>
										<!-- End Email and Website -->
									</address>
								</div>
								<!-- End Footer Content -->								
							</div>
						</div>
					</div>
					<!-- End Footer -->

					<!-- Copyright Footer -->
					<footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
						<div class="container">
							<div class="row">
								<div class="col-md-8 text-center text-md-left g-mb-15 g-mb-0--md">
									<div class="d-lg-flex">
										<small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2017 © Meritop. Todos los derechos reservados</small>
										<ul class="u-list-inline">
											<li class="list-inline-item">
												<a class="g-color-white" href="{{ url('politics') }}" target="_blank" title="Politicas">Politicas de privacidad</a>
											</li>
											<li class="list-inline-item">
												<span>|</span>
											</li>
											<li class="list-inline-item">
												<a class="g-color-white" href="{{ url('terms') }}" target="_blank" title="Terminos">Terminos de uso</a>
											</li>
											<li class="list-inline-item">
												<span>|</span>
											</li>
											<li class="list-inline-item">
												<a class="g-color-white" href="{{ url('faq') }}" target="_blank" title="Terminos">Preguntas frecuentes</a>
											</li>
										</ul>
									</div>
								</div>

								<div class="col-md-4 align-self-center">
									<ul class="list-inline text-center text-md-right mb-0">
										<li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
											<a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
											<a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Instagram">
											<a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
												<i class="fa fa-instagram"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</footer>
					<!-- End Copyright Footer -->
				</div>
			</section>
			<!-- End Footer #01 -->