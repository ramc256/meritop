
	<!-- User Image -->
	<div class="u-block-hover g-pos-rel">
		<figure>
			@if (empty(Auth::user() -> image))
				@if (Auth::user() -> gender == 0)
					<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}">
				@else
					<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}">
				@endif
			@else
				<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{ $_ENV['STORAGE_PATH'].Auth::user() -> image }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}">
			@endif
		</figure>

		<!-- Figure Caption -->
		<figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
			<div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
				<!-- Figure Social Icons -->
				<ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
					<li class="list-inline-item align-middle g-mx-7">
						<a class="u-icon-v1 u-icon-size--md g-color-white"  data-toggle="modal" data-target="#myModal">
							<i class="icon-note u-line-icon-pro"></i>
						</a>
					</li>
				</ul>
				<!-- End Figure Social Icons -->
			</div>
		</figcaption>
		<!-- End Figure Caption -->

		<!-- User Info -->
		<span class="g-pos-abs g-top-20 g-left-0">
			<span class="btn btn-sm u-btn-primary rounded-0">{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}</span>
		</span>
		<!-- End User Info -->
	</div>
	<!-- User Image -->

	<!-- Sidebar Navigation -->
	<div class="list-group list-group-border-0 g-mb-40">
		<!-- Overall -->
		<a href="{{ route('my-account.index') }}" class="list-group-item @if (Request::is('my-account')) active @else color-primary @endif">
			<span><i class="icon-home g-pos-rel g-top-1 g-mr-8"></i> Transacciones</span>
		</a>
		<!-- End Overall -->

		<!-- Profile -->
		<a href="{{ route('my-account.orders') }}" class="list-group-item @if (Request::is('my-account/orders')) active @else color-primary @endif">
			<span><i class="icon-cursor g-pos-rel g-top-1 g-mr-8"></i> Ordenes</span>
		</a>
		<!-- End Profile -->

		<!-- Settings -->
		<a href="{{ route('imessages.index') }}" class="list-group-item @if (Request::is('my-account/imessages') || Request::is('my-account/imessages/*')) active @else color-primary @endif">
			<span><i class="icon-envelope g-pos-rel g-top-1 g-mr-8"></i> Mensajes</span>
		</a>
		<!-- End Settings -->

		<!-- messages -->
		<a href="{{ route('my-account.configuration') }}" class="list-group-item @if (Request::is('my-account/configuration') || Request::is('my-account/configuration/*')) active @else color-primary @endif">
			<span><i class="icon-settings g-pos-rel g-top-1 g-mr-8"></i> Configuración</span>
		</a>
		<!-- End messages -->
	</div>
	<!-- End Sidebar Navigation -->
