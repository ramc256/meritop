
<header id="header-page">
	<div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
		<nav class="js-mega-menu navbar navbar-toggleable-md">
			<div class="container">
				<!-- Responsive Toggle Button -->
				<button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-right-0" type="button"
								aria-label="Toggle navigation"
								aria-expanded="false"
								aria-controls="navBar"
								data-toggle="collapse"
								data-target="#navBar">
					<span class="hamburger hamburger--slider">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
				</button>
				<!-- End Responsive Toggle Button -->

				<!-- Logo -->
				<a href="{{ url('/') }}" class="navbar-brand">
					<img class="m-w-60" src="{{ $_ENV['STORAGE_PATH'] . session('club') -> image }}" alt="{{ ucwords(session('club') -> name) }}">
				</a>
				<!-- End Logo -->

				<!-- Navigation -->
				<div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg" id="navBar">
					<ul class="navbar-nav text-uppercase g-font-weight-600 ml-auto  u-dropdown--css-animation ">
						<li class="nav-item g-mx-10--lg g-mx-15--xl">
							<a href="{{ url('/') }}" class="nav-link g-px-0">Inicio</a>
						</li>
						<li class="nav-item g-mx-10--lg g-mx-15--xl">
							<a href="{{ url('/programs') }}" class="nav-link g-px-0">Programas</a>
						</li>
						<li class="nav-item g-mx-10--lg g-mx-15--xl">
							<a href="{{ url('/directory') }}" class="nav-link g-px-0">Directorio</a>
						</li>
						<!-- Mega Menu Item -->
						<li class="nav-item hs-has-mega-menu g-mx-10--lg g-mx-15--xl"
							data-animation-in="fadeIn"
							data-animation-out="fadeOut"
							data-position="right">
							<a id="mega-menu-label-3" class="nav-link g-px-0" href="{{ route('store.index') }}" aria-haspopup="true" aria-expanded="false">Mall <i class="hs-icon hs-icon-arrow-bottom g-font-size-11 g-ml-7"></i></a>

							<!-- Mega Menu -->
							<div class="w-100 hs-mega-menu u-shadow-v11 g-text-transform-none g-font-weight-400 g-brd-top g-brd-primary g-brd-top-2 g-bg-white g-pa-30 g-mt-17 g-mt-7--lg--scrolling" aria-labelledby="mega-menu-label-3">
								<div class="row">
									@foreach (session('subcategories') as $key => $element)
									<div class="col-sm-6 col-md-3 g-mb-30 g-mb-0--sm">
										<h4 class="h5 text-uppercase g-font-weight-600 g-color-primary"><a href="{{ url('mall/' . $element[0] -> slug_category) }}">{{ $key }}</a></h4>
										<ul class="list-unstyled">
											@foreach ($element as $value)
												<li class="g-mb-5"><a class="g-color-main text-capitalize" href="{{ url('mall/' . $value -> slug_category . '/' . $value -> slug ) }}">{{ $value -> name }}</a></li>
											@endforeach
										</ul>
									</div>
									@endforeach
								</div>
							</div>
							<!-- End Mega Menu -->
						</li>		
						<li class="nav-item hs-has-sub-menu g-mx-10--lg g-mx-15--xl">
							<a id="nav-link--pages" class="nav-link g-px-0" href="#" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu--pages">Mi cuenta</a>
							<!-- Submenu -->
							<ul class="hs-sub-menu list-unstyled g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-py-7 g-mt-22 g-mt-12--lg--scrolling" id="nav-submenu--pages" aria-labelledby="nav-link--pages">
								<li class="g-color-primary g-font-weight-400 g-font-size-14 g-py-10 text-center">
									<span class="g-pa-10">{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}</span>
								</li>
								<hr class="g-my-0">
								<li class="dropdown-item"><a class="nav-link" href="{{ route('my-account.index') }}">Transacciones</a></li>
								<li class="dropdown-item"><a class="nav-link" href="{{ route('my-account.orders') }}">Ordenes</a></li>
								<li class="dropdown-item"><a class="nav-link" href="{{ route('imessages.index') }}">Mensajes</a></li>
								<li class="dropdown-item g-brd-gray-light-v4"><a class="nav-link" href="{{ route('my-account.configuration') }}">Configuración</a></li>
								<hr class="g-my-0">
								<li class="dropdown-item">
									<form id="logout-form" action="{{ route('logout') }}" method="post">
										{{ csrf_field() }}
									</form>
									<a class="nav-link" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Salir</a>
								</li>
							</ul>
							<!-- End Submenu -->
						</li>					
					</ul>
				</div>
				<!-- End Navigation -->	


				<!-- Basket -->
				<div class="u-basket d-inline-block g-valign-middle g-pt-6 g-ml-30 g-ml-0--lg">
					<a href="#" id="basket-bar-invoker" class="u-icon-v1 g-color-main g-text-underline--none--hover g-width-20 g-height-20"
						 aria-controls="basket-bar"
						 aria-haspopup="true"
						 aria-expanded="false"
						 data-dropdown-event="hover"
						 data-dropdown-target="#basket-bar"
						 data-dropdown-type="css-animation"
						 data-dropdown-duration="500"
						 data-dropdown-hide-on-scroll="false"
						 data-dropdown-animation-in="fadeIn"
						 data-dropdown-animation-out="fadeOut">
						<span class="u-badge-v1--sm g-color-white g-bg-primary g-rounded-50x">{{ sizeof(Cart::content()) }}</span>
						<i class="fa fa-shopping-cart"></i>
					</a>

					<div id="basket-bar" class="u-basket__bar u-dropdown--css-animation u-dropdown--hidden g-brd-top g-brd-2 g-brd-primary  g-color-main g-mt-25--lg g-mt-15--lg--scrolling" aria-labelledby="basket-bar-invoker">
						<div class="js-scrollbar">
							@if (sizeof(Cart::content()) > 0)
								@foreach (Cart::content() as $element)
									<!-- Product -->
									<div class="u-basket__product g-brd-white-opacity-0_1">
										<div class="row align-items-center no-gutters">
											<div class="col-4 g-pr-20">
												<a href="{{ route('store.product', $element -> id) }}" class="u-basket__product-img">	
													@if ($element -> options -> has('image'))
														<img src="{{ $_ENV['STORAGE_PATH'].$element -> options -> image }}" alt="Image Description">
													@else										
														<img src="{{ Storage::disk('images')->url('product-default.jpg') }}" alt="Image Description">
													@endif
												</a>
											</div>

											<div class="col-8">
												<h6 class="g-font-weight-600 g-mb-0"><a href="{{ route('store.product', $element -> options -> id_product) }}" class="g-color-white g-color-white--hover g-text-underline--none--hover text-capitalize">{{ $element -> name }}</a></h6>
												<small class="g-color-gray-dark-v5 g-font-size-14">{{ $element -> qty . ' x ' . $element -> price }}</small>
											</div>
										</div>
									</div>
									<!-- End Product -->
								@endforeach
							@else
								<p class="text-center g-my-20 g-font-size-18">Carrito vacio</p>
							@endif
						</div>

						<div class="g-brd-top g-brd-gray-light-v4 g-pa-15 g-pb-20">
							<div class="d-flex flex-row align-items-center justify-content-between g-letter-spacing-1 g-font-size-16 g-mb-15">
								<strong class="text-uppercase g-font-weight-600">Subtotal</strong>
								<strong class="g-color-primary g-font-weight-600">{{ Cart::subtotal(0, ',', '.') }}</strong>
							</div>

							<div class="d-flex flex-row align-items-center justify-content-between g-font-size-18">
								<a href="{{ route('cart.show') }}" class="btn btn-block u-btn-primary rounded-0">Ver carrito</a>
							</div>
						</div>
					</div>
				</div>
				<!-- End Basket -->
			</div>
		</nav>
	</div>
</header>
<!-- End Header
