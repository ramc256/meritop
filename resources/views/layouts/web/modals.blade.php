
	<!-- ================== Global Modal ================== -->
	<div class="modal fade  bs-example-modal-lg" id="newMessage" tabindex="-1" role="dialog" aria-labelledby="newMessageLabel">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		    	<form action="{{ route('imessages.store') }}" method="post">
		    		{{ csrf_field() }}
			      	<div class="modal-header">
				        <h4 class="modal-title text-center" id="myModalLabel">Crear mensaje</h4>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      	</div>
			      	<div class="modal-body">
			        	<div class="form-group">
			        		<label for="subject">Asunto <span class="text-danger">*</span></label>
			        		<input name="subject" id="subject" type="text" class="form-control" placeholder="Indique el asunto del mensaje" required="required" maxlength="250">
			        	</div>
			        	<div class="form-group">
			        		<label for="message">Mensaje <span class="text-danger">*</span></label>
			        		<textarea name="message" id="message" rows="5" class="form-control" placeholder="Escriba su mensaje..." required="required"></textarea>
			        	</div>
			      	</div>
			      	<div class="modal-footer">
				        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
				        <button type="submit" class="btn btn-primary">Enviar</button>
			      	</div>
		      	</form>
		    </div>
	  	</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action=" {{ route('my-account.change-image', Auth::user() -> id) }} " method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Cambiar Imagen</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<input name="image" type="file" class="form-control" id="image">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<button type="submit" class="btn btn-primary">Cambiar</button>
					</div>
				</form>
			</div>
		</div>
	</div>