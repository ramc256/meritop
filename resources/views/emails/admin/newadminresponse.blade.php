@component('mail::message')
# Buenas tardes {{ $name }}

Ha recibido un nuevo mensaje, por favor ingrese a su cuenta.

<div>
    Saludos cordiales.
</div>

Gracias,<br>
{{ config('app.name') }}
@endcomponent
