@component('mail::message')
# Buenas tardes Administrador

El usuario {{ $name }} con el correo {{ $email }}, ha generado un nuevo ticket.

<div>
    Saludos cordiales.
</div>

@component('mail::button', ['url' => $url ])
Ingrese a su cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
