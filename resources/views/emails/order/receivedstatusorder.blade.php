@component('mail::message')
# Introduction

The body of your message.

@component('mail::button', ['url' => $_ENV['APP_URL']])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
