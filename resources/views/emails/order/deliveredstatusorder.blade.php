@component('mail::layout')
<div align="center">
  <img class="img-fluid" src="{{ $logo }}" alt="Logo">
</div>
<br>
----------------------------------------------------------------------
{{-- Body --}}
<div>

  <p>Hola <strong> {{ $data_order->first_name }} {{ $data_order->last_name }},</strong></p>

  <p> Tenemos confirmación que su orden número: <strong>{{ $data_order->id }}</strong>, enviada fue recibida </p>


#Lista de productos

<div class="container">
      @foreach($items as $element)
         
        <label>{{'Nombre: '. $element -> name .'  Cantidad: '.$element -> quantity}}</label>
           
     @endforeach     
</div>

</div>

<br>
@if (!empty($data_order -> note))
  <strong>Nota</strong>
  {{ $data_order -> note }}
@endif

{{-- @component('mail::panel')
El tiempo estimado de llegada a su dirección es de 5 días aproximadamente.
@endcomponent --}}
<br>
<br>
Gracias,<br>
{{ config('app.name') }}
<br>
<br>

{{-- Footer --}}

{{-- ![Meritop][logo]2017 © Meritop. Todos los derechos reservados --}}
<div align="center">
  <img class="img-fluid" src="{{ asset('images/logo-2.png') }}" alt="Logo">
  2017 © Meritop. Todos los derechos reservados
</div>
{{-- [logo]:{{ asset('images/logo-2.png') }} --}}

@endcomponent
