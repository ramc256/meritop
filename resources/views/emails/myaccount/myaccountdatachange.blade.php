@component('mail::message')
# Hola {{ $first_name . ' '. $last_name }}

Este email llega a usted, debido a que usted ha actualizado los datos personales de la plataforma.

Si cree que es un error por favor notifiquelo al número 0212 - 239 - 8820 o al siguiente email soporte@meritop.com


@component('mail::button', ['url' => 'http://meiritop.dev/'])
Ingresa a tu Perfil
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
