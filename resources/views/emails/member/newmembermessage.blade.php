@component('mail::message')
# Hola {{ $name }}

Muchas gracias por contactarnos, hemos recibido el mensaje satisfactoriamente
en breve le responderemos.

<div>
    Saludos cordiales.
</div>

{{-- @component('mail::button', ['url' => 'http://meiritop.dev/'])
Meritop
@endcomponent --}}

Gracias,<br>
{{ config('app.name') }}
@endcomponent
