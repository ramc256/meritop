@component('mail::layout')
<div align="center">
	<img class="img-fluid" src="{{ $logo }}" alt="Logo">
</div>
{{-- Body --}}
<div>	
	Hola <strong>{{ $first_name }}</strong>,

	Bienvenido a la plataforma de incentivos y beneficios que te ofrece tu club <strong>{{ $alias }}</strong> 

	Podrás ingresar a la plataforma ingresando tu email y contraseña: 

	Clave: <strong>{{ $password }}</strong>		
	
</div>
	
@component('mail::button', ['url' => $url ])
Ingresa ya!
@endcomponent

{{-- Footer --}}

{{-- ![Meritop][logo]2017 © Meritop. Todos los derechos reservados --}}
<div align="center">
	<img class="img-fluid" src="{{ asset('images/logo-2.png') }}" alt="Logo">
	2017 © Meritop. Todos los derechos reservados
</div>
{{-- [logo]:{{ asset('images/logo-2.png') }} --}}

@endcomponent