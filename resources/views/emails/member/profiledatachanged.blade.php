@component('mail::message')
# Hola {{ $name }}

Este email llega a usted, debido a que usted ha actualizado los datos personales de la plataforma.

Si cree que es un error por favor notifiquelo al número {{ $phone }} o al siguiente email {{ $email }}


@component('mail::button', ['url' => $_ENV['APP_URL']])
Ingresa a tu Perfil
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
