@component('mail::layout')
<div align="center">
	<img class="img-fluid" src="{{ $logo }}" alt="Logo">
</div>
{{-- Body --}}

# Meritop

Le notificamos que usted ahora es parte del club: {{$name_club}}

@component('mail::button', ['url' => ['url' => $url ] ])
Ingresa ya!
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

{{-- Footer --}}

{{-- ![Meritop][logo]2017 © Meritop. Todos los derechos reservados --}}
<div align="center">
	<img class="img-fluid" src="{{ asset('images/logo-2.png') }}" alt="Logo">
	2017 © Meritop. Todos los derechos reservados
</div>

@endcomponent