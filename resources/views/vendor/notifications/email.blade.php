@component('mail::message')
<div align="center">
    <img class="img-fluid" src="{{ $salutation }}" alt="Logo">
</div>
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Hello!
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

<!-- Salutation -->
@if (! empty($salutation))
@else
Regards,<br>{{ config('app.name') }}
@endif

<!-- Salutation -->
@if (! empty($attach))
{{ $attach }}
@else
Regards,<br>{{ config('app.name') }}
@endif

<!-- Subcopy -->
@isset($actionText)
@component('mail::subcopy')

<div align="center">
    2017 © Meritop. Todos los derechos reservados
</div>

Si tiene problemas al hacer clic en el botón "{{ $actionText }}" copie y pegue la URL a continuación en su navegador web:
 {{-- [{{ $actionUrl }}]({{ $actionUrl }}) --}}
 {{-- [logo]({{ $logo }}) --}}
@endcomponent
@endisset
@endcomponent
