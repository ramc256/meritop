@extends('layouts.web.default')
@section('title', 'Bienvenido a Meritop')
@section('styles')
	<link rel="stylesheet" href="{{ asset('vendor/cubeportfolio-full/cubeportfolio/css/cubeportfolio.min.css') }}">
	<style>
		.btn-brd, .btn-brd:hover{
			border-color: #999;
			color: #999;
			margin-left: auto;
			margin-right: auto;
		}
	</style>
@stop
@section('content')

<!-- ====================== User Bar ====================== -->
<div class="g-brd-bottom g-brd-gray-light-v4">
	<div class="container g-py-30">
		<div class="row">
			<div class="col-lg-2 text-center">
				@if (empty(Auth::user() -> image))
					@if (Auth::user() -> gender == 0)
					<img width="90%" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name  }}">
					@else
					<img width="90%" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name  }}">
					@endif
				@else
				<img width="90%" src="{{ $_ENV['STORAGE_PATH'] . Auth::user() -> image }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name  }}">
				@endif
			</div>
			<div class="col-lg-10">
				<div class="row">
					<div class="col-lg-4">
						<!-- User Details -->
						<div class="d-flex align-items-center justify-content-sm-between mb-2">
							<h2 class="g-color-black g-font-weight-300 h4 g-mr-10">{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name  }}</h2>
						</div>
						<!-- End User Details -->

						<!-- User Position -->
						<h4 class="h6 g-font-weight-100 g-mb-10 g-color-gray-dark-v5">
							<i class="fa fa-envelope g-pos-rel g-top-1 g-mr-5"></i> {{ Auth::user() -> email}}
						</h4>
						<!-- End User Position -->

						<!-- User Info -->
						<ul class="list-inline g-font-weight-300 g-color-gray-dark-v5">
							<li class="list-inline-item g-mr-20">
								<i class="fa fa-building g-pos-rel g-top-1 g-mr-5"></i> {{ session('club') -> department }}
							</li>
							<li class="list-inline-item g-mr-20">
								<i class="fa fa-briefcase g-pos-rel g-top-1 g-mr-5"></i> {{ session('club') -> title }}
							</li>
						</ul>
						<!-- End User Info -->
					</div>
					<div class="col-lg-4 text-center g-brd-left--lg g-brd-gray-light-v4">
						<h2 class="g-font-weight-300 g-mr-10 h4">{{ ucwords(session('club') -> system_currency) }} disponibles</h2>
						<p><span class="h2 g-color-gray-dark-v5 g-font-weight-300">{{ number_format($account -> points, 0, '', '.') }}</span></p>

					</div>
					<div class="col-lg-4 text-center g-brd-left--lg g-brd-gray-light-v4">
						<h2 class="g-font-weight-300 h4">Ranking</h2>
						<!-- Counter Pie Chart -->
						<div class=" g-mb-20 g-mb-0--xl">
							<i class="fa fa-trophy g-font-size-50 text-center g-color-gray-dark-v5"></i>
						</div>
						<p><span class="h6 g-color-gray-dark-v5 g-font-weight-300">{{ $rank -> rank . '/' . $total_ranking}}</span></p>
						<!-- End Counter Pie Chart -->
					</div>
					<!-- End User Block -->
				</div>
				<div class="row text-center">
					<div class="col-lg-4">
						<!-- User Link -->
						<div class="d-flex text-cente">
							<a href="{{ url('my-account/configuration') }}" class="btn btn-brd btn-xs g-ma-auto"><i class="fa fa-pencil g-pos-rel g-top-1 g-mr-5"></i> Actualiza tus datos</a>
						</div>
						<!-- End User Link -->
					</div>
					<div class="col-lg-4">

						<div class="d-flex text-center">
							<a href="{{ route('my-account.index') }}" class="btn btn-brd btn-xs g-ma-auto"><i class="fa fa-file-text g-pos-rel g-top-1 g-mr-5"></i> Detalle</a>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="d-flex text-center0">
							<a href="{{ url('/directory') }}" class="btn btn-brd btn-xs g-ma-auto"><i class="fa fa-eye g-pos-rel g-top-1 g-mr-5"></i> Ver </a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ====================== End User Bar ====================== -->


<!-- ====================== Products ====================== -->
{{-- <section class="container g-pb-100">
	<header class="row justify-content-center text-center g-my-50">
		<div class="col-lg-9">
			<h2 class="h2 g-color-black g-font-weight-600 mb-2">Últimos productos</h2>
			<div class="d-inline-block g-width-30 g-height-2 g-bg-primary mb-2"></div>
			<p class="lead mb-0">Mira nuestros nuevos productos dispuestos para tí</p>
		</div>
	</header>
	<div id="js-carousel-1" class="js-carousel g-pb-100 g-mx-minus-10" data-infinite="1" data-slides-show="4" data-arrows-classes="u-arrow-v1 g-pos-abs g-bottom-0 g-width-45 g-height-45 g-color-gray-dark-v5 g-bg-secondary g-color-white--hover g-bg-primary--hover rounded" data-arrow-left-classes="fa fa-angle-left g-left-10" data-arrow-right-classes="fa fa-angle-right g-right-10" data-pagi-classes="u-carousel-indicators-v1 g-absolute-centered--x g-bottom-20 text-center">
		@foreach ($parents_products as $element)
			<div class="js-slide">
				<!-- Product -->
				<figure class="g-px-10">
					<a class="u-link-v5 g-color-black g-color-primary--hover" href="{{ route('store.product', $element -> id_product) }}">
						<div class="g-pos-rel g-mb-20">
							@if (empty($element -> image))
								<img class="img-fluid" src="{{ Storage::disk('images') -> url('img-default.jpg') }}" alt="{{ $element -> name }}">
							@else
								<img class="img-fluid" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> name }}">
							@endif
						</div>

						<div class="media">
							<div class="d-flex flex-column text-center" style="width: 100%;">
								<h4 class="h6 g-color-black mb-1">
									{{ ucwords($element -> name) }}
								</h4>
								<span class="d-inline-block g-color-gray-dark-v5 g-font-size-13 text-capitalize">{{ $element -> brand }}</span>
								<span class="d-block g-color-black g-font-size-17">{{ number_format($element -> points, 0 , '', '.') . ' ' . session('club') -> system_currency}} </span>
							</div>
						</div>
					</a>
				</figure>
				<!-- End Product -->
			</div>
		@endforeach
	</div>
		<div class="text-center">
			<a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mx-auto" href="{{ url('mall') }}">Ver todos</a>
		</div>
</section> --}}

<section class="container g-pb-100">
	<header class="row justify-content-center text-center g-my-50">
		<div class="col-lg-9">
			<h2 class="h2 g-color-black g-font-weight-600 mb-2">Últimos productos</h2>
			<div class="d-inline-block g-width-30 g-height-2 g-bg-primary mb-2"></div>
			<p class="lead mb-0">Mira nuestros nuevos productos dispuestos para tí</p>
		</div>
	</header>
	<div class="row g-pt-30 g-mb-50">
		@foreach (array_slice($parents_products, 0, 8) as $element)
		<!-- Product -->
		<div class="col-lg-3 g-mb-30">
				<figure class="u-shadow-v11 rounded g-px-15  g-py-15">
			<a class="u-link-v5 g-color-black g-color-primary--hover" href="{{ route('store.product', $element -> id_product) }}">
					<div class="g-pos-rel g-mb-20">
						@if (empty($element -> image))
							<img class="img-fluid" src="{{ Storage::disk('images') -> url('img-default.jpg') }}" alt="{{ $element -> name }}">
						@else
							<img class="img-fluid" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> name }}">
						@endif
					</div>
					<hr>
					<div class="media">
						<div class="d-flex flex-column">
							<h2 class="h4 g-mb-15">
				              	<a class="u-link-v5 g-color-primary g-color-primary--hover" href="{{ route('store.product', $element -> id_product) }}">{{ ucwords($element -> name) }}</a>
				            </h2>

							<span class="d-inline-block g-color-gray-dark-v5 g-font-size-13"><i class="fa fa-bookmark" aria-hidden="true"></i> {{ $element -> brand }}</span>
							<span class="d-block g-color-gray-dark-v5 g-font-size-17 g-mt-10"><i class="fa fa-trophy" aria-hidden="true"></i> {{ number_format($element -> points, 0 , '', '.') . ' ' . session('club') -> system_currency}}</span>
						</div>
					</div>
			</a>
				</figure>
		</div>
		<!-- End Product -->
		@endforeach
	</div>
	<div class="text-center">
		<a href="{{ route('store.index') }}" class="btn u-btn-primary">Ver todos</a>
	</div>
</section>

<!-- ====================== End Products ====================== -->


<!-- ====================== Categories ====================== -->
{{-- <div class="container-fluid g-px-4 g-my-40">
	<div class="cbp"
			 data-controls="#filterControls1"
			 data-animation="quicksand"
			 data-x-gap="5"
			 data-y-gap="5"
			 data-media-queries='[{"width": 1500, "cols": 4}, {"width": 1100, "cols": 4}, {"width": 800, "cols": 3}, {"width": 480, "cols": 2}, {"width": 300, "cols": 1}]'>

		@foreach ($categories as $element)
			<!-- Category -->
			<div class="cbp-item identity design">
				<a href="{{ url('mall/' . $element -> slug) }}">
				<div class="u-block-hover g-parent">
					@if (empty($element -> image))
						<img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_5 g-transition--ease-in-out" src="{{ asset('images/img-default.png') }}" alt="Image description">
					@else
						<img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_5 g-transition--ease-in-out" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> name }}">
					@endif
					<div class="d-flex w-100 h-100 u-block-hover__additional--fade u-block-hover__additional--fade-in g-bg-bluegray-opacity-0_5 opacity-0 g-opacity-1--parent-hover g-pos-abs g-top-0 g-left-0 g-transition-0_3 g-transition--ease-in g-pa-20">
						<div class="d-flex align-items-start flex-column mb-auto">
							<h3 class="h5 g-color-white g-font-weight-600 mb-1 text-capitalize">{{ $element -> name }}</h3>
						</div>
					</div>
				</div>
				</a>
			</div>
			<!-- End category -->
		@endforeach

	</div>
</div> --}}
<!-- ====================== End Categories ====================== -->


<!-- ====================== Quotes ====================== -->
<section class="dzsparallaxer auto-init height-is-based-on-content use-loading g-bg-cover g-bg-black-opacity-0_4--after">
	<div class="divimage dzsparallaxer--target w-100" style="height: 120%; background-image: url({{ asset('images/img3.jpg') }});"></div>
	<div class="container g-z-index-1 g-py-120">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="g-brd-x g-brd-y g-brd-3 g-brd-cyan-gradient-opacity-v1 g-color-white text-center g-z-index-1 g-pa-30">
					<blockquote class="u-blockquote-v4 g-font-size-30 g-mb-30">{{ $quote -> content }}</blockquote>
					<h4 class="h5 g-color-cyan g-font-weight-700 text-uppercase g-mb-0">{{ $quote -> author }}</h4>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ====================== End Quotes ====================== -->


<!-- ====================== Options ====================== -->
<section class="container g-pt-100 g-pb-60">
	<header class="row justify-content-center text-center g-mb-50">
		<div class="col-lg-9">
			<h2 class="h2 g-color-black g-font-weight-600 mb-2">Eres miembro del club <span class="text-capitalize">{{ $club -> alias }}</span></h2>
			<div class="d-inline-block g-width-30 g-height-2 g-bg-primary mb-2"></div>
			<p class="lead mb-0">Mantente en contacto con a tus compañeros de trabajo y únete a los programas de incentivos más atractivos.</p>
		</div>
	</header>
	<div class="row">
		<div class="col-sm-6 col-lg-6 g-mb-30">
			<article>
				<img class="img-fluid w-100" src="{{ asset('images/6234256122.jpg') }}" alt="Image Description">
				<div class="g-width-80x g-bg-white g-pos-rel g-z-index-1 g-pa-30 g-mt-minus-50 mx-auto">
					<h2 class="h5 g-color-black g-font-weight-600 mb-3">
						<a class="u-link-v5 g-color-black g-color-primary--hover g-cursor-pointer" href="{{ url('programs') }}">Programas de incentivos</a>
					</h2>
					<p class="g-color-gray-dark-v4 g-line-height-1_8">Conoce los mejores programas de incentivos, mientras más puntos, más puedes canjear.</p>
					<a class="g-font-size-13" href="{{ url('programs') }}"> Ir a programas</a>
				</div>
			</article>
		</div>
		<div class="col-sm-6 col-lg-6 g-mb-30">
			<article>
				<img class="img-fluid w-100" src="{{ asset('images/6546532434.jpg') }}" alt="Image Description">
				<div class="g-width-80x g-bg-white g-pos-rel g-z-index-1 g-pa-30 g-mt-minus-50 mx-auto">
					<h2 class="h5 g-color-black g-font-weight-600 mb-3">
						<a class="u-link-v5 g-color-black g-color-primary--hover g-cursor-pointer" href="{{ url('directory') }}"> Directorio de miembros</a>
					</h2>
					<p class="g-color-gray-dark-v4 g-line-height-1_8">Mantente en contacto con a tus compañeros de trabajo y únete a los programas de incentivos más atractivos.</p>
					<a class="g-font-size-13" href="{{ url('directory') }}">Ir al directorio</a>
				</div>
			</article>
		</div>
	</div>
</section>

<!-- ====================== End Options ====================== -->



<!-- ====================== Rankings ====================== -->
<section class="container g-py-50">
	<header class="row justify-content-center text-center g-mb-50">
		<div class="col-lg-9">
			<h2 class="h2 g-color-black g-font-weight-600 mb-2">Ranking de miembros</h2>
			<div class="d-inline-block g-width-30 g-height-2 g-bg-primary mb-2"></div>
			<p class="lead mb-0">Conoce los más destacados</p>
		</div>
	</header>
	<div class="row">
		<!-- ====================== Top 3 ====================== -->
		{{-- <article class="col-md-4">
			<div class="u-heading-v3-1 g-brd-white-opacity-0_3 g-mb-25">
				<h2 class="u-heading-v3__title h6 text-uppercase g-brd-primary">Top 3</h2>
			</div>
			@foreach ($ranking as $element)
			<!-- member -->
			<div class="g-px-10">
				<div class="media g-brd-around g-brd-gray-light-v4 g-bg-white rounded g-pa-10 g-mb-20">
					<div class="g-max-width-100 g-mr-15">
						@if (empty($element -> image))
							@if ($element -> gender == 0)
								<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}">
							@else
								<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}">
							@endif
						@else
							<img class="d-flex w-100" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> first_name . ' ' . $element -> last_name }}">
						@endif
					</div>
					<div class="media-body align-self-center">
						<h4 class="h5 g-mb-7">
							<a class="g-color-black g-color-primary--hover g-text-underline--none--hover" href="#">{{ $element -> first_name . ' ' . $element -> last_name }}</a>
						</h4>
						<p class="g-color-gray-dark-v5 g-font-size-13 g-mb-0" href="#"><i class="fa fa-building"></i> {{ $element -> department }}</p>
						<p class="g-color-gray-dark-v5 g-font-size-13 g-mb-0" href="#"><i class="fa fa-briefcase"></i> {{ $element -> title }}</p>
						<p class="g-color-gray-dark-v5 g-font-size-13 g-mb-10" href="#">{{ number_format($element -> points, 0 , '', '.') . ' ' . session('club')->system_currency }}</p>
					</div>
				</div>
			</div>
			<!-- End member -->
			@endforeach
		</article> --}}
		<!-- ====================== End Top 3 ====================== -->

		<!-- ====================== Last Members ====================== -->
		<article class="col-md-6">
			<div class="u-heading-v3-1 g-brd-white-opacity-0_3 g-mb-25">
				<h2 class="u-heading-v3__title h6 text-uppercase g-brd-primary">Ultimos miembros</h2>
			</div>
			@foreach ($members_last as $element)
				<!-- member -->
				<div class="g-px-10">
					<div class="media g-brd-around g-brd-gray-light-v4 g-bg-white rounded g-pa-10 g-mb-20">
						<div class="g-max-width-100 g-mr-15">
							@if (empty($element -> image))
								@if ($element -> gender == 0)
									<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}">
								@else
									<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}">
								@endif
							@else
								<img class="d-flex w-100" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> first_name . ' ' . $element -> last_name }}">
							@endif
						</div>
						<div class="media-body align-self-center">
							<h4 class="h5 g-mb-7">
								<a class="g-color-black g-color-primary--hover g-text-underline--none--hover" href="#">{{ $element -> first_name . ' ' . $element -> last_name }}</a>
							</h4>
							<p class="g-color-gray-dark-v5 g-font-size-13 g-mb-0" href="#"><i class="fa fa-building"></i> {{ $element -> department }}</p>
							<p class="g-color-gray-dark-v5 g-font-size-13 g-mb-0" href="#"><i class="fa fa-briefcase"></i> {{ $element -> title }}</p>
							@php($date_create = date_create($element -> created_at))
							<p class="g-color-gray-dark-v5 g-font-size-13 g-mb-10" href="#"><i class="fa fa-calendar"></i> Desde el {{ date_format($date_create, 'd/m/Y') }}</p>
							<footer class="d-flex justify-content-between g-font-size-16">
							</footer>
						</div>
					</div>
				</div>
				<!-- End member -->
			@endforeach
		</article>
		<!-- ====================== End Last Member ====================== -->

		<!-- ====================== Next birthday ====================== -->
		<article class="col-md-6">
			<div class="u-heading-v3-1 g-brd-white-opacity-0_3 g-mb-25">
				<h2 class="u-heading-v3__title h6 text-uppercase g-brd-primary">Proximos cumpleaños</h2>
			</div>
			@foreach ($members_last as $element)
				<!-- member -->
				<div class="g-px-10">
					<div class="media g-brd-around g-brd-gray-light-v4 g-bg-white rounded g-pa-10 g-mb-20">
						<div class="g-max-width-100 g-mr-15">
							@if (empty($element -> image))
								@if ($element -> gender == 0)
									<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}">
								@else
									<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name }}">
								@endif
							@else
								<img class="d-flex w-100" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> first_name . ' ' . $element -> last_name }}">
							@endif
						</div>
						<div class="media-body align-self-center">
							<h4 class="h5 g-mb-7">
								<a class="g-color-black g-color-primary--hover g-text-underline--none--hover" href="#">{{ $element -> first_name . ' ' . $element -> last_name }}</a>
							</h4>
							<p class="g-color-gray-dark-v5 g-font-size-13 g-mb-0" href="#"><i class="fa fa-building"></i> {{ $element -> department }}</p>
							<p class="g-color-gray-dark-v5 g-font-size-13 g-mb-0" href="#"><i class="fa fa-briefcase"></i> {{ $element -> title }}</p>
							<p class="g-color-gray-dark-v5 g-font-size-13 g-mb-10" href="#"><i class="fa fa-calendar"></i>
								@if (!empty($element -> birthdate))
									@php
										$create_date = date_create($element -> birthdate);
										$birthdate = date_format($create_date, 'd/m');
									@endphp
									@if (date_format($create_date, 'd/m/Y') == date('d/m/Y'))
										HOY
									@else
										{{ $birthdate }}
									@endif
								@endif
							</p>
						</div>
					</div>
				</div>
				<!-- End member -->
			@endforeach
		</article>
		<!-- ====================== End Next Birthday ====================== -->

	</div>
</section>
<!-- ====================== End Rankings ====================== -->

@stop
@section('scripts')
		<!-- JS Revolution Slider -->
		<script src="{{ asset('vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
		<script src="{{ asset('vendor/revolution-slider/revolution-addons/typewriter/js/revolution.addon.typewriter.min.js') }}"></script>
		<!-- JS Plugins Init. -->


		<!-- JS Plugins Init. -->
		<script>
			$(document).ready(function () {
				// Carousel
				$.HSCore.components.HSCarousel.init('[class*="js-carousel"]');
			});

			var tpj = jQuery;

			var revapi1014;
			tpj(document).ready(function () {
				if (tpj("#rev_slider_1014_1").revolution == undefined) {
					revslider_showDoubleJqueryError("#rev_slider_1014_1");
				} else {
					revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
						sliderType: "standard",
						jsFileLocation: "revolution/js/",
						sliderLayout: "fullscreen",
						dottedOverlay: "none",
						delay: 9000,
						navigation: {
							keyboardNavigation: "off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation: "off",
							mouseScrollReverse: "default",
							onHoverStop: "off",
							touch: {
								touchenabled: "on",
								swipe_threshold: 75,
								swipe_min_touches: 1,
								swipe_direction: "horizontal",
								drag_block_vertical: false
							}
							,
							arrows: {
								style: "uranus",
								enable: true,
								hide_onmobile: true,
								hide_under: 768,
								hide_onleave: false,
								tmp: '',
								left: {
									h_align: "left",
									v_align: "center",
									h_offset: 20,
									v_offset: 0
								},
								right: {
									h_align: "right",
									v_align: "center",
									h_offset: 20,
									v_offset: 0
								}
							}
						},
						parallax: {
							type:"mouse",
							origo:"slidercenter",
							speed:2000,
							levels:[2,3,4,5,6,7,12,16,10,50],
							disable_onmobile:"on"
						},
						responsiveLevels: [1240, 1024, 778, 480],
						visibilityLevels: [1240, 1024, 778, 480],
						gridwidth: [1240, 1024, 778, 480],
						gridheight: [868, 768, 960, 600],
						lazyType: "none",
						shadow: 0,
						spinner: "off",
						stopLoop: "on",
						stopAfterLoops: 0,
						stopAtSlide: 1,
						shuffle: "off",
						autoHeight: "off",
						fullScreenAutoWidth: "off",
						fullScreenAlignForce: "off",
						fullScreenOffsetContainer: "",
						fullScreenOffset: "60px",
						disableProgressBar: "on",
						hideThumbsOnMobile: "off",
						hideSliderAtLimit: 0,
						hideCaptionAtLimit: 0,
						hideAllCaptionAtLilmit: 0,
						debugMode: false,
						fallbacks: {
							simplifyAll: "off",
							nextSlideOnWindowFocus: "off",
							disableFocusListener: false,
						}
					});
				}

				RsTypewriterAddOn(tpj, revapi1014);
			});

		</script>


		{{-- <script>
			// $(document).on('ready', function () {
			// 	$('[data-toggle="tooltip"]').tooltip();
			// });

			;(function ($) {
				'use strict';
				$(document).on('ready', function () {
					// Carousel
					$.HSCore.components.HSCarousel.init('[class*="js-carousel"]');

					$('#js-carousel-1').slick('setOption', 'responsive', [{
						breakpoint: 992,
						settings: {
							slidesToShow: 3
						}
					}, {
						breakpoint: 768,
						settings: {
							slidesToShow: 2
						}
					}, {
						breakpoint: 554,
						settings: {
							slidesToShow: 2
						}
					}], true);
				});

				initialiation of countdowns
				var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
					yearsElSelector: '.js-cd-years',
					monthElSelector: '.js-cd-month',
					daysElSelector: '.js-cd-days',
					hoursElSelector: '.js-cd-hours',
					minutesElSelector: '.js-cd-minutes',
					secondsElSelector: '.js-cd-seconds'
				});
			})(jQuery);
		</script> --}}

		{{-- <script>
			;(function ($) {
				'use strict';
				$(document).on('ready', function () {
					// Header
					$.HSCore.components.HSHeader.init($('#js-header'));
					$.HSCore.helpers.HSHamburgers.init('.hamburger');

					// Initialization of HSDropdown component
					$.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
						afterOpen: function(){
							$(this).find('input[type="search"]').focus();
						}
					});

					// Initialization of HSScrollBar component
					$.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

					// Initialization of HSMegaMenu plugin
					$('.js-mega-menu').HSMegaMenu({
						event: 'hover',
						pageContainer: $('.container'),
						breakpoint: 991
					});
					$.HSCore.components.HSTabs.init('[data-tabs-mobile-type]');
				});
			})(jQuery);
		</script>
 --}}
@endsection
