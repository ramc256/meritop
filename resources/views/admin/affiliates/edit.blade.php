@extends('layouts.admin.default')

@section('title', 'Editar Afiliado')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Seccion para la edicion de Afiliados</p>
        </div>
    </header>
    <article class="content">
        <form id="form-validation" action="{{ route('affiliates.update', $affiliate[0] -> id) }}" method="POST" >
            <div class="col-sm-12 col-md-7">
                <div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title">Datos importantes</h3>
                    </div>
                    <div class="panel-body">
                    {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label for="alias" class="control-label">Alias <span class="text-danger">*</span></label>
                            <input id="alias" name="alias" placeholder="Alias" maxlength="40" type="text" class="form-control" required="required" value="{{ $affiliate[0] -> alias }}">
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input id="name" name="name" placeholder="Nombre" maxlength="40" type="text" class="form-control" required="required" value="{{ $affiliate[0] -> name }}">
                        </div>
                        <div class="form-group">
                            <label for="fiscal" class="control-label">Fiscal <span class="text-danger">*</span></label>
                            <input id="fiscal" name="fiscal" placeholder="Fiscal" maxlength="40" type="number" class="form-control" required="required" value="{{ $affiliate -> fiscal }}">
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label">Teléfono <span class="text-danger">*</span></label>
                            <input id="phone" name="phone" placeholder="Teléfono" maxlength="40" type="phone" class="form-control" required="required" value="{{ $affiliate[0] -> phone }}">
                        </div>
                        <div class="form-group">
                            <label for="contact_person" class="control-label">Persona de contacto <span class="text-danger">*</span></label>
                            <input id="contact_person" name="contact_person" placeholder="Persona de contacto" maxlength="40" type="text" class="form-control" required="required" value="{{ $affiliate[0] -> contact_person }}">
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">E-Mail <span class="text-danger">*</span></label>
                            <input id="email" name="email" placeholder="E-Mail" maxlength="40" type="email" class="form-control" required="required" value="{{ $affiliate[0] -> email }}">
                        </div>
                        <div class="form-group">
                            <label for="open_hours" class="control-label">Horario <span class="text-danger">*</span></label>
                            <input id="open_hours" name="open_hours" placeholder="Horario de trabajo" maxlength="40" type="text" class="form-control" required="required" value="{{ $affiliate[0] -> open_hours }}">
                        </div>
                        <div class="form-group">
                            <label for="slug" class="control-label">Slug <span class="text-danger">*</span></label>
                            <input id="slug" name="slug" placeholder="Slug" maxlength="40" type="text" class="form-control" required="required" value="{{ $affiliate[0] -> slug }}">
                        </div>        
                        <div class="form-group">
                            <label for="estatus" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select id="status" name="status" class="form-control" data-bv-field="status" required="required">
                                <option value="">
                                    Seleccione un estatus...
                                </option>
                                <option value="1" @if ($affiliate[0] -> status == 1) selected="selected" @endif>Activo</option>
                                <option value="0" @if ($affiliate[0] -> status == 0) selected="selected" @endif>Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <a href="{{route('affiliates.index')}}" type="button" class="btn btn-warning">Regresar</a>  
                        <button type="reset" class="btn btn-default bttn_reset">Limpiar</button>
                        <button type="submit" class="btn btn-primary">Aceptar</button>  
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-5">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos complementarios</h3>
                    </div>
                    <div class="panel-body"> 
                        <div class="form-group">
                            <label for="city" class="control-label">Ciudad <span class="text-danger">*</span></label>
                            <select id="city" name="city" class="form-control" required="required">
                                <option value="">Seleccione un pais...</option> 
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" @if ($affiliate[0] -> city == $city -> id) selected="selected" @endif>{{ $city->name }}</option> 
                                @endforeach
                            </select>
                        </div>     
                        <div class="form-group">
                            <label for="latitude" class="control-label">Latitud <span class="text-danger">*</span></label>
                            <input id="latitude" name="latitude" placeholder="Latitud" maxlength="40" type="number" class="form-control" required="required" value="{{ $affiliate -> latitude }}">
                        </div>
                        <div class="form-group">
                            <label for="longitude" class="control-label">Longitud <span class="text-danger">*</span></label>
                            <input id="longitude" name="longitude" placeholder="Longitud" maxlength="40" type="number" class="form-control" required="required" value="{{ $affiliate -> longitude }}">
                        </div>     
                        <div class="form-group">
                            <label for="address" class="control-label">Dirección <span class="text-danger">*</span></label>
                            <input id="address" name="address" placeholder="Direccion" maxlength="40" type="text" class="form-control" required="required" value="{{ $affiliate[0] -> address }}">
                        </div>       
                        <div class="form-group">
                            <label for="facebook" class="control-label">Facebook</label>
                            <input id="facebook" name="facebook" placeholder="Facebook" maxlength="40" type="text" class="form-control"  value="{{ $affiliate[0] -> facebook }}">
                        </div>
                        <div class="form-group">
                            <label for="twitter" class="control-label">Twitter</label>
                            <input id="twitter" name="twitter" placeholder="Twitter" maxlength="40" type="text" class="form-control"   value="{{ $affiliate[0] -> twitter }}">
                        </div>
                        <div class="form-group">
                            <label for="instagram" class="control-label">Instagram</label>
                            <input id="instagram" name="instagram" placeholder="Instagram" maxlength="40" type="text" class="form-control"  value="{{ $affiliate[0]-> instagram }}">
                        </div>
                        <div class="form-group">
                            <label for="google_plus" class="control-label">Google Plus</label>
                            <input id="google_plus" name="google_plus" placeholder="Google plus" maxlength="40" type="text" class="form-control"   value="{{ $affiliate[0] -> google_plus }}">
                        </div>
                    </div>
                </div> 
            </div>
        </form>
    </article>
@endsection