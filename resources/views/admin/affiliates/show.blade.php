@extends('layouts.admin.default')

@section('title', 'Ver afiliado')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Vista de caracteristicas de Afiliado</p>
        </div>
    </header>
    <article class="content">
        <div class="col-sm-12 col-md-7">
            <div class="panel filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos importantes</h3>
                </div>
                <div class="panel-body">
                    <ul class="panel-list">
                        <li>
                            <p><b>Alias</b></p>
                            <p class="text-capitalize">{{ $affiliate[0] -> alias }}</p>
                        </li>
                        <li>
                            <p><b>Nombre</b></p>
                            <p>{{ $affiliate[0] -> nombre }}</p>
                        </li>
                        <li>
                            <p><b>Fiscal</b></p>
                            <p>{{ $affiliate[0] -> fiscal }}</p>
                        </li>
                        <li>
                            <p><b>Teléfono</b></p>
                            <p>{{ $affiliate[0] -> phone }}</p>
                        </li>
                        <li>
                            <p><b>Perona de contacto</b></p>
                            <p>{{ $affiliate[0] -> contact_person }}</p>
                        </li>
                        <li>
                            <p><b>E-Mail</b></p>
                            <p>{{ $affiliate[0] -> mail }}</p>
                        </li>
                        <li>
                            <p><b>Horario</b></p>
                            <p>{{ $affiliate[0] -> open_hours }}</p>
                        </li>
                        <li>
                            <p><b>Slug</b></p>
                            <p>{{ $affiliate[0] -> slug }}</p>
                        </li>
                    </ul>
                </div>
                <div class="panel-footer text-center">               
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{route('affiliates.index')}}" type="button" class="btn btn-warning">Regresar</a>  
                    <a href="{{route('affiliates.edit', $affiliate[0]->id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5">
            <div class="panel filterable">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6">
                                Datos complementarios
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div  class=" text-center thumbnail mbl">
                            <!-- Button trigger modal -->
                            <a type="button"  data-toggle="modal" data-target="#myModal">
                                @if (isset($media[0]-> image) && $media[0] -> image != false ) 
                                    @php $disk = 'media'; $route_image = $media[0] -> image; @endphp 
                                @else 
                                    @php $disk = 'images'; $route_image = 'img-default.png'; @endphp 
                                @endif
                            <div class="image-thumbnail" style="background-image: url({{ Storage::disk($disk)->url($route_image) }}" alt="{{ $media[0] -> nombre . ' ' . $media[0] -> id}}">
                            </div>
                            </a>
                        </div>
                        <div  class="thumbnail mbl">
                           
                        </div>
                    </div>
                    <ul class="panel-list">
                        <li>
                            <p><b>Ciudad</b></p>
                            <p class="text-capitalize">{{ $affiliate[0] -> city }}</p>
                        </li>
                        <li>
                            <p><b>Latitud</b></p>
                            <p class="text-capitalize">{{ $affiliate[0] -> latitude }}</p>
                        </li>
                        <li>
                            <p><b>Longitud</b></p>
                            <p class="text-capitalize">{{ $affiliate[0] -> longitude }}</p>
                        </li>
                        <li>
                            <p><b>Dirección</b></p>
                            <p class="text-capitalize">{{ $affiliate[0] -> address }}</p>
                        </li>
                        <li>
                            <p><b>Facebook</b></p>
                            <p class="text-capitalize">{{ $affiliate[0] -> facebook }}</p>
                        </li>
                        <li>
                            <p><b>Twitter</b></p>
                            <p class="text-capitalize">{{ $affiliate[0] -> twitter }}</p>
                        </li>
                        <li>
                            <p><b>Instagram</b></p>
                            <p class="text-capitalize">{{ $affiliate[0] -> Instagram }}</p>
                        </li>
                        <li>
                            <p><b>Google Plus</b></p>
                            <p class="text-capitalize">{{ $affiliate[0] -> google_plus }}</p>
                        </li>
                        <li>
                            <p><b>Fecha de Creacion</b></p>
                            <p>{{ $affiliate[0] -> created_at }}</p>
                        </li>
                        <li>
                            <p><b>Fecha de Actualización</b></p>
                            <p>{{ $affiliate[0] -> updated_at }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
@section('modals')
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <form action="{{ route('images.update', $media[0] -> id) }}" method="post" enctype="multipart/form-data"> 
        {{ csrf_field() }}  
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cambiar imagen</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="iamge" class="col-md-3 control-label">Imagen</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input name="image" type="file" class="form-control" id="image">
                                    <input name="image_old" type="hidden" value=" {{ $media[0] -> name }}">
                                    <input name="table" type="hidden" value="affiliates">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('affiliates.destroy', $affiliate[0]->id) }}" method="POST"> 
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Afiliado</h4>
                    </div>
                    <div class="modal-body">     
                        <p>¿Esta seguro de eliminar el Afiliado <b>{{ $affiliate[0] -> name}}</b>?</p>               
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>            
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scritps')
    <!-- page js -->
    <script>
        $('#myModal').on('shown.bs.modal', function () {
          $('#myInput').focus()
        })
    </script>
    <script>
        $('#modal-delete').on('shown.bs.modal', function () {
          $('#modal-delete').focus()
        })
    </script>
    <!-- end page js -->
@endsection

