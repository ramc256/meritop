@extends('layouts.admin.default')

@section('title', 'Nuevo Afiliado')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Seccion para crear afiliados</p>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('affiliates.store') }}" method="POST"  enctype="multipart/form-data">
            {{ csrf_field() }}
    		<div class="col-sm-12 col-md-7">
    			<div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title"> Datos importantes</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="alias" class="control-label">Alias <span class="text-danger">*</span></label>
                            <input id="alias" name="alias" placeholder="Alias" maxlength="45" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input id="name" name="name" placeholder="Nombre" maxlength="45" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="fiscal" class="control-label">Fiscal <span class="text-danger">*</span></label>
                            <input id="fiscal" name="fiscal" placeholder="Fiscal" maxlength="20" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label">Teléfono <span class="text-danger">*</span></label>
                            <span class="input-group-addon"><i class="fa fa-fw fa-user-md"></i></span>
                            <input id="phone" name="phone" placeholder="Teléfono" maxlength="40" type="phone" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="contact_person" class="control-label">Persona de contacto <span class="text-danger">*</span></label>
                            <input id="contact_person" name="contact_person" placeholder="Persona de contacto" maxlength="40" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">E-Mail <span class="text-danger">*</span></label>
                            <input id="email" name="email" placeholder="E-Mail" maxlength="40" type="email" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="open_hours" class="control-label">Horario <span class="text-danger">*</span></label>
                            <input id="open_hours" name="open_hours" placeholder="Horario de trabajo" maxlength="40" type="datetime-local" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="slug" class="control-label">Slug <span class="text-danger">*</span></label>
                            <input id="slug" name="slug" placeholder="Slug" maxlength="40" type="text" class="form-control" required="required">
                        </div>               
                        <div class="form-group">
                            <label for="iamge" class="control-label">Imagen</label>
                            <input name="image[]" type="file" class="form-control" id="image" multiple>
                        </div> 
                        <div class="form-group">
                            <label for="estatus" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select id=git"status" name="status" class="form-control" data-bv-field="status" required="required">
                                <option value="">
                                    Seleccione un estatus...
                                </option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div> 
                    <div class="panel-footer text-center">
                            <a href="{{route('affiliates.index')}}" type="button" class="btn btn-warning">Regresar</a>  
                            <button type="reset" class="btn btn-default bttn_reset">Limpiar</button>
                            <button type="submit" class="btn btn-primary">Aceptar</button>  
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-5">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos complementarios</h3>
                    </div>
                    <div class="panel-body"> 
                        <div class="form-group">
                            <label for="city" class="control-label">Ciudad <span class="text-danger">*</span></label>
                                <select id="fk_id_city" name="fk_id_city" class="form-control" required="required">
                                    <option value="">Seleccione un pais...</option> 
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option> 
                                    @endforeach
                                </select>
                        </div>    
                        <div class="form-group">
                            <label for="latitude" class="control-label">Latitud <span class="text-danger">*</span></label>
                            <input id="latitude" name="latitude" placeholder="Latitud" maxlength="40" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="longitude" class="control-label">Longitud</label>
                            <input id="longitude" name="longitude" placeholder="Longitud" maxlength="40" type="number" class="form-control" required="required">
                        </div>     
                        <div class="form-group">
                            <label for="address" class="control-label">Dirección <span class="text-danger">*</span></label>
                            <input id="address" name="address" placeholder="Direccion" maxlength="40" type="text" class="form-control" required="required">
                        </div>       
                        <div class="form-group">
                            <label for="facebook" class="control-label">Facebook</label>
                            <input id="facebook" name="facebook" placeholder="Facebook" maxlength="40" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="twitter" class="control-label">Twitter</label>
                            <input id="twitter" name="twitter" placeholder="Twitter" maxlength="40" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="instagram" class="control-label">Instagram</label>
                            <input id="instagram" name="instagram" placeholder="Instagram" maxlength="40" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="google_plus" class="control-label">Google Plus</label>
                            <input id="google_plus" name="google_plus" placeholder="Google plus" maxlength="40" type="text" class="form-control">
                        </div>
                    </div>
                </div> 
            </div>
        </form>
	</article>
@endsection

@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/password-validator.js') }}"></script>
    <!-- end page js -->
@endsection
