@extends('layouts.admin.default')

@section('title', 'Picking List')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/custom_css/dashboard1.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Gestiones</small></h1>
        </div>
    </header>
    @include('flash::message')
	<article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix"><div class="btns-header">
                <button  data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-1" type="button" class="btn btn-labeled btn-meritop-primary">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-down"></i></span> Filtros
                    </button>
                </div>
                <div class="panel-faq">
                    <div id="faq-cat-1-sub-1" class="collapse">
                        <div id="filter-club" class="form-inline m-t-10">
                            <div class="form-group pull-left">
                                <label class="control-label">Club</label>
                                <select class="form-control text-capitalize" id="filterInput" name="clubs" class="form-control" required="required">
                                        <option class="text-capitalize" value="all">Todos</option>
                                    @foreach($clubs as $element)
                                        <option class="text-capitalize" value="{{ $element -> name }}">{{ $element -> name }}</option>
                                     @endforeach
                                </select>
                            </div>
                        </div>                            
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table id="filter-table" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th valign="middle">DNI</th>
                            <th>Ordenes abiertas</th>
                            <th >Club</th>
                        </tr>
                    </thead>
                    <tbody>
                       @foreach($orders as $element)
                            <tr>
                                <td style="vertical-align: middle !important;"> <p>{{$element -> dni }}</p> </td>
                                <td  class="text-capitalize">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <b># Orden:</b> {{ $element -> num_order }} | {{ date_format(
                                                new DateTime($element -> created_at), 'd/m/Y h:i:s A') }}|{{$element -> first_name . ' ' . $element -> last_name}}
                                        </div>
                                        <div class="col-md-12">
                                            <b>Sede: </b> {{$element -> name}} | {{$element -> address . ' ' . $element -> postal_code . ' ' . $element -> city . ' . ' .$element -> state }} | {{ $element -> phone}}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px; margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <b>SKU</b>
                                        </div>
                                        <div class="col-md-4">
                                            <b>Nombre</b>
                                        </div>
                                        <div class="col-md-4">
                                            <b>Cantidad</b>
                                        </div>
                                    </div>
                                    @foreach ($orders_products as $value)
                                        @if ($element -> num_order == $value -> num_order)
                                            <div class="row" style="padding: 10px 0; border-top: solid 1px #e1e1e1;">
                                                <div class="col-md-4">
                                                   {{ $value -> sku }}
                                                </div>
                                                <div class="col-md-4">
                                                    {{ $value -> product . ' ' . $value -> feature}}
                                                </div>
                                                <div class="col-md-4">
                                                    {{ $value -> quantity }}
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach


                                    <div class="col-md-4 pull-right">
                                        <b><hr> {{$element -> total }}</b><br>
                                    </div>
                                </td>
                                <td class="text-capitalize filter-column" style="vertical-align: middle !important;">{{ $element -> club }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </article>
@stop
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/jquery.filters.js') }}"></script>

    <!-- Datable js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script> 

    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.print.js') }}"></script>
    
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/pdfmake.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#filterInput").filterTables();
            //table tools example
            var table = $('#filter-table').DataTable({
                // dom: 'Bflrtip',
                "dom": '<"m-t-10"B><"m-t-10 pull-left"f><"m-t-10 pull-right"l>rt<"pull-left m-t-10"i><"m-t-10 pull-right"p>',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection
