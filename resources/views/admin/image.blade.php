@extends('layouts.admin.default')

@section('title', 'Imagen')
@section('content')
<!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Cambio de imagen</p>
        </div>
    </header>
    <article class="content">        
        <input type="hidden" name="_method" value="head">
            <div class="col-sm-12">
                <div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title"> Image</h3>
                    </div>
                    <div class="panel-body"> 
                        <div>
                            @if (isset($image -> image) && $image -> image != '' && $image -> image != false )
                                <img style="width: 200px;display: block; margin: 20px auto;" src="{{ Storage::disk('media') -> url($image -> image) }}" alt="{{  $image -> identificator }}">
                            @else
                                <img style="width: 200px;display: block; margin: 20px auto;"  src="{{ Storage::disk('images') -> url( 'default.png' ) }}" alt="{{ $image -> identificator  }}">
                            @endif
                        </div>                
                    </div>
                    <div class="panel-footer text-center">
                        <a href="{{ route( $image -> table.'.show', $image -> identificator) }}" type="button" class="btn btn-danger">Regresar</a>
                        <button id="btn-image-delete" type="button" class="btn btn-primary" data-toggle="modal" data-target="#image-delete">Eliminar</button>
                        <button id="btn-image-change" type="button" class="btn btn-success" data-toggle="modal" data-target="#image-change">Cambiar</button>
                    </div>
                </div>
            </div>
    </article>
@stop
@section('modals')
    <div class="modal fade" id="image-change" tabindex="-1" role="dialog" aria-labelledby="image-change">
        <form action="{{ route('images.update', $image -> id ) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="put">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cambiar imagen</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="iamge" class="col-md-3 control-label">Imagen</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input name="image" type="file" class="form-control" id="image" required="required">
                                    <input name="table" type="hidden" class="form-control" value="{{ $image -> table }}">
                                    <input name="id" type="hidden" class="form-control" value="{{ $image -> id }}">
                                    <input name="identificator" type="hidden" class="form-control" value="{{ $image -> identificator }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="image-delete" tabindex="-1" role="dialog" aria-labelledby="image-delete">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Imagen</h4>
                    </div>
                    <div class="modal-body">     
                        <p>¿Esta seguro de eliminar esta imagen</b>?</p>             
                    </div>
                    <form action="{{ route('images.destroy', $image -> id ) }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                        <div class="modal-footer text-center">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-success">Si</button>
                        </div>
                    </form>
                </div>
            </div>   
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <!-- page js -->
    <script>
        $('#image-change').on('shown.bs.modal', function () {
          $('#btn-image-change').focus()
        })
    </script>
    <script>
        $('#image-delete').on('shown.bs.modal', function () {
          $('#btn-image-delete').focus()
        })
    </script>
    <!-- end page js -->
@endsection