@extends('layouts.admin.default')

@section('title', 'Mensajes')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') </h1>
        </div>
    </header>
    @include('flash::message')
	<article class="content">
		<div class="panel filterable">
            <div class="panel-body">
                <ul class="nav nav-tabs mar-b-20 nav-custom" role="tablist" style="margin-top: -2px; margin-bottom: 30px;">
                   <li role="presentation" class="active"> <a href="#open" aria-controls="open" role="tab" data-toggle="tab">Abiertos</a></li>
                    <li role="presentation"><a href="#close" aria-controls="close" role="tab" data-toggle="tab">Cerrados</a></li>
                </ul>
                <div class="tab-content">
                    <div id="open" role="tabpanel" class="tab-pane active" >
                        <div class="table-responsive">
                            <table class="table table-striped table-hover datatable-desc">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Asunto</th>
                                        <th>Mensaje</th>
                                        {{-- <th>Estatus</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($open as $element)
                                        <tr>
                                            <td>{{ $element -> created_at }}</td>
                                            <td><a href="{{route('messages.show', $element -> id)}}">{{ $element -> subject }}</a></td>
                                            <td>{{ substr($element -> message, 0,200) }}...</td>
                                            {{-- <td>@if ( $element -> status == 1) Abierto @else Cerrado @endif</td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="close" role="tabpanel" class="tab-pane">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover datatable-desc">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Asunto</th>
                                        <th>Mensaje</th>
                                        {{-- <th>Estatus</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($close as $element)
                                        <tr>
                                            <td>{{ $element -> created_at }}</td>
                                            <td><a href="{{route('messages.show', $element -> id)}}">{{ $element -> subject }}</a></td>
                                            <td>{{ substr($element -> message, 0,200) }}...</td>
                                            {{-- <td>@if ( $element -> status == 1) Abierto @else Cerrado @endif</td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</article>
@stop
@section('scripts')
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
@endsection
