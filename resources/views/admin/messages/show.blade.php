@extends('layouts.admin.default')
@section('styles')
    <!--DataTable css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/custom_css/messages.css') }}"/>
@stop
@section('title', 'Estados de cuentas')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Ver</small></h1>
        </div>
    </header>
    @include('flash::message')
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                <div class="pull-right">
                    <a href="{{ route('messages.index') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i>Atrás</a>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#changeStatus"><i class="fa fa-refresh"></i> Cambiar estatus</button>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteMessage"><i class="fa fa-trash"></i> Eliminar</button>
                </div>
            </div>
            <div class="panel-body">
                <div class="messages">
                    <div class="body-message brd brd-b mb-30">
                        <h3 class="text-capitalize">{{ $message -> subject }}</h3>
                        <span class="font-8 color-gray">{{ date_format($message -> created_at, 'd/m/Y h:i:s A') }} @if ($message -> status == 1) <i class="fa fa-circle text-success"></i> Abierto @else <i class="fa fa-circle text-danger"></i> Cerrado @endif</span>
                        <p>{{ $message -> message }}</p>
                        <!-- Show Code -->
                        <div>
                            <span class="message-data">
                                <i class="fa fa-phone" aria-hidden="true"></i> {{ $message -> cell_phone }}
                            </span>
                            <span class="message-data">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i> {{ $message -> email }}
                            </span>
                            <span class="message-data text-capitalize">
                                <i class="fa fa-building" aria-hidden="true"></i> {{ $message -> club}}
                            </span>
                        </div>
                        <!-- End Show Code -->
                    </div>

                    @php($iname = Auth::user() -> first_name . ' ' . Auth::user() -> last_name)
                    @if (count($message) > 0)
                    <ul class="responses brd brd-b mb-30">
                        @foreach ($responses as $element)
                        <li class="item">
                            <div class="bag">@if ($element -> name == $iname) YO @else RE @endif</div>
                            <div class="response-content">
                                <div class="">
                                    <h4>
                                        <span class="text-capitalize">{{ $element -> name }}</span>
                                    </h4>
                                    <span class="font-8 color-gray">{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}</span>
                                </div>

                                <p>{{ $element -> message }}</p>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    @endif


                    @if ($message -> status == 1)
                        <form action="{{ route('message.response', $message -> id) }}" method="post">
                            <div>
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <textarea name="message" id="message" rows="8" class="form-control r-brd-none no-resize" required="required" placeholder="Escriba su respuesta acá..."></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Responder</button>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </article>
@stop
@section('modals')
@if ($message -> status == 1)
<!-- Modal -->
<div class="modal fade modal-l" id="deleteMessage" tabindex="-1" role="dialog" aria-labelledby="deleteMessageLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('messages.destroy', $message -> id) }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Eliminar mensaje</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>¿Esta seguro de eliminar este mensaje?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>
 <!-- MODAL PARA ELIMINAR UNA RESPUESTA -->
<div class="modal fade" id="deleteResponse" tabindex="-1" role="dialog" aria-labelledby="deleteResponseLabel">
    <form action=" {{ route('responses.destroy') }} " method="post">
        {{ csrf_field() }}
        <input class="id_response" type="hidden" name="id_response" readonly="readonly">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="change-image">Eliminar respuesta</h4>
                </div>
                <div class="modal-body">
                    <p class="">¿Esta seguro de eliminar su respuesta?</p>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-success">Si</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endif
 <!-- MODAL PARA CAMBIAR ESTATUS -->
<div class="modal fade" id="changeStatus" tabindex="-1" role="dialog" aria-labelledby="deleteResponseLabel">
    <form action=" {{ route('message.change-status') }} " method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id_message" readonly="readonly" value="{{$message->id}}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="change-image">Cambiar estatus</h4>
                </div>
                <div class="modal-body">
                    <p class="">¿Esta seguro de cambiar el estatus a @if ($message -> status == 1) <span class="text-danger">Cerrado</span> @else <span class="text-success">Abierto</span>@endif?</p>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-success">Si</button>
                </div>
            </div>
        </div>
    </form>
</div>
@stop

@section('scripts')
    <!-- page js -->
    <script>
        $(document).ready(function(){

            // Obtiene el ID de la imagen y la inserta en el formulario para posteriormente enviarlo al metodo de cambio de imagen
            $('.btnResponse').click(function(){
                value = $(this).children('.data').text();
                $('.id_response').val(value);
            });
        });
    </script>
    <!-- DataTable js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
@endsection
