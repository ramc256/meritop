@extends('layouts.admin.default')

@section('title', 'Subcategoría')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Ver </small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">               
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{route('subcategories.index', $subcategory[0]->id)}}" type="button" class="btn btn-warning">Atrás</a>  
                    <a href="{{route('subcategories.edit', $subcategory[0]->id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-sm-12 col-md-6">
                     <ul class="panel-list">
                        <li>
                            <p><b>Nombre</b></p>
                            <p class="text-capitalize">{{ $subcategory[0] -> name }}</p>
                        </li>    
                        <li>
                            <p><b>Descripción</b></p>
                            <p class="text-capitalize">{{ $subcategory[0] -> description }}</p>
                        </li>       
                        <li>
                            <p><b>Slug</b></p>
                            <p class="text-capitalize">{{ $subcategory[0] -> slug }}</p>
                        </li>       
                    </ul>
                </div>
                <div class="col-sm-12 col-md-6">
                     <ul class="panel-list">                        
                        <li>
                            <p><b>Categoría</b></p>
                            <p class="text-capitalize">{{ $subcategory[0] -> category }}</p>
                        </li> 
                        <li>
                            <p><b>Estatus</b></p>
                            <p>@if ( $subcategory[0] -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span> @endif</p>
                        </li> 
                        <li class="col-xs-12 col-sm-6">
                            <p><b>Creación</b></p>
                            <p>{{ date_format($subcategory[0] -> created_at, 'd/m/Y') }}</p>
                        </li>   
                        <li class="col-xs-12 col-sm-6">
                            <p><b>Modificación</b></p>
                            <p>{{ date_format($subcategory[0] -> updated_at, 'd/m/Y') }}</p>
                        </li>             
                    </ul>
                </div>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('subcategories.destroy', $subcategory[0]->id) }}" method="POST"> 
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Subcategoria</h4>
                    </div>
                    <div class="modal-body">     
                        <p>¿Esta seguro de eliminar la Subcategoria <b>{{ $subcategory[0] -> name}}</b>?</p>               
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>            
        </form>
    </div>
    <!-- end modal -->
@endsection