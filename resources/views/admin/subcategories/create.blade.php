@extends('layouts.admin.default')

@section('title', ' Subcategoría')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
     <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Nuevo</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('subcategories.store') }}" method="POST" >
        {{ csrf_field() }}
        @include('layouts.form-errors')
    		<div class="col-sm-12 col-md-7">
    			<div class="panel">
                    <div class="panel-heading clearfix">
                       <div class="pull-right text-center">
                            <a href="{{route('subcategories.index')}}" type="button" class="btn btn-warning">Atrás</a>
                            <button type="submit" class="btn btn-success">Aceptar</button>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required="required">
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Descripción<span class="text-danger">*</span></label>
                            <textarea name="description" class="form-control" id="description" placeholder="Descripcion" required="required" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="slug" class="control-label">Slug <span class="text-danger">*</span></label>
                            <input name="slug" type="text" class="form-control" id="slug" placeholder="Slug" required="required">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-5">
                <div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title"> Datos complementarios</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="category" class="control-label">Categoría <span class="text-danger">*</span></label>
                                @if (count($categories)>0)
                                    <select id="category" name="fk_id_category" class="form-control text-capitalize" data-bv-field="status" required="required">
                                        <option value="">Seleccione una categoría...</option>
                                        @foreach ($categories as $value)
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <p class="text-danger">No hay categorías registradas.</p>
                                    <p>Si desea registrar una categoría haga click <a href="{{ route(categories.index) }}" title="Categorias">aquí</a>.</p>
                                @endif
                        </div>
                        <div class="form-group">
                            <label for="estatus" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select id="status" name="status" class="form-control" data-bv-field="status" required="required">
                                <option value="">Seleccione un estatus...</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@endsection
