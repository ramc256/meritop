@extends('layouts.admin.default')

@section('title', 'Subcategoría')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Editar</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('subcategories.update',$subcategory -> id) }}" method="POST">
            {{ csrf_field() }}
            @include('layouts.form-errors')
            <input name="_method" type="hidden" value="PUT">
    			<div class="panel">
                    <div class="panel-heading clearfix">
                        <div class="pull-right text-center">
                            <a href="{{route('subcategories.show', $subcategory -> id) }}" type="button" class="btn btn-warning">Atrás</a>
                            <button type="submit" class="btn btn-success">Aceptar</button>
                        </div>
                    </div>
                    <div class="panel-body row">
                        <div class="col-sm-12 col-md-7">
                            <div class="form-group">
                                <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                                <input name="name" type="text" class="form-control  text-capitalize" id="name" placeholder="Nombre" value="{{$subcategory -> name}}" required="required">
                            </div>
                            <div class="form-group">
                                <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                                <textarea name="description" class="form-control" id="description" placeholder="Descripcion"" required="required" rows="10">{{ $subcategory -> description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="slug" class="control-label">Slug <span class="text-danger">*</span></label>
                                <input name="slug" type="text" class="form-control" id="slug" placeholder="Slug" value="{{$subcategory -> slug}}" required="required">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-5">
                        <div class="form-group">
                            <label for="category" class="control-label">Categoria <span class="text-danger">*</span></label>
                            @if (count($categories)>0)
                                <select id="category" name="fk_id_category" class="form-control text-capitalize" data-bv-field="status" required="required">
                                    <option value="">Seleccione una categoria...</option>
                                    @foreach ($categories as $value)
                                        <option value="{{ $value->id }}" @if ($value->id == $subcategory->fk_id_category) selected="selected" @endif>{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <p class="text-danger">No hay categorias registradas.</p>
                                <p>Si desea registrar una categoria haga click <a href="{{ route(categories.index) }}" title="Categorias">aquí</a>.</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="estatus" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select id="status" name="status" class="form-control" data-bv-field="status" required="required">
                                <option value="">Seleccione un estatus...</option>
                                <option value="1" @if($subcategory -> status == 1 ) selected="selected" @endif>Activo</option>
                                <option value="0" @if($subcategory -> status == 0 ) selected="selected" @endif>Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
            <div>
        </form>
	</article>
@endsection
