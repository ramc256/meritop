@extends('layouts.admin.default')

@section('title', 'Club')
@section('styles')
    <style>
        input[type=number]::-webkit-outer-spin-button,

        input[type=number]::-webkit-inner-spin-button {

            -webkit-appearance: none;

            margin: 0;

        }
        input[type=number] {

            -moz-appearance:textfield;

        }
    </style>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <h1>@yield('title')<small> - Editar</small></h1>
    </header>
    @include('flash::message')
    @include('layouts.form-errors')
    <article class="content">
        <form id="form-validation" action="{{ route('clubs.update', $club -> id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="put">
            <div class="panel">
                <div class="panel-heading clearfix">
                     <div class="pull-left text-center">
                        <h3>Datos principales</h3>
                    </div>
                    <div class="pull-right text-center">
                        <a href="{{route('clubs.show', $club -> id )}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-md-12">
                        <div class="row">   
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fiscal" class="control-label">Identificación Fiscal <span class="text-danger">*</span></label>
                                    <input name="fiscal" type="text" class="form-control" id="fiscal" value="{{$club -> fiscal}}" placeholder="Identificación Fiscal" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" value="{{$club -> name}}" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alias" class="control-label">Alias <span class="text-danger">*</span></label>
                            <input name="alias" type="text" class="form-control" id="alias" placeholder="Alias" value="{{$club -> alias}}" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="slug" class="control-label">Slug <span class="text-danger">*</span></label>
                            <input id="slug" name="slug" type="text" class="form-control" placeholder="Alias" value="{{$club -> slug}}" required="required">
                        </div> 
                    </div> 
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Dirección</h4>
                        <hr>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group">
                            <label for="address" class="control-label">Dirección <span class="text-danger">*</span></label>
                            <input name="address" type="text" class="form-control" id="address" value="{{$club -> address}}"  placeholder="Dirección" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="countries" class="control-label">País <span class="text-danger">*</span></label>
                            <select id="countries" name="fk_id_country" class="form-control" required="required">
                                <option value="">Seleccione un país...</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" @isset($club)@if($club -> fk_id_country == $country -> id) selected="selected" @endif @endisset>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="state" class="control-label"> Estado <span class="text-danger">*</span></label>
                            <input name="state" type="text" class="form-control" id="state" value="{{$club -> state}}"  placeholder="Estado" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="city" class="control-label"> Ciudad <span class="text-danger">*</span></label>
                            <input name="city" type="text" class="form-control" id="city" value="{{$club -> city}}"  placeholder="Ciudad" required="required">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">  
                                <div class="form-group">
                                    <label for="phone" class="control-label">Teléfono <span class="text-danger">*</span></label>
                                    <input name="phone" type="text" class="form-control" id="phone" value="{{$club -> phone}}" placeholder="Teléfono" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Configuración visual</h4>
                        <hr>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <div class="text-center thumbnail relative mbl" >
                                @if (empty($logo_image -> image))
                                    <img class="image-thumbnail max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $club -> name }}">
                                @else
                                    <img class="image-thumbnail max-size" src="{{ $_ENV['STORAGE_PATH'].$logo_image -> image }}" alt="{{ $club -> name }}">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="logo_image" class="control-label">Imagen de logo </label>
                            <input id="logo_image" name="logo_image" type="file" class="form-control">
                            @if (!empty($logo_image))
                                <input name="id_logo_image" type="hidden" class="form-control" readonly="readonly" value="{{ $logo_image -> id }}">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="text-center thumbnail relative mbl" >
                                @if (empty($login_image -> image))
                                    <img class="image-thumbnail max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $club -> name }}">
                                @else
                                    <img class="image-thumbnail max-size" src="{{ $_ENV['STORAGE_PATH'].$login_image -> image }}" alt="{{ $club -> name }}">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image" class="control-label">Imagen de fondo</label>
                            <input id="login_image" name="login_image" type="file" class="form-control">
                            @if (!empty($login_image))
                                <input name="id_login_image" type="hidden" class="form-control" readonly="readonly" value="{{ $login_image -> id }}">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="color" class="control-label">Color <span class="text-danger">*</span></label>
                            <input id="color" name="color" type="text" class="form-control" required="required" placeholder="#FFFFFF" value="{{$club -> color}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Configuración</h4>
                        <hr>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fk_id_currency" class="control-label">Moneda <span class="text-danger">*</span></label>
                            <select id="fk_id_currency" name="fk_id_currency" class="form-control" required="required">
                                <option value="">Seleccione una moneda...</option>
                                @foreach($currencies as $currency)
                                    <option value="{{ $currency->id }}" @isset($club)@if($club -> fk_id_currency == $currency -> id) selected="selected" @endif @endisset>{{ $currency -> currency_code . ' - ' .$currency -> currency }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="system_currency" class="control-label">Moneda del Sistema<span class="text-danger">*</span></label>
                            <input name="system_currency" type="text" class="form-control" id="system_currency" value="{{$club -> system_currency }}"  placeholder="Moneda del sistema" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="convertion_rate" class="control-label">Tasa de conversión<span class="text-danger">*</span></label>
                            <input name="convertion_rate" type="text" class="form-control" id="convertion_rate" value="{{$club -> convertion_rate}}"  placeholder="Taza de conversión" required="required" pattern="[0-9]+([,\.][0-9]*)">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="member_format_import" class="control-label">Tipo de formato del miembro<span class="text-danger">*</span></label>
                            <select id="menber_format_import" name="member_format_import" class="form-control" required="required">
                                <option value="">Seleccione un formato...</option>
                                <option value="0" @if($club -> member_format_import == 0) selected="selected" @endif>DNI</option>
                                <option value="1" @if($club -> member_format_import == 1) selected="selected" @endif>Carnet</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Estatus</h4>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="status" class="control-label">Estatus<span class="text-danger">*</span></label>
                            <select id="status" name="status" class="form-control" required="required">
                                <option value="">Seleccione un estatus...</option>
                                <option value="1" @if($club -> status == 1) selected="selected" @endif>Activo</option>
                                <option value="0" @if($club -> status == 0) selected="selected" @endif>Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </article>
@stop
@section('scripts')
    <!--page js bootstrapvalidation-->
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <!-- End page js bootstrapvalidation -->
@endsection
