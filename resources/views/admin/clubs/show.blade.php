@extends('layouts.admin.default')

@section('title', 'Club')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                 <div class="pull-left text-center">
                    <h3>Datos principales</h3>
                </div>
                <div class="pull-right text-center">
                    <a href="{{route('clubs.index')}}" type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{route('clubs.edit', $club->id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-sm-12 col-md-12">
                    <ul class="panel-list">
                        <li class="row">
                             <div class="col-xs-12 col-sm-6">
                                <p><b>Identificación Fiscal</b></p>
                                <p>{{ $club -> fiscal }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div  class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Nombre</b></p>
                                <p class="text-capitalize">{{ $club -> name }}</p>
                            </div>
                            <div  class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Alias</b></p>
                                <p class="text-capitalize">{{ $club -> alias }}</p>
                            </div>
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Slug</b></p>
                                <p>{{ $club -> slug }}</p>
                            </div>
                        </li>
                        <li>
                            <h4><b>Dirección</b></h4>
                        </li>
                        <li>
                            <p><b>Dirección</b></p>
                            <p>{{ $club -> address }}</p>
                        </li>
                        <li class="row">
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>País</b></p>
                                <p>{{ $club -> country }}</p>
                            </div>
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Ciudad</b></p>
                                <p>{{ $club -> city }}</p>
                            </div>
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Estado</b></p>
                                <p>{{ $club -> state }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Teléfono</b></p>
                                <p>{{ $club -> phone }}</p>
                            </div>
                           
                        </li>
                        <li>
                            <h4><b>Configuración visual</b></h4>
                        </li>
                        <div class="col-md-4">
                            <div class="form-group">
                                <p><b>Imagen del logo</b></p>
                                 <div class="text-center thumbnail mbl">
                                    @if (empty($logo_image -> image))
                                        <img  class="image-thumbnail img-responsive max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $club -> name }}">
                                    @else
                                        <img  class="image-thumbnail img-responsive max-size" src="{{ $_ENV['STORAGE_PATH'].$logo_image -> image }}" alt="{{ $club -> name }}">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <p><b>Imagen del fondo</b></p>
                                <div class="text-center thumbnail mbl">
                                    @if (empty($login_image -> image))
                                        <img  class="image-thumbnail img-responsive max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $club -> name }}">
                                    @else
                                        <img  class="image-thumbnail img-responsive max-size" src="{{ $_ENV['STORAGE_PATH'].$login_image -> image }}" alt="{{ $club -> name }}">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <li class="row">
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Color</b></p>
                                <p>{{ $club -> color }}</p>
                            </div>
                        </li>
                         <li>
                            <h4><b>Configuración</b></h4>
                        </li>
                        <li class="row">
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Moneda</b></p>
                                <p>{{$club -> currency}}</p>
                            </div>
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Moneda del sistema</b></p>
                                <p>{{ $club -> system_currency }}</p>
                            </div>
                              <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>tasa de conversión</b></p>
                                <p>{{ $club -> convertion_rate }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-4 col-md-12 col-lg-4">
                                <p><b>Tipo de formato de miembro</b></p>
                                <p>@if ( $club -> member_format_import == 0) <span>DNI</span> @else <span>Carnet</span> @endif</p>
                            </div>
                        </li>
                         <li>
                            <h4><b>Estatus</b></h4>
                        </li>
                        <li>
                            <p><b>Estatus</b></p>
                            <p>@if ( $club -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span> @endif</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
@stop
@section('scripts')
    <!-- Page js-->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->
@endsection
