@extends('layouts.admin.default')

@section('title', 'Club')
@section('styles')
<link type="text/css" href="css/app.css" rel="stylesheet"/>
    <!-- end of global css -->
    <!--page level css -->
    <link rel="stylesheet" href="{{ asset('core-plus-theme/vendors/select2/css/select2.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('core-plus-theme/vendors/select2/css/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('core-plus-theme/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}">
    <link rel="stylesheet" href="{{ asset('core-plus-theme/vendors/iCheck/css/all.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('core-plus-theme/css/custom_css/wizard.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}">
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Nuevo</small></h1>
        </div>
    </header>
    <article class="content">
        @include('layouts.form-errors')
        <div class="panel">
            <div class="panel-body">
                <div class="stepwizard">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step">
                            <a href="#step-1" class="btn btn-primary btn-circle">1</a>
                            <p>Step 1</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-2" class="btn btn-default btn-circle">2</a>
                            <p>Step 2</p>
                        </div>
                    </div>
                </div>
                <form id="form-validation" action="{{ route('clubs.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row setup-content" id="step-1">
                        <div class="col-md-12">
                            <h3>Datos principales</h3>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fiscal" class="control-label">Identificación fiscal <span class="text-danger">*</span></label>
                                        <input name="fiscal" type="text" class="form-control" id="fiscal" placeholder="Identificacion fiscal" required="required">
                                    </div>
                                </div>
                             </div>
                        </div>
                         <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                                        <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required="required">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="alias" class="control-label">Alias <span class="text-danger">*</span></label>
                                        <input name="alias" type="text" class="form-control" id="alias" placeholder="Alias" required="required">
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="slug" class="control-label">Slug <span class="text-danger">*</span></label>
                                        <input name="slug" type="text" class="form-control" id="slug" placeholder="Url de la pagina" required="required">
                                    </div>
                                </div>
                            </div>
                         </div>
                         <div class="col-md-12">
                            <h4 class="text-capitalize">Dirección</h4>
                            <hr>
                        </div>
                         <div class="col-md-12">
                            <div class="form-group">
                                <label for="address" class="control-label">Dirección <span class="text-danger">*</span></label>
                                <input name="address" type="text" class="form-control" id="address" placeholder="Dirección" required="required">
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="countries" class="control-label">País <span class="text-danger">*</span></label>
                                <select id="countries" name="fk_id_country" class="form-control" required="required">
                                    <option value="">Seleccione un país...</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country -> name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="state" class="control-label">Estado <span class="text-danger">*</span></label>
                                <input name="state" type="text" class="form-control" id="state" placeholder="Estado" required="required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="city" class="control-label">Ciudad <span class="text-danger">*</span></label>
                                <input name="city" type="text" class="form-control" id="city" placeholder="Ciudad" required="required">
                            </div>
                        </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                                <label for="phone" class="control-label">Teléfono <span class="text-danger">*</span></label>
                                <input name="phone" type="text" class="form-control phone" id="phone" placeholder="Teléfono"  required="required">
                            </div>
                        </div> 
                        <div class="col-md-12">
                            <h4 class="text-capitalize">Configuración Visual</h4>
                            <hr>
                        </div>
                           <div class="col-md-4">
                            <div class="form-group">
                                <label for="logo_image" class="control-label">Imagen de logo <span class="text-danger">*</span></label>
                                <input name="logo_image" type="file" class="form-control" id="logo_image" placeholder="Imagen de logo">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="login_image" class="control-label">Imagen de fondo <span class="text-danger">*</span></label>
                                <input name="login_image" type="file" class="form-control" id="login_image" placeholder="Imagen del login">
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="color" class="control-label">Color <span class="text-danger">*</span></label>
                                <!-- Color Picker -->
                                <input id="color" name="color" type="text" class="form-control" required="required" placeholder="#FFFFFF">
                            </div>
                        </div>
                         <div class="col-md-12">
                            <h4 class="text-capitalize">Configuración</h4>
                            <hr>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="fk_id_currency" class="control-label">Moneda <span class="text-danger">*</span></label>
                                <select id="fk_id_currency" name="fk_id_currency" class="form-control" required="required">
                                    <option value="">Seleccione una moneda...</option>
                                    @foreach($currencies as $currency)
                                        <option value="{{ $currency->id }}">{{ $currency -> currency_code . ' - ' .$currency -> currency }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="system_currency" class="control-label">Moneda del Sistema <span class="text-danger">*</span></label>
                                <input name="system_currency" type="text" class="form-control" id="system_currency" placeholder="Moneda del sistema" required="required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="convertion_rate" class="control-label">Tasa de conversión <span class="text-danger">*</span></label>
                                <input name="convertion_rate" type="text" class="form-control" id="convertion_rate" placeholder="Tasa de conversion" required="required" pattern="[0-9]+([,\.][0-9]*)">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="member_format_import" class="control-label">Tipo de formato del miembro<span class="text-danger">*</span></label>
                                <select id="member_format_import" name="member_format_import" class="form-control" required="required">
                                    <option value="">Seleccione un formato...</option>
                                    <option value="0">DNI</option>
                                    <option value="1">Carnet</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4 class="text-capitalize">Estatus</h4>
                            <hr>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status" class="control-label">Estatus<span class="text-danger">*</span></label>
                                <select id="status" name="status" class="form-control" required="required">
                                    <option value="">Seleccione un estatus...</option>
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary nextBtn pull-right" type="button">Siguiente </button>
                        </div>
                    </div>
                    <div class="setup-content" id="step-2">
                        <h3>Productos</h3>
                        <div class="form-inline" style="display: block; overflow: hidden;">
                            <div class="form-group ">
                                <span class="control-label" ><font size=3>Categoría</font></span>
                                    <select  class="text-capitalize" id="filterInput" name="categories" class="form-control" required="required">
                                         <option class="text-capitalize" value="all">Todos </option>
                                         @foreach($categories as $category)
                                                <option class="text-capitalize" >{{ $category -> name }}</option>
                                         @endforeach
                                    </select>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="filter-table" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="selectAll"/></th>
                                        <th>Producto</th>
                                        <th>Modelo</th>
                                        <th>Categoría</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($parents_products as $element)
                                        <tr>
                                            <td><input id="{{ $element -> name . $element -> id }}" type="checkbox" name="parents_products[]" class="seleted-check" value="{{ $element -> id }}"></td>
                                            <td>{{ $element -> name }}</td>
                                            <td>{{ $element -> model }}</td>
                                            <td class="text-capitalize filter-column">{{ $element -> category }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-warning prevBtn pull-left" type="button">
                                Anterior
                            </button>
                            <button class="btn btn-success pull-right" type="submit">
                                Finalizar!
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </article>
@stop
@section('scripts')

{{-- <script type="text/javascript">

        $(document).ready(function(){
           
            $("#checkTodos").change(function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });

            $('.seleted-check').change(function () {
                modulo = $(this).val();
                $('.'+modulo).prop('checked', $(this).prop("checked"));
            });
        }); 
    </script>
 --}}
<script>
    $(document).ready(function () { 
        var oTable = $('#filter-table').dataTable({
            stateSave: true
        });

        var allPages = oTable.fnGetNodes();

        $('body').on('click', '#selectAll', function () {
            if ($(this).hasClass('allChecked')) {
                $('input[type="checkbox"]', allPages).prop('checked', false);
            } else {
                $('input[type="checkbox"]', allPages).prop('checked', true);
            }
            $(this).toggleClass('allChecked');
        })
    });
</script>
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{ asset('assets/jquery.filters.js') }}"></script>
    <script src="{{ asset('core-plus-theme/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('core-plus-theme/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('core-plus-theme/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('core-plus-theme/vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('core-plus-theme/js/custom_js/form_wizards.js') }}" type="text/javascript"></script>
    <!-- End of page level js -->
    <!-- Page js bootstrap validation-->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- End page js -->
     <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
@endsection
