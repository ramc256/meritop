@extends('layouts.admin.default')

@section('title', 'Usuario')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Nuevo</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('layouts.form-errors')
			<div class="panel">
               <div class="panel-heading clearfix">
                    <div class="pull-right text-center">
                        <a href="{{route('users.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-sm-12 col-md-7">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="first_name" type="text" class="form-control" id="name" placeholder="Nombre" required="required">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Apellido <span class="text-danger">*</span></label>
                            <input name="last_name" type="text" class="form-control" id="lastname" placeholder="Apellido"  required="required">
                        </div>
                        <div class="form-group">
                            <label for="user_name" class="control-label">Usuario <span class="text-danger">*</span></label>
                            <input name="user_name" type="text" class="form-control" id="user_name" placeholder="username"  required="required">
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">E-mail <span class="text-danger">*</span></label>
                            <input name="email" type="mail" class="form-control" id="email" placeholder="E-mail"  required="required">
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label">Contraseña <span class="text-danger">*</span></label>
                            <input name="password" type="password" class="input-md form-control" id="password1" placeholder="Contraseña" autocomplete="off"  required="required">
                        </div>
                        <br/>
                        <div class="hidden validator">
                            <div class="col-sm-6 padding">
                                <span id="8char" class="glyphicon glyphicon-remove"
                                style="color:#FE5B5B;"></span> 8 caracteres de longitud
                                <br>
                                <span id="ucase" class="glyphicon glyphicon-remove"
                                style="color:#FE5B5B;"></span> 1 Letra Minuscula
                            </div>
                            <div class="col-sm-6 padding">
                                <span id="lcase" class="glyphicon glyphicon-remove"
                                style="color:#FE5B5B;"></span> 1 Letra Mayuscula
                                <br>
                                <span id="num" class="glyphicon glyphicon-remove" style="color:#FE5B5B;"></span>
                                1 numero
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="re-password" class="control-label">Repitir Contraseña <span class="text-danger">*</span></label>
                            <input name="re-password" type="password" class="form-control" id="password2" placeholder="Repetir Contraseña"  required="required">
                        </div>
                        <div class="col-sm-12 padding validator hidden">
                        <br/>
                            <span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FE5B5B;"></span>
                            Passwords Match
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Club <span class="text-danger">*</span></label>
                            @if(count($clubs)>0)
                                @foreach($clubs as $club)
                                    <input id="{{ $club -> name }}" name="clubs[]" type="checkbox"  value="{{ $club -> id }}">
                                    <label class="text-capitalize label left label-primary" for="{{ $club -> name }}"> {{ $club -> name }}</label>
                                @endforeach
                            @else
                                <p class="text-danger" >No hay clubs registrados</p>
                                <p>Si desea registrar un club puede hacer click <a href="{{ route('clubs.create') }}">aquí</a></p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="form-group">
                            <label for="kind" class="control-label">Tipo <span class="text-danger">*</span></label>
                            <select id="kind" name="kind" class="form-control" data-bv-field="kind" required="required">
                                <option value="">
                                    Seleccione un tipo...
                                </option>
                                <option value="1">Meritop</option>
                                <option value="0">Club</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role" class="control-label">Rol <span class="text-danger">*</span></label>
                            <select id="fk_id_role" name="fk_id_role" class="form-control" required="required">
                                <option value="">Seleccione un rol...</option>
                                @foreach($roles as $dato)
                                    <option value="{{ $dato->id }}">{{ $dato->name }}</option>
                                @endforeach
                            </select>
                        </div>
                         <div class="form-group">
                            <label for="estatus" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select id="status" name="status" class="form-control" data-bv-field="status" required="">
                                <option value="">
                                    Seleccione un estatus...
                                </option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="countries" class="control-label">País <span class="text-danger">*</span></label>
                            <select id="fk_id_country" name="fk_id_country" class="form-control" required="required">
                                <option value="">Seleccione un país...</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/js/password-validator.js')}}"></script>
    <!-- end page js -->
    <!-- page validation -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page validation -->

@endsection
