@extends('layouts.admin.default')

@section('title', 'Usuario')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Editar</small></h1>
        </div>
    </header>
    @include('flash::message')
    @include('layouts.form-errors')
	<article class="content">
		<form id="form-validation" action="{{ route('users.update', $user -> id) }}" method="POST">
            <div class="panel filterable">
                <div class="panel-heading clearfix">
                    <div class="pull-right text-center">
                        <a href="{{route('users.show', $user -> id) }}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-xs-12 col-sm-7">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="first_name" type="text" class="form-control" id="first_name" placeholder="Nombre" value="{{ $user -> first_name }}" required="required">
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="control-label">Apellido <span class="text-danger">*</span></label>
                            <input name="last_name" type="text" class="form-control" id="lastname" placeholder="Apellido" value="{{ $user -> last_name }}"  required="required">
                        </div>
                        <div class="form-group">
                            <label for="user_name" class="control-label">Usuario <span class="text-danger">*</span></label>
                            <input name="user_name" type="text" class="form-control" id="user_name" placeholder="username" value="{{ $user -> user_name }}" required="required">
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">E-mail <span class="text-danger">*</span></label>
                            <input name="email" type="mail" class="form-control" id="email" placeholder="E-mail" value="{{ $user -> email }}" required="required">
                        </div>
                        <div class="form-group">
                            <label for="club" class="control-label">Clubs <span class="text-danger">*</span></label>
                            @if(count($clubs)>0)
                                @if (count($clubs_final)>0)
                                    @php $i=0; @endphp
                                    @foreach($clubs_final as $club)
                                        <input id="{{ $club['id'] }}" name="clubs[]" type="checkbox" value="{{ $club['id'] }}" @if ($club['club_check']=='on') checked="checked" @endif>
                                        <label class="text-capitalize label left label-primary" for="{{ $club['id'] }}">{{ $club['name'] }}</label>
                                        @php $i++; @endphp
                                    @endforeach
                                @else
                                    @foreach($clubs as $club)
                                      <input id="{{ $club['id'] }}" name="clubs[]" type="checkbox" value="{{ $club['id'] }}">
                                      <label class="text-capitalize label left label-primary" for="{{ $club['id'] }}">{{ $club['alias'] }}</label>
                                    @endforeach
                                @endif
                            @else
                                <p style="color:red;" >No hay clubs registrados</p>
                                <p>Si desea registrar un club puede hacer click <a href="{{ route('clubs.create') }}">aquí</a></p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5">                        
                        <div class="form-group">
                            <label for="kind" class="control-label">Tipo <span class="text-danger">*</span></label>
                            <select id="kind" name="kind" class="form-control" data-bv-field="kind" required="required">
                                <option value="">Seleccione un tipo...</option>
                                <option value="1" @if ($user->kind == 1) selected='selected' @endif>Meritop</option>
                                <option value="0" @if ($user->kind == 0) selected='selected' @endif>Club</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role" class="control-label">Rol <span class="text-danger">*</span></label>
                            <select id="role" name="fk_id_role" class="form-control" required="required">
                                <option value="">Seleccione un rol...</option>
                                @foreach($roles as $dato)
                                    <option value="{{ $dato->id }}" @if ($user->fk_id_role == $dato->id) selected='selected' @endif>{{ $dato->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select id="status" name="status" class="form-control" required="required">
                                <option value="">Seleccione un estatus...</option>
                                <option value="1" @if ($user->status == 1) selected='selected' @endif>Activo</option>
                                <option value="0" @if ($user->status == 0) selected='selected' @endif>Inactivo</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="countries" class="control-label">País <span class="text-danger">*</span></label>
                            <select id="countries" name="fk_id_country" class="form-control" required="required">
                                <option value="">Seleccione un pais...</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" @if ($country->id == $user->fk_id_country) selected='selected' @endif>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- page validation -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page validation -->

@endsection
