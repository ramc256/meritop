@extends('layouts.admin.default')

@section('title', 'Usuarios')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Gestiones</small></h1>
        </div>
    </header>
    @include('flash::message')
	<article class="content">
		<div class="panel filterable">  
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left m-t-6">
                </h3>
                <a href="{{ route('users.create')}}" class="btn btn-success btn-md pull-right" id="addButton">Nuevo</a>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-asc">
                        <thead>
                            <tr>
                                <th style=" width:25px">Nombre</th>
                                <th style=" width:25px">Apellido</th>
                                <th style=" width:15px">Rol</th>
                                <th style=" width:15px">Tipo</th>
                                <th style=" width:20px">País</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $datos)
                                <tr>
                                    <td class="text-capitalize"><a href="{{route('users.show', $datos -> id)}}">{{ $datos -> first_name }}</a></td>
                                    <td class="text-capitalize">{{$datos -> last_name}}</td>
                                    <td class="text-capitalize">{{ $datos -> role }}</td>
                                    <td>@if ( $datos -> kind == 1) Meritop @else Club @endif</td>
                                    <td>{{ $datos -> country }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</article>
@stop
@section('scripts')
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
@endsection
