@extends('layouts.admin.default')

@section('title', 'Usuario')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">
                    <a href="{{route('users.index')}}" type="button" class="btn btn-warning">Atrás</a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" @if( $user -> id == 1) disabled="disabled" @endif>Eliminar</button>
                    <a href="{{route('users.edit', $user -> id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-sm-12 col-md-5">
                    <div class="form-group">
                        <div  class="text-center mbl">
                            @if (empty($user -> image))
                                <img class="image-thumbnail img-circle max-size"  src="{{ Storage::disk('images') -> url('user-default.jpg') }}" alt="{{ $user -> name }}">
                            @else
                                <img class="image-thumbnail img-circle max-size"  src="{{ $_ENV['STORAGE_PATH'].$user -> image }}" alt="{{ $user -> name }}">
                            @endif
                        </div>
                    </div>
                    <ul class="panel-list">
                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Tipo de usuario</b></p>
                                <p class="text-capitalize">@if( $user -> kind == 1) Meritop @else Club @endif</p>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Rol</b></p>
                                <p class="text-capitalize">{{ $user -> role }}</p>
                            </div>
                        </li>
                        <li>
                            <p><b>Estatus</b></p>
                            <p class="text-capitalize">@if( $user -> status == 0) <span class="label label-danger">Inactivo</span> @else <span class="label label-success">Activo</span> @endif</p>
                        </li>
                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Fecha de Creación</b></p>
                                <p>{{ date_format($user -> created_at, 'd/m/Y') }}</p>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Fecha de Actualización</b></p>
                                <p>{{ date_format($user -> updated_at, 'd/m/Y') }}</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-7">
                    <ul class="panel-list">
                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Nombre y Apellido</b></p>
                                <p class="text-capitalize">{{ $user -> first_name . ' ' . $user -> last_name }}</p>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Nombre de Usuario</b></p>
                                <p>{{ $user -> user_name }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>E-mail</b></p>
                                <p>{{ $user -> email }}</p>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Pais</b></p>
                                <p class="text-capitalize">{{ $user -> country }}</p>
                            </div>
                        </li>
                        <li>
                            <p><b>Clubs</b></p>
                            @if(count($users_clubs)>0)
                                @foreach($users_clubs as $datos)
                                    <p class="text-capitalize label left label-primary">{{ $datos -> name }}</p>
                                @endforeach
                            @else
                                <p class="text-danger">Este usuario no esta asociado a ningun club</p>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('users.destroy', $user->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Usuario</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el usuario <b>{{ $user -> first_name .' '. $user -> last_name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scritps')
    <!-- page js-->
    <script>
        $('#modal-delete').on('shown.bs.modal', function () {
          $('#btn-modal-delete').focus()
        })
    </script>
    <!-- end page js -->
@endsection
