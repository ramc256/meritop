@extends('layouts.admin.default')

@section('title', 'Order')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Devolución</small></h1>
        </div>
    </header>
    @include('flash::message')
    @include('layouts.form-errors')
    <article class="content">
        <form action="{{ route('orders.refund-store', $order -> id ) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
            <div class="panel">
                <div class="panel-heading clearfix">
                    <div class="pull-right">
                        <a href="{{route('orders.show', $order -> id)}}" type="button" class="btn btn-warning">Atrás</a>
                    </div>
                </div>
                <div class="panel-body">
                    <section class="content p-l-r-15" id="invoice-stmt">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="pull-left text-left" style="padding-left: 0;">
                                    <li><b>Miembro</b> {{ $order -> first_name . ' ' . $order -> last_name }}</li>
                                    <li><b>DNI</b> {{ $order -> dni }}</li>
                                    <li><b>Carnet</b> {{ $order -> carnet }}</li>
                                    <li><b>Sede</b> {{ $order -> branch }}</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="pull-right text-right">
                                    <li><b>Orden #</b>{{ str_pad( $order -> id, 10 , 0, STR_PAD_LEFT) }}</li>
                                    <li><b>Estatus</b>
                                        @if ($order -> status == 0) <span class="label label-default" style="color: #333">Abierto</span>
                                        @elseif($order -> status == 1) <span class="label label-primary">Enviado </span>
                                        @elseif($order -> status == 2) <span class="label label-success"> Recibido </span>
                                        {{-- @elseif($order -> status == 3) <span class="label label-warning">Devuelto</span> --}}
                                        @elseif($order -> status == 3) <span class="label label-danger">Cancelado</span>
                                        @else
                                            Estatus incorrecto
                                        @endif
                                    </li>
                                    <li><b>Fecha</b> {{ $order -> created_at }}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed" id="order">
                                        <thead>
                                            <tr>
                                                <th>SKU #</th>
                                                <th>Nombre</th>
                                                <th>Cantidad</th>
                                                <th>Puntos</th>
                                                <th class="text-right">
                                                    <strong>Total de puntos</strong>
                                                </th>
                                                {{-- <th class="text-center" id="add_row"><i class="fa fa-fw fa-plus"></i></th> --}}
                                            </tr>
                                        </thead>
                                        <tbody id="items">
                                             @foreach($orders_products as $element)
                                                 <tr class="item">
                                                    <td>{{ $element -> sku }}</td>
                                                    <td>{{ $element -> name }}</td>
                                                    <td class="text-center"><input class="quantity" type="number" name="quantity[{{ $element -> id }}]" value="{{ $element -> quantity }}" ></td>
                                                    <td class="price text-center"><input type="hidden" name="points[{{ $element -> id }}]" value="{{ $element -> points }}">{{ $element -> points }}</td>
                                                    <td class="subtotal text-right">{{ $element -> points * $element -> quantity }}</td>
                                                    {{-- <td class="text-center row_delete" ><i class="fa fa-fw fa-times" id="row_delete"></i></td> --}}
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-right">
                                                    <strong>
                                                        Sub total: &nbsp;
                                                    </strong>
                                                </td>
                                                <td id="subtotal_order" class="text-right"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-right">
                                                    <strong>
                                                        Descuento
                                                    </strong>
                                                </td>
                                                <td class="text-right"><input id="discount_order" name="discount" value="{{ $order -> discount}}"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="livicon" data-name="barcode" data-size="60" data-loop="true"></i>
                                                     <b>Cantidad de items</b> <span id="items_quantity"></span>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-right">
                                                    <strong>
                                                        Total: &nbsp;
                                                    </strong>
                                                </td>
                                                <td class="text-right" id="total_order"></td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                               <div class="form-group">
                                   <label for="note" class="control-label">Nota</label>
                                   <textarea name="note" id="note" cols="4" rows="4" maxlength="200" class="form-control" placeholder="Nota" ></textarea>
                               </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                   <label for="internal_note" class="control-label">Nota interna</label>
                                   <textarea name="internal_note" id="internal_note" cols="4" rows="4" maxlength="1000" class="form-control" placeholder="Nota interna"></textarea>
                               </div>
                            </div>
                        </div>
                        {{-- <div class="col-md-12">
                            <h4><Strong>Terminos y Condiciones:</Strong></h4>
                            <ul>
                                <li>An invoice must accompany products returned for warantty</li>
                                <li>Balance due within 10 days of invoice date,1.5% interest/month thereafter.</li>
                                <li>All goods returned for replacement/credit must be saleable condition with original
                                    packaging.
                                </li>
                            </ul>
                        </div> --}}
                        <div class="btn-section">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-success">Aceptar</button>
                                </span>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </form>
    </article>
@stop
@section('scripts')
    <!--  page order js -->
    <script>
       $(document).ready(function(){

            function calculator(){
                var subtotal_order = 0;
                var items_quantity = 0;

                $("#items .subtotal").each(function(){
                    subtotal_order += parseInt($(this).text());
                });

                $("#items .quantity").each(function(){
                    items_quantity += parseInt($(this).val());
                });

                discount_order = parseInt($("#discount_order").val());

                $("#items_quantity").text(items_quantity);
                $("#subtotal_order").text(subtotal_order);
                $("#total_order").text((subtotal_order)-(discount_order));
            }
            calculator();

            $("#items .item").change(function(){

                cantidad = $(this).find("input.quantity").val();
                price = $(this).find(".price").text();
                $(this).find(".subtotal").text(parseInt(cantidad) * parseInt(price));
                calculator();

            });
            console.log( "No hay error en la funcion ");
        });
    </script>
    <!--  end page order js -->
@endsection
