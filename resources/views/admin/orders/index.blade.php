@extends('layouts.admin.default')

@section('title', 'Ordenes')
@section('styles')
    <!-- Page style -->
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <!-- End Page style -->
    <!--Datepicker css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datetime/css/jquery.datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/airdatepicker/css/datepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/datepicker.css') }}">
    <!--Datepicker css -->
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Gestiones</small></h1>
        </div>
    </header>
    @include('flash::message')
	<article class="content">
		<div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="pull-right">
                    <a href="{{ route('orders.create') }}" class="btn btn-success btn-md" id="addButton">Nuevo</a>
                </div>
                <div class="btns-header">
                    <button  data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-1" type="button" class="btn btn-labeled btn-meritop-primary">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-down"></i></span> Filtros
                    </button>
                </div>          
                <div class="panel-faq">          
                    <div id="faq-cat-1-sub-1" class="collapse p-t-10">
                        <div class="pull-left m-r-10">
                            <form class="form-inline" method="post" action=" {{ route('orders.filter') }} ">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <label class="control-label" for="dateranges">Rango de fecha: </label>
                                <input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" class="form-control" id="dateranges" readonly="readonly" value="{{ date('01/m/Y') }} - {{ date('d/m/Y') }}"/>
                                <button type="submit" class="btn btn-meritop-primary btn-md" id="addButton">Buscar</button>
                            </form>
                        </div>
                        <div id="filter-club" class="m-l-10 form-inline pull-left">
                            <label class="control-label">Club</label>
                            <select class="form-control text-capitalize" id="filterInput" name="clubs" class="form-control" required="required">
                                <option class="text-capitalize" value="all">Todos</option>
                                @foreach($users_clubs as $element)
                                    <option class="text-capitalize" value="{{ $element -> name }}">{{ $element -> name }}</option>
                                 @endforeach
                            </select>
                        </div>  
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs mar-b-20  nav-custom" role="tablist" style="margin-top: -3px; margin-bottom: 30px;">
                    <li role="presentation" class="active"><a href="#open" aria-controls="open" role="tab" data-toggle="tab">Abierto</a></li>
                    <li role="presentation"><a href="#sent" aria-controls="sent" role="tab" data-toggle="tab">Enviado</a></li>
                    <li role="presentation"><a href="#delivered" aria-controls="delivered" role="tab" data-toggle="tab">Recibido</a></li>
                    <li role="presentation"><a href="#canceled" aria-controls="canceled" role="tab" data-toggle="tab">Cancelado</a></li>
                    {{-- <li role="presentation"><a href="#refund" aria-controls="refund" role="tab" data-toggle="tab">Devolución</a></li> --}}
                </ul>
                <!--END Nav tabs -->
                <div class="tab-content">
                    <div id="open" role="tabpanel" class="tab-pane active" >
                        <div class="table-responsive">
                            <table id="filter-table" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Order #</th>
                                        <th>Fecha</th>
                                        <th>DNI</th>
                                        <th>Nombre</th>
                                        <th>Club</th>
                                        <th>Estado</th>
                                        <th>Ciudad</th>
                                        <th>Cantidad de productos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($open as $element)
                                        <tr>
                                            <td><a href="{{ route('orders.show', $element -> id ) }}">{{ str_pad( $element -> id, 10 , 0, STR_PAD_LEFT) }}</a></td>
                                            <td>{{ date_format($element -> created_at, 'd/m/Y') }}</td>
                                            <td>{{ $element -> dni }}</td>
                                            <td>{{ $element -> first_name . ' ' . $element -> last_name }}</td>
                                            <td class="filter-column">{{ $element -> club }}</td>
                                            <td>{{ $element -> state }}</td>
                                            <td>{{ $element -> city }}</td>
                                            <td>{{ $element -> quantity }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="sent" role="tabpanel" class="tab-pane">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Order#</th>
                                        <th>Fecha</th>
                                        <th>Cédula</th>
                                        <th>Nombre</th>
                                        <th>Club</th>
                                        <th>Estado</th>
                                        <th>Ciudad</th>
                                        <th>Cantidad de productos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sent as $element)
                                        <tr>
                                            <td><a href="{{ route('orders.show', $element -> id ) }}">{{ str_pad( $element -> id, 10 , 0, STR_PAD_LEFT) }}</a></td>
                                            <td>{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}</td>
                                            <td>{{ $element -> dni }}</td>
                                            <td>{{ $element -> first_name . ' ' . $element ->last_name }}</td>
                                            <td>{{ $element -> club }}</td>
                                            <td>{{ $element -> state }}</td>
                                            <td>{{ $element -> city }}</td>
                                            <td>{{ $element -> quantity }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="delivered" role="tabpanel" class="tab-pane">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Order #</th>
                                        <th>Fecha</th>
                                        <th>Cédula</th>
                                        <th>Nombre</th>
                                        <th>Club</th>
                                        <th>Estado</th>
                                        <th>Ciudad</th>
                                        <th>Cantidad de productos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($delivered as $element)
                                        <tr>
                                            <td><a href="{{ route('orders.show', $element -> id ) }}">{{ str_pad( $element -> id, 10 , 0, STR_PAD_LEFT) }}</a></td>
                                            <td>{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}</td>
                                            <td>{{ $element -> dni }}</td>
                                            <td>{{ $element -> first_name . ' ' . $element ->last_name }}</td>
                                            <td>{{ $element -> club }}</td>
                                            <td>{{ $element -> state }}</td>
                                            <td>{{ $element -> city }}</td>
                                            <td>{{ $element -> quantity }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="canceled" role="tabpanel" class="tab-pane">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Order #</th>
                                        <th>Fecha</th>
                                        <th>Cédula</th>
                                        <th>Nombre</th>
                                        <th>Club</th>
                                        <th>Estado</th>
                                        <th>Ciudad</th>
                                        <th>Cantidad de productos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($canceled as $element)
                                        <tr>
                                            <td><a href="{{ route('orders.show', $element -> id ) }}">{{ str_pad( $element -> id, 10 , 0, STR_PAD_LEFT) }}</a></td>
                                            <td>{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}</td>
                                            <td>{{ $element -> dni }}</td>
                                            <td>{{ $element -> first_name . ' ' . $element ->last_name }}</td>
                                            <td>{{ $element -> club }}</td>
                                            <td>{{ $element -> state }}</td>
                                            <td>{{ $element -> city }}</td>
                                            <td>{{ $element -> quantity }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{-- <div id="refund" role="tabpanel" class="tab-pane ">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover datatable-desc">
                                <thead>
                                    <tr>
                                        <th>Order #</th>
                                        <th>Fecha</th>
                                        <th>Cédula</th>
                                        <th>Nombre</th>
                                        <th>Club</th>
                                        <th>Estado</th>
                                        <th>Ciudad</th>
                                        <th>Cantidad de productos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($refounds as $element)
                                            <tr>
                                                <td><a href="{{ route('orders.show', $element -> id ) }}">{{ str_pad( $element -> id, 10 , 0, STR_PAD_LEFT) }}</a></td>
                                                <td>{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}</td>
                                                <td>{{ $element -> dni }}</td>
                                                <td>{{ $element -> first_name . ' ' . $element ->last_name }}</td>
                                                <td>{{ $element -> club }}</td>
                                                <td>{{ $element -> state }}</td>
                                                <td>{{ $element -> city }}</td>
                                                <td>{{ $element -> quantity }}</td>
                                            </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> --}}
                </div>
            </div>  
        </div>
    </article>
@stop
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/jquery.filters.js') }}"></script>

    <!-- Page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- End page js -->

    <!-- DateRange Picked js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datetime/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/advanceddate_pickers.js') }}"></script>
    <!-- End DateRange Picked js -->

    <script type="text/javascript">
        $(document).ready(function(){
            $("#filterInput").filterTables();
            //table tools example
            var table = $('#filter-table').DataTable({
                // dom: 'Bflrtip',
                "dom": '<"m-t-10"B><"m-t-10 pull-left"f><"m-t-10 pull-right"l>rt<"pull-left m-t-10"i><"m-t-10 pull-right"p>',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection
