@extends('layouts.admin.default')

@section('title', 'Order')
@section('styles')
    <link href="{{ asset('core-plus-theme/vendors/colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Editar</small></h1>
        </div>
    </header>
    @include('flash::message')
    @include('layouts.form-errors')
	<article class="content">
        <form id="form-validation" action="{{ route('orders.update', $order -> id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="_method" type="hidden" value="PUT">
			<div class="panel">
               <div class="panel-heading clearfix">
                    <div class="pull-right">
                        <a href="{{route('orders.show', $order -> id)}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="note" class="control-label">Nota</label>
                        <textarea name="note" id="note" cols="30" rows="10" maxlength="200" class="form-control" placeholder="Nota"  required="required">{{$order -> note}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="internal_note" class="control-label">Nota interna</label>
                        <textarea name="internal_note" id="internal_note" cols="30" rows="10" maxlength="1000" class="form-control" placeholder="Nota interna" required="required">{{$order -> internal_note}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="branch" class="control-label">Sede<span class="text-danger">*</span></label>
                        <select name="fk_id_branch" id="fk_id_branch" class="form-control" required="required">
                            <option value="">Seleccione una Sede</option>
                                @foreach($branches as $branch)
                                <option value="{{ $branch -> id }}" 
                                    @if ($order -> fk_id_branch == $branch -> id) selected="selected" 
                                    @endif>
                                    {{ $branch -> name }}
                                </option>
                                @endforeach
                        </select>
                    </div>     
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/clockface/js/clockface.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/pickers.js') }}"></script>
    <!-- end page js -->
@endsection
