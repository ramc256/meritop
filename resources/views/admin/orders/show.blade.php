@extends('layouts.admin.default')

@section('title', 'Ordenes')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/invoice.css')}}">
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Ver</small></h1>
        </div>
    </header>
    @include('flash::message')
    @include('layouts.form-errors')
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">
                    @if ($order -> status != 4 && $order -> status != 1)
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-close"></i> Cancelar</button>
                    @endif
                    {{-- @if ($order -> status != 4 && $order -> status != 3)
                        <a href="{{ route('orders.refund-create', $order -> id) }}" type="button" class="btn btn-success"><i class="fa fa-mail-reply"></i> Devolver</a>
                    @endif --}}
                    <a href="{{ route('orders.edit', $order -> id) }}" type="button" class="btn btn-primary">Editar</a>
                    <a href="{{route('orders.index')}}" type="button" class="btn btn-warning">Atrás</a>
                </div>
            </div>
            <section class="panel-body p-l-r-15" id="invoice-stmt">
                <div class="row ">
                    <h4><img src="/images/logo.svg" alt="meritop" class="m-l-40" width="50%"/></h4>
                    <div class="col-md-12">
                        <div class="col-md-4 col-sm-4 col-xs-4 invoice_bg">
                            <ul class="pull-left text-left" style="padding-left: 0 !important;">
                                <li><b>Miembro:</b> {{ $order -> first_name . ' ' . $order -> last_name }}</li>
                                <li><b>DNI:</b> {{ $order -> dni }}</li>
                                <li><b>Carnet:</b> {{ $order -> carnet }}</li>
                                <li><b>Email:</b>   {{$order -> email}}</li>
                            </ul>
                        </div>
                         <div class="col-md-6 col-sm-6 col-xs-6  invoice_bg">
                            <ul class="pull-left text-left" style="padding-left: 0 !important;">
                                <li class="text-capitalize"><b>Club:</b> {{ $order -> club }}</li>
                                <li><b>Datos de envÍo:</b> {{ $order -> name }}<br>{{ $order -> postal_code . ' ' . $order -> address . ' ' . $order -> city  . ', ' . $order -> state}}. <br> {{ $order -> phone}}
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 invoice_bg">
                            <ul class="pull-right text-right">
                                <li><b>Orden #</b>{{ str_pad( $order -> id, 10 , 0, STR_PAD_LEFT) }}</li>
                                <li><b>Estatus</b>
                                    @if ($order -> status == 0) <span class="label label-primary" style="color: #333">Abierto</span>
                                    @elseif($order -> status == 1) <span class="label label-info">Enviado </span>
                                    @elseif($order -> status == 2) <span class="label label-success"> Recibido </span>
                                    {{-- @elseif($order -> status == 3) <span class="label label-warning">Devuelto</span> --}}
                                    @elseif($order -> status == 4) <span class="label label-danger">Cancelado</span>
                                    @else
                                        Estatus incorrecto
                                    @endif
                                </li>
                                <li><b>Fecha</b> {{ date_format( $order -> created_at,'d/m/Y h:i:s A') }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped" id="customtable">
                                <thead>
                                    <tr>
                                        <th>
                                            <strong>SKU #</strong>
                                        </th>
                                        <th>
                                            <strong>Nombre</strong>
                                        </th>
                                         <th>
                                            <strong>Cantidad</strong>
                                        </th>
                                        <th>
                                            <strong>Puntos</strong>
                                        </th>
                                        <th></th>
                                        <th class="text-right">
                                             <strong>Total de puntos</strong>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                     @foreach($orders_products as $element)
                                        <tr>
                                            <td>{{ $element -> sku }}</td>
                                            <td>{{ $element -> name .' '. $element -> feature }}</td>
                                            <td class="text-center ">{{ $element -> quantity }}</td>
                                            <td class="text-center">{{ $element -> points }}</td>
                                            <td></td>
                                            <td class="text-right item_points"> {{ $element -> points * $element -> quantity }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="highrow"></td>
                                        <td class="highrow"></td>
                                        <td class="highrow"></td>
                                        <td class="highrow text-center"></td>
                                        <td class="highrow text-right">
                                            <strong>
                                                Sub total: &nbsp;
                                            </strong>
                                        </td>
                                        <td class="highrow text-right item_points">
                                            <strong>{{  $subtotal }}</strong>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow text-center"></td>
                                        <td class="emptyrow text-right id=">
                                            <strong>
                                                Descuento: &nbsp;
                                            </strong>
                                        </td>
                                        <td class="highrow text-right item_points">
                                            <strong> {{  $discount }}</strong>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="emptyrow">
                                            <i class="livicon" data-name="barcode" data-size="60" data-loop="true"></i>
                                            <b>Cantidad de items: </b> <span id="items_quantity">{{$quantity}} </span>
                                        </td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow text-center"></td>
                                        <td class="emptyrow text-right">
                                            <strong>
                                                Total: &nbsp;
                                            </strong>
                                        </td>
                                        <td class="highrow text-right item_points">
                                            <strong>{{  $total }}</strong>
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="btn-section">
                    <span class="pull-right">
                        @if ($order -> status == 0 )
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-sent" id="btn-modal-sent"><i class="fa fa-refresh"></i> Procesar</button>
                        @elseif($order -> status == 1)
                            {{-- <form action="{{ route('orders.status', $order -> id) }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }} --}}
                                {{-- <input type="hidden" name="status" value="change"> --}}
                              {{--   <input name="_method" type="hidden" value="PUT"> --}}
                                <span class="pull-right">
                                 <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#process"><i class="fa fa-refresh"></i>Procesar</button>
                            {{-- </form> --}}
                        @endif
                        <button type="button" class="btn btn-responsive button-alignment btn-primary" data-toggle="button">
                            <span style="color:#fff;" onclick="javascript:window.print();"> <i class="fa fa-fw fa-print"></i>Imprimir</span>
                        </button>
                    </span>
                </div>
            </section>
        </div>
        <div class="panel">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left m-t-6">Envios</h3>
               {{--  <div class="pull-right">
                    <a href="{{ route('orders.edit', $order -> id) }}" type="button" class="btn btn-primary">Agregar</a>
                </div> --}}
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-asc table-order">
                        <thead>
                            <tr>
                                <th style="width: 15%">Agente</th>
                                <th style="width: 15%">Guia#</th>
                                <th style="width: 10%">Fecha</th>
                                <th style="width: 30%">Nota</th>
                                <th style="width: 30%">Nota interna</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($guides as $element)
                                <tr>
                                    <td>{{ $element -> agent }}</td>
                                    <td>{{ $element -> num_guide }}</td>
                                    <td>{{ date_format($element -> updated_at, 'd/m/Y h:i:s A') }}</td>
                                    <td>{{ $element -> note }}</td>
                                    <td>{{ $element -> internal_note }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('orders.destroy', $order->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cancelar order</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de Cancelar la order <b>{{ $order -> name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="modal-sent" tabindex="-1" role="dialog" aria-labelledby="modal-sent">
        <form action="{{ route('guides.store', $order->id) }}" method="POST">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="panel-title">Guía</h3>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                         <div class="form-group">
                            <label for="agent" class="control-label">Agente <span class="text-danger">*</span></label>
                            <select id="agent" name="fk_id_agent" class="form-control" required="required">
                                <option value="">Seleccione una agente...</option>
                                @foreach($agents as $agent)
                                    <option value="{{ $agent -> id }}">{{ ucwords($agent -> name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="id" class="control-label">Guia #<span class="text-danger">*</span></label>
                            <input name="num_guide" type="text" class="form-control" id="num_guide" maxlength="20" placeholder="id" required="required">
                        </div>
                        <div class="form-group ">
                            <label for="note" class="control-label">Nota</label>
                            <textarea name="note" class="form-control" rows="5" maxlength="200" id="note" placeholder="Nota"></textarea>
                        </div>
                        <div class="form-group ">
                            <label for="internal_note" class="control-label">Nota interna</label>
                            <textarea  name="internal_note" class="form-control" rows="5" maxlength="1000" id="internal_note" placeholder="Nota interna"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="process" tabindex="-1" role="dialog" aria-labelledby="process">
        <form action="{{ route('orders.status', $order -> id) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar recepción</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de que desea confirmar la recepción de la order</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-success">Confirmar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{asset('vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/datatables_custom.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/invoice.js')}}"></script>
    <!-- end page js -->
@endsection
