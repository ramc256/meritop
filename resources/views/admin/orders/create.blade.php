@extends('layouts.admin.default')

@section('title', 'Orden')
@section('styles')
    <!-- Page Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/select2/css/select2-bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/iCheck/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/clockpicker/css/bootstrap-clockpicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/bootstrap-fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/custom_css/wizard.css') }}">
    <!-- End Page Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Nuevo</small></h1>
        </div>
    </header>
    @include('flash::message')
    @include('layouts.form-errors')
	<article class="content">  
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"> Gestión de Orden</h3>
            </div>
            <div class="panel-body row">
                <div class="stepwizard">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step">
                            <a href="#step-1" class="btn btn-primary btn-circle">1</a>
                            <p>Paso 1</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-2" class="btn btn-default btn-circle">2</a>
                            <p>Paso 2</p>
                        </div>
                    </div>
                </div>
                <form id="form-validation" action="{{ route('orders.store') }}" method="POST" >
                    {{ csrf_field() }}
                    <div class="col-md-12 setup-content" id="step-1">
                        <h3> <b>Paso 1:</b> Datos de la orden</h3>
                        <div class="col-md-6">  
                            <div class="form-group">
                                <label for="branch" class="control-label">Miembros <span class="text-danger">*</span></label>
                                <select id="fk_id_member" name="fk_id_member" class="form-control" required="required">
                                    <option value="">Seleccione un miembro...</option>
                                    @foreach($members as $member)
                                        <option value="{{ $member->id }}"><b>DNI:</b> {{$member->dni }}<b> Nombre y Apellido: </b> {{ $member->first_name }} {{ $member->last_name }}.</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"> 
                            <div class="form-group">
                                <label for="branch" class="control-label">Sedes <span class="text-danger">*</span></label>
                                <select id="fk_id_branch" name="fk_id_branch" class="form-control" required="required">
                                    <option value="">Seleccione una sede...</option>
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                               <label for="note" class="control-label">Nota</label>
                               <textarea name="note" id="note" cols="4" rows="4" maxlength="200" class="form-control" placeholder="Nota" ></textarea>
                           </div>
                       </div>
                        <div class="col-md-6">
                            <div class="form-group">
                               <label for="internal_note" class="control-label">Nota interna</label>
                               <textarea name="internal_note" id="internal_note" cols="4" rows="4" maxlength="1000" class="form-control" placeholder="Nota interna"></textarea>
                           </div>
                       </div>
                       <button class="btn btn-primary nextBtn pull-right" type="button">Siguiente</button>
                    </div>
                    <div class="col-md-12 setup-content" id="step-2">
                        <h3> <b>Paso 2:</b> Lista de productos</h3>
                        <div class="table-responsive">
                          <table class="table table-striped table-hover datatable-asc " >
                              <thead>
                                  <tr>
                                      <th>
                                          <button type="button" class="btn btn-xs btn-meritop btn-add"></button>
                                      </th>
                                      <th>SKU</th>
                                      <th>Código de barras</th>
                                      <th>Cantidad</th>
                                      <th>Descripción</th>
                                      <th>puntos</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach($products as $element)
                                    <tr>
                                        <td>
                                            <span class="camp-add hidden" style="display: inline-block;"> +
                                                <input name="quantities[{{ $element -> id }}]" type="number"  style="width: 30px;" value="0">
                                                <input name="quantity_current[{{ $element -> id }}]" type="hidden"  style="width: 30px;" value="{{ $element -> quantity_current }}">

                                            </span>
                                        </td>
                                        <td>{{ $element -> sku }}</td>
                                        <td class="text-capitalize"><label style="display: block; width: 100%; height: 100%; cursor: pointer;" for="{{ $element -> barcode . $element -> id }}">{{ $element -> barcode }}</label></td>
                                        <td class="text-capitalize"><label style="display: block; width: 100%; height: 100%; cursor: pointer;" for="{{ $element -> quantity_current . $element -> id }}">{{ $element -> quantity_current }}</label></td>
                                        <td class="text-capitalize"><label style="display: block; width: 100%; height: 100%; cursor: pointer;" for="{{ $element -> product .$element -> color . $element -> id }}">{{ $element -> product . $element -> color }}</label></td>
                                        <td class="text-capitalize"><label style="display: block; width: 100%; height: 100%; cursor: pointer;" for="{{ $element -> points . $element -> id }}">{{ $element -> points }}</label></td>
                                    </tr>
                                @endforeach
                              </tbody>
                          </table>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary prevBtn pull-left" type="button"> Anterior </button>
                            <button class="btn btn-success pull-right" type="submit">
                                Finalizar!
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/select2/js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/form_wizards.js') }}" ></script>
    <!-- page js -->
    <script>
        $(document).ready(function(){
            $(".btn-add").on('click', function(){
                if($( '.camp-add').hasClass("hidden")) {
                    $('.camp-add').removeClass("hidden").addClass("visible");
                } else {
                    $('.camp-add').removeClass("visible").addClass("hidden");
                }
            });
        });
    </script>
    <!-- end page js -->
@endsection
