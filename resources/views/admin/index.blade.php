@extends('layouts.admin.default')

@section('title', 'Dashboard')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
	<header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Bienvenido a Meritop</small></h1>
        </div>
    </header>
    @include('flash::message')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row tiles-row">
                    @if(Auth::user() -> kind == 0)
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tile-bottom">
                            <div class="widget" data-count=".num" data-from="0"
                                 data-to="512" data-duration="3">
                                <div class="canvas-interactive-wrapper2 grad-color">
                                    <canvas id="canvas-interactive2"></canvas>
                                    <div class="cta-wrapper2">
                                        <div class="item">
                                            <div class="widget-icon pull-left icon-color animation-fadeIn">
                                                <i class="fa fa-fw fa-users fa-size"></i>
                                            </div>
                                        </div>
                                        <div class="widget-count panel-white">
                                            <div class="item-label text-center">
                                                <div id="count-box2" class="count-box">{{ $members }}</div>
                                                <span class="title">Miembros</span>
                                            </div>
                                            <div class="text-center">
                                                <a href="{{ route('members.index') }}" style="color: white"><i class="fa fa-level-up" aria-hidden="true"></i> Ver miembros</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      {{--   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 tile-bottom">
                            <div class="canvas-interactive-wrapper1 grad-color">
                                <canvas id="canvas-interactive1"></canvas>
                                <div class="cta-wrapper1">
                                    <div class="widget" data-count=".num" data-from="0"
                                         data-to="99.9" data-suffix="%" data-duration="2">
                                        <div class="item">
                                            <div class="widget-icon pull-left icon-color animation-fadeIn">
                                                <i class="fa fa-fw fa-shopping-cart fa-size"></i>
                                            </div>
                                        </div>
                                        <div class="widget-count panel-white">
                                            <div class="item-label text-center">
                                                <div id="count-box" class="count-box">{{ $orders }}</div>
                                                <span class="title">Ordenes</span>
                                            </div>
                                            <div class="text-center">
                                                <a href="{{ route('orders.index') }}" style="color: white"><i class="fa fa-level-up" aria-hidden="true"></i> Ver ordenes</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    @endif
                    @if(Auth::user() -> kind == 1)
                         <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 tile-bottom">
                        <div class="widget" data-count=".num" data-from="0"
                             data-to="512" data-duration="3">
                            <div class="canvas-interactive-wrapper2 grad-color">
                                <canvas id="canvas-interactive2"></canvas>
                                <div class="cta-wrapper2">
                                    <div class="item">
                                        <div class="widget-icon pull-left icon-color animation-fadeIn">
                                            <i class="fa fa-fw fa-users fa-size"></i>
                                        </div>
                                    </div>
                                    <div class="widget-count panel-white">
                                        <div class="item-label text-center">
                                            <div id="count-box2" class="count-box">{{ $members }}</div>
                                            <span class="title">Miembros</span>
                                        </div>
                                        <div class="text-center">
                                            <a href="{{ route('members.index') }}" style="color: white"><i class="fa fa-level-up" aria-hidden="true"></i> Ver miembros</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 tile-bottom">
                        <div class="canvas-interactive-wrapper1 grad-color">
                            <canvas id="canvas-interactive1"></canvas>
                            <div class="cta-wrapper1">
                                <div class="widget" data-count=".num" data-from="0"
                                     data-to="99.9" data-suffix="%" data-duration="2">
                                    <div class="item">
                                        <div class="widget-icon pull-left icon-color animation-fadeIn">
                                            <i class="fa fa-fw fa-shopping-cart fa-size"></i>
                                        </div>
                                    </div>
                                    <div class="widget-count panel-white">
                                        <div class="item-label text-center">
                                            <div id="count-box" class="count-box">{{ $orders }}</div>
                                            <span class="title">Ordenes abiertas</span>
                                        </div>
                                        <div class="text-center">
                                            <a href="{{ route('orders.index') }}" style="color: white"><i class="fa fa-level-up" aria-hidden="true"></i> Ver ordenes abiertas</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 tile-bottom">
                            <div class="widget" data-suffix="k" data-count=".num"
                                 data-from="0" data-to="310" data-duration="4" data-easing="false">
                                <div class="canvas-interactive-wrapper3 grad-color">
                                    <canvas id="canvas-interactive3"></canvas>
                                    <div class="cta-wrapper3">
                                        <div class="item">
                                            <div class="widget-icon pull-left icon-color animation-fadeIn">
                                                <i class="fa fa-fw fa-usd fa-size"></i>
                                            </div>
                                        </div>
                                        <div class="widget-count panel-white">
                                            <div class="item-label text-center">
                                                <div id="count-box3" class="count-box">{{ $parents_products }}</div>
                                                <span class="title">Productos</span>
                                            </div>
                                            <div class="text-center">
                                                <a href="{{ route('products.index') }}" style="color: white"><i class="fa fa-level-up" aria-hidden="true"></i> Ver productos</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <h4>Bienvenido {{ Auth::user()-> first_name . ' ' . Auth::user()-> last_name }}</h1>
            </div>
        </div>
    </section>
@endsection
