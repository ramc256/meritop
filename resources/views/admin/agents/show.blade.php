@extends('layouts.admin.default')

@section('title', 'Ver Agentes')
@section('content')
    <!-- Header Page -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Vista de características para las agentes</p>
        </div>
    </header>
    <!-- End Header Page -->
    <!-- Content Page -->
    <article class="content">
        <div class="col-sm-12 col-md-7">
            <div class="panel">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6"> Datos importantes</h3>
                </div>
                <div class="panel-body">
                     <ul class="panel-list">
                        <li>
                            <p><b>Nombre</b></p>
                            <p class="text-capitalize">{{ $agent -> name }}</p>
                        </li>               
                    </ul>
                </div>
                <div class="panel-footer text-center">               
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{route('agents.index')}}" type="button" class="btn btn-warning">Regresar</a>  
                    <a href="{{route('agents.edit', $agent->id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5">
            <div class="panel">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6"> Datos complementarios</h3>
                </div>
                <div class="panel-body">
                     <ul class="panel-list">
                        <li>
                            <p><b>Fecha de creación</b></p>
                            <p class="text-capitalize">{{ $agent -> created_at }}</p>
                        </li>   
                        <li>
                            <p><b>Fecha de modificación</b></p>
                            <p class="text-capitalize">{{ $agent -> updated_at }}</p>
                        </li>             
                    </ul>
                </div>
            </div>
    </article>
    <!-- End Content Page -->
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('agents.destroy', $agent->id) }}" method="POST"> 
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Agente</h4>
                    </div>
                    <div class="modal-body">     
                        <p>¿Esta seguro de eliminar el Agente <b>{{ $agent -> name}}</b>?</p>               
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>            
        </form>
    </div>
    <!-- end Modal -->
@stop
@section('scripts')
    <!-- Page js --> 
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->
@endsection
