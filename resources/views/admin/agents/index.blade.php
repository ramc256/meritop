@extends('layouts.admin.default')

@section('title', 'Agentes')
@section('styles')
    <!-- Page style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/dashboard.css') }}" />
    <!-- End Page style -->
@stop
@section('content')
    <!-- Header Page -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Modulo para la gestion de @yield('title')</p>
        </div>
    </header>
    <!-- End Header Page -->
    <!-- Content Page -->
	<article class="content">
		<div class="col-md-12">
			<div class="panel filterable">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6">
                         Lista de agentes
                    </h3>
                    <div class="pull-right">
                        <a href="{{ route('agents.create') }}" class="btn btn-meritop btn-sm" id="addButton">Nuevo</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover datatable-asc">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Fecha de creación</th>
                                <th>Fecha de modificación</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($agents as $element)
                            <tr>
                                <td><a href="{{ route('agents.show', $element -> id) }}">{{ $element -> name }}</a></td>
                                <td>{{ $element -> created_at }}</td>
                                <td>{{ $element -> updated_at }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</article>
    <!-- End Content Page -->
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js - ->
@endsection
