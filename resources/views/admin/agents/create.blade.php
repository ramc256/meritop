@extends('layouts.admin.default')

@section('title', 'Nuevo agente')
@section('content')
    <!-- Header Page -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Seccion para crear agentes</p>
        </div>
    </header>
    <!-- End Header Page -->
    <!-- Content Page -->
	<article class="content">
        <form action="{{ route('agents.store') }}" method="POST" >
        {{ csrf_field() }}
    		<div class="col-sm-12 col-md-7">
    			<div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title"> Datos importantes</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required="required">
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <a href="{{route('agents.index')}}" type="button" class="btn btn-warning">Regresar</a>
                    <button type="reset" class="btn btn-default bttn_reset">Limpiar</button>
                    <button type="submit" class="btn btn-primary">Aceptar</button>
                </div>
            </div>
        </form>
	</article>
    <!-- End Content Page -->
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- End pPge js -->
@endsection

