@extends('layouts.admin.default')

@section('title', ' Banner')
@section('styles')
     <!--page level css -->
    <!--end of page level css-->
    <link href="{{ asset('core-plus-theme/vendors/colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
     <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Nuevo</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('banners.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }} 
            @include('layouts.form-errors')
            <div class="panel">
               <div class="panel-heading clearfix">
                     <div class="pull-right">
                        <a href="{{route('banners.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title" class=" control-label">Título <span class="text-danger">*</span></label>
                            <input name="title" id="title" placeholder="Titulo" type="text" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="subtitle" class=" control-label">Subtítulo <span class="text-danger">*</span></label>
                            <input id="subtitle" name="subtitle" placeholder="Subtítulo" maxlength="250" type="text" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="image" class=" control-label">Imagen</label>
                            <input name="image" type="file" class="form-control" id="image" placeholder="Url de Imagen" multiple>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script src="{{ asset('core-plus-theme/vendors/colorpicker/js/bootstrap-colorpicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('core-plus-theme/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('core-plus-theme/js/custom_js/pickers.js') }}" type="text/javascript"></script>
    <!-- end page js -->
    <script>
    $('#warranty').change(function(){
      if($(this).prop("checked")) {
        $('#data_warranty').removeClass("hidden").addClass("visible");
      } else {
        $('#data_warranty').removeClass("visible").addClass("hidden");
      }
    });
    </script>

@endsection
