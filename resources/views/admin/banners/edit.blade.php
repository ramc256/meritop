@extends('layouts.admin.default')

@section('title', 'Banner')
@section('styles')
     <!--page level css -->
@stop
@section('content')
    <!-- Content Header (Page header) -->
     <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Editar</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('banners.update', $banner-> id) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts.form-errors')
            <input type="hidden" name="_method" value="PUT">
            <div class="panel">
               <div class="panel-heading clearfix">
                    <div class="pull-right text-center">
                        <a href="{{route('banners.show', $banner -> id )}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title" class="control-label">Título<span class="text-danger">*</span></label>
                                <input name="title" id="title" placeholder="Nombre" type="text" class="form-control" required="required" value="{{ $banner -> title }}">
                            </div>
                            <div class="form-group">
                                <label for="subtitle" class="control-label">Subtítulo<span class="text-danger">*</span></label>
                                <input id="subtitle" name="subtitle" placeholder="Subtítulo" maxlength="250" type="text" class="form-control" required="required" value="{{ $banner -> subtitle }}">
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <div class="text-center thumbnail relative mbl" >
                                    @if (empty($image -> image))
                                        <img class="image-thumbnail max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $banner -> title }}">
                                    @else
                                        <img class="image-thumbnail max-size" src="{{ $_ENV['STORAGE_PATH'].$image -> image }}" alt="{{ $banner -> title }}">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="control-label">Imagen</label>
                                <input id="image" name="image" type="file" class="form-control">
                                @if (!empty($image))
                                    <input name="id_image" type="hidden" class="form-control" readonly="readonly" value="{{ $image -> id}}">
                                @endif
                            </div>
                        </div>
                    </div>       
                </div>
            </div>
        </form>
	</article>
@endsection
