@extends('layouts.admin.default')

@section('title', 'Banners')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Gestiones</small></h1>
        </div>
    </header>
    @include('flash::message')
	<article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="pull-right">
                    <a href="{{ route('banners.create')}}" class="btn btn-success btn-md" id="addButton">Nuevo</a>
                </div>
            </div> 
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-asc">
                        <thead>
                            <tr>
                                <th style=" width: 20%">Fecha de creación</th>
                                <th style=" width: 35%">Título</th>
                                <th style=" width: 45%">Subtítulo</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($banners as $datos)
                            <tr>
                                <td>{{ date_format($datos -> created_at, 'd/m/Y h:i:s A') }}</td>
                                <td class="text-capitalize"><a href="{{ route('banners.show',$datos -> id) }}">{{ $datos -> title }}</a></td>
                                <td> {{ $datos -> subtitle }} </td>
                                
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js -->
@endsection
