@extends('layouts.admin.default')

@section('title', 'Banner')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel filterable">
            @include('flash::message')
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{route('banners.index')}}" type="button" class="btn btn-warning">Atrás</a>    
                    <a href="{{route('banners.edit', $banner -> id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div> 
            <div class="panel-body">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <ul class="panel-list">
                        <li class="row"> 
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <p><b>Título</b></p>
                                <p>{{ $banner -> title }}</p>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <p><b>Subtítulo</b></p>
                                <p>{{ $banner -> subtitle }}</p>
                            </div>  
                        </li>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group"> 
                        <div  class="thumbnail relative text-center  mbl">
                            @if (empty($image-> image))
                                <img class="image-thumbnail max-size" src="{{ Storage::disk('images')->url('img-default.png') }}" alt="{{ $banner -> title}}">
                            @else
                                <img class="image-thumbnail max-size" src="{{ $_ENV['STORAGE_PATH'].$image -> image }}" alt="{{ $banner -> title }}">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <!-- MODAL PARA ELIMINAR REGISTRO -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('banners.destroy', $banner -> id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="modal-delete" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Banner</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el banner <b>{{ $banner -> title}}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- END Modal  -->
    <!-- END MODAL PARA ELIMINAR REGISTRO -->
@endsection