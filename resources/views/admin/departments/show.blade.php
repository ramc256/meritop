@extends('layouts.admin.default')

@section('title', 'Departamento')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{ route('departments.index') }}" type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{ route('departments.edit', $department -> id ) }}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                <ul class="panel-list">
                    <li class="row">
                        <div class="col-sm-6 col-md-12 col-lg-6">
                            <p><b>Código</b></p>
                            <p>{{ $department -> code }} - {{ $department -> id }}</p>
                        </div>
                        <div class="col-sm-6 col-md-12 col-lg-6">
                            <p><b>Nombre</b></p>
                            <p class="text-capitalize">{{ $department -> name }}</p>
                        </div>
                    </li>
                    <li class="row">
                         <div class="col-sm-6 col-md-12 col-lg-6">
                            <p><b>Club</b></p>
                            <p class="text-capitalize">{{ $department -> club }}</p>
                        </div>
                    </li>
                    <li class="row">
                         <div class="col-sm-6 col-md-12 col-lg-6">
                            <p><b>Creación</b></p>
                            <p>{{ $department -> created_at }}</p>
                        </div>   
                        <div class="col-sm-6 col-md-12 col-lg-6">
                            <p><b>Moficación</b></p>
                            <p>{{ $department -> updated_at }}</p>
                        </div>
                    </li>                                  
                </ul>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('departments.destroy', $department->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar departamento</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el departamento <b>{{ $department -> name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js -->
@endsection
