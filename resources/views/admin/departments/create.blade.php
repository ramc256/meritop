@extends('layouts.admin.default')

@section('title', 'Departamento')
@section('styles')
    <link href="{{ asset('core-plus-theme/vendors/colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Nuevo</small></h1>
        </div>
    </header>
	<article class="content">
        <form  id="form-validation" action="{{ route('departments.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('layouts.form-errors')
			<div class="panel">
               <div class="panel-heading clearfix">
                    <div class="pull-right">
                        <a href="{{route('departments.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                        <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required="required">
                    </div>                
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/clockface/js/clockface.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/pickers.js') }}"></script>
    <!-- end page js -->
@endsection
