@extends('layouts.admin.default')

@section('title', 'Productos')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/advbuttons.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Items</small></h1>
        </div>
    </header>
    @include('flash::message')
	<article class="content">
			<div class="panel filterable">
                <div class="panel-heading clearfix">
                    @if (Auth::user() -> kind == 1)
                        <div class="pull-right">
                            <a class="btn btn-success" href="{{ route('parents-products.create') }}">Nuevo</a>
                        </div>
                    @endif
                </div>
                <div class="panel-body">
                    <table id="table4" class="table table-striped table-hover datatable-asc"  data-toolbar="#toolbar" data-search="true" data-show-refresh="false"
                               data-show-toggle="true" data-show-columns="true" data-show-export="true"
                               data-detail-view="true" data-detail-formatter="detailFormatter"
                               data-minimum-count-columns="2" data-show-pagination-switch="true"
                               data-pagination="true" data-id-field="id" data-page-list="[10, 20,40,ALL]"
                               data-show-footer="false" data-height="503">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Modelo</th>
                                <th>Mercado</th>
                                <th>Marca</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($parents_products as $datos)
                                <tr>
                                    <td><a href="{{route('parents-products.show', $datos -> id)}}" class="text-capitalize">{{ $datos -> name }}</a></td>
                                    <td>{{ $datos -> model }}</td>
                                    <td class="text-capitalize">@if ( $datos -> market == 0) Ambos @elseif( $datos -> market == 1) Masculino @else Femenino @endif</td>
                                    <td class="text-capitalize">{{ $datos -> brand }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js -->
@endsection
