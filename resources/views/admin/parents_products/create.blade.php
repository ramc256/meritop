@extends('layouts.admin.default')

@section('title', 'Producto')
@section('styles')
     <!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{ asset('core-plus-theme/vendors/summernote/summernote.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/trumbowyg/css/trumbowyg.min.css') }}" >
    <link rel="stylesheet" type="text/css" href="{{ asset ('core-plus-theme/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/form_editors.css') }}">
    <!--end of page level css-->
   {{--  <link href="{{ asset('core-plus-theme/vendors/colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/> --}}
   <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
     <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Nuevo</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('parents-products.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts.form-errors')
            <div class="panel">
               <div class="panel-heading clearfix">
                    <div class="pull-left text-center">
                        <h3>Datos principales</h3>
                    </div>
                     <div class="pull-right">
                        <a href="{{route('parents-products.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class=" control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" id="name" placeholder="Nombre" type="text" class="form-control" required="required">
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="feature" class=" control-label">Característica <span class="text-danger">*</span></label>
                            <input name="feature" id="feature" placeholder="Característica" type="text" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="model" class=" control-label">Modelo <span class="text-danger">*</span></label>
                            <input id="model" name="model" placeholder="Modelo" maxlength="100" type="text" class="form-control" required="required">
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="brand" class=" control-label">Marcas <span class="text-danger">*</span></label>
                            @if (count($brands)>0)
                                <select name="fk_id_brand" id="fk_id_brand" class="form-control" required="required">
                                    <option value="">Seleccione un marcas...</option>
                                    @foreach ($brands as $brand)
                                        <option value="{{ $brand -> id }}" class="text-capitalize">{{ $brand -> name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <p class="text-danger">No hay marcas registradas</p>
                                <p>Para crear una marca haga click <a href="{{ url('admin/brands/create') }}">aquí</a></p>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sku" class=" control-label">SKU <span class="text-danger">*</span></label>
                            <input name="sku" id="sku" placeholder="SKU" maxlength="15" type="text" class="form-control"  required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="barcode" class=" control-label">Código de Barras</label>
                            <input id="barcode" name="barcode" placeholder="Codigo de barras" maxlength="15" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="poins" class=" control-label">Puntos <span class="text-danger">*</span></label>
                            <input id="points" name="points" placeholder="Puntos" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="excerpt" class=" control-label">Extracto <span class="text-danger">*</span></label>
                            <input id="excerpt" name="excerpt" placeholder="Extracto" maxlength="250" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="summernote" class="control-label">Descripción<span class="text-danger">*</span></label>
                            <div class="panel-body">
                                <div class="bootstrap-admin-panel-content summer_noted">
                                    <textarea id="summernote" name="description" placeholder="Descripción" maxlength="65500" class="form-control" required="required" rows="15"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <h4 class="text-capitalize">Imagenes</h4>
                        <hr>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="parent_image" class=" control-label">Imagen principal</label>
                            <input name="parent_image" type="file" class="form-control" id="parent_image" placeholder="Url de Imagen">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="product_image" class=" control-label">Imagen de la variante</label>
                            <input name="product_image[]" type="file" class="form-control" id="product_image" placeholder="Url de Imagen" multiple>
                        </div>
                    </div>
                     <div class="col-md-12">
                            <h4 class="text-capitalize">Caraterísticas</h4>
                            <hr>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="width" class=" control-label">Ancho <span class="text-danger">*</span></label>
                            <input id="width" name="width" placeholder="Ancho" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="heigth" class=" control-label">Alto <span class="text-danger">*</span></label>
                            <input id="heigth" name="heigth" placeholder="Alto" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="depth" class=" control-label">Profundidad <span class="text-danger">*</span></label>
                            <input id="depth" name="depth" placeholder="Profundidad" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="volume" class=" control-label">Volumen <span class="text-danger">*</span></label>
                            <input id="volume" name="volume" placeholder="Volumen" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <label for="size" class=" control-label">Tamaño <span class="text-danger">*</span></label>
                            <input id="size" name="size" placeholder="Tamaño" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="weight" class=" control-label">Peso <span class="text-danger">*</span></label>
                            <input id="weight" name="weight" placeholder="Peso" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <label for="measure" class=" control-label">Medida <span class="text-danger">*</span></label>
                            <input id="measure" name="measure" placeholder="Medida" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="color" class=" control-label">Color <span class="text-danger">*</span></label>
                            <!-- Color Picker -->
                            <input id="color" name="color" type="text" class="form-control{{--  my-colorpicker1 --}}" {{-- id="cp1" --}} required="required" {{-- readonly="readonly" --}} placeholder="#FFFFFF" {{-- value="#FFF" --}}>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                            <label for="brand" class=" control-label">Mercado<span class="text-danger">*</span></label>
                            @if (count($markets)>0)
                                <select name="fk_id_market" id="fk_id_market" class="form-control" required="required">
                                    <option value="">Seleccione un mercado...</option>
                                    @foreach ($markets as $market)
                                        <option value="{{ $market -> id }}" class="text-capitalize">{{ $market -> name }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="warranty" class=" control-label">Vender fuera de Stock</label>
                            <input id="sell_out_stock" name="sell_out_stock" type="checkbox">
                        </div>
                    </div>
                    <hr>
                    <div class="row panel-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                 <h4>Categorías</h4>
                            <hr>
                                @php
                                    $aux = '';
                                @endphp
                                @foreach ($subcategories as $category)
                                    @if ($aux != $category -> category)
                                        <div class="col-md-12" style="display:block; margin-top:10px; border-bottom:1px solid #e1e1e1;">
                                        <h5 class="text-capitalize" style="display:block;">{{-- <b> --}}{{ $category -> category }}{{-- </b> --}}</h5>
                                    </div>
                                        @php
                                            $aux = $category -> category;
                                        @endphp
                                    @endif
                                    <div class="input-group float-box-l">
                                        <input id="{{ $category -> name }}" name="fk_id_subcategory[]" type="checkbox" value="{{ $category -> id }}">
                                        <label class="text-capitalize left " for="{{ $category -> name }}"> {{ $category -> name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <hr>
                     <div class="col-md-12">
                        <h4>Clubs</h4>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Clubs <span class="text-danger">*</span></label>
                            @if (count($clubs)>0)
                                @foreach ($clubs as $club)
                                        <input id="{{ $club -> name }}" name="fk_id_club[]" type="checkbox" value="{{ $club -> id }}">
                                        <label class="text-capitalize label left label-primary" for="{{ $club -> name }}"> {{ $club -> name }}</label>
                                @endforeach
                            @else
                                <p class="text-danger">No hay clubs registrados</p>
                                <p>Para registrar un club haga click <a href="{{ url('admin/clubs/create') }}">aquí</a></p>
                            @endif
                        </div>
                        <hr>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="brand" class=" control-label">País <span class="text-danger">*</span></label>
                            <select name="fk_id_country" id="fk_id_country" class="form-control" required="required">
                                <option value="">Seleccione un país...</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country -> id }}">{{ $country -> name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                     <div class="col-md-12">
                            <h4>Garantía y condiciones de envío</h4>
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="warranty" class=" control-label">Garantía</label>
                                <input id="warranty" name="warranty" type="checkbox">
                                <label for="warranty" class="text-primary"> El producto posee garantía</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div {{-- id="data_warranty" class="hidden" --}}>
                                <div class="form-group">
                                    <label for="warranty_conditons" class=" control-label">Condiciones de Garantía</label>
                                    <textarea id="warranty_conditions" name="warranty_conditions" placeholder="Condiciones de Garantia" maxlength="250" class="form-control" rows="10"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="warranty_term" class=" control-label">Meses de Garantía</label>
                                    <input id="warranty_day" name="warranty_day" placeholder="Tiempo de Garantia" type="number" maxlength="8" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="delivery-conditions" class=" control-label">Condiciones de Envío <span class="text-danger">*</span></label>
                                <textarea id="delivery-conditions" name="delivery_conditions" placeholder="Condiciones de Envío" maxlength="250" class="form-control" required="required" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4>Estatus</h4>
                            <hr>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                            <label for="market" class=" control-label">Estatus <span class="text-danger">*</span></label>
                            <select name="status" id="status" class="form-control" required="required">
                                <option value="">Seleccione un estatus</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    {{--  <script>
    $('#warranty').change(function(){
      if($(this).prop("checked")) {
        $('#data_warranty').removeClass("hidden").addClass("visible");
      } else {
        $('#data_warranty').removeClass("visible").addClass("hidden");
      }
    });
    </script> --}}
     <!-- begining of page level js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/trumbowyg/js/trumbowyg.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/summernote/summernote.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/form_editors.js') }}" ></script>
    <!-- end of page level js -->
   {{--   <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/colorpicker/js/bootstrap-colorpicker.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/clockface/js/clockface.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/pickers.js') }}" ></script>
    <!-- end page js --> --}}
     <!-- validationboostrap -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end validationboostrap -->
@endsection
