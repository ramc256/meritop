@extends('layouts.admin.default')

@section('title', 'Producto')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Ver</small></h1>
        </div>
    </header>
    @include('flash::message')
    <article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="pull-left text-center">
                    <h3>Datos principales</h3>
                </div>
                <div class="pull-right text-center">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" >Eliminar</button>
                    <a href="{{route('parents-products.index')}}" type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{route('parents-products.edit', $parent_product -> id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body row">
                <div class="col-md-6">
                    <ul class="panel-list">
                        <li class="row">
                            <div class="col-xs-12 col-sm-12">
                                <p><b>Nombre</b></p>
                                <p>{{ $parent_product -> name }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4">
                                <p><b>Modelo</b></p>
                                <p>{{ $parent_product -> model }}</p>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <p><b>Marca</b></p>
                                <p class="text-capitalize">{{ $parent_product -> brand }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-12">
                                <p><b>Descripción</b></p>
                                <p>{!! $parent_product -> description !!}</p>
                            </div>
                        </li>   
                        <h4>Características</h4>
                        <hr>
                        <li class="row">
                            <div class="col-xs-12 col-sm-12">
                                <p><b>Mercado</b></p>
                                <p>{{ $parent_product -> market}}</p>
                            </div>
                            <hr>
                        </li>   
                        <h4>Sub-categorías</h4>
                        <hr>   
                        <li class="row">
                            <div class="col-xs-12">
                                <p><b>Sub Categorías</b></p>
                                @foreach ($parents_products_subcategories as $subcategory)
                                    <p class="text-capitalize label label-primary">{{ $subcategory -> name }}</p>
                                @endforeach
                            </div>
                        </li>
                         <h4>Clubs</h4>
                        <hr>
                        <li class="row">
                            <div class="col-xs-12">
                                <p><b>Clubs</b></p>
                                @foreach ($clubs_parents_products as $club)
                                        <p class="text-capitalize label label-primary">{{ $club -> name }}</p>
                                @endforeach
                            </div>
                        </li>
                    </ul>
                    <ul class="panel-list">
                        <h4>Garantía y condiciones de envío</h4>
                        <hr>
                        <li class="row">
                            @if ($parent_product -> warranty == 1)
                                <div class="col-xs-12">
                                    <p><b>Garantia</b></p>
                                    <span class="label label-success">Posee garantía</span>
                                </div>
                        </li>
                        <li class="row">
                                <div class="col-sm-8">
                                    <p><b>Condiciones de garantía</b></p>
                                    <p>{{ $parent_product -> warranty_conditions }}</p>
                                </div>
                                <div class="col-sm-4">
                                    <p><b>Tiempo de garantía</b></p>
                                    <p>{{ $parent_product -> warranty_day }} días</p>
                                </div>
                            @endif
                        </li>
                        <li class="row">
                            <div class="col-xs-12">
                                <p><b>Condiciones de envío</b></p>
                                <p>{{ $parent_product -> delivery_conditions }}</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="from-group">
                        <div class="text-center thumbnail mbl">
                            @if (empty($parent_image -> image))
                                <img  class="image-thumbnail max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $parent_product-> name }}">
                            @else
                                <img  class="image-thumbnail max-size" src="{{ $_ENV['STORAGE_PATH'].$parent_image -> image }}" alt="{{ $parent_product -> name }}">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (count($products)>0)
            <div class="panel filterable">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6"> Variantes</h3>
                    <a href="{{ url('admin/products/create/'. $parent_product -> id) }}" class="btn btn-success pull-right">Nuevo</a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Característica</th>
                                    <th>SKU</th>
                                    <th>Código de barras</th>
                                    <th>Puntos</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $datos)
                                    <tr>
                                        <td><a href="{{route('products.show', $datos -> id )}}">{{ $datos -> feature}}</a></td>
                                        <td>{{ $datos -> sku }}</td>
                                        <td>{{$datos -> barcode}}</td>
                                        <td>{{ number_format($datos -> points,0,',','.') }}</td>
                                        <td>@if ($datos -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span> @endif</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </article>
@stop
@section('modals')
    <!-- MODAL PARA ELIMINAR REGISTRO -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('parents-products.destroy', $parent_product -> id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="modal-delete" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Producto</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el producto <b>{{ $parent_product -> name}}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- END MODAL PARA ELIMINAR REGISTRO -->
@endsection
