@extends('layouts.admin.default')

@section('title', 'Productos')
@section('styles')
     <!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{ asset('core-plus-theme/vendors/summernote/summernote.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/trumbowyg/css/trumbowyg.min.css') }}" >
    <link rel="stylesheet" type="text/css" href="{{ asset ('core-plus-theme/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/form_editors.css') }}">
    <!--end of page level css-->
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
     <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Editar</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('parents-products.update', $parent_product-> id) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts.form-errors')
            <input type="hidden" name="_method" value="PUT">
            <div class="panel">
               <div class="panel-heading clearfix">
                <div class="pull-left text-center">
                    <h3>Datos principales</h3>
                </div>
                    <div class="pull-right text-center">
                        <a href="{{route('parents-products.show', $parent_product -> id )}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                                <input name="name" id="name" placeholder="Nombre" type="text" class="form-control" required="required" value="{{ $parent_product -> name }}">
                            </div>
                            <div class="form-group">
                                <label for="model" class="control-label">Modelo<span class="text-danger">*</span></label>
                                <input id="model" name="model" placeholder="Modelo" maxlength="100" type="text" class="form-control" required="required" value="{{ $parent_product -> model }}">
                            </div>
                            <div class="form-group">
                                <label for="brand" class="control-label">Marcas <span class="text-danger">*</span></label>
                                @if (count($brands)>0)
                                    <select name="fk_id_brand" id="fk_id_brand" class="form-control" required="required">
                                        <option value="">Seleccione un marcas...</option>
                                        @foreach ($brands as $brand)
                                            <option class="text-capitalize" value="{{ $brand -> id }}" @if ($brand -> id ==  $parent_product -> id_brand ) selected="selected" @endif>{{ $brand -> name }}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <p class="text-danger">No posee marca</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <div class="text-center thumbnail relative mbl" >
                                    @if (empty($parent_image -> image))
                                        <img class="image-thumbnail max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $parent_product -> name }}">
                                    @else
                                        <img class="image-thumbnail max-size" src="{{ $_ENV['STORAGE_PATH'].$parent_image -> image }}" alt="{{ $parent_product -> name }}">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="control-label">Imagen principal </label>
                                <input id="image" name="image" type="file" class="form-control">
                                @if (!empty($parent_image))
                                    <input name="id_image" type="hidden" class="form-control" readonly="readonly" value="{{ $parent_image -> id }}">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="summernote" class="control-label">Descripción<span class="text-danger">*</span></label>
                        <div class="panel-body ">
                            <div class="bootstrap-admin-panel-content summer_noted">
                                <textarea id="summernote" name="description" placeholder="Descripción" maxlength="65500" class="form-control" required="required" rows="15">{{ $parent_product -> description }}</textarea>
                            </div>
                        </div>
                    </div>
                        <h4 class="text-capitalize">Características</h4>
                        <hr>
                    <div class="col-md-6 row">
                        <div class="form-group">
                           <label for="brand" class=" control-label">Mercado<span class="text-danger">*</span></label>
                           @if (count($markets)>0)
                               <select name="fk_id_market" id="fk_id_market" class="form-control" required="required">
                                   <option value="">Seleccione un mercado...</option>
                                   @foreach ($markets as $market)
                                       <option class="text-capitalize" value="{{ $market -> id }}" @if ($market -> id ==  $parent_product -> fk_id_market ) selected="selected" @endif>{{ $market -> name }}</option>
                                   @endforeach
                               </select>
                           @endif
                       </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <h4 class="text-capitalize">Categorías</h4>
                        <hr>
                                @php
                                    $aux = '';
                                @endphp
                                @foreach ($subcategories as $category)
                                    @if ($aux != $category -> category)
                                        <div class="col-md-12 " style="display:block; margin-top:10px; border-bottom:1px solid #e1e1e1;">
                                            <h5 class="text-capitalize" style="display:block;">{{ $category -> category }}</h5>
                                        </div>
                                        @php
                                            $aux = $category -> category;
                                        @endphp
                                    @endif
                                    <div class="input-group float-box-l">
                                        {{-- {{  dd($category -> id, $parent_products_subcategories) }} --}}
                                        <input id="{{ $category -> name }}" name="fk_id_subcategory[]" type="checkbox" value="{{ $category -> id }}"
                                        @if (in_array($category -> id, $parents_products_subcategories))
                                            checked="checked"
                                        @endif>
                                        <label class="text-capitalize left btn-pointer" for="{{ $category -> name }}"> {{ $category -> name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    <hr>
                    {{-- <div class="form-group">
                        <label for="" class="control-label">Clubs <span class="text-danger">*</span></label>
                        @if (count($clubs)>0)
                            @foreach ($clubs as $club)
                                    <input id="{{ $club -> name }}" name="fk_id_club[]" type="checkbox" @if ($clubs_products -> fk_id_club == $club -> id) checked="checked" @endif value="{{ $club -> id }}">
                                    <label class="text-capitalize label left label-primary btn-pointer" for="{{ $club -> name }}"> {{ $club -> name }}</label>
                            @endforeach
                        @else
                            <p class="text-danger">No hay clubs registrados</p>
                            <p>Para registrar un club haga click <a href="{{ url('admin/clubs/create') }}">aquí</a></p>
                        @endif
                    </div> --}}
                    <div class="form-group">
                        <label for="" class="control-label">Clubs <span class="text-danger">*</span></label>
                        @if (count($clubs_parents_products)>0)
                            @foreach ($clubs_parents_products as $club)
                                <input id="{{ $club -> name }}" name="fk_id_club[]" type="checkbox"  checked="checked" value="{{ $club -> id }}">
                                    <label class="text-capitalize label left label-primary btn-pointer" for="{{ $club -> name }}"> {{ $club -> name }}</label>
                            @endforeach
                        @else
                            <p class="text-danger">No hay clubs registrados</p>
                            <p>Para registrar un club haga click <a href="{{ url('admin/clubs/create') }}">aquí</a></p>
                        @endif
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Garantía y condiciones de envío</h4>
                        <hr>
                    </div>
                    <div class="form-group">
                        <label for="warranty" class="control-label">Garantía</label>
                        <input id="warranty" name="warranty" type="checkbox" @if ($parent_product -> warranty == 1) checked="checked" value="on" @endif>
                        <label for="warranty" class="text-primary"> El producto posee garantía</label>
                    </div>
                    <div id="data_warranty" class="@if ($parent_product -> warranty == 1) visible
                    @else hidden @endif">
                        <div class="form-group">
                            <label for="warranty_conditons" class="control-label">Condiciones de Garantía</label>
                            <textarea id="warranty_conditions" name="warranty_conditions" placeholder="Condiciones de Garantia" maxlength="250" class="form-control" rows="10" @if ($parent_product -> warranty == 1) required="required"
                             @endif> {{ $parent_product -> warranty_conditions }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="warranty_term" class="control-label">Meses de Garantía</label>
                            <input id="warranty_day" name="warranty_day" placeholder="Tiempo de Garantia" type="number" maxlength="8" class="form-control" value="{{ $parent_product -> warranty_day }}" @if ($parent_product -> warranty == 1) required="required"
                             @endif>
                        </div>
                        <div class="form-group">
                            <label for="delivery-conditions" class="control-label">Condiciones de Envío <span class="text-danger">*</span></label>
                            <textarea id="delivery-conditions" name="delivery_conditions" placeholder="Condiciones de Envio" maxlength="65000" class="form-control" required="required" rows="10"> {{ $parent_product -> delivery_conditions }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('modals')
    <!-- MODAL PARA AGREGAR UNA IMAGEN -->
    <div class="modal fade" id="update-image" tabindex="-1" role="dialog" aria-labelledby="add-image">
        <form action=" {{ route('images.update') }} " method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="change-image">Agregar imagen</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="image" class="control-label">Imagen <span class="text-danger">*</span></label>
                            <input id="image" name="image" type="file" class="form-control" required="required">
                            <input class="id" name="id" type="hidden" readonly="readonly">
                            <input id="identificator" name="identificator" type="hidden" readonly="readonly" value="{{ $parent_product -> id }}">
                            <input id="disk" name="disk" type="hidden" value="products" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="slug" class="control-label">Slug</label>
                            <input name="slug" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- MODAL PARA ELIMINAR UNA IMAGEN -->
    <div class="modal fade" id="delete-image" tabindex="-1" role="dialog" aria-labelledby="add-image">
        <form action=" {{ route('images.destroy') }} " method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="change-image">Eliminar imagen</h4>
                    </div>
                    <div class="modal-body">
                        <p class="">¿Esta seguro de eliminar la imagen?</p>
                        <input class="id" type="hidden" name="id" readonly="readonly">
                        <input id="disk" name="disk" type="hidden" value="products" readonly="readonly">
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-success">Si</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('scripts')
   {{--  <script>
        $('#warranty').change(function(){
          if($(this).prop("checked")) {
            $('#data_warranty').removeClass("hidden").addClass("visible");
          } else {
            $('#data_warranty').removeClass("visible").addClass("hidden");
          }
        });
    </script> --}}
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/trumbowyg/js/trumbowyg.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/summernote/summernote.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/form_editors.js') }}" ></script>
    <!-- end of page level js -->
      <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page js -->
@endsection
