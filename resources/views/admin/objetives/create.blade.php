@extends('layouts.admin.default')

@section('title', 'Objetivo')
@section('content')
    <!-- Header Page -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Nuevo</small></h1>
        </div>
    </header>
    <!-- End Header Page -->
    <!-- Content Page -->
	<article class="content">
        <form id="form-validation" action="{{ route('objetives.store') }}" method="POST">
        {{ csrf_field() }}
        @include('layouts.form-errors')
        <input type="hidden" name="fk_id_program" value="{{ $program  }}">
			<div class="panel">
               <div class="panel-heading clearfix">
                    <div class="pull-right text-center">
                        <a href="{{route('programs.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="code" class="control-label">Código<span class="text-danger">*</span></label>
                            <input name="code" type="text" class="form-control"  maxlength="6"  id="code" placeholder="Código" required="required">
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required="required">
                        </div>
                         <div class="form-group">
                        <label for="description" class="control-label">Descripción<span class="text-danger">*</span></label>
                        <textarea id="description" name="description" placeholder="Descipción" maxlength="65000" class="form-control" required="required" rows="10"></textarea>
                    </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="start-date" class="control-label">Inicio <span class="text-danger">*</span></label>
                            <input name="start_date" type="date" class="form-control" id="start_date" required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="due-date" class="control-label">Fin <span class="text-danger">*</span></label>
                            <input name="due_date" type="date" class="form-control" id="due_date" required="required">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="fk_id_perspective" class="control-label">Perspectivas <span class="text-danger">*</span></label>
                            @if (count($perspectives)>0)
                               <select name="fk_id_perspective" id="fk_id_perspective" class="form-control" required="required">
                                    <option value="">Seleccione...</option>
                                    @foreach ($perspectives as $element)
                                        <option value="{{ $element -> id }}">{{ $element -> name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <p>No hay perspectivas registradas. Si desea registrar una dirigase a <a href="{{ route('perspectives.create') }}">Crear un perspectiva</a></p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>    
        </form>
	</article>
    <!-- End Content Page -->
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- End pPge js -->
@endsection
