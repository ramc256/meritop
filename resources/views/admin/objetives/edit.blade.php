@extends('layouts.admin.default')

@section('title', 'Objetivo')
@section('content')
    <!-- Header Page -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Editar</small></h1>
        </div>
    </header>
    <!-- End Header Page -->
    <!-- Content Page -->
	<article class="content">
        <form id="form-validation" action="{{ route('objetives.update', $objetive -> id) }}" method="POST">
        {{ csrf_field() }}
        @include('layouts.form-errors')
        <input name="_method" type="hidden" value="PUT">
			<div class="panel">
                <div class="panel-heading clearfix">
                     <div class="pull-right text-center">
                        <a href="{{route('objetives.show', $objetive -> id)}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-md-12"> 
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name"  id="name" placeholder="Nombre" type="text" class="form-control" required="required" value="{{ $objetive -> name }}">
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                            <textarea name="description" id="description" placeholder="Descripción del objetivo" class="form-control" maxlength="65000" required="required" rows="10"> {{ $objetive -> description }}</textarea>
                        </div>
                    </div>
                    <div class=" col-md-6">
                        <div class="form-group">
                            <label for="start_date" class="control-label">Inicio <span class="text-danger">*</span></label>
                            <input name="start_date" type="date" class="form-control" id="start_date" required="required" value="{{ $objetive -> start_date }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="due_date" class="control-label">Fin <span class="text-danger">*</span></label>
                            <input name="due_date" type="date" class="form-control" id="due_date" required="required" value="{{ $objetive -> due_date }}">
                        </div>
                    </div>
                    <div class="col-md-12"> 
                        <div class="form-group">
                            <label for="fk_id_perspective" class="control-label">Perspectivas <span class="text-danger">*</span></label>
                            @if (count($perspectives)>0)
                               <select name="fk_id_perspective" id="fk_id_perspective" class="form-control" required="required">
                                    <option value="">Seleccione...</option>
                                    @foreach ($perspectives as $element)
                                        <option value="{{ $element -> id }}" @if ($objetive -> fk_id_perspective == $element -> id) selected="selected" @endif>{{ $element -> name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <p>No hay perspectivas registradas. Si desea registrar una dirigase a <a href="{{ route('perspectives.create') }}">Crear un perspectiva</a></p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="market" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select name="status" id="status" class="form-control" required="required">
                                <option value="">Seleccione un estatus</option>
                                <option value="1" @if ($objetive -> status == 1) selected="selected" @endif>Activo</option>
                                <option value="0" @if ($objetive -> status == 0) selected="selected" @endif>Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>     
        </form>
	</article>
    <!-- End Content Page -->
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js - ->
@endsection
