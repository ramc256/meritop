@extends('layouts.admin.default')

@section('title', 'Objetivo')
@section('content')
    <!-- Header Page -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Ver</small></h1>
        </div>
    </header>
    <!-- End Header Page -->
    <!-- Content Page -->
    @include('flash::message')
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{route('programs.show', $objetive-> fk_id_program)}} " type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{route('objetives.edit', $objetive->id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-sm-12 col-md-5">
                     <ul class="panel-list">
                         <li class="row">
                             <div class="col-sm-6 col-md-12 col-lg-6">
                                 <p><b>Código</b></p>
                                 <p class="text-capitalize">{{ $objetive -> code }}</p>
                             </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Nombre</b></p>
                                <p class="text-capitalize">{{ $objetive -> name }}</p>
                            </div>
                        </li>
                        <li>
                            <p><b>Descripción</b></p>
                            <p class="text-capitalize">{{ $objetive -> description }}</p>
                        </li>
                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Fecha de inicio</b></p>
                                <p>{{ date_format( new DateTime($objetive -> start_date), 'd/m/Y h:i:s A') }}</p>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Fecha de culminación</b></p>
                                <p>{{ date_format( new DateTime($objetive -> due_date), 'd/m/Y h:i:s A') }}</p>
                            </div>
                        </li>

                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Perspectiva</b></p>
                                <p><span class="label label-primary">{{ $objetive -> perspective }}</span></p>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Estatus</b></p>
                                <p>@if ($objetive -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span> @endif</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-5">
                    <ul class="panel-list">
                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Fecha de creación</b></p>
                                <p class="text-capitalize">{{ date_format($objetive -> created_at, 'd/m/Y') }}</p>
                           </div>
                           <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Fecha de modificación</b></p>
                                <p class="text-capitalize">{{ date_format($objetive -> updated_at, 'd/m/Y') }}</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
    <!-- End Content Page -->
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('objetives.destroy', $objetive->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Objetivo</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el Objetivo <b>{{ $objetive -> name}}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end Modal -->
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->
@endsection
