@extends('layouts.admin.default')

@section('title' ,'Perfil')
@section('styles')
 <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
<header class="header-page">
    <div class="header-data">
        <h1>@yield('title')<small> - Usuario</small></h1>
    </div>
</header>
<!-- Main content -->
@include('flash::message')
@include('layouts.form-errors')
<section class="content">
    <div class="panel">
        <div class="panel-body">
            <div class="col-md-4">
                <div class="form-group">
                    <div class="text-center mbl">
                         <!-- Button trigger modal -->
                         <a id="btn-image-change"  data-toggle="modal" data-target="#image-change">
                            <div class="text-center relative mbl">
                                <div class="overlay">
                                    <div class="btns">
                                        <button type="button" class="img-product btn btn-success btn-xs" data-toggle="modal" data-target="#update-image"><i class="fa fa-check"></i></button>
                                    </div>
                                </div>
                                @if (empty(Auth::guard('user')->user()-> image))
                                    <div class="img-circle img-show" style=" background-image: url( {{ Storage::disk('images') -> url('img-default-male.jpg') }} );"></div>
                                @else
                                    <div class="img-circle img-show" style=" background-image: url( {{ $_ENV['STORAGE_PATH'].Auth::guard('user')->user() -> image }} );"></div>
                                @endif
                            </div>
                        </a>
                        <!-- End button modal -->
                    </div>
                </div>
                <div class="profile_user text-center">
                    <h3 class="user_name_max  text-capitalize">{{ $user -> first_name . ' ' . $user -> last_name }}</h3>
                    <p>{{ $user -> email }}</p>
                </div>
                <br/>
                <div class="panel panel-widget panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"> Datos de usuario</span>
                    </div>
                    <div class="panel-body profile_status">
                        <div>
                            <p>
                                <strong>Admin Template</strong>
                                <small class="pull-right">
                                    {{ $pua }}%
                                </small>
                            </p>
                            <div class="progress progress-xs progress-striped active">
                                <div class="progress-bar progress-bar-success" role="progressbar"
                                     aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                     style="width: {{ $pua }}%">
                                        <span class="sr-only">
                                    {{ $pua }}%
                                </span>
                                </div>
                            </div>
                        </div>
                        <ul class="panel-list">
                            <li><b>Usuario:</b> {{ $user -> user_name }}</li>
                            <li><b>Email:</b> {{ $user -> email }}</li>
                            <li><b>Tipo:</b> <span class="label label-primary">@if ($user -> kind == 1) Meritop @else Club @endif</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-custom" style="margin-top: 3px;">
                            <li class="active">
                                <a href="#historials" data-toggle="tab">
                                    <strong>Actividades</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#data-profile" data-toggle="tab">
                                    <strong>Datos de perfil</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#data-old-password" data-toggle="tab">
                                    <strong>Modificar contraseña</strong>
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content nopadding noborder">
                            <div id="historials" class="tab-pane fade in active">
                                <div class="table-responsive">
                                    <table class="table table-responsive">
                                        <tbody>
                                            @foreach($historical_actions as $historical)
                                                <tr>
                                                    <td>
                                                        <p>{{ $historical -> description }}</p>
                                                    </td>
                                                    <td>
                                                        <p>@if ($historical -> action == 'actualizar')
                                                           <span class="label label-primary text-capitalize">{{ $historical -> action }}</span>
                                                        @elseif($historical -> action == 'crear')
                                                            <span class="label label-success text-capitalize">{{ $historical -> action }}</span>
                                                        @elseif( $historical -> action == 'eliminar')
                                                            <span class="label label-danger text-capitalize">{{ $historical -> action }}</span>
                                                        @elseif($historical -> action == 'exportar')
                                                            <span class="label label-warning text-capitalize">{{ $historical -> action }}</span>
                                                        @elseif($historical -> action == 'importar')
                                                            <span class="label label-success text-capitalize">{{ $historical -> action }}</span>
                                                        @endif </p>
                                                    </td>
                                                    <td>
                                                        <p>{{date_format( $historical -> created_at, 'd/m/Y') }}</p>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                            {{ $historical_actions -> links() }}
                                </div>
                            </div>
                            <!-- tab-pane -->
                            <div id="data-profile" class="tab-pane">
                                <div class="panel-body">
                                    <ul class="panel-list">
                                        <li>
                                            <p><b>Nombre y Apellido</b></p>
                                            <p class="text-capitalize">{{ $user -> first_name . ' ' . $user -> last_name }}</p>
                                        </li>
                                        <li>
                                            <p><b>Usuario</b></p>
                                            <p>{{ $user -> user_name }}</p>
                                        </li>
                                        <li>
                                            <p><b>E-mail</b></p>
                                            <p>{{ $user  -> email }}</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="panel-footer text-center">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-edit">Editar</button>
                                </div>
                            <!-- tab-pane -->
                            </div>
                            <div id="data-old-password" class="tab-pane">
                                <div class="panel-body">
                                    <ul class="panel-list">
                                        <li>
                                            <p><b>Nombre y Apellido</b></p>
                                            <p class="text-capitalize">{{ $user -> first_name . ' ' . $user -> last_name }}</p>
                                        </li>
                                        <li>
                                            <p><b>Usuario</b></p>
                                            <p>{{ $user -> user_name }}</p>
                                        </li>
                                        <li>
                                            <p><b>E-mail</b></p>
                                            <p>{{ $user  -> email }}</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="panel-footer text-center">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-edit-password">Cambiar contraseña</button>
                                </div>
                            <!-- tab-pane -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit" >
        <form action="{{ route('profile.update', $user -> id) }}" method="POST">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="panel-title">Editar Perfil</h3>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="form-group">
                            <label for="name" class=" control-label">Nombre <span class="text-danger">*</span></label>
                           
                                    <input name="first_name" type="text" class="form-control" id="first_name" placeholder="Nombre" value="{{ $user -> first_name }}" required="required">
                           </div>  
                        <div class="form-group">
                            <label for="last_name" class=" control-label">Apellido <span class="text-danger">*</span></label>
                            <input name="last_name" type="text" class="form-control" id="lastname" placeholder="Apellido" value="{{ $user -> last_name }}"  required="required">     
                        </div>
                        <div class="form-group">
                            <label for="user_name" class="control-label">Usuario <span class="text-danger">*</span></label>
                            <input name="user_name" type="text" class="form-control" id="user_name" placeholder="username" value="{{ $user -> user_name }}" required="required">
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">E-mail <span class="text-danger">*</span></label>
                            <input name="email" type="mail" class="form-control" id="email" placeholder="E-mail" value="{{ $user -> email }}" required="required">
                        </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal change password -->
    <div class="modal fade" id="modal-edit-password" tabindex="-1" role="dialog" aria-labelledby="modal-edit" >
        <form action="{{ route('profile.change-password') }}" method="POST">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="panel-title">Modificar contraseña</h3>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                         <div class="form-group">
                            <label for="old_password" class="control-label">Contraseña anterior <span class="text-danger">*</span></label>
                            <input name="old_password" type="password" class="input-md form-control" id="old_password" placeholder="Anterior Contraseña" autocomplete="off"  required="required">
                        </div>
                        <div class="form-group">
                            <label for="new_password" class="control-label">Contraseña nueva<span class="text-danger">*</span></label>
                            <input name="new_password" type="password" class="input-md form-control" id="new_password" placeholder="Contraseña" autocomplete="off"  required="required">
                        </div>
                        <br/>
                        <div class="hidden validator">
                            <div class="col-sm-6 padding">
                                <span id="8char" class="glyphicon glyphicon-remove"
                                style="color:#FE5B5B;"></span> 8 caracteres de longitud
                                <br>
                                <span id="ucase" class="glyphicon glyphicon-remove"
                                style="color:#FE5B5B;"></span> 1 Letra Minuscula
                            </div>
                            <div class="col-sm-6 padding">
                                <span id="lcase" class="glyphicon glyphicon-remove"
                                style="color:#FE5B5B;"></span> 1 Letra Mayuscula
                                <br>
                                <span id="num" class="glyphicon glyphicon-remove" style="color:#FE5B5B;"></span>
                                1 numero
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="re_password" class="control-label">Repitir Contraseña <span class="text-danger">*</span></label>
                            <input name="re_password" type="password" class="form-control" id="re_password" placeholder="Repetir Contraseña"  required="required">
                        </div>
                        <div class="col-sm-12 padding validator hidden">
                            <span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FE5B5B;"></span>
                            Passwords Match
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- modal change password-->
    <!-- Change modal image-->
    <div class="modal fade" id="update-image" tabindex="-1" role="dialog" aria-labelledby="update-image">
        <form action="{{ action('Admin\ProfileController@imageChange', $user -> id ) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cambiar imagen</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="iamge" class="control-label">Imagen <span class="text-danger">*</span></label>
                            <input name="image" type="file" class="form-control" id="image" required="required">
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- End modal image -->
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/password-validator.js')}}"></script>
    <!-- end page js -->
    <!-- page validation -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page validation -->
@endsection
