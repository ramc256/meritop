@extends('layouts.admin.default')

@section('title', 'Mercado')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Ver</small></h1>
        </div>
    </header>
    @include('flash::message')
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">               
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"> Eliminar</button>
                    <a href="{{ route('markets.index') }}" type="button" class="btn btn-warning">Atrás</a>  
                    <a href="{{ route('markets.edit', $market->id) }}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                 <ul class="panel-list">
                    <li class="row">
                        <div class="col-md-12">
                            <p><b>Nombre</b></p>
                            <p class="text-capitalize">{{ $market -> name }}</p>
                        </div>
                    </li>
                    <li class="row">
                        <div class="col-md-12">
                            <p><b>Descripción</b></p>
                            <p class="text-capitalize">{{ $market -> description }}</p>
                        </div>
                    </li>
                    <li class="row">            
                        <div class="col-xs-12 col-sm-6">
                            <p><b>Creación</b></p>
                            <p class="text-capitalize">{{ date_format($market -> created_at, 'd/m/Y h:i:s A') }}</p>
                        </div>   
                        <div class="col-xs-12 col-sm-6">
                            <p><b>Modificación</b></p>
                            <p class="text-capitalize">{{ date_format($market -> updated_at, 'd/m/Y h:i:s A' ) }}</p>
                        </div> 
                    </li>            
                </ul>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('markets.destroy', $market->id) }}" method="POST"> 
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar mercado</h4>
                    </div>
                    <div class="modal-body">     
                        <p>¿Esta seguro de eliminar el <b>{{ $market -> name }}</b>?</p>               
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>            
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- End page js -->
@endsection
