@extends('layouts.admin.default')

@section('title', 'Mercado')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Header Page -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Nuevo</small></h1>
        </div>
    </header>
    <!-- End Header Page -->
    <!-- Content Page -->
    @include('layouts.form-errors') 
	<article class="content">
        <form id="form-validation" action="{{ route('markets.store') }}" method="POST" >
        {{ csrf_field() }}
			<div class="panel">
               <div class="panel-heading clearfix">
                    <div class="pull-right text-center">
                        <a href="{{route('markets.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                        <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required="required">
                    </div>
                     <div class="form-group">
                        <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                            <textarea name="description" class="form-control" id="description" placeholder="Descripcion" required="required" rows="10"></textarea>
                    </div>
                </div>
            </div>
        </form>
	</article>
    <!-- End Content Page -->
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- End pPge js -->
@endsection
