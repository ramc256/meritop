@extends('layouts.admin.default')

@section('title', 'Mercado')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Editar</small></h1>
        </div>
    </header>
	<article class="content">
        @include('flash::message')
        @include('layouts.form-errors')  
        <form id="form-validation" action="{{ route('markets.update',$market -> id) }}" method="POST" >
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
			<div class="panel">
                <div class="panel-heading clearfix">
                    <div class="text-center pull-right">
                        <a href="{{route('markets.show', $market -> id )}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div> 
                </div>
                <div class="panel-body row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" value="{{$market -> name}}" required="required">
                        </div>
                        <div class="form-group">
                        <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                            <textarea name="description" class="form-control" id="description" placeholder="Descripción" value="{{$market -> description}}" required="required" rows="10"></textarea>
                    </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js - ->
@endsection