@extends('layouts.admin.default')

@section('title', 'Salida de Inventario')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>

    <!--Datepicker css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datetime/css/jquery.datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/airdatepicker/css/datepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/datepicker.css') }}">
    <!-- End Datepicker css -->
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
         <div class="header-data">
            <h1>@yield('title')<small> - Gestiones</small></h1>
        </div>
    </header>
    @include('layouts.form-errors')
    @include('flash::message')
	<article class="content">
		<div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="btns-header">
                    {{-- <button  data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-1" type="button" class="btn btn-labeled btn-meritop-primary">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-down"></i></span> Filtros
                    </button> --}}
                </div>
                <div class="panel-faq">
                    <form method="post" action=" {{ route('reports.inventory-output.filter') }} ">
                        {{ csrf_field() }}
                        @include('flash::message')
                        <input type="hidden" name="_method" value="get">
                        {{-- <div id="faq-cat-1-sub-1" class="collapse"> --}}
                            <div class="panel-body">
                                <div class="form-inline" style="display: block; overflow: hidden;">
                                    <div class="form-group pull-left">
                                        <label class="control-label" for="dateranges">Rango de fecha</label>
                                        <input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" class="form-control" id="dateranges" readonly="readonly" value="{{ date('01/m/Y') }} - {{ date('d/m/Y') }}"/>
                                       <span class="control-label">Club</span>
                                        <select id="filterInput" name="fk_id_club" class="form-control" required="required">
                                            <option value="">Seleccione un club...</option>
                                            @foreach($users_clubs as $club)
                                                <option value="{{ $club -> id }}">{{ $club -> name }}</option>
                                             @endforeach
                                        </select>
                                        <button type="submit" class="btn btn-meritop-primary btn-md" id="addButton">Buscar</button>
                                    </div>
                                </div>
                            </div>
                        {{-- </div> --}}
                    </form>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="filter-table" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#SKU </th>
                                <th>Item</th>
                                <th>Modelo</th>
                                <th>Marca</th>
                                <th>Cantidad</th>
                                <th>Club</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($inventory_report as $element)
                                <tr>
                                    <td>{{ $element -> sku}}</td>
                                    <td>{{ $element -> name }}</td>
                                    <td>{{ $element -> model }}</td>
                                    <td class="text-capitalize">{{$element -> brand}}</td>
                                    <td class="text-capitalize">{{ $element -> quantity}}</td>
                                    <td class="text-capitalize filter-column" >{{$element -> club}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="active"><b>Total items:</b></td>
                                <td class="active"><b>{{ $total}}</b></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
	</article>
@stop
@section('scripts')

    <script type="text/javascript" src="{{ asset('assets/jquery.filters.js') }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->

    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.print.js') }}"></script>
    
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/pdfmake.min.js') }}"></script>

    <!-- DateRange Picked js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datetime/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/advanceddate_pickers.js') }}"></script>
    <!--End DateRange Picked js -->

     <script type="text/javascript">
        $(document).ready( function() {
             $("#filterInput").filterTables();
            //table tools example
             var table = $('#filter-table').DataTable({
                // dom: 'Bflrtip',
                "dom": '<"m-t-10"B><"m-t-10 pull-left"f><"m-t-10 pull-right"l>rt<"pull-left m-t-10"i><"m-t-10 pull-right"p>',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection
