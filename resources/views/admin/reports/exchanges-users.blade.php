@extends('layouts.admin.default')

@section('title', 'Reporte de usuarios')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>

     <!--Datepicker css -->
    <link rel="stylesheet" href="{{ asset('core-plus-theme/vendors/datetime/css/jquery.datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/airdatepicker/css/datepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/datepicker.css') }}">
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Gestiones</small></h1>
        </div>
    </header>
    @include('flash::message')
    <article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="btns-header">
                    {{-- <button  data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-1" type="button" class="btn btn-labeled btn-meritop-primary"> --}}
                        {{-- <span class="btn-label"><i class="glyphicon glyphicon-chevron-down"></i></span> Filtros --}}
                    </button>
                    <div class="pull-right">
                        {{-- <button class="btn btn-meritop-secundary" type="button"  id="btn-data-import"  data-toggle="modal" data-target="#data-import">Importar</button> --}}
                        {{-- <button class="btn btn-meritop-secundary" type="button"  id="btn-data-export"  data-toggle="modal" data-target="#data-export">Exportar</button> --}}
                    </div>
                </div>
                {{-- <div class="panel-faq"> --}}
                    <form method="post" action=" {{ route('exchange-user.filter') }} ">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="get">
                        {{-- <div id="faq-cat-1-sub-1" class="collapse"> --}}
                            <div class="panel-body">
                                <div class="form-inline" style="display: block; overflow: hidden;">
                                    <div class="form-group pull-left">
                                        <label for="dateranges">Rango de fecha</label>
                                        <input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" class="form-control" id="dateranges" readonly="readonly" value="{{ date('01/m/Y') }} - {{ date('d/m/Y') }}"/>
                                        <span>Club</span>
                                        <select class="text-capitalize form-control" id="filterInput" name="users_clubs" required="required">
                                            <option class="text-capitalize" value="all">Todos</option>
                                            @foreach($users_clubs as $element)
                                                <option class="text-capitalize" value="{{ $element -> id }}">{{ $element -> name }}</option>
                                             @endforeach
                                        </select>
                                        <button type="submit" class="btn btn-meritop-primary btn-md" id="addButton">Buscar</button>
                                    </div>
                                </div> 
                            </div>
                      {{--   </div> --}}
                    </form>
                {{-- </div> --}}
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="filter-table" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>DNI</th>
                                <th>Carnet</th>
                                <th>Nombre</th>
                                <th>Club</th>
                                <th>Sede</th>
                                <th>Ciudad</th>
                                <th>Estado</th>
                                <th>Item</th>
                                <th>Cantidad</th>
                                <th>Puntos Canjeados</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($exchanges_users as $element)
                            <tr>
                                <td>{{$element -> dni}}</td>
                                <td>{{$element -> carnet}}</td>
                                <td class="text-capitalize">{{$element -> first_name . ' ' . $element -> last_name}}</td>
                                <td class="filter-column text-capitalize">{{$element -> club}}</td>
                                <td class="text-capitalize">{{$element -> branch}}</td>
                                <td class="text-capitalize">{{$element -> city}}</td>
                                <td class="text-capitalize">{{$element -> state }}</td>
                                <td class="text-capitalize">{{$element -> product }}</td>
                                <td>{{$element -> quantity}}</td>
                                <td>{{$element -> points}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                         <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total</b></td>
                                <td class="active"><b>{{ $total_quantity }}</b></td>
                                <td class="active"><b>{{ $total_points }}</b></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </article>
@stop
@section('scripts')

    <script type="text/javascript" src="{{ asset('assets/jquery.filters.js') }}"></script>

    <!-- Datable js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- DateRange Picked js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datetime/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/advanceddate_pickers.js') }}"></script>
    <!--END DateRange Picked js -->
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/pdfmake.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#filterInput").filterTables();
            //table tools example
            var table = $('#filter-table').DataTable({
                // dom: 'Bflrtip',
                "dom": '<"m-t-10"B><"m-t-10 pull-left"f><"m-t-10 pull-right"l>rt<"pull-left m-t-10"i><"m-t-10 pull-right"p>',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection

