@extends('layouts.admin.default')

@section('title', 'Cuentas')
@section('styles')
    <!--DataTable css -->
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Estados de cuenta</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel filterable">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-asc">
                        <thead>
                            <tr>
                                <th style="width:15%">N° de cuenta</th>
                                <th style="width:15% ">DNI</th>
                                <th style="width: 30% ">Miembro</th>
                                <th style="width: 20%" >Saldo</th>
                                <th style="width: 20% ">Tipo</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if (count($accounts) > 0)
                                @foreach($accounts as $element)
                                    <tr>
                                        <td><a href="{{ route('accounts.show', $element -> id) }}">{{ str_pad( $element -> code, 10 , 0, STR_PAD_LEFT) }}</a></td>
                                        <td>{{ $element -> dni }}</td>
                                        <td>{{ $element -> first_name . ' ' . $element -> last_name }}</td>
                                        <td>{{ number_format($element -> points, 0 , '', '.') }}</td>
                                        <td> {{ $element -> kind }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>
@stop
@section('scripts')
    <!-- DataTable js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
@endsection