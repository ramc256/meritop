@extends('layouts.admin.default')
@section('styles')   
    <!--DataTable css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <!--Datepicker css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datetime/css/jquery.datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/airdatepicker/css/datepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('title', 'Estados de cuentas')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">                    
                <div class="pull-right">                                    
                    <a href="{{ route('accounts.index') }}" class="btn btn-warning">Atrás</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-sm-12">
                     <ul class="panel-list">
                        <li class="col-md-4">
                            <p><b>Nombre y Apellido</b></p>
                            <p class="text-capitalize">{{ $account -> first_name . ' ' . $account -> last_name }}</p>
                        </li>
                        <li class="col-md-4">
                            <p><b>DNI</b></p>
                            <p class="text-capitalize">{{ $account -> dni }}</p>
                        </li>
                        <li class="col-md-4">
                            <p><b>Carnet</b></p>
                            <p class="text-capitalize">{{ $account -> carnet }}</p>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12">
                     <ul class="panel-list">
                        <li class="col-md-4">
                            <p><b> Ganados</b></p>
                            <p>{{ $total_points }}</p>
                        </li>
                        <li class="col-md-4">
                            <p><b> Canjes</b></p>
                            <p>{{ $traded_points }}</p>
                        </li>
                        <li class="col-md-4">
                            <p><b> Disponibles</b></p>
                            <p class="text-capitalize">{{ $account -> points }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="btns-header">
                    <button  data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-1" type="button" class="btn btn-labeled btn-meritop-primary">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-down"></i></span> Filtros
                    </button>  
                </div>
                <div class="panel-faq">
                    <form method="post" action=" {{ route('accounts.filter', $account -> id) }} ">
                        {{ csrf_field() }}
                        @include('layouts.form-errors')
                        <input type="hidden" name="id" value="{{ $account -> id  }}">
                        <input type="hidden" name="_method" value="get">
                        <div id="faq-cat-1-sub-1" class="collapse">
                            <div class="panel-body">
                               <div class="form-inline" style="display: block; overflow: hidden;">
                                    <div class="form-group pull-left">
                                        <label class="control-label" for="dateranges">Rango de fecha: </label>
                                        <input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" class="form-control" id="dateranges" readonly="readonly" value="{{ date('01/m/Y') }} - {{ date('d/m/Y') }}"/>
                                    </div>
                                    <div class="form-group pull-left">
                                        <span class="control-label">Operación: </span>
                                        <select name="operation" id="operation" class="form-control">
                                            <option value="">Ambos</option>
                                            <option value="1">Aporte</option>
                                            <option value="0">Canje</option>
                                        </select>
                                    </div>
                                    <div class="form-group pull-left">
                                        <button type="submit" class="btn btn-meritop-primary btn-md" id="addButton">Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-desc">
                        <thead>
                            <tr>
                                <th># Transacción</th>
                                <th>Fecha</th>
                                <th>Descripción</th>
                                <th>Puntos</th>
                                <th>Balance</th>
                                <th>Operación</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($transactions) > 0)
                                @foreach($transactions as $element)
                                    <tr>
                                        <td width="120"><a href="{{ route('transactions.show', $element -> id) }}">{{ str_pad( $element -> id, 10 , 0, STR_PAD_LEFT) }}</a></td>
                                        <td>{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}</td>
                                        <td><a href="{{ route('transactions.show', $element -> id) }}">{{ substr($element -> description, 0,100) }}...</a></td>
                                        <td>{{ $element -> points }}</td>
                                        <td>{{ $element -> balance }}</td>
                                        <td>@if ( $element -> operation  == 1) <span class="label label-success">Aporte</span> @else <span class="label label-danger">Canje</span> @endif</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>
@stop
@section('scripts')
    <!-- DataTable js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>

    <!-- DateRange Picked js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datetime/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/advanceddate_pickers.js') }}"></script>

    <!-- Export MASTER js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/FileSaver/FileSaver.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/js-xlsx/xlsx.core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/jsPDF/jspdf.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/tableExport.min.js') }}"></script>
@endsection
