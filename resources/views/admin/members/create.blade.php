@extends('layouts.admin.default')

@section('title', 'Miembro')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Nuevo</small></h1>
        </div>
    </header>
    @include('flash::message')
    @include('layouts.form-errors')
	<article class="content">
        <form id="form-validation" action="{{ route('members.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
			<div class="panel">
               <div class="panel-heading clearfix">
                    <div class="pull-left text-center">
                        <h3>Datos principales</h3>
                    </div>
                     <div class="pull-right text-center">
                        <a href="{{route('members.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success" id="form-validation">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dni" class="control-label">DNI <span class="text-danger">*</span></label>
                                    <input name="dni" type="text" class="form-control" id="dni" placeholder="DNI"  name="edad" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="first_name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="first_name" type="text" class="form-control" id="first_name" placeholder="Nombre" required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastname" class="control-label">Apellido <span class="text-danger">*</span></label>
                            <input name="last_name" type="text" class="form-control" id="last_name" placeholder="Apellido"  required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="birthdate" class="control-label">Fecha de nacimiento<span class="text-danger">*</span></label>
                            <input name="birthdate" type="date" class="form-control" id="birthdate" placeholder="Fecha de nacimiento"  required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                            <label for="gender" class="control-label">Género <span class="text-danger">*</span></label>
                            <select id="gender" name="gender" class="form-control" data-bv-field="gender" required="">
                                <option value="">
                                    Seleccione un Género...
                                </option>
                                <option value="1">Masculino</option>
                                <option value="0">Femenino</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email" class="control-label">E-mail <span class="text-danger">*</span></label>
                            <input name="email" type="mail" class="form-control" id="email" placeholder="E-mail"  required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cell_phone" class="control-label">Teléfono <span class="text-danger">*</span></label>
                            <input name="cell_phone" type="text" class="form-control" id="cell_phone" placeholder="Telefono"  required="required">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Datos de club</h4>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="carnet" class="control-label">Carnet</label>
                                    <input name="carnet" type="text" class="form-control" id="carnet" placeholder="Carnet" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                            <label for="title" class="control-label">Cargo</label>
                            <input name="title" type="text" class="form-control" id="title" placeholder="Cargo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="emailcorporate" class="control-label">Email</label>
                            <input name="email_corporate" type="email" class="form-control" id="emailcorporate" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="branch" class="control-label">Sedes</label>
                            <select id="fk_id_branch" name="fk_id_branch" class="form-control">
                                <option value="">Seleccione una sede...</option>
                                @foreach($branches as $branch)
                                    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="departments" class="control-label">Departamentos</label>
                            <select id="fk_id_department" name="fk_id_department" class="form-control" >
                                <option value="">Seleccione un departamento...</option>
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="supervisorcode" class="control-label">Código supervisor</label>
                            <input name="supervisor_code" type="text" class="form-control" id="supervisor_code" placeholder="Codigo de supervisor">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Estatus</h4>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status" class="control-label">Estatus <span class="text-danger">*</span></label>
                                    <select id="status" name="status" class="form-control" data-bv-field="status" required="required">
                                        <option value="">
                                            Seleccione un estatus...
                                        </option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page js -->
@endsection
