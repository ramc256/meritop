@extends('layouts.admin.default')

@section('title', 'Miembro')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Ver</small></h1>
        </div>
    </header>
    @include('flash::message')
    <article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="pull-left text-center">
                    <h3>Datos principales</h3>
                </div>
                <div class="pull-right text-center">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{ route('members.index') }}" type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{ route('members.edit', $member -> id) }}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-xs-6 col-md-6">
                    <ul class="panel-list">
                        <li>
                            <p><b>DNI</b></p>
                            <p>{{ $member -> dni }}</p>
                        </li>
                        <li>
                            <p><b>Nombre y Apellido</b></p>
                            <p class="text-capitalize">{{ $member -> first_name . ' ' . $member -> last_name }}</p>
                        </li>
                        <li class="col-md-6">
                            <p><b>Fecha de Nacimiento</b></p>
                            <p>@if (!empty($member -> birthdate)) {{ date_format(date_create($member -> birthdate), 'd/m/Y') }} @endif</p>
                        </li>
                        <li class="col-md-6">
                            <p><b>Género</b></p>
                            <p class="text-capitalize"><p class="text-capitalize"> @if( $member -> gender == 1) <span class="label label-success">Masculino</span> @else <span class="label label-danger">Femenino</span> @endif</p>
                        </li>
                        <li class="col-md-6">
                            <p><b>Email</b></p>
                            <p>{{ $member -> email }}</p>
                        </li>
                        <li  class="col-md-6">
                            <p><b>Teléfono</b></p>
                            <p>{{ $member -> cell_phone }}</p>
                        </li>
                         <li class="col-md-12" >
                            <p><b>Estatus</b></p>
                            @if( $member -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span>@endif
                         </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <div  class="text-center img-responsive mbl">
                            @if (empty($member -> image))
                                @if ($member -> gender == 1)
                                    <img class="img-circle" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="{{ $member -> first_name . ' ' . $member -> last_name }}">
                                @else
                                    <img class="img-circle" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="{{ $member -> first_name . ' ' . $member -> last_name }}">
                                @endif
                            @else
                            <img class="img-circle" src="{{ $_ENV['STORAGE_PATH'].$member -> image }}" alt="{{ $member -> first_name . ' ' . $member -> last_name }}">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <hr>
                    <h4>Datos del club</h4>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover datatable-asc">
                            <thead>
                                <tr>
                                    <th>Carnet</th>
                                    <th>Email</th>
                                    <th>Código de departamento</th>
                                    <th>Código de supervisor</th>
                                    <th>Club</th>
                                    <th>Sede</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($members_clubs as $element)
                                    <tr>
                                        <td>{{ $element -> carnet }}</td>
                                        <td>{{ $element -> email }}</td>
                                        <td>{{ $element -> department }}</td>
                                        <td>{{ $element -> supervisor_code }}</td>
                                        <td>{{ $element -> club }}</td>
                                        <td>{{ $element -> branch }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('members.destroy', $member->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Miembro</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el Miembro <b>{{ $member -> first_name .' '. $member -> last_name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js -->
@endsection
