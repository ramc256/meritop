@extends('layouts.admin.default')
@section('title', 'Resultado')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
<!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Registros</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">Total de registros</h3>
                <div class="pull-right text-center">
                    <a href="{{route('members.index')}}" type="button" class="btn btn-warning">Atrás</a>
                </div>
            </div>
            <div class="panel-body">
               <ul>
                   	<li>Total de registro: {{ count($members_registers) }}</li>
                   	<li>Miembros con datos correctos: {{ $registers  }}</li>
                   	<li>Miembros con datos incorrectos: {{ $no_registers }} </li>
                    <li>Estatus:

                        @if($status_registers == 0)
                            <span class="label label-danger">No se realizo la operacion, existen registros con datos incorrectos</span>
                        @else
                            <span class="label label-success">Se realizo satifactoriamente</span>
                         @endif
                     </li>
               </ul>
            </div>
        </div>
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left m-t-6">
                    Registro de miembros
                </h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-asc">
                        <thead>
                                <th>Nombre y apellido</th>
                                <th>Email</th>
                                <th>Teléfono</th>
                                <th>Sede</th>
                                <th>Estatus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($members_registers as $element)
                                <tr>
                                    <td>{{ $element['dni'] }}</td>
                                    <td>{{ $element['first_name'] . ' ' . $element['last_name'] }}</td>
                                    <td>{{ $element['email'] }}</td>
                                    <td> {{ $element['cell_phone'] }}</td>
                                    <td> {{ $element['branch'] }}</td>
                                    <td>
                                        @if( $element['status'] == 0)
                                            <span class="label label-warning"> miembro previamente registrado</span>
                                        @elseif($element['status'] == 1)
                                            <span class="label label-danger">miembro con codigo de sede o departamento incorrecto</span>
                                        @elseif($element['status'] == 2)
                                            <span class="label label-success">miembro con datos correctos</span>
                                        @elseif($element['status'] == 3)
                                            <span class="label label-success">miembro con datos correctos</span>
                                        @elseif($element['status'] == 4)
                                            <span class="label label-success">miembro con correo ya registrado en la plataforma</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js -->
@endsection
