@extends('layouts.admin.default')

@section('title', 'Miembro')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Editar</small></h1>
        </div>
    </header>
    @include('flash::message')
    @include('layouts.form-errors')
	<article class="content">
		<form id="form-validation" action="{{ route('members.update', $member -> id) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="panel filterable">
                <div class="panel-heading clearfix">
                    <div class="pull-left text-center">
                        <h3>Datos principales</h3>
                    </div>
                    <div class="pull-right text-center">
                        <a href="{{ route('members.show', $member -> id) }}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success" id="form-validation">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="dni" class="control-label">DNI <span class="text-danger">*</span></label>
                            <input name="dni" type="text" class="form-control" id="dni" placeholder="DNI" value="{{ $member -> dni }}" required="required">
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="first_name" type="text" class="form-control" id="first_name" placeholder="Nombre" value="{{ $member -> first_name }}" required="required">
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="control-label">Apellido <span class="text-danger">*</span></label>
                            <input name="last_name" type="text" class="form-control" id="last_name" placeholder="Apellido" value="{{ $member -> last_name }}"  required="required">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <div  class="text-center img-responsive mbl">
                                @if (empty($member -> image))
                                    @if ($member -> gender == 1)
                                        <img class="img-circle" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="{{ $member -> first_name . ' ' . $member -> last_name }}">
                                    @else
                                        <img class="img-circle" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="{{ $member -> first_name . ' ' . $member -> last_name }}">
                                    @endif
                                @else
                                <img class="img-circle" src="{{ $_ENV['STORAGE_PATH'].$member -> image }}" alt="{{ $member -> first_name . ' ' . $member -> last_name }}">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="birthdate" class="control-label">Fecha de nacimiento<span class="text-danger">*</span></label>
                            <input name="birthdate" type="date" class="form-control" id="birthdate" placeholder="Fecha de nacimiento" value="{{$member -> birthdate }}"  required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="gender" class="control-label">Género <span class="text-danger">*</span></label>
                            <select id="gender" name="gender" class="form-control" data-bv-field="gender" required="">
                                <option value="">Seleccione un Género...</option>
                                <option value="1" @if($member -> gender == 1) selected="selected" @endif>Masculino</option>
                                <option value="0" @if($member -> gender == 0) selected="selected" @endif>Femenino</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email" class="control-label">E-mail <span class="text-danger">*</span></label>
                            <input name="email" type="mail" class="form-control" id="email" placeholder="E-mail" value="{{ $member -> email }}" required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cellphone" class="control-label">Teléfono <span class="text-danger">*</span></label>
                            <input name="cell_phone" type="text" class="form-control" id="cellphone" placeholder="Telefono" value="{{ $member -> cell_phone }}" required="required">
                        </div>
                    </div>
                    <div class="col-md-12">
                            <h4 class="text-capitalize">Datos de club</h4>
                            <hr>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="carnet" class="control-label">Carnet</label>
                            <input name="carnet" type="text" class="form-control" id="carnet" placeholder="Carnet" value="{{ $members_clubs -> carnet }}">
                        </div>
                    </div>
                    <div class="col-xs-6">
                         <div class="form-group">
                            <label for="title" class="control-label">Cargo</label>
                            <input name="title" type="text" class="form-control" id="title" placeholder="Cargo" value="{{ $members_clubs -> title }}">
                        </div>
                    </div>
                    <div class="col-xs-6">
                         <div class="form-group">
                            <label for="emailcorporate" class="control-label">Email</label>
                            <input name="email_corporate" type="mail" class="form-control" id="emailcorporate" placeholder="Email" value="{{ $members_clubs -> email }}">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="branch" class="control-label">Sede<span class="text-danger">*</span></label>
                            <select id="branches" name="branch" class="form-control" required="required">
                                <option value="">Seleccione una Sede...</option>
                                @foreach($branches as $branch)
                                    <option value="{{ $branch->id }}" @isset($member_branch)@if($member_branch -> fk_id_branch == $branch -> id) selected="selected" @endif @endisset>{{ $branch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                         <div class="form-group">
                            <label for="departments" class="control-label">Departamento<span class="text-danger">*</span></label>
                            <select id="department" name="department" class="form-control" required="required">
                                <option value="">Seleccione un departamento...</option>
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}" @isset($member_department)@if($member_department -> fk_id_department == $department -> id) selected="selected" @endif @endisset>{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="supervisorcode" class="control-label">Código supervisor</label>
                            <input name="supervisor_code" type="text" class="form-control" id="supervisor_code" placeholder="Codigo de supervisor" value="{{ $members_clubs -> supervisor_code }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Datos de club</h4>
                        <hr>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="status" class="control-label">Estatus<span class="text-danger">*</span></label>
                            <select id="status" name="status" class="form-control" required="required">
                                <option value="">Seleccione un estatus...</option>
                                <option value="1" @if ($members_clubs->status == 1) selected='selected' @endif>Activo</option>
                                <option value="0" @if ($members_clubs->status == 0) selected='selected' @endif>Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- page validation -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page validation -->
@endsection
