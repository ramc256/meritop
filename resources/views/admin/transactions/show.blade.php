@extends('layouts.admin.default')

@section('title', 'Transacción')
@include('flash::message')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">
                    <a href="{{ url()->previous() }}" type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{ url('admin/downloadExcel/pdf/Transaction', $transaction -> id) }}" type="button" class="btn btn-meritop-secundary">Exportar</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-sm-6">
                     <ul class="panel-list">
                        <li class="col-md-5">
                            <p><b>DNI</b></p>
                            <p class="text-capitalize">{{ $transaction -> dni }}</p>
                        </li>
                        <li class="col-md-7">
                            <p><b>Miembro</b></p>
                            <p class="text-capitalize">{{ $transaction -> first_name . ' ' . $transaction -> last_name }}</p>
                        </li>
                        <li>
                            <p><b>Descripción</b></p>
                            <p class="text-capitalize">{{ $transaction -> description }}</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="panel-list">
                        <li class="col-md-6">
                            <p><b>Operación</b></p>
                            <p class="text-capitalize">@if ($transaction -> operation = 1) <span class="label label-success">Aporte</span> @else <span class="label label-danger">Canje</span> @endif</p>
                        </li>
                        <li class="col-md-6">
                            <p><b>Puntos</b></p>
                            <p class="text-capitalize">{{ $transaction -> points }}</p>
                        </li>
                        <li>
                            <p><b>Fecha</b></p>
                            <p class="text-capitalize">{{ date_format($transaction -> created_at, 'd/m/Y h:i:s A') }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
@endsection
