@extends('layouts.admin.default')

@section('title', 'Categoría')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{route('categories.index')}}" type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{route('categories.edit', $category->id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                     <ul class="panel-list">
                        <li>
                            <p><b>Nombre</b></p>
                            <p class="text-capitalize">{{ $category -> name }}</p>
                        </li>
                        <li>
                            <p><b>Descripción</b></p>
                            <p class="text-capitalize">{{ $category -> description }}</p>
                        </li>
                        <li>
                            <p><b>Slug</b></p>
                            <p class="text-capitalize">{{ $category -> slug }}</p>
                        </li>
                        <li>
                            <p><b>Estatus</b></p>
                            <p>@if ( $category -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span> @endif</p>
                        </li>
                        <li class="col-xs-12 col-sm-6">
                            <p><b>Fecha de creación</b></p>
                            <p>{{ date_format($category -> created_at, 'd/m/Y h:i:s A') }}</p>
                        </li>
                        <li class="col-xs-12 col-sm-6">
                            <p><b>Fecha de modificación</b></p>
                            <p>{{ date_format($category -> updated_at, 'd/m/Y h:i:s A') }}</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="from-group">
                        <div class="text-center thumbnail mbl">
                            @if (empty($image -> image))
                                <img  class="image-thumbnail max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $category -> name }}">
                            @else
                                <img  class="image-thumbnail max-size" src="{{ $_ENV['STORAGE_PATH'].$image -> image }}" alt="{{ $category -> name }}">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('categories.destroy', $category-> id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Categoría</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar la Categoría <b>{{ $category -> name}}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js-->
@endsection
