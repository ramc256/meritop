@extends('layouts.admin.default')

@section('title', 'Categoría')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Editar</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('categories.update',$category -> id) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts.form-errors')
            <input name="_method" type="hidden" value="PUT">
			<div class="panel">
                <div class="panel-heading clearfix">
                   <div class="pull-right text-center">
                        <a href="{{route('categories.show', $category -> id) }}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-sm-12 col-md-7">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" value="{{$category -> name}}" required="required">
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                            <textarea name="description" class="form-control" id="description" placeholder="Descripcion"" required="required" rows="10">{{ $category -> description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="slug" class="control-label">Slug <span class="text-danger">*</span></label>
                            <input name="slug" type="text" class="form-control" id="slug" placeholder="Slug" required="required" placeholder="Nombre" value="{{$category -> slug}}">
                        </div>
                        <div class="form-group">
                            <label for="estatus" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select id="status" name="status" class="form-control" data-bv-field="status" required="required">
                                <option value="">Seleccione un estatus...</option>
                                <option value="1" @if($category -> status == 1 ) selected="selected" @endif>Activo</option>
                                <option value="0" @if($category -> status == 0 ) selected="selected" @endif>Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="form-group">
                            <div class="text-center thumbnail relative mbl" >
                                @if (empty($image -> image))
                                    <img class="image-thumbnail max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $category -> name }}">
                                @else
                                    <div class="overlay">
                                        <div class="btns">
                                            <button type="button" class="img-product btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-image"><i class="fa fa-close"></i></button>
                                        </div>
                                    </div>
                                    <img class="image-thumbnail max-size" src="{{ $_ENV['STORAGE_PATH'].$image -> image }}" alt="{{ $category -> name }}">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image" class="control-label">Imagen </label>
                            <input id="image" name="image" type="file" class="form-control">
                            @if (!empty($image))
                                <input name="id_image" type="hidden" class="form-control" readonly="readonly" value="{{ $image -> id }}">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('modals')
    <!-- Modal delete image  -->
        @if (!empty($image))
        <!-- Modal -->
        <div class="modal fade" id="delete-image" tabindex="-1" role="dialog" aria-labelledby="delete-image">
            <form action="{{ route('images.destroy') }}" method="post">
                {{ csrf_field() }}
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Eliminar imagen</h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Esta seguro de eliminar la imagen <b>{{ $category -> image }}</b>?</p>
                            <input id="id" name="id" type="hidden" value="{{ $image -> id }}">
                        </div>
                        <div class="modal-footer text-center">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-danger">Si</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- end Modal -->
    @endif
    <!-- End -->
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js - ->
@endsection


