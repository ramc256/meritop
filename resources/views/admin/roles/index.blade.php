@extends('layouts.admin.default')

@section('title', 'Roles')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Gestiones</small></h1>
        </div>
    </header>
    @include('flash::message')
	<article class="content">		
		<div class="panel filterable">            
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left m-t-6">
                </h3>
                <div class="pull-right">
                    <a href="{{ route('roles.create') }}" class="btn btn-success btn-md" id="addButton">Nuevo</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-asc">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $datos)
                            <tr>
                                <td class="text-capitalize"><a href="{{route('roles.show', $datos -> id)}}">{{$datos -> name}}</a></td>
                                <td>{{$datos -> description}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>        
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->
@endsection
