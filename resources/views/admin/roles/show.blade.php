@extends('layouts.admin.default')

@section('title', 'Rol')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                 <div class="pull-right text-center">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" @if( $role -> id == 1) disabled="disabled" @endif>Eliminar</button>
                    <a href="{{route('roles.index')}}" type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{route('roles.edit', $role -> id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                    <ul class="panel-list">
                        <li class="row">
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Nombre de rol</b></p>
                                <p class="text-capitalize">{{ $role -> name }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Tipo</b></p>
                                <p class="text-capitalize">{{ $role -> kind }}</p>
                            </div>
                        </li>
                        <li>
                            <p><b>Descripción</b></p>
                            <p>{{ $role -> description }}</p>
                        </li>
                        <li class="row">
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Fecha de creación</b></p>
                                <p>{{ date_format($role -> created_at, 'd/m/Y') }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Fecha de modificación</b></p>
                                <p>{{ date_format($role -> updated_at, 'd/m/Y') }}</p>
                            </div>
                        </li>
                        <li>
                            <p><b>Privilegios</b></p>
                            <table class="table">
                                <thead>
                                    <th>Modulos</th>
                                    <th>Listar</th>
                                    <th>Ver</th>
                                    <th>Crear</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                    <th>Importar</th>
                                    <th>Exportar</th>
                                </thead>
                                @php $a=0; $b=0; @endphp
                                @foreach($modules as $i)
                                <tr>
                                    <td style="text-transform:capitalize; font-weight:bold;">{{$i->name}}</td>
                                    @foreach($actions as $j)
                                    <td>
                                        @if (count($rma) > 0)
                                            @if(($i -> id == $rma[$b]["fk_id_module"]) && ($j -> id == $rma[$b]["fk_id_action"]))
                                                <i class="fa fa-check text-success"></i>
                                            @else
                                                <i class="fa fa-close text-danger"></i>
                                            @endif
                                            @if(($i -> id == $rma[$b]["fk_id_module"]) && ($j -> id == $rma[$b]["fk_id_action"]) && ($b < count($rma) -1))
                                                @php $b++; @endphp
                                            @endif
                                        @else
                                            <i class="fa fa-close text-danger"></i>
                                        @endif
                                    </td>
                                    @endforeach
                                </tr>
                                @endforeach
                            </table>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('roles.destroy', $role -> id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Rol</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el rol <b>{{ $role -> name}}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end Modal -->
@endsection
