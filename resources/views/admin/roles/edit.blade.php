@extends('layouts.admin.default')

@section('title', 'Rol')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Editar</small></h1>
        </div>
    </header>
    <article class="content">
        <form id="form-validation" action="{{ route('roles.update', $role->id) }}" method="POST" >
            {{ csrf_field() }}
            @include('layouts.form-errors')
            <input type="hidden" name="_method" value="PUT">
                <div class="panel filterable">
                    <div class="panel-heading clearfix">
                        <div class="pull-right text-center">
                            <a href="{{route('roles.show', $role -> id)}}" type="button" class="btn btn-warning">Atrás</a>
                            <button type="submit" class="btn btn-success">Aceptar</button>
                        </div>
                    </div>
                    <div class="panel-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required="required" value="{{ $role->name }}">
                        </div>
                        <div class="form-group">
                            <label for="fk_id_kind_role" class=" control-label">Tipo <span class="text-danger">*</span></label>
                            <select name="fk_id_kind_role" class="form-control text-capitalize" id="fk_id_kind_role" required="required">
                                <option value="">Seleccione un tipo...</option>
                                @foreach ($kind_roles as $element)
                                    <option value="{{ $element -> id }}" @if($role -> fk_id_kind_role) selected="selected" @endif>{{ $element -> name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                            <textarea name="description" class="form-control" id="address" placeholder="Descripcion" required="required"  rows="10" >{{ $role->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <p><b>Privilegios <span class="text-danger">*</span></b></p>
                            <label><input type="checkbox" id="checkTodos" />Marcar/Desmarcar Todos</label>
                            <table class="table">
                                <thead>
                                    <th width="20"><i class="fa fa-check"></i></th>
                                    <th>Modulos</th>
                                    <th>Listar</th>
                                    <th>Ver</th>
                                    <th>Crear</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                    <th>Importar</th>
                                    <th>Exportar</th>
                                </thead>
                                @php $a=0; $b=0; @endphp
                                @foreach($modules as $i)
                                <tr>
                                    <td><input class="seleted-check" type="checkbox" value="{{$i->id}}"></td>
                                    <td style="text-transform:capitalize; font-weight:bold;">{{$i->name}}</td>
                                    @foreach($actions as $j)
                                    <td>
                                        @if (count($rma)>0)
                                            {{-- <input name="{{$i -> id}}/{{$j -> id}}" type="checkbox" @if(($i -> id == $rma[$b]["fk_id_module"]) && ($j -> id == $rma[$b]["fk_id_action"])) checked="checked" @endif> --}}
                                            <input class="{{$i->id}}" name="privileges[]" type="checkbox" @if(($i -> id == $rma[$b]["fk_id_module"]) && ($j -> id == $rma[$b]["fk_id_action"])) checked="checked" @endif value="{{$i -> id}}/{{$j -> id}}">
                                            @if(($i -> id == $rma[$b]["fk_id_module"]) && ($j -> id == $rma[$b]["fk_id_action"]) && ($b < count($rma) -1))
                                                @php $b++; @endphp
                                            @endif
                                        @else
                                            <input  class="{{$i->id}}" name="privileges[]" type="checkbox" value="{{$i -> id}}/{{$j -> id}}">
                                        @endif
                                    </td>
                                    @endforeach
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->
    <script type="text/javascript">

        $(document).ready(function(){
            $("#checkTodos").change(function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });

            $('.seleted-check').change(function () {
                modulo = $(this).val();
                $('.'+modulo).prop('checked', $(this).prop("checked"));
            });
        });
    </script>
        <!-- page validation -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page validation -->
@endsection
