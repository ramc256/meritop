@extends('layouts.admin.default')

@section('title', 'Ver usuario')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Vista de caracteristicas de Usuario</p>
        </div>
    </header>
    <article class="content">
        <div class="col-sm-12 col-md-7">
            <div class="panel filterable">
                <div class="panel-heading">
                    <h3 class="panel-title"> Datos importantes</h3>
                </div>
                <div class="panel-body">
                    <ul class="panel-list">
                        <li>
                            <p><b>Nombre y Apellido</b></p>
                            <p class="text-capitalize">{{ $user[0] -> first_name . ' ' . $user[0] -> last_name }}</p>
                        </li>
                        <li>
                            <p><b>Nombre de Usuario</b></p>
                            <p>{{ $user[0] -> user_name }}</p>
                        </li>
                        <li>
                            <p><b>E-mail</b></p>
                            <p>{{ $user[0] -> email }}</p>
                        </li>
                        <li>
                            <p><b>Contraseña</b></p>
                            <p>*****************</p>
                        </li>                        
                        <li>
                            <p><b>Pais</b></p>
                            <p class="text-capitalize">{{ $user[0] -> country }}</p>
                        </li> 
                        <li>
                            <p><b>Clubs</b></p>
                            @if(count($users_clubs)>0)
                                @foreach($users_clubs as $datos) 
                                    <p class="text-capitalize label left label-primary">{{ $datos -> name }}</p>
                                @endforeach
                            @else
                                <p class="text-danger">Este usuario no esta asociado a ningun club</p>
                            @endif
                        </li>
                    </ul>
                </div>
                 <div class="panel-footer text-center">               
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" @if( $offer -> id == 1) disabled="disabled" @endif>Eliminar</button> 
                    <a href="{{route('offers.index')}}" type="button" class="btn btn-warning">Regresar</a>  
                    <a href="{{route('offers.edit', $offer->id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5">
            <div class="panel filterable">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6">
                         Datos complementarios 
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="text-center mbl">
                            <!-- Button trigger modal -->
                            <a type="button"  data-toggle="modal" data-target="#myModal">
                                <img class="img-circle img-bor" src="{{ asset('images/avatar1.jpg') }}"/>
                            </a>
                        </div>
                    </div>
                    <ul class="panel-list">
                        <li>
                            <p><b>Tipo de usuario</b></p>
                            <p class="text-capitalize">@if( $user[0] -> kind == 0) Meritop @else Club @endif</p>
                        </li>
                        <li>
                            <p><b>Rol</b></p>
                            <p class="text-capitalize">{{ $user[0] -> role }}</p>
                        </li>
                        <li>
                            <p><b>Estatus</b></p>
                            <p class="text-capitalize">@if( $user[0] -> kind == 0) Inactivo @else Activo @endif</p>
                        </li>                      
                        <li>
                            <p><b>Fecha de Creación</b></p>
                            <p>{{ $user[0] -> created_at }}</p>
                        </li>
                        <li>
                            <p><b>Fecha de Actualización</b></p>
                            <p>{{ $user[0] -> updated_at }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
@section('modals')
    <!-- modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <form action="" method="post" enctype="multipart/form-data">    
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cambiar imagen</h4>
                    </div>
                    <div class="modal-body">      
                        <div class="form-group">
                            <label for="iamge" class="col-md-3 control-label">Imagen</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input name="image" type="file" class="form-control" id="image">
                                </div>
                            </div>
                        </div>                 
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Aceptar</button>
                    </div>
                </div>
            </div>            
        </form>
    </div>
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('offers.destroy', $offer->id) }}" method="POST"> 
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Oferta</h4>
                    </div>
                    <div class="modal-body">     
                        <p>¿Esta seguro de eliminar el Oferta <b>{{ $offer -> name}}</b>?</p>               
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>            
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scritps')
    <!-- Page js -->
    <script>        
        $('#myModal').on('shown.bs.modal', function () {
          $('#myInput').focus()
        })
    </script>
    <script>        
        $('#modal-delete').on('shown.bs.modal', function () {
          $('#modal-delete').focus()
        })
    </script>
    <!-- end page js -->
@endsection