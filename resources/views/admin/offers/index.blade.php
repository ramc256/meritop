@extends('layouts.admin.default')

@section('title', 'Ofertas')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/dashboard.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Modulo para la gestion de @yield('title')</p>
        </div>
    </header>
	<article class="content">
		<div class="col-md-12 col-lg-8">
			<div class="panel filterable">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6">
                         Lista de Ofertas
                    </h3>
                    <div class="pull-right">
                        <a href="{{ route('offers.create') }}" class="btn btn-meritop btn-sm" id="addButton">Nuevo</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover datatable-asc">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($offers as $datos)
                                    <tr>
                                        <td class="text-capitalize"><a href="{{route('offers.show', $datos -> id)}}">{{ $datos -> name }}</a></td>
                                        <td class="text-capitalize">{{$datos -> description}}</td>
                                        <td>{{ $datos -> status }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title"> Datos de Ofertas</h3>
                </div>
                <div class="panel-body real-time">
                    <div class="total-visits">
                        <span>23</span>
                        <small>Ofertas</small>
                        <div class="progress">
                            <div role="progressbar" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 33%;" class="progress-bar progress-bar-info">
                            </div>
                        </div>
                    </div>
                    <div class="total-visits">
                        <span>11</span>
                        <small>Ofertas</small>
                        <div class="progress">
                            <div role="progressbar" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 55%;" class="progress-bar progress-bar-primary">
                            </div>
                        </div>
                    </div>
                    <div class="total-visits">
                        <span>31</span>
                        <small>Ofertas</small>
                        <div class="progress">
                            <div role="progressbar" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 77%;" class="progress-bar progress-bar-danger">
                            </div>
                        </div>
                    </div>
                    <div class="visit-source">
                        <div class="visit-count pull-left">
                            <small>82 <span>Direct</span></small>
                            <div class="progress">
                                <div role="progressbar" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100"
                                     style="width: 29.1%;" class="progress-bar progress-bar-primary">
                                </div>
                            </div>
                        </div>
                        <div class="visit-count pull-left">
                            <small>156 <span>Search</span></small>
                            <div class="progress">
                                <div role="progressbar" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100"
                                     style="width: 70.9%;" class="progress-bar progress-bar-danger">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</article>
@stop

@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->
@endsection
