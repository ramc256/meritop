@extends('layouts.admin.default')

@section('title', 'Nuevo Oferta')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Seccion para crear Ofertas</p>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('offers.store') }}" method="POST" >
            {{ csrf_field() }}
    		<div class="col-sm-12 col-md-7">
    			<div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title">Datos importantes</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input id="name" name="name" placeholder="Nombre" maxlength="40" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="excerpt" class="control-label">Estracto <span class="text-danger">*</span></label>
                            <span class="input-group-addon"><i class="fa fa-fw fa-user-md"></i></span>
                            <input id="excerpt" name="excerpt" placeholder="Estracto" maxlength="250" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                            <textarea id="description" name="description" placeholder="Descripción" maxlength="250" class="form-control" required="required"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="control-label">Cantidad <span class="text-danger">*</span></label>
                            <input id="quantity" name="quantity" placeholder="Cantidad" maxlength="10" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="quantity_per_person" class="control-label">Cantidad por persona <span class="text-danger">*</span></label>
                            <input id="quantity_per_person" name="quantity_per_person" placeholder="Cantidad por persona" maxlength="10" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="frecuency" class="control-label">Frecuencia <span class="text-danger">*</span></label>
                            <input id="frecuency" name="frecuency" placeholder="Frecuencia" maxlength="10" type="number" class="form-control" required="required">
                        </div>
                    </div> 
                    <div class="panel-footer text-center">
                        <a href="{{route('offers.index')}}" type="button" class="btn btn-warning">Regresar</a>  
                        <button type="reset" class="btn btn-default bttn_reset">Limpiar</button>
                        <button type="submit" class="btn btn-primary">Aceptar</button>  
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-5">      
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos complementarios</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="star_date" class="control-label">Fecha de inicio <span class="text-danger">*</span></label>
                            <input id="star_date" name="star_date" placeholder="Fecha de inicio" type="date" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="due_date" class="control-label">Fecha de culminación <span class="text-danger">*</span></label>
                            <input id="due_date" name="due_date" placeholder="Fecha de culminación" type="date" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="star_hour" class="control-label">Horario <span class="text-danger">*</span></label>
                            <div class="col-md-5">
                                    <input id="star_hour" name="star_hour" placeholder="Inicio" maxlength="10" type="number" class="form-control" required="required">
                            </div>
                            <div class="col-md-4">
                                    <input id="due_hour" name="due_hour" placeholder="Fin" maxlength="10" type="number" class="form-control" required="required">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="slug" class="control-label">Slug <span class="text-danger">*</span></label>
                            <input id="slug" name="slug" placeholder="Slug" maxlength="120" type="text" class="form-control" required="required">
                        </div>              
                        <div class="form-group">
                            <label for="iamge" class="control-label">Imagen</label>
                            <input name="image" type="file" class="form-control" id="image">
                        </div> 
                        <div class="form-group">
                            <label for="estatus" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select id="status" name="status" class="form-control" data-bv-field="status" required="required">
                                <option value="">
                                    Seleccione un estatus...
                                </option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('core-plus-theme/js/password-validator.js')}}"></script>
@endsection
