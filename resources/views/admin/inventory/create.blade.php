@extends('layouts.admin.default')

@section('title', 'Nuevo Producto')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Sección para crear Producto</p>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-sm-12 col-md-6">
                <div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title"> Datos importantes</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" id="name" placeholder="Nombre" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="excerpt" class="control-label">Extracto <span class="text-danger">*</span></label>
                            <input id="excerpt" name="excerpt" placeholder="Estracto" maxlength="45" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="model" class="control-label">Modelo <span class="text-danger">*</span></label>
                            <input id="model" name="model" placeholder="Modelo" maxlength="100" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                            <textarea id="description" name="description" placeholder="Descripcion" maxlength="250" class="form-control" required="required" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="brand" class="control-label">Marcas <span class="text-danger">*</span></label>
                            @if (count($brands)>0)
                                <select name="fk_id_brand" id="fk_id_brand" class="form-control" required="required">
                                    <option value="">Seleccione un marcas...</option>
                                    @foreach ($brands as $brand)
                                        <option value="{{ $brand -> id }}">{{ $brand -> name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <p class="text-danger">No hay marcas registradas</p>
                                <p>Para crear una marca haga click <a href="{{ url('admin/brands/create') }}">aquí</a></p>
                            @endif
                        </div>
                         <div class="form-group">
                            <label for="fk_id_currency" class="control-label">Moneda <span class="text-danger">*</span></label>
                            <select id="fk_id_currency" name="fk_id_currency" class="form-control" required="required">
                                <option value="">Seleccione una moneda...</option>
                                @foreach($currencies as $currency)
                                    <option value="{{ $currency->id }}" @isset($club)@if($club -> fk_id_currency == $currency -> id) selected="selected" @endif @endisset>{{ $currency -> currency_code . ' - ' .$currency -> currency }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="delivery-conditions" class="control-label">Condiciones de Envio <span class="text-danger">*</span></label>
                            <textarea id="delivery-conditions" name="delivery_conditions" placeholder="Condiciones de Envio" maxlength="250" class="form-control" required="required" rows="10"></textarea>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="" class="control-label">Categorias <span class="text-danger">*</span></label>
                            @php
                                $aux = '';
                            @endphp
                            @foreach ($subcategories as $category)
                                @if ($aux != $category -> category)
                                    <div class="col-md-12" style="display:block; margin-top:10px; border-bottom:1px solid #e1e1e1;">
                                    <h4 class="text-capitalize" style="display:block;">{{ $category -> category }}</h4>
                                </div>
                                    @php
                                        $aux = $category -> category;
                                    @endphp
                                @endif
                                <div class="input-group float-box-l">
                                    <input id="{{'fk_id_subcategory/'.$category->id}}" name="{{'fk_id_subcategory/'.$category->id}}" type="checkbox" >
                                    <label class="text-capitalize label left label-primary" for="{{ $category -> name }}"> {{ $category -> name }}</label>
                                </div>
                            @endforeach
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="" class="control-label">Clubs <span class="text-danger">*</span></label>
                            @if (count($clubs)>0)
                                @foreach ($clubs as $club)
                                    <div class="input-group float-box-l">
                                        <input id="{{ 'fk_id_club/'.$club -> id }}" name="{{ 'fk_id_club/'.$club -> id }}" type="checkbox" >
                                        <label class="text-capitalize label left label-primary" for="{{ $club -> name }}"> {{ $club -> name }}</label>
                                    </div>
                                @endforeach
                            @else
                                <p class="text-danger">No hay clubs registrados</p>
                                <p>Para registrar un club haga click <a href="{{ url('admin/clubs/create') }}">aquí</a></p>
                            @endif
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="warranty" class="control-label">Garantía</label>
                            <input id="warranty" name="warranty" type="checkbox">
                            <label for="warranty" class="text-primary"> El producto posee garantía</label>
                        </div>
                        <div id="data_warranty" class="hidden">
                            <div class="form-group">
                                <label for="warranty_conditons" class="control-label">Condiciones de Garantía</label>
                                <textarea id="warranty_conditions" name="warranty_conditions" placeholder="Condiciones de Garantia" maxlength="250" class="form-control" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="warranty_term" class="control-label">Meses de Garantía</label>
                                <input id="warranty_day" name="warranty_day" placeholder="Tiempo de Garantia" type="number" maxlength="8" class="form-control">
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="{{route('products.index')}}" type="button" class="btn btn-warning">Regresar</a>
                            <button type="reset" class="btn btn-default bttn_reset">Limpiar</button>
                            <button type="submit" class="btn btn-primary">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title"> Datos complementarios</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="sku" class="control-label">SKU <span class="text-danger">*</span></label>
                            <input name="sku" id="sku" placeholder="SKU" maxlength="15" type="text" class="form-control"  required="required">
                        </div>
                        <div class="form-group">
                            <label for="barcode" class="control-label">Código de Barras</label>
                            <input id="barcode" name="barcode" placeholder="Codigo de barras" maxlength="15" type="text" class="form-control">>
                        </div>
                        <div class="form-group">
                            <label for="image" class="control-label">Imagen</label>
                            <input name="image[]" type="file" class="form-control" id="image" placeholder="Url de Imagen" multiple>
                        </div>
                        <div class="form-group">
                            <label for="size" class="control-label">Tamaño <span class="text-danger">*</span></label>
                            <input id="size" name="size" placeholder="Tamaño" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="color" class="control-label">Color <span class="text-danger">*</span></label>
                            <input id="color" name="color" placeholder="Color" maxlength="6" type="text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="measure" class="control-label">Medida <span class="text-danger">*</span></label>
                            <input id="measure" name="measure" placeholder="Medida" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="weight" class="control-label">Peso <span class="text-danger">*</span></label>
                            <input id="weight" name="weight" placeholder="Peso" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="width" class="control-label">Ancho <span class="text-danger">*</span></label>
                            <input id="width" name="width" placeholder="Ancho" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="heigth" class="control-label">Alto <span class="text-danger">*</span></label>
                            <input id="heigth" name="heigth" placeholder="Alto" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="depth" class="control-label">Profundidad <span class="text-danger">*</span></label>
                            <input id="depth" name="depth" placeholder="Profundidad" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="volume" class="control-label">Volumen <span class="text-danger">*</span></label>
                            <input id="volume" name="volume" placeholder="Volumen" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="poins" class="control-label">Puntos <span class="text-danger">*</span></label>
                            <input id="points" name="points" placeholder="Puntos" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label for="market" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select name="status" id="status" class="form-control" required="required">
                                <option value="">Seleccione un estatus</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="warranty" class="control-label">Vender fuera de Stock</label>
                            <input id="sell_out_stock" name="sell_out_stock" type="checkbox">
                        </div>
                        <div class="form-group">
                            <label for="brand" class="control-label">País <span class="text-danger">*</span></label>
                            <select name="fk_id_country" id="fk_id_country" class="form-control" required="required">
                                <option value="">Seleccione un país...</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country -> id }}">{{ $country -> name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/js/password-validator.js')}}"></script>
    <script>
    $('#warranty').change(function(){
     if($(this).prop("checked")) {
        $('#data_warranty').removeClass("hidden").addClass("visible");
    } else {
        $('#data_warranty').removeClass("visible").addClass("hidden");
     }
    });
    </script>
    <!-- end page js -->
@endsection
