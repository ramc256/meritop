@extends('layouts.admin.default')

@section('title', 'Editar Producto')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Sección para editar Producto</p>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('products.update', $product[0]->id) }}" method="POST"  enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="col-sm-12 col-md-6">
                <div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title"> Datos importantes</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" id="name" placeholder="Nombre" type="text" class="form-control" required="required" value="{{ $product[0] -> name }}">
                        </div>
                        <div class="form-group">
                            <label for="excerpt" class="control-label">Extracto <span class="text-danger">*</span></label>
                            <input id="excerpt" name="excerpt" placeholder="Estracto" maxlength="45" type="text" class="form-control" required="required" value="{{ $product[0] -> excerpt }}">
                        </div>
                        <div class="form-group">
                            <label for="model" class="control-label">Modelo <span class="text-danger">*</span></label>
                            <input id="model" name="model" placeholder="Modelo" maxlength="100" type="text" class="form-control" required="required" value="{{ $product[0] -> model }}">
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                            <textarea id="description" name="description" placeholder="Descripcion" maxlength="250" class="form-control" required="required" rows="10">{{ $product[0] -> description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="brand" class="control-label">Marcas <span class="text-danger">*</span></label>
                            @if (count($brands)>0)
                                <select name="fk_id_brand" id="fk_id_brand" class="form-control" required="required">
                                    <option value="">Seleccione un marcas...</option>
                                    @foreach ($brands as $brand)
                                        <option value="{{ $brand -> id }}" @if ($brand -> id ==  $product[0] -> id_brand ) selected="selected" @endif>{{ $brand -> name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <p class="text-danger">No posee marca</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="market" class="control-label">Mercado <span class="text-danger">*</span></label>
                            <select name="market" id="market" class="form-control" required="required">
                                <option value="">Seleccione un mercado...</option>
                                <option value="0" @if ($product[0] -> market == 0) selected="selected" @endif>Ambos</option>
                                <option value="1" @if ($product[0] -> market == 1) selected="selected" @endif>Hombre</option>
                                <option value="2" @if ($product[0] -> market == 2) selected="selected" @endif>Mujer</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="delivery-conditions" class="control-label">Condiciones de Envio <span class="text-danger">*</span></label>
                            <textarea id="delivery-conditions" name="delivery_conditions" placeholder="Condiciones de Envio" maxlength="250" class="form-control" required="required" rows="10"> {{ $product[0] -> delivery_conditions }}</textarea>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="" class="control-label">Categorías <span class="text-danger">*</span></label>  
                            @php
                                $aux = '';
                            @endphp
                            @foreach ($subcategories as $category)
                                @if ($aux != $category -> category)
                                    <div class="col-md-12" style="display:block; margin-top:10px; border-bottom:1px solid #e1e1e1;">
                                        <h4 class="text-capitalize" style="display:block;">{{ $category -> category }}</h4>
                                    </div>
                                    @php
                                        $aux = $category -> category;
                                    @endphp
                                @endif
                                    <div class="input-group float-box-l">
                                        <input id="{{'fk_id_subcategory/'.$category->id}}" name="{{'fk_id_subcategory/'.$category->id}}" type="checkbox" @foreach ($products_subcategories as $value)@if ($value-> fk_id_subcategory == $category -> id) checked="checked" value="on" @endif @endforeach>
                                        <label class="text-capitalize label left label-primary" for="{{ $category -> name }}"> {{ $category -> name }}</label>
                                    </div>
                            @endforeach
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="" class="control-label">Clubs <span class="text-danger">*</span></label>
                            @if (count($clubs)>0)
                                @foreach ($clubs as $club)
                                    <div class="input-group float-box-l">
                                        <input id="{{ 'fk_id_club/'.$club -> id }}" name="{{ 'fk_id_club/'.$club -> id }}" type="checkbox" @if ($clubs_products[0] -> fk_id_club == $club -> id) checked="checked" value="on" @endif>
                                        <label class="text-capitalize label left label-primary" for="{{ $club -> name }}"> {{ $club -> name }}</label>
                                    </div>  
                                @endforeach
                            @else
                                <p class="text-danger">No hay clubs registrados</p>
                                <p>Para registrar un club haga click <a href="{{ url('admin/clubs/create') }}">aquí</a></p>
                            @endif
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="warranty" class="control-label">Garantia</label>
                            <input id="warranty" name="warranty" type="checkbox" @if ($product[0] -> warranty == 1) checked="checked" value="on" @endif>
                            <label for="warranty" class="text-primary"> El producto posee garantía</label>
                        </div>
                        <div id="data_warranty" class="@if ($product[0] -> warranty == 1) visible @else hidden @endif">
                            <div class="form-group">
                                <label for="warranty_conditons" class="control-label">Condiciones de Garantía</label>
                                <textarea id="warranty_conditions" name="warranty_conditions" placeholder="Condiciones de Garantia" maxlength="250" class="form-control" rows="10" @if ($product[0] -> warranty == 1) required="required" @endif> {{ $product[0] -> warranty_conditions }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="warranty_term" class="control-label">Meses de Garantía</label>
                            <input id="warranty_day" name="warranty_day" placeholder="Tiempo de Garantia" type="number" maxlength="8" class="form-control" value="{{ $product[0] -> warranty_day }}" @if ($product[0] -> warranty == 1) required="required" @endif>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <a href="{{route('products.index')}}" type="button" class="btn btn-warning">Regresar</a>
                        <button type="reset" class="btn btn-default bttn_reset">Limpiar</button>
                        <button type="submit" class="btn btn-primary">Aceptar</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title"> Datos complementarios</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="sku" class="control-label">SKU <span class="text-danger">*</span></label>
                            <input name="sku" id="sku" placeholder="SKU" maxlength="15" type="text" class="form-control"  required="required" value="{{ $variant[0] -> sku }}">
                        </div>
                        <div class="form-group">
                            <label for="barcode" class="control-label">Código de Barras</label>
                            <input id="barcode" name="barcode" placeholder="Codigo de barras" maxlength="15" type="text" class="form-control" value="{{ $variant[0] -> barcode }}">
                        </div>
                        <div class="form-group">
                            <label for="size" class="control-label">Tamaño <span class="text-danger">*</span></label>
                            <input id="size" name="size" placeholder="Tamaño" maxlength="8" type="number" class="form-control" required="required" value="{{ $variant[0] -> size }}">
                        </div>
                        <div class="form-group">
                            <label for="color" class="control-label">Color <span class="text-danger">*</span></label>
                            <input id="color" name="color" placeholder="Color" maxlength="6" type="text" class="form-control" required="required" value="{{ $variant[0] -> color }}">
                        </div>
                        <div class="form-group">
                            <label for="measure" class="control-label">Medida <span class="text-danger">*</span></label>
                            <input id="measure" name="measure" placeholder="Medida" maxlength="8" type="number" class="form-control" required="required" value="{{ $variant[0] -> measure }}">
                        </div>
                        <div class="form-group">
                            <label for="weight" class="control-label">Peso <span class="text-danger">*</span></label>
                            <input id="weight" name="weight" placeholder="Peso" maxlength="8" type="number" class="form-control" required="required" value="{{ $variant[0] -> weight }}">
                        </div>
                        <div class="form-group">
                            <label for="width" class="control-label">Ancho <span class="text-danger">*</span></label>
                            <input id="width" name="width" placeholder="Ancho" maxlength="8" type="number" class="form-control" required="required" value="{{ $variant[0] -> width }}">
                        </div>
                        <div class="form-group">
                            <label for="heigth" class="control-label">Alto <span class="text-danger">*</span></label>
                            <input id="heigth" name="heigth" placeholder="Alto" maxlength="8" type="number" class="form-control" required="required" value="{{ $variant[0] -> heigth }}">
                        </div>
                        <div class="form-group">
                            <label for="depth" class="control-label">Profundidad <span class="text-danger">*</span></label>
                            <input id="depth" name="depth" placeholder="Profundidad" maxlength="8" type="number" class="form-control" required="required" value="{{ $variant[0] -> depth }}">
                        </div>
                        <div class="form-group">
                            <label for="volume" class="control-label">Volumen <span class="text-danger">*</span></label>
                            <input id="volume" name="volume" placeholder="Volumen" maxlength="8" type="number" class="form-control" required="required" value="{{ $variant[0] -> volume }}">
                        </div>
                        <div class="form-group">
                            <label for="points" class="control-label">Puntos <span class="text-danger">*</span></label>
                            <input id="points" name="points" placeholder="Puntos" maxlength="8" type="number" class="form-control" required="required" value="{{ $variant[0] -> points }}">
                        </div>
                        <div class="form-group">
                            <label for="market" class="control-label">Estatus <span class="text-danger">*</span></label>
                            <select name="status" id="status" class="form-control" required="required">
                                <option value="">Seleccione un estatus</option>
                                <option value="1" @if ($variant[0] -> status == 1) selected="selected" @endif>Activo</option>
                                <option value="0" @if ($variant[0] -> status == 0) selected="selected" @endif>Inactivo</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="warranty" class="control-label">Vender fuera de Stock</label>
                            <input id="sell_out_stock" name="sell_out_stock" type="checkbox"  @if ($variant[0] -> sell_out_stock == 1) checked="checked" value="on" @endif>
                        </div>
                        <div class="form-group">
                            <label for="brand" class="control-label">País <span class="text-danger">*</span></label>
                            <select name="fk_id_country" id="fk_id_country" class="form-control" required="required">
                                <option value="">Seleccione un país...</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country -> id }}"  @if ($variant[0] -> fk_id_country == $country -> id) selected="selected" @endif>{{ $country -> name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/js/password-validator.js')}}"></script>
    <script>
    $('#warranty').change(function(){
        if($(this).prop("checked")) {
    $('#data_warranty').removeClass("hidden").addClass("visible");
        } else {
    $('#data_warranty').removeClass("visible").addClass("hidden");
        }
    });
    </script>
    <!-- end page js -->
@endsection
