@extends('layouts.admin.default')

@section('title', 'Producto')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel filterable">
            <div class="panel-body">
                <ul class="panel-list">
                <li class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <p><b>Nombre</b></p>
                        <p>{{ $parent_product -> name }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <p><b>Marca</b></p>
                        <p>{{ $parent_product -> brand }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <p><b>Modelo</b></p>
                        <p>{{ $parent_product -> model }}</p>
                    </div>
                </li>
                </ul>
            </div>
        </div>
        <form action="{{ route('inventory.update', $parent_product -> id) }}" method="post">
            {{ csrf_field() }}
            @include('layouts.form-errors')
            <input name="_method" type="hidden" value="PUT">
            <input name="fk_id_product" type="hidden" value="{{$parent_product -> id}}">
            <div class="panel filterable">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6"> Variantes</h3>
                    <div class="pull-right text-center">
                        <a href="{{route('inventory.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button  type="submit" class="btn btn-success">Agregar todos</button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Caracteristica</th>
                                    <th>SKU</th>
                                    <th>Código de barras</th>
                                    <th>Puntos</th>
                                    <th>Estatus</th>
                                    <th class="text-center">
                                        Cantidad
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $datos)
                                    <tr>
                                        <td>{{ $datos-> feature }}</td>
                                        <td><a href="{{route('products.show', $datos -> id)}}">{{ $datos -> sku }}</a></td>
                                        <td>{{ $datos -> barcode }}</td>
                                        <td>{{ number_format($datos -> points, 0 , '', '.') }}</td>
                                        <td>@if ($datos -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span> @endif</td>
                                        <td align="center">
                                            {{ number_format($datos -> quantity_current, 0 , '', '.') }}
                                            <span> <i class="fa fa-plus"></i>
                                                <input class="text-center" name="quantities[]" type="number" style="max-width: 50px;" value="0">
                                                <input name="products[]" type="hidden"  style="width: 30px;" value="{{ $datos -> id }}">
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </article>
@endsection
