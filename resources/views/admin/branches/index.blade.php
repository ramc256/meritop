@extends('layouts.admin.default')

@section('title', 'Sedes')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
         <div class="header-data">
            <h1>@yield('title')<small> - Gestiones</small></h1>
        </div>   
    </header>
    @include('flash::message')
	<article class="content">
		<div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="pull-right">
                   <button class="btn btn-meritop-secundary" type="button"  id="btn-data-import"  data-toggle="modal" data-target="#data-import">Importar</button>
                   <a href="{{ route('branches.create')}}" class="btn btn-success btn-md" id="addButton">Nuevo</a>
                </div>
            </div>
            @include('layouts.form-errors')
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-asc">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Ciudad</th>
                            <th>Estado</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($branches as $element)
                                <tr>
                                    <td><a href="{{route('branches.show', $element -> id)}}">{{ $element -> code }}</a></td>
                                    <td class="text-capitalize">{{$element ->name}}</td>

                                    <td class="text-capitalize">{{ $element -> city  }}</td>
                                    <td class="text-capitalize">{{ $element -> state }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</article>
@stop
@section('modals')
    <div class="modal fade" id="data-import" tabindex="-1" role="dialog" aria-labelledby="data-import">
        <form action="{{ route('branches.import')}}" method="post" enctype="multipart/form-data" style="display: block; margin-top: 20px;">
            {{ csrf_field() }}

            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Importar datos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="archive" class="control-label">Archivo<span class="text-danger">*</span></label>
                            <input name="import_file" type="file" class="form-control" id="archive" required="required">
                            <input name="table" type="hidden" class="form-control" value="members">
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->
@endsection
