@extends('layouts.admin.default')
@section('title', 'Resultado')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
<!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Registros</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">Total de registros </h3>
                 <div class="pull-right text-center">
                    <a href="{{route('branches.index')}}" type="button" class="btn btn-success">Finalizar</a>
                </div>
            </div>
            <div class="panel-body">
               <ul>
                    <li>Total de registro: {{ count($branches_registers) }}</li>
                    <li>Sedes registrados: {{ $registers  }}</li>
                    <li>Sedes no se han registrado: {{ $no_registers }} </li>
               </ul>
            </div>                
        </div>
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left m-t-6">
                    Registro de Sedes
                </h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-asc">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Dirección</th>
                                <th>Teléfono</th>
                                <th>Estatus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($branches_registers as $element)
                                <tr>
                                    <td>{{ $element['name'] }}</td>
                                    <td>{{ $element['address'] }}</td>
                                    <td> {{ $element['phone'] }}</td>
                                    <td>@if( $element['status'] == 1) <span class="label label-success">Importado</span> @else <span class="label label-danger">No importado</span>@endif</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js -->
@endsection
