@extends('layouts.admin.default')

@section('title', 'Sede')
@section('styles')
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Editar</small></h1>
        </div>
    </header>
    @include('flash::message') 
	<article class="content">
        <form id="form-validation" action="{{ route('branches.update', $branch -> id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
         <input name="_method" type="hidden" value="PUT">
			<div class="panel">
               <div class="panel-heading clearfix text-center">
                    <div class="pull-right">
                        <a href="{{route('branches.show', $branch -> id )}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                @include('layouts.form-errors')
                <div class="panel-body row">
                    <div class="col-md-6">
                         <div class="form-group">
                            <label for="code" class="control-label">Código<span class="text-danger">*</span></label>
                            <input name="code" type="text" class="form-control" id="code" placeholder="Código" value="{{$branch -> code}}" required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" value="{{$branch -> name}}" required="required">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="address" class="control-label">Dirección <span class="text-danger">*</span></label>
                            <input name="address" type="text" class="form-control" id="address" placeholder="Dirección" value="{{$branch -> address}}" required="required">
                        </div>    
                    </div>                       
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="countries" class="control-label">País <span class="text-danger">*</span></label>
                            <select id="countries" name="fk_id_country" class="form-control" required="required">
                                <option value="">Seleccione un país...</option> 
                                @foreach($countries as $country)
                                    <option value="{{ $country -> id }}" @if( $branch->id_country == $country->id ) selected="selected" @endif>{{ $country -> name }}</option> 
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">   
                        <div class="form-group">
                            <label for="state" class="control-label">Estado <span class="text-danger">*</span></label>
                             <select id="states" name="fk_id_state" class="form-control" required="required">
                                <option value="">Seleccione un estado...</option> 
                                @foreach($states as $state)
                                    <option value="{{ $state -> id }}" @if( $branch->id_state == $state->id ) selected="selected" @endif>{{ $state -> name }}</option> 
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">                            
                        <div class="form-group">
                            <label for="city" class="control-label">Ciudad <span class="text-danger">*</span></label>
                            <select id="cities" name="fk_id_city" class="form-control" required="required">
                            <option value="">Seleccione un ciudad...</option> 
                                @foreach($cities as $city)
                                    <option value="{{ $city -> id }}" @if( $branch->id_city == $city->id ) selected="selected" @endif>{{ $city -> name }}</option> 
                                @endforeach
                            </select>      
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="postal_code" class="control-label">Zona postal <span class="text-danger">*</span></label>
                            <input name="postal_code" type="text" class="form-control" id="postal_code" placeholder="Zona Postal" value="{{$branch -> postal_code }}" required="required">
                        </div> 
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="dispatch_route" class="control-label">Ruta de despacho<span class="text-danger">*</span></label>
                            <input name="dispatch_route" type="text" class="form-control" id="dispatch_route" placeholder="Ruta de despacho" value="{{$branch -> dispatch_route }}" required="required">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="phone" class="control-label">Teléfono<span class="text-danger">*</span></label>
                            <input name="phone" type="text" class="form-control" id="phone" placeholder="Teléfono" value="{{$branch -> phone }}" required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="secondary_phone" class="control-label">Teléfono secundario<span class="text-danger">*</span></label>
                            <input name="secondary_phone" type="text" class="form-control" id="secondary_phone" placeholder="Teléfono secundario" value="{{$branch -> secondary_phone }}" required="required">
                        </div>
                    </div>                   
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/locations.js') }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/clockface/js/clockface.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/pickers.js') }}"></script>
    <!-- end page js -->
@endsection
