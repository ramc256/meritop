@extends('layouts.admin.default')

@section('title', 'Sede')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Ver</small></h1>
        </div>
    </header>
    @include('flash::message') 
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                <div class="pull-right text-center">               
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{route('branches.index')}}" type="button" class="btn btn-warning">Atrás</a>  
                    <a href="{{route('branches.edit', $branch -> id)}}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                <ul class="panel-list">
                    <li class="row">
                        <div class="col-xs-12 col-sm-6">
                            <p><b>Código</b></p>
                            <p class="text-capitalize">{{ $branch -> code }}</p>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <p><b>Nombre</b></p>
                            <p class="text-capitalize">{{ $branch -> name }}</p>
                        </div>
                    </li>
                    <li class="row">
                        <div class="col-xs-6 col-sm-6">
                            <p><b>Dirección</b></p>
                            <p>{{ $branch -> address }}</p>
                        </div>
                         <div class="col-xs-6 col-sm-6">
                            <p><b>Club</b></p>
                            <p class="text-capitalize">{{ $branch -> club }}</p>
                        </div> 
                    </li>
                    <li class="row">
                        <div class="col-xs-4 col-sm-4">
                            <p><b>Ciudad</b></p>
                            <p>{{ $branch -> city }}</p>
                        </div> 
                        <div class="col-xs-4 col-sm-4">
                            <p><b>Estado</b></p>
                            <p>{{ $branch -> state }}</p>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <p><b>País</b></p>
                            <p>{{ $branch -> country }}</p>
                        </div>
                    </li>
                    <li class="row">
                         <div class="col-xs-6 col-sm-6">
                            <p><b>Zona postal</b></p>
                            <p>{{ $branch -> postal_code }}</p>
                        </div>  
                        <div class="col-xs-6 col-sm-6">
                            <p><b>Ruta de despacho</b></p>
                            <p>{{ $branch -> dispatch_route }}</p>
                        </div> 
                    </li>
                     <li class="row">
                        <div class="col-xs-12 col-sm-6">
                            <p><b>Teléfono</b></p>
                            <p>{{ $branch -> phone }}</p>
                        </div> 
                        <div class="col-xs-12 col-sm-6">
                            <p><b>Teléfono secundario</b></p>
                            <p>{{ $branch -> secondary_phone }}</p>
                        </div>
                    </li> 
                    <li class="row">
                        <div class="col-xs-12 col-sm-6">
                            <p><b>Creación</b></p>
                            <p>{{ $branch -> created_at }}</p>
                        </div>   
                        <div class="col-xs-12 col-sm-6">
                            <p><b>Moficación</b></p>
                            <p>{{ $branch -> updated_at }}</p>
                        </div> 
                    </li>                                               
                </ul>
            </div>
        </div>  
    </article>
@stop
@section('modals')
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('branches.destroy', $branch -> id) }}" method="POST"> 
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Sede</h4>
                    </div>
                    <div class="modal-body">     
                        <p>¿Esta seguro de eliminar la Sede <b>{{ $branch -> name}}</b>?</p>               
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>            
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <!-- Page js-->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->   
@endsection
