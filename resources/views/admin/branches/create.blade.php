@extends('layouts.admin.default')

@section('title', 'Sede')
@section('styles')

@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Nuevo</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('branches.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('layouts.form-errors')
			<div class="panel">
               <div class="panel-heading clearfix">
                    <div class="pull-right">
                        <a href="{{route('branches.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="code" class="control-label">Código<span class="text-danger">*</span></label>
                        <input name="code" type="text" class="form-control" id="code" placeholder="Código" required="required">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                        <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required="required">
                    </div>
                </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="address" class="control-label">Dirección <span class="text-danger">*</span></label>
                            <input name="address" type="text" class="form-control" id="address" maxlength="300" placeholder="Dirección" required="required">
                        </div>
                    </div>                        
                     <div class="col-sm-4">
                        <div class="form-group">
                            <label for="countries" class="control-label">País <span class="text-danger">*</span></label>
                            <select id="countries" name="fk_id_country" class="form-control" required="required">
                                <option value="">Seleccione un país...</option> 
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country -> name }}</option> 
                                @endforeach
                            </select>
                        </div>
                    </div>  
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="state" class="control-label">Estado <span class="text-danger">*</span></label>
                             <select id="states" name="fk_id_state" class="form-control" required="required">
                                <option value="">Seleccione un estado...</option> 
                            </select>
                        </div> 
                    </div> 
                    <div class="col-sm-4">    
                         <div class="form-group">
                            <label for="city" class="control-label">Ciudad <span class="text-danger">*</span></label>
                             <select id="cities" name="fk_id_city" class="form-control" required="required">
                                <option value="">Seleccione una ciudad...</option> 
                            </select>        
                        </div>
                    </div>          
                     <div class="col-sm-6">
                        <div class="form-group">
                            <label for="postal_code" class="control-label">Zona postal <span class="text-danger">*</span></label>
                            <input name="postal_code" type="number" class="form-control" id="postal_code" placeholder="Zona Postal" required="required">
                        </div>
                    </div>
                    <div class="col-sm-6"> 
                        <div class="form-group">
                            <label for="dispatch_route" class="control-label">Ruta de despacho<span class="text-danger">*</span></label>
                            <input name="dispatch_route" type="text" class="form-control" id="dispatch_route" placeholder="Ruta de despacho" required="required">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="phone" class="control-label">Teléfono<span class="text-danger">*</span></label>
                            <input name="phone" type="text" class="form-control" id="phone" placeholder="Teléfono" required="required">
                        </div> 
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="secondary_phone" class="control-label">Teléfono Secundario<span class="text-danger">*</span></label>
                            <input name="secondary_phone" type="text" class="form-control" id="secondary_phone" placeholder="Teléfono" required="required">
                        </div>
                    </div>                    
                </div>
            </div> 
        </form>
	</article>
@stop
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/locations.js') }}"></script>
@endsection
