@extends('layouts.admin.auth')

@section('title', 'Login')
@section('content')
<body class="bg-slider">
    <div class="container" style="padding: 100px;">
        <div class="row" id="form-login" >
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-content">
                <div class="row" id="form-login">
                    <div class="col-md-12">
                        <div class="header mb-4">
                            <h2 class="text-center">
                                <img width="120" src="{{asset('images/logo-white.svg')}}" alt="logo">
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row row-bg-color">
                    <div class="col-md-12 core-login">
                       <form class="form-horizontal" role="form" action="{{ route('admin.login') }}" id="authentication" method="POST">
                            {{ csrf_field() }}
                            @include('flash::message')
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group {{ $errors->has('user_name') ? ' has-error' : '' }}">
                                        <label class="control-label" for="user_name">USUARIO</label>
                                        <div class="input-group">
                                            <input id="user_name" type="text" class="form-control" name="user_name" value="{{ old('user_name') }}" placeholder="Usuario" required autofocus>
                                            @if ($errors->has('user_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('user_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label" for="password">CONTRASE&Ntilde;A</label>
                                        <div class="input-group">
                                            <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> &nbsp;
                                <label for="remember"> Recuérdame </label>
                                <a href="{{ route('admin.password.request') }}" id="forgot" class="text-primary forgot1  pull-right"> ¿Olvidó su contraseña? </a>
                            </div>
                            <div class="form-group ">
                                <button type="submit"  class="btn btn-meritop-primary login-btn btn-lg btn-block">Entrar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection
