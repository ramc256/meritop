@extends('layouts.admin.auth')

@section('title', 'Reset')
@section('content')
<body class="bg-slider">
    <div class="container" style="padding: 100px;">
        <div class="row " id="form-login">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header">
                            <h2 class="text-center">
                                Login
                                <small> de</small>
                                <img src="{{asset('images/logo-white.png')}}" alt="logo">
                            </h2>
                            
                        </div>
                    </div>
                </div>
                <div class="row row-bg-color">
                    <div class="col-md-12 core-login">
                        <form class="form-horizontal" role="form" action="{{ route('admin.password.email') }}" method="POST">
                            {{ csrf_field() }}
                            <h4 class="text-center"> Admin Reset Password</h4>
                            @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label" for="email">E-Mail Address</label>
                                        <div class="input-group">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-meritop-primary login-btn btn-lg btn-block">Enviar email</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
