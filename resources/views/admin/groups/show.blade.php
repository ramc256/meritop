@extends('layouts.admin.default')

@section('title', 'Grupo')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel">
            <div class="panel-heading clearfix">
                <div class="pull-right">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" @if (count($members) > 0) disabled="disabled" title="No puede eliminar grupos asociados a Miembros" @endif>Eliminar</button>

                    <a href="{{ route('groups.index') }}" type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{ route('groups.edit', $group -> id) }}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
                <ul class="panel-list">
                    <li class="row">
                        <div class="col-sm-6 col-md-12 col-lg-6">
                            <p><b>Nombre</b></p>
                            <p class="text-capitalize">{{ $group -> name }}</p>
                        </div>
                        <div class="col-sm-6 col-md-12 col-lg-6">
                            <p><b>Descripción</b></p>
                            <p class="text-capitalize">{{ $group -> description }}</p>
                        </div>
                    </li>
                    <li class="row">
                        <div class="col-sm-6 col-md-12 col-lg-6">
                            <p><b>Creación</b></p>
                            <p class="text-capitalize">{{ date_format($group -> created_at, 'd/m/Y h:i:s A') }}</p>
                        </div>
                        <div class="col-sm-6 col-md-12 col-lg-6">
                            <p><b>Modificación</b></p>
                            <p class="text-capitalize">{{ date_format($group -> updated_at, 'd/m/Y h:i:s A') }}</p>
                        </div>
                    </li>  
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel filterable">
                    <div class="panel-heading clearfix">
                        <h3 class="panel-title pull-left m-t-6">Miembros</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover datatable-asc ">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>DNI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($members) > 0)
                                        @foreach($members as $element)
                                            <tr>
                                                <td><a href="{{route('members.show', $element -> fk_id_member)}}">{{ $element -> first_name }}</a></td>
                                                <td><a href="{{route('members.show', $element -> fk_id_member)}}">{{ $element -> last_name }}</a></td>
                                                <td>{{ $element -> dni }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9"><p class="text-center">No hay Miembros registrados para este grupo</p></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('groups.destroy', $group -> id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar grupo</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el grupo <b>{{ $group -> name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    </script>
    <!-- end page js -->
@endsection
