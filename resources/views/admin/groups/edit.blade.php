@extends('layouts.admin.default')

@section('title', 'Grupo')
@section('styles')
    <!-- Page Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/select2/css/select2-bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/iCheck/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/clockpicker/css/bootstrap-clockpicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/bootstrap-fileinput/css/fileinput.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/custom_css/wizard.css') }}">
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Editar</small></h1>
        </div>
    </header>
    <article class="content">
        @include('layouts.form-errors')
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Gestión del grupo</h3>
            </div>
            <div class="panel-body">
                <div class="stepwizard">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step">
                            <a href="#step-1" class="btn btn-primary btn-circle">1</a>
                            <p>Paso 1</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-2" class="btn btn-default btn-circle">2</a>
                            <p>Paso 2</p>
                        </div>
                        {{-- <div class="stepwizard-step">
                            <a href="#step-3" class="btn btn-default btn-circle">3</a>
                            <p>Paso 3</p>
                        </div> --}}
                    </div>
                </div>
                <form id="form-validation" action="{{ route('groups.update',  $group -> id  ) }}" method="POST" >
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="col-md-12 setup-content" id="step-1">
                        <h3> <b>Paso 1:</b> Datos del grupo</h3>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input id="name" name="name" placeholder="Nombre del grupo" maxlength="100" type="text" class="form-control" required="required" value="{{ $group -> name }}" />
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Descripción <span class="text-danger">*</span></label>
                            <textarea id="description" name="description" placeholder="Descripción" maxlength="65000" class="form-control" rows="10" required="required">{{ $group -> description }}</textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary nextBtn pull-right" type="button"> Siguiente </button>
                        </div>
                    </div>
                    <div class="col-md-12 setup-content" id="step-2">         
                        <h3> <b>Paso 2:</b> Lista de usuarios</h3>
                        <div class="form-inline" style="display: block; overflow: hidden;">
                            <div class="form-group pull-left">
                                <span class="control-label"><font size=3>Departamento</font></span>
                                <select  class="text-capitalize" id="filterInput" name="departments" class="form-control" required="required">
                                        <option class="text-capitalize" value="all">Todos</option>
                                    @foreach($departments as $department)
                                        <option class="text-capitalize" value="{{ $department -> name }}">{{ $department -> name }}</option>
                                     @endforeach
                                </select>
                            </div>
                        </div>
                        </br>
                        <div class="table-responsive">
                            <table id="filter-table" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="checkTodos"/></th>
                                       
                                        <th>DNI</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Departamento</th>   
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($members as $element)
                                       <tr>
                                            <td>
                                                <input id="{{ $element -> first_name . $element -> id }}" type="checkbox" name="members[]" value="{{ $element -> id }}" @foreach ($members_groups as $association) @if ($element -> id == $association -> fk_id_member ) checked="checked" @endif @endforeach  class="seleted-check">
                                            </td>
                                            <td>{{ $element -> dni }}</td>
                                            <td class="text-capitalize"><label style="display: block; width: 100%; height: 100%; cursor: pointer;" for="{{ $element -> first_name . $element -> id }}">{{ $element -> first_name }}</label></td>
                                            <td class="text-capitalize"><label style="display: block; width: 100%; height: 100%; cursor: pointer;" for="{{ $element -> first_name . $element -> id }}">{{ $element -> last_name }}</label></td>
                                            <td class="text-capitalize filter-column">{{ $element -> name }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary prevBtn pull-left" type="button"> Anterior </button>
                            <button class="btn btn-success pull-right" type="submit">Finalizar!</button>
                        </div>
                    </div>
                    {{-- <div class="col-md-12 setup-content" id="step-3">
                        <h3> Paso 3</h3>
                        <div class="form-group">
                            <label for="acceptTerms1">
                                <input id="acceptTerms1" name="acceptTerms" type="checkbox" class="custom-checkbox" required="required"> Todos los datos son correctos.
                            </label>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary prevBtn pull-left" type="button">Anterior</button>
                            <button class="btn btn-success pull-right" type="submit">Finalizar!</button>
                        </div>
                    </div> --}}
                </form>
            </div>
        </div>
    </article>
@stop
@section('scripts')
     {{-- check all --}}
     <script type="text/javascript">

        $(document).ready(function(){

            $('#filter-table').dataTable({
                "paginate": false,
                "info":     false

            });
            $("#checkTodos").change(function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });

            $('.seleted-check').change(function () {
                modulo = $(this).val();
                $('.'+modulo).prop('checked', $(this).prop("checked"));
            });
        });
    </script>
    {{-- End check all --}}
     <!-- page validation -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page validation -->
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('assets/jquery.filters.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/select2/js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/form_wizards.js') }}" ></script>
@endsection
