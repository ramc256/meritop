@extends('layouts.admin.default')

@section('title', 'Cuentas')
@section('styles')
    <!--DataTable css -->
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>

    <!--Datepicker css -->
    <link rel="stylesheet" href="{{ asset('core-plus-theme/vendors/datetime/css/jquery.datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/airdatepicker/css/datepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/datepicker.css') }}">
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Aporte de puntos</small></h1>
        </div>
    </header>
    @include('flash::message')
	<article class="content">
		<div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="btns-header">
                    <button  data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-1" type="button" class="btn btn-labeled btn-meritop-primary">
                        <span class="btn-label"><i class="glyphicon glyphicon-chevron-down"></i></span> Filtros
                    </button>
                    <div class="pull-right">
                        <button class="btn btn-meritop-secundary" type="button"  id="btn-data-import"  data-toggle="modal" data-target="#data-import">Importar</button>
                        {{-- <button class="btn btn-meritop-secundary" type="button"  id="btn-data-export"  data-toggle="modal" data-target="#data-export">Exportar</button> --}}
                    </div>
                </div>
                <div class="panel-faq">
                    <form method="post" action=" {{ route('payload.filter') }} ">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="get">
                        <div id="faq-cat-1-sub-1" class="collapse">
                            <div class="panel-body">
                                <div class="form-inline" style="display: block; overflow: hidden;">
                                    <div class="form-group pull-left">
                                        <label class="control-label" for="dateranges">Rango de fecha: </label>
                                        <input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" class="form-control" id="dateranges" readonly="readonly" value="{{ date('01/m/Y') }} - {{ date('d/m/Y') }}"/>
                                        <button type="submit" class="btn btn-meritop-primary btn-md" id="addButton">Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="customers" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th style="width: 10%"># ID</th>
                                <th style="width: 10%">Fecha</th>
                                <th style="width: 15% ">IP</th>
                                <th style="width: 30%">Nombre de archivo</th>
                                <th style="width: 25%">Usuario</th>
                                <th style="width: 25%">Puntos aportados</th>
                                <th style="width: 10%">Estatus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($contributions))
                                @foreach($contributions as $element)

                                    <tr>
                                        <td><a href="{{route('payload.show', $element -> id)}}">{{ str_pad( $element -> id, 10 , 0, STR_PAD_LEFT) }}</td>
                                        <td>{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}</td>
                                        <td>{{ $element -> ip_address }}</td>
                                        <td>{{ $element -> file_name }}</td>
                                        <td>{{ $element -> first_name . ' ' . $element -> last_name }}</td>
                                        <td>{{ $element -> total_points }}</td>
                                        <td>@if ($element -> status == 1) <span class="label label-success">Exitoso</span> @else <span class="label label-danger">Fallido</span> @endif</td>
                                    </tr>

                                @endforeach
                            @endif
                            <h5 class="pull-right">Total de puntos aportados:<span><b> {{ $sum_points}}</b></span></h5>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</article>
@stop
@section('modals')
    <div class="modal fade" id="data-import" tabindex="-1" role="dialog" aria-labelledby="data-import">
        <form action="{{ route('payload.store')}}" method="post" enctype="multipart/form-data" style="display: block; margin-top: 20px;">
            {{ csrf_field() }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" id="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Importar datos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="archive" class="control-label">Archivo <span class="text-danger">*</span></label>
                            <input name="import_file" type="file" class="form-control" id="file" required="required">
                            <input name="table" type="hidden" class="form-control" value="members">
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-warning" id="boton1" data-dismiss="modal">Atrás</button>
                        <button type="submit" class="btn btn-success" id="boton">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- <div class="modal fade" id="data-export" tabindex="-1" role="dialog" aria-labelledby="data-export">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Exportar datos</h4>
                </div>
                <div class="modal-body text-center">
                    <button type="button" class="btn btn-primary" onClick="$('#customers').tableExport({type:'excel',excelFileFormat:'xmls'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</button>
                    <button type="button" class="btn btn-primary" onClick="$('#customers').tableExport({type:'csv'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> CSV</button>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                </div>
            </div>
        </div>
    </div> --}}
@stop
@section('scripts')
    <!-- DataTable js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>

    <!-- DateRange Picked js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datetime/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/advanceddate_pickers.js') }}"></script>

    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/buttons.print.js') }}"></script>

    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{  asset('core-plus-theme/vendors/datatables/js/pdfmake.min.js') }}"></script>

     <!-- Export MASTER js -->
   {{--  <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/FileSaver/FileSaver.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/js-xlsx/xlsx.core.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/jsPDF/jspdf.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/tableExport.min.js')}}"></script> --}}

    <script type="text/javascript">
        $(document).ready( function() {
            $('#boton').attr('disabled',true);
            $('#file').change(
            function(){
                if ($(this).val()){
                    $('#boton').removeAttr('disabled');
                }
                else {
                    $('#boton').attr('disabled',true);
                }
                 $('#boton').click(function(){

                    $('#boton').hide();
                    $('#boton1').hide();
                    $('#close').hide();
                });
            });

           // $('#customers').DataTable({
           //   "paginate": false,
           //   "info":     false
           // });

            //table tools example
            var table = $('#customers').DataTable({
                // dom: 'Bflrtip',
                "dom": '<"m-t-10"B><"m-t-10 pull-left"f><"m-t-10 pull-right"l>rt<"pull-left m-t-10"i><"m-t-10 pull-right"p>',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

         });
    </script>
@endsection
