@extends('layouts.admin.default')
@section('title', 'Resultado')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <!--Datepicker css -->
    <link rel="stylesheet" href="{{ asset('core-plus-theme/vendors/datetime/css/jquery.datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/airdatepicker/css/datepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/datepicker.css') }}">
    <!-- End Datepicker css -->
@stop
@include('flash::message')
@section('content')
<!-- Content Header (Page header) -->
     <header class="header-page">
        <div class="header-data">
            <h1>@yield('title')<small> - Registros </small></h1>
        </div>
    </header>
    <article class="content">
            <div class="panel filterable">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6">
                        Paso 2: puntos insertados.
                    </h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                              <th></th>
                              <th>Procesados</th>
                              <th>Negados</th>
                              <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td scope="row">Miembros</td>
                              <td>{{$contributions-> processed_users }}</td>
                              <td>{{$contributions-> rejected_users }} </td>
                              <td>{{ $contributions-> total_users }}</td>
                            </tr>
                            <tr>
                              <td scope="row">Puntos</td>
                              <td>{{number_format($contributions-> processed_points, 0, '','.') }} </td>
                              <td>{{number_format($contributions-> rejected_points, 0, '','.')}}</td>
                              <td>{{number_format($contributions-> total_points, 0, '','.')}}</td>
                            </tr>
                      </tbody>
                    </table>
                </div>
            </div>
            <div class="panel filterable">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left m-t-6">
                        Errores de inserción de puntos
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="customers" class="table table-striped table-hover datatable-asc">
                            <thead>
                                <tr>
                                    @if($format_import -> member_format_import == 0)
                                        <th>DNI</th>
                                    @elseif($format_import -> member_format_import == 1)
                                        <th>Carnet</th>
                                    @endif
                                    <th>Código del objetivo</th>
                                    <th>Puntos</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($failed_records as $element)
                                    <tr>
                                        
                                        <td>
                                            @if($format_import -> member_format_import == 0)
                                                {{ $element->dni }}
                                            @elseif($format_import -> member_format_import == 1)
                                                {{ $element->carnet }}
                                            @endif
                                        </td>
                                        <td>{{ $element->objetive_code  }}</td>
                                        <td>{{ $element->points }}</td>
                                        <td>@if( $element->status == 1) <span class="label label-success">Registro con datos correctos</span> @elseif( $element->status == 2) <span class="label label-danger">Registro código incorrecto o estatus inactivo</span> @else <span class="label label-danger">Registro con miembro no registrado en el sistema</span>@endif</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </article>
@stop
@section('modals')
    <div class="modal fade" id="data-export" tabindex="-1" role="dialog" aria-labelledby="data-export">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Exportar datos</h4>
                </div>
                <div class="modal-body text-center">
                    <button type="button" class="btn btn-primary" onClick="$('#customers').tableExport({type:'excel',excelFileFormat:'xmls'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</button>
                    <button type="button" class="btn btn-primary" onClick="$('#customers').tableExport({type:'csv'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> CSV</button>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Atrás</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js -->

    <!-- DateRange Picked js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datetime/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/advanceddate_pickers.js') }}"></script>
     <!-- Export MASTER js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/FileSaver/FileSaver.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/js-xlsx/xlsx.core.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/jsPDF/jspdf.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/tableExport.jquery.plugin-master/tableExport.min.js')}}"></script>
@endsection
