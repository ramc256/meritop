@extends('layouts.admin.default')

@section('title', 'Variante')
@section('styles')
     {{-- <link href="{{ asset('core-plus-theme/vendors/colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/> --}}
@stop
@section('content')
    <!-- Content Header (Page header) -->
   <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Nuevo</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('products.store') }}" method="post"  enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('layouts.form-errors')
        <input type="hidden" name="fk_id_parent_product" value="{{ $parent_product }}">
            <div class="panel">
               <div class="panel-heading clearfix">
                     <div class="pull-left text-center">
                    <h3>Datos principales</h3>
                </div>
                    <div class="pull-right text-center">
                        <a href="{{route('parents-products.index')}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="feature" class=" control-label">Característica <span class="text-danger">*</span></label>
                                    <input name="feature" id="feature" placeholder="Caracteristica" type="text" class="form-control" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sku" class="control-label">SKU<span class="text-danger">*</span></label>
                            <input name="sku" id="sku" placeholder="SKU" maxlength="15" type="text" class="form-control"  required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="barcode" class="control-label">Código de Barras</label>
                            <input id="barcode" name="barcode" placeholder="Código de barras" maxlength="15" type="text" class="form-control">
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="points" class="control-label">Puntos<span class="text-danger">*</span></label>
                            <input id="points" name="points" placeholder="Puntos" maxlength="8" type="number" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="excerpt" class="control-label">Extracto</label>
                            <input id="excerpt" name="excerpt" placeholder="Extracto" maxlength="250" type="text" class="form-control">
                        </div>
                    </div>
                     <div class="col-md-12">
                        <h4 class="text-capitalize">Imagenes</h4>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                 <div class="form-group">
                                    <label for="image" class=" control-label">Imagenes del producto</label>
                                    <input name="image[]" type="file" class="form-control" id="image" placeholder="Url de Imagen" multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <h4 class="text-capitalize">Características</h4>
                        <hr>
                    </div>
                    <div class="col-md-3">
                         <div class="form-group">
                            <label for="width" class="control-label">Ancho<span class="text-danger">*</span></label>
                            <input id="width" name="width" placeholder="Ancho" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="heigth" class="control-label">Alto<span class="text-danger">*</span></label>
                            <input id="heigth" name="heigth" placeholder="Alto" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="depth" class="control-label">Profundidad<span class="text-danger">*</span></label>
                            <input id="depth" name="depth" placeholder="Profundidad" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                     <div class="col-md-3">
                        <div class="form-group">
                            <label for="volume" class="control-label">Volumen<span class="text-danger">*</span></label>
                            <input id="volume" name="volume" placeholder="Volumen" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="size" class="control-label">Tamaño<span class="text-danger">*</span></label>
                            <input id="size" name="size" placeholder="Tamaño" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="weight" class="control-label">Peso<span class="text-danger">*</span></label>
                            <input id="weight" name="weight" placeholder="Peso" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <label for="measure" class="control-label">Medida<span class="text-danger">*</span></label>
                            <input id="measure" name="measure" placeholder="Medida" maxlength="8" type="text" class="form-control" value="0" required="required">
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="color" class=" control-label">Color<span class="text-danger">*</span></label>
                            <!-- Color Picker -->
                            <input id="color" name="color" type="text" class="form-control" {{-- my-colorpicker1" id="cp1" --}} required="required" {{-- readonly="readonly" --}} placeholder="#FFFFFF" {{-- value="#FFF" --}}>
                        </div>
                    </div>
                </div>
                     <div class="col-md-12">
                        <div class="form-group">
                            <br/>
                            <label for="warranty" class="control-label">Vender fuera de Stock</label>
                            <input id="sell_out_stock" name="sell_out_stock" type="checkbox">
                        </div>
                    </div>
                     <div class="col-md-12">
                        <h4 class="text-capitalize">Estatus</h4>
                        <hr>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="market" class="control-label">Estatus<span class="text-danger">*</span></label>
                            <select name="status" id="status" class="form-control" required="required">
                                <option value="">Seleccione un estatus</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')

 {{--    <!-- end page js colorpiker -->
    <script src="{{ asset('core-plus-theme/vendors/colorpicker/js/bootstrap-colorpicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('core-plus-theme/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('core-plus-theme/js/custom_js/pickers.js') }}" type="text/javascript"></script>
<!-- end page js --> --}}
  <!-- page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page js -->
@endsection
