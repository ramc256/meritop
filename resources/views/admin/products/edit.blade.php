@extends('layouts.admin.default')

@section('title', 'Variante')
@section('styles')
     {{-- <link href="{{ asset('core-plus-theme/vendors/colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/> --}}
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> - Editar</small></h1>
        </div>
    </header>
    <article class="content">
         <form id="form-validation" action="{{ route('products.update', $product-> id) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts.form-errors')
            <input type="hidden" name="_method" value="PUT">
            <div class="panel">
               <div class="panel-heading clearfix">
                     <div class="pull-left text-center">
                    <h3>Datos principales</h3>
                </div>
                    <div class="pull-right text-center">
                        <a href="{{route('products.show', $product -> id )}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="feature" class=" control-label">Característica<span class="text-danger">*</span></label>
                                    <input name="feature" id="feature" placeholder="Caracteristica" type="text" class="form-control" required="required" value=" {{ $product -> feature }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sku" class="control-label">SKU<span class="text-danger">*</span></label>
                            <input name="sku" id="sku" placeholder="SKU" maxlength="15" type="text" class="form-control" value="{{$product -> sku}}"   required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="barcode" class="control-label">Código de Barras</label>
                            <input id="barcode" name="barcode" placeholder="Código de barras" maxlength="15" type="text" value="{{$product -> barcode}}"  class="form-control">
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="points" class="control-label">Puntos<span class="text-danger">*</span></label>
                            <input id="points" name="points" placeholder="Puntos" maxlength="8" type="number" class="form-control" required="required" value="{{$product -> points}}" >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="excerpt" class="control-label">Extracto</label>
                            <input id="excerpt" name="excerpt" placeholder="Extracto" maxlength="250" type="text" class="form-control" value="{{$product -> excerpt}}" >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Imagenes</h4>
                        <hr>
                    </div>
                    <div class="col-md-12">
                    <div class="row">
                        @foreach ($images as $element)
                            <div class="col-xs-6 col-sm-4 col-md-2">
                                <div class="thumbnail relative">
                                    <div class="overlay">
                                        <div class="btns">
                                            <button type="button" class="img-product btn btn-success btn-xs" data-toggle="modal" data-target="#update-image"><i class="fa fa-pencil"></i><span class="data hidden">{{ $element -> id }}</span></button>
                                            <button type="button" class="img-product btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-image"><i class="fa fa-close"></i><span class="data hidden">{{ $element -> id }}</span></button>
                                        </div>
                                    </div>
                                    @if (empty($element-> image))
                                        <img class="image-thumbnail" src="{{ Storage::disk('image')->url('img-default.png') }}" alt="{{ $element -> nombre . ' ' . $element -> id}}">
                                    @else
                                        <img class="image-thumbnail" src="{{ $_ENV['STORAGE_PATH'].$element-> image }}" alt="{{ $element -> nombre . ' ' . $element -> id}}">
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        @if (count($images)<4)
                            <div class="col-xs-6 col-sm-4 col-md-2" >
                                <div class="thumbnail">
                                    <a id="btn-add-image" data-toggle="modal" data-target="#update-image" style=" height: 140px; display: flex; align-items: center; text-align: center; cursor: pointer;">
                                        <i class="fa fa-plus" style="font-size: 50pt; display: block; margin: auto; color: #E1E1E1"></i>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                        <h4 class="text-capitalize">Características</h4>
                        <hr>
                    </div>
                    <div class="col-md-3">
                         <div class="form-group">
                            <label for="width" class="control-label">Ancho<span class="text-danger">*</span></label>
                            <input id="width" name="width" placeholder="Ancho" maxlength="8" type="text" class="form-control" value="{{$product -> width}}"  required="required">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="heigth" class="control-label">Alto<span class="text-danger">*</span></label>
                            <input id="heigth" name="heigth" placeholder="Alto" maxlength="8" type="text" class="form-control" value="{{$product -> heigth}}"  required="required">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="depth" class="control-label">Profundidad<span class="text-danger">*</span></label>
                            <input id="depth" name="depth" placeholder="Profundidad" maxlength="8" type="text" class="form-control" value="{{$product -> depth}}" required="required">
                        </div>
                    </div>
                     <div class="col-md-3">
                        <div class="form-group">
                            <label for="volume" class="control-label">Volumen<span class="text-danger">*</span></label>
                            <input id="volume" name="volume" placeholder="Volumen" maxlength="8" type="text" class="form-control" required="required" value="{{$product -> volume}}" >
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="size" class="control-label">Tamaño<span class="text-danger">*</span></label>
                            <input id="size" name="size" placeholder="Tamaño" maxlength="8" type="text" class="form-control" required="required" value="{{$product -> size}}" >
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="weight" class="control-label">Peso<span class="text-danger">*</span></label>
                            <input id="weight" name="weight" placeholder="Peso" maxlength="8" type="text" class="form-control" required="required" value="{{$product -> weight}}" >
                        </div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <label for="measure" class="control-label">Medida<span class="text-danger">*</span></label>
                            <input id="measure" name="measure" placeholder="Medida" maxlength="8" type="text" class="form-control" required="required" value="{{$product -> measure}}" >
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="color" class=" control-label">Color<span class="text-danger">*</span></label>
                            <!-- Color Picker -->
                            <input id="color" name="color" type="text" class="form-control {{-- my-colorpicker1 clockpicker --}}" {{-- id="cp1" --}} required="required" {{-- readonly="readonly" --}} placeholder="Color" value="{{ $product -> color }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                            <div class="form-group">
                                <br/>
                                <label for="warranty" class="control-label">Vender fuera de Stock</label>
                                <input id="sell_out_stock" name="sell_out_stock" type="checkbox" @if ($product -> sell_out_stock == 1)
                                checked="checked"
                            @endif>
                            </div>
                        </div>
                    <div class="col-md-12">
                        <h4 class="text-capitalize">Estatus</h4>
                        <hr>
                    </div>   
                    <div class="col-md-12">
                        <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status" class="control-label">Estatus<span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control" required="required">
                                    <option value="">Seleccione un estatus</option>
                                    <option value="1"  @if($product -> status == 1) selected="selected" @endif>Activo</option>
                                    <option value="0"  @if($product -> status == 0) selected="selected" @endif>Inactivo</option>
                                </select>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </article>
@stop
@section('modals')
    <!-- MODAL PARA AGREGAR UNA IMAGEN -->
    <div class="modal fade" id="update-image" tabindex="-1" role="dialog" aria-labelledby="add-image">
        <form action=" {{ route('images.update') }} " method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="change-image">Agregar imagen</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="image" class="control-label">Imagen <span class="text-danger">*</span></label>
                            <input id="image" name="image" type="file" class="form-control" required="required">
                            <input class="id" name="id" type="hidden" readonly="readonly">
                            <input id="identificator" name="identificator" type="hidden" readonly="readonly" value="{{ $product -> id }}">
                            <input id="disk" name="disk" type="hidden" value="products" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="slug" class="control-label">Slug</label>
                            <input name="slug" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- MODAL PARA ELIMINAR UNA IMAGEN -->
    <div class="modal fade" id="delete-image" tabindex="-1" role="dialog" aria-labelledby="add-image">
        <form action=" {{ route('images.destroy') }} " method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="change-image">Eliminar imagen</h4>
                    </div>
                    <div class="modal-body">
                        <p class="">¿Esta seguro de eliminar la imagen?</p>
                        <input class="id" type="hidden" name="id" readonly="readonly">
                        <input id="disk" name="disk" type="hidden" value="products" readonly="readonly">
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-success">Si</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@section('scripts')
     <!-- page js -->
    <script>
        $(document).ready(function(){
            // Muestra el campo de descripción para la garantia
            $('#warranty').change(function(){
                    if($(this).prop("checked")) {
                $('#data_warranty').removeClass("hidden").addClass("visible");
                } else {
                    $('#data_warranty').removeClass("visible").addClass("hidden");
                }
            });

            // Obtiene el ID de la imagen y la inserta en el formulario para posteriormente enviarlo al metodo de cambio de imagen
            $('.img-product').click(function(){
                value = $(this).children('.data').text();
                $('.id').val(value);
            });
        });
    </script>
    {{--  <!-- Page js colorpiker -->
    <script src="{{ asset('core-plus-theme/vendors/colorpicker/js/bootstrap-colorpicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('core-plus-theme/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('core-plus-theme/js/custom_js/pickers.js') }}" type="text/javascript"></script>
    <!-- end page js --> --}}
     <!-- page js bootstrapvalidation-->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end page js -->
@endsection
