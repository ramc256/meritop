@extends('layouts.admin.default')

@section('title', 'Variante')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Ver</small></h1>
        </div>
    </header>
    <article class="content">
        <div class="panel filterable">
            <div class="panel-heading clearfix">
                <div class="pull-left text-center">
                    <h3>Datos principales</h3>
                </div>
                <div class="pull-right text-center">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                    <a href="{{ route('parents-products.show', $product -> fk_id_parent_product) }}" type="button" class="btn btn-warning">Atrás</a>
                    <a href="{{ route('products.edit', $product -> id) }}" type="button" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <div class="panel-body">
            <div class="col-sm-12 col-md-12">
                <ul class="panel-list">
                    <li class="row">
                        <div class="col-xs-12 col-sm-12">
                            <p><b>Característica</b></p>
                            <p>{{ $product -> feature }}</p>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <p><b>SKU</b></p>
                            <p>{{ $product -> sku }}</p>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <p><b>Barcode</b></p>
                            <p>{{ $product -> barcode }}</p>
                        </div>
                         <div class="col-xs-12 col-sm-4">
                            <p><b>Puntos</b></p>
                            <p>{{ number_format($product -> points, 0, '','.') }}</p>
                        </div>
                         <div class="col-xs-12 col-sm-4">
                            <p><b>Extracto</b></p>
                            <p>{{ $product -> excerpt }}</p>
                        </div>
                    </li>
                    <h4>Imagenes</h4>
                        <hr>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($images as $element)
                                <div class="col-xs-6 col-sm-4 col-md-2">
                                    <div  class="thumbnail">
                                        @if (empty($element-> image))
                                            <img class="image-thumbnail" src="{{ Storage::disk('image')->url('img-default.png') }}" alt="{{ $element -> nombre . ' ' . $element -> id}}">
                                        @else
                                            <img class="image-thumbnail" src="{{ $_ENV['STORAGE_PATH'].$element-> image }}" alt="{{ $element -> nombre . ' ' . $element -> id}}">
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <h4>Características</h4>
                        <hr>
                    <li class="row">
                        <div class="col-xs-12 col-sm-3">
                            <p><b>Ancho</b></p>
                            <p>{{ $product -> width }}</p>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <p><b>Alto</b></p>
                            <p>{{ $product -> heigth }}</p>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <p><b>Profundidad</b></p>
                            <p>{{ $product -> depth }}</p>
                        </div>
                         <div class="col-xs-12 col-sm-3">
                            <p><b>Volumen</b></p>
                            <p>{{ $product -> volume }}</p>
                        </div>
                    </li>
                    <li class="row">
                        <div class="col-xs-12 col-sm-4">
                            <p><b>Tamaño</b></p>
                            <p>{{ $product -> size }}</p>
                        </div>
                         <div class="col-xs-12 col-sm-4">
                            <p><b>Peso</b></p>
                            <p>{{ $product -> weight }}</p>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <p><b>Medida</b></p>
                            <p>{{ $product -> measure }}</p>
                        </div>
                    </li>
                    <li class="row">
                       <div class="col-xs-12 col-sm-6">
                            <p><b>Color</b></p>
                            <p>{{ $product -> color }}</p>
                        </div>
                        <div class="col-xs-12  col-sm-6">
                            <p><b>Vender fuera de stock</b></p>
                            <p>@if ($product -> sell_out_stock == 1) <i class="fa fa-check text-success"></i> @else <i class="fa fa-close text-danger"></i> @endif</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-12">
                    <ul class="panel-list">
                        <li class="row">
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Creación</b></p>
                                <p>{{ date_format($product -> created_at, 'd/m/Y h:i:s A') }}</p>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <p><b>Modificación</b></p>
                                <p>{{ date_format($product -> updated_at, 'd/m/Y h:i:s A') }}</p>
                            </div>
                            <div class="col-xs-12">
                                <h4>Estatus</h4>
                        <hr>
                                <p><b>Estatus</b></p>
                                <p>@if ($product -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span> @endif</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
</article>
@stop
@section('modals')
    <!-- Modal ELIMINAR UN REGISTRO -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('products.destroy', $product->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Variante</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar la Variante <b>{{ $product -> name}}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scritps')
    <!-- page js -->
    <script>
        $('#image-change').on('shown.bs.modal', function () {
          $('#btn-image-change').focus()
        })
    </script>
    <script>
        $('#modal-delete').on('shown.bs.modal', function () {
          $('#modal-delete').focus()
        })
    </script>
    <!-- end js -->
@endsection
