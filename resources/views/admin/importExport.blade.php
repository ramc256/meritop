@extends('layouts.admin.default')

@section('title', 'Modulo de importar y exportar')
@section('content')
    <!-- Content Header (Page header) -->
    <header class="content-header">
        <div class="header-data">
            <h1>@yield('title')</h1>
            <p>Vista para importar y exportar datos</p>
        </div>
    </header>
	<article class="content">
		<div class="col-sm-12">
			<div class="panel">
               <div class="panel-heading">
                    <h3 class="panel-title"> Opciones</h3>
                </div>
                <div class="panel-body">
                	<a href="{{ url('admin/downloadExcel/xls/Members')}}"><button class="btn btn-success">Download Excel xls</button></a>
					<a href="{{ url('admin/downloadExcel/csv/Members') }}"><button class="btn btn-success">Download CSV</button></a>
					<a href="{{ url('admin/downloadExcel/pdf/Members') }}"><button class="btn btn-success">Download PDF</button></a>

                </div>
            </div>
        </div>		            
		<form action="{{ URL::to('admin/import-members') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
      		{{ csrf_field() }}
      		<div class="col-sm-12">
    			<div class="panel">
                   <div class="panel-heading">
                        <h3 class="panel-title"> Cargar</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">Archivo <span class="text-danger">*</span></label>
                            <div class="col-md-9">
      							<input id="archive" class="form-control" type="file" name="import_file" required="required" />
                            </div>
                        </div>
                    <div class="panel-footer text-center">
						<button type="submit" class="btn btn-primary">Import File</button>
                    </div>
                </div>
            </div>
		</form>
	</article>
@endsection