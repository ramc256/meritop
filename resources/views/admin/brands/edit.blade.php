@extends('layouts.admin.default')

@section('title', 'Marca')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Editar</small></h1>
        </div>
    </header>
	<article class="content">
        @include('layouts.form-errors')  
        <form id="form-validation" action="{{ route('brands.update',$brand -> id) }}" method="POST" >
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
			<div class="panel">
                <div class="panel-heading clearfix">
                    <div class="text-center pull-right">
                        <a href="{{route('brands.show', $brand -> id )}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div> 
                </div>
                <div class="panel-body row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" value="{{$brand -> name}}" required="required">
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <!-- Page js -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js - ->
@endsection
