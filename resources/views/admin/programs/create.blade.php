@extends('layouts.admin.default')

@section('title', 'Programa')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />

    <link href="{{asset('core-plus-theme/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{asset('core-plus-theme/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{asset('core-plus-theme/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet">
    <link href="{{asset('core-plus-theme/vendors/iCheck/css/all.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('core-plus-theme/css/custom_css/wizard.css') }}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{ asset('core-plus-theme/vendors/summernote/summernote.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/trumbowyg/css/trumbowyg.min.css') }}" >
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/form_editors.css') }}">
    
  {{--   <link href="{{asset('core-plus-theme/vendors/iCheck/css/all.css') }}" rel="stylesheet"/>
    <link href="{{asset('core-plus-theme/vendors/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('core-plus-theme/vendors/bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/core-plus-theme/vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('core-plus-theme/css/custom.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('core-plus-theme/css/custom_css/radio_checkbox.css') }}" rel="stylesheet" type="text/css"> --}}
@stop
@section('content')
    <!-- Content Header (Page header) -->
     <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small> Nuevo</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('programs.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('layouts.form-errors')
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Gestión del programa
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="stepwizard">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step">
                                        <a href="#step-1" class="btn btn-primary btn-circle">1</a>
                                        <p>Paso 1</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-2" class="btn btn-default btn-circle">2</a>
                                        <p>Paso 2</p>
                                    </div>
                                    {{-- <div class="stepwizard-step">
                                        <a href="#step-3" class="btn btn-default btn-circle">3</a>
                                        <p>Paso 3</p>
                                    </div> --}}
                                </div>
                            </div>
                            <form id="form-validation" action="{{ route('programs.store') }}" method="POST" enctype="multipart/form-data">
                                <div class="row setup-content" id="step-1">
                                    <div class="col-xs-12">
                                        <div class="col-md-12">
                                            <h3> <b>Paso 1:</b> Datos del programa</h3>
                                            <input type="hidden" name="fk_id_club">
                                            <div class="form-group">
                                                <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                                                <input id="name" name="name" placeholder="Nombre del programa" maxlength="100" type="text" class="form-control" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <label for="summernote" class="control-label">Descripción<span class="text-danger">*</span></label>
                                                <div class="panel-body">
                                                    <div class="bootstrap-admin-panel-content summer_noted">
                                                        <textarea id="summernote" name="description" placeholder="Descripción" maxlength="65500" class="form-control" required="required" rows="15"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Grupo <span class="text-danger">*</span></label>
                                                <div class="col-xs-12">
                                                    @if (count($groups)>0)
                                                        @foreach ($groups as $element)
                                                            <div class="checkbox checkbox-primary col-md-3" >
                                                                <input id="check{{ $element -> id }}" name="fk_id_group[]" value="{{ $element -> id }}" class="styled" type="checkbox">
                                                                <label for="check{{ $element -> id }}">
                                                                    &nbsp;{{ $element -> name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <p>No hay grupos registrados. Si desea registrar un grupo dirigase a <a href="{{ route('groups.create') }}">Crear un grupo</a></p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="image" class="control-label">Imagen <span class="text-danger">*</span></label>
                                                <input name="image" type="file" class="form-control" id="image" placeholder="Url de Imagen">
                                            </div>
                                            <button class="btn btn-primary nextBtn pull-right" type="button">Siguiente</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row setup-content" id="step-2">
                                    <div class="col-xs-12">
                                        <div class="col-md-12">
                                            <h3> <b>Paso 2:</b> Datos del objetivo</h3>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for="code" class="control-label">Código<span class="text-danger">*</span></label>
                                                    <input name="code" type="text" class="form-control"  maxlength="6"  id="code" placeholder="Código" required="required">
                                                </div>
                                                <label for="name" class="control-label">Nombre<span class="text-danger">*</span></label>
                                                <input id="name" name="objetive_name" placeholder="Nombre del objetivo" maxlength="100" type="text" class="form-control" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <label for="description" class="control-label">Descripción<span class="text-danger">*</span></label>
                                                <textarea id="description" name="objetive_description" placeholder="Descipción" maxlength="65000" class="form-control" required="required" rows="10"></textarea>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="start_date" class="control-label">Fecha de inicio<span class="text-danger">*</span></label>
                                                        <input id="start_date" name="start_date" placeholder="Fecha de inicio" type="date" class="form-control" required="required" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="due_date" class="control-label">Fecha de finalización<span class="text-danger">*</span></label>
                                                        <input id="due_date" name="due_date" placeholder="Fecha de finalización" type="date" class="form-control" required="required" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="fk_id_perspective" class="control-label">Perspectivas<span class="text-danger">*</span></label>
                                                @if (count($perspectives)>0)
                                                   <select name="fk_id_perspective" id="fk_id_perspective" class="form-control" required="required">
                                                        <option value="">Seleccione...</option>
                                                        @foreach ($perspectives as $element)
                                                            <option value="{{ $element -> id }}">{{ $element -> name }}</option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <p>No hay perspectivas registradas. Si desea registrar una dirigase a <a href="{{ route('perspectives.create') }}">Crear un perspectiva</a></p>
                                                @endif
                                            </div>
                                            <button class="btn btn-primary prevBtn pull-left" type="button"> Anterior </button>
                                            <button class="btn btn-success pull-right" type="submit">Finalizar!</button>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row setup-content" id="step-3">
                                    <div class="col-xs-12">
                                        <div class="col-md-12">
                                            <h3> Paso 3</h3>
                                            <div class="form-group">
                                                <label for="acceptTerms1">
                                                    <input id="acceptTerms1" name="acceptTerms" type="checkbox" class="custom-checkbox" required="required"> Revise con cuidado los datos ingresados antes de culminar el proceso.
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary prevBtn pull-left" type="button">
                                                    Anterior
                                                </button>
                                                <button class="btn btn-success pull-right" type="submit">
                                                    Finalizar!
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
        <!-- page js -->
    <script src="{{asset('core-plus-theme/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{asset('core-plus-theme/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{asset('core-plus-theme/vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{asset('core-plus-theme/js/custom_js/form_wizards.js') }}" type="text/javascript"></script>
    <script src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
        <!-- end of page level js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/trumbowyg/js/trumbowyg.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/summernote/summernote.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/form_editors.js') }}" ></script>
@endsection
