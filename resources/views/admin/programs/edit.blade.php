@extends('layouts.admin.default')

@section('title', 'Programa')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />

    <!--Datepicker css -->
    <link rel="stylesheet" href="{{ asset('core-plus-theme/vendors/datetime/css/jquery.datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/airdatepicker/css/datepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/datepicker.css') }}">
    <!-- end Datepicker css -->

    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{ asset('core-plus-theme/vendors/summernote/summernote.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/trumbowyg/css/trumbowyg.min.css') }}" >
     <link rel="stylesheet" type="text/css" href="{{ asset ('core-plus-theme/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/form_editors.css') }}">
@stop
@section('content')
    <!-- Content Header (Page header) -->
     <header class="header-page">
        <div class="header-data">
            <h1>@yield('title') <small>- Editar</small></h1>
        </div>
    </header>
	<article class="content">
        <form id="form-validation" action="{{ route('programs.update', $program -> id) }}" method="POST">
            {{ csrf_field() }}
            @include('layouts.form-errors')
            <input name="_method" type="hidden" value="PUT">
			<div class="panel">
                <div class="panel-heading clearfix">
                    <div class="pull-right text-center">
                        <a href="{{route('programs.show',$program -> id )}}" type="button" class="btn btn-warning">Atrás</a>
                        <button type="submit" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name" class="control-label">Nombre <span class="text-danger">*</span></label>
                        <input id="name" name="name" placeholder="Nombre del programa" maxlength="100" type="text" class="form-control" required="required" value="{{ $program -> name }}" />
                    </div>
                     <div class="form-group">
                        <label for="summernote" class="control-label">Descripción<span class="text-danger">*</span></label>
                        <div class="panel-body ">
                            <div class="bootstrap-admin-panel-content summer_noted">
                                <textarea id="summernote" name="description" placeholder="Descripcion" maxlength="65500" class="form-control" required="required" rows="15">{{ $program -> description }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Grupo <span class="text-danger">*</span></label>
                        <div class="col-xs-12">
                            @if (count($groups)>0)
                                @foreach ($groups as $element)
                                    <div class="checkbox checkbox-primary col-md-3" >
                                        <input id="check{{ $element -> id }}" name="fk_id_group[]" value="{{ $element -> id }}" class="styled" type="checkbox"
                                        @foreach ($groups_programs as $data)
                                            @if ($element -> id == $data -> fk_id_group) checked="checked" @endif
                                        @endforeach
                                        >
                                        <label for="check{{ $element -> id }}" name="fk_id_group[]" value="{{ $element -> id }}"  @if ($program -> group == $element -> id) selected="selected" @endif>
                                            &nbsp;{{ $element -> name }}
                                        </label>
                                    </div>
                                @endforeach
                            @else
                                <p>No hay grupos registrados. Si desea registrar un grupo dirigase a <a href="{{ route('groups.create') }}">Crear un grupo</a></p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status" class="control-label">Estatus <span class="text-danger">*</span></label>
                        <select name="status" id="status" class="form-control" required="required">
                            <option value="">Seleccione...</option>
                            <option value="1" @if ($program -> status == 1) selected="selected" @endif>Activo</option>
                            <option value="0" @if ($program -> status == 0) selected="selected" @endif>Inactivo</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
	</article>
@stop
@section('scripts')
      <!-- form editores -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/trumbowyg/js/trumbowyg.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/summernote/summernote.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/form_editors.js') }}" ></script>
    <!-- end form editores -->

    <!-- Page js -->
    <script type="text/javascript" src="{{asset('vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom_js/datatables_custom.js')}}"></script>
    <!-- end page js -->
    <!-- bootstrap validation -->
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js')}}"></script>
    <script type="text/javascript" src="{{asset('core-plus-theme/customs/form-validator.js')}}"></script>
    <!-- end bootstrap validation -->
@endsection