@extends('layouts.admin.default')

@section('title', 'Programas')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <!--Datepicker css -->
    <link rel="stylesheet" href="{{ asset('core-plus-theme/vendors/datetime/css/jquery.datetimepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/airdatepicker/css/datepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/css/datepicker.css') }}">
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <header class="header-page">
         <div class="header-data">
            <h1>@yield('title')<small> - Gestiones</small></h1>
        </div>
    </header>
    @include('flash::message')
	<article class="content">
		<div class="panel filterable">
            @if (Auth::user() -> kind == 0)
                <div class="panel-heading clearfix">
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('programs.create') }}">Nuevo</a>
                    </div> 
                </div>
            @endif
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover datatable-asc " style="vertical-align: middle;">
                        <thead>
                            <tr>
                                {{-- <th>Fecha</th> --}}
                                {{-- <th>Imagen</th> --}}
                                <th>Nombre</th>
                                <th>Grupos</th>
                                <th>Estatus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($programs as $element)
                                <tr>
                                   {{--  <td style="vertical-align: middle;">{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}</td> --}}
                                    {{-- <td style="vertical-align: middle;">
                                        <div class="text-center mbl">
                                            @if (isset($element  -> image) && $element  -> image != '')
                                                @php $route_imagen =  $element  -> image ; $disk = 'programs'; @endphp
                                            @else
                                                @php $route_imagen = 'img-default.png'; $disk = 'images'; @endphp
                                            @endif
                                            <img class="img-show" src=" {{ Storage::disk($disk) -> url($route_imagen) }} " style="max-width: 50px; max-height: 50px">
                                        </div>
                                    </td> --}}
                                    <td class="text-capitalize" style="vertical-align: middle; width: 50%"><a href="{{ route('programs.show', $element -> id) }}">{{ $element -> name }}</a></td>
                                    <td style="vertical-align: middle;">{{ $element -> groups }}</td>
                                    <td style="vertical-align: middle;">@if ($element -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span>@endif</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</article>
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js -->
     <!-- DateRange Picked js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datetime/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/advanceddate_pickers.js') }}"></script>
@endsection
