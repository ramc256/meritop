@extends('layouts.admin.default')

@section('title', 'Programa')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('core-plus-theme/css/custom_css/dashboard1.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('core-plus-theme/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
@stop
@section('content')
    <!-- Content Header (Page header) -->
     <header class="header-page">
         <div class="header-data">
            <h1>@yield('title')<small> - Ver</small></h1>
        </div>
    </header>
    @include('flash::message')
    <article class="content">
            <div class="panel">
                <div class="panel-heading clearfix">
                    <div class="pull-right">
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Eliminar</button>
                        <a href="{{ route('programs.index') }}" type="button" class="btn btn-warning">Atrás</a>
                        <a href="{{ route('programs.edit', $program -> id) }}" type="button" class="btn btn-primary">Editar</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12 col-md-6">
                         <ul class="panel-list">
                            <li class="row">
                                <p><b>Nombre</b></p>
                                <p class="text-capitalize">{{ $program -> name }}</p>
                            </li>
                            <li class="row">
                                <br>    
                                <p><b>Grupos</b></p>
                                @foreach ($groups_programs as $element)
                                    <span class="label label-primary">{{ $element -> name }}</span>
                                @endforeach
                            </li>   
                             <li class="row">
                                <p><b>Estatus</b></p>
                                <p>@if ($program -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span> @endif</p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                             <div class="text-center thumbnail mbl">
                                @if (empty($program -> image))
                                    <img  class="image-thumbnail img-responsive max-size" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $program -> name }}">
                                @else
                                    <img  class="image-thumbnail img-responsive max-size" src="{{ $_ENV['STORAGE_PATH'].$program -> image }}" alt="{{ $program -> name }}">
                                @endif
                            </div>
                        </div>
                    </div>
                    <ul class="panel-list">
                        <li class="row">
                            <div class="col-sm-12">
                                <p><b>Descripción</b></p>
                                <p>{!! $program -> description !!}</p>
                            </div>
                        </li> 
                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Creación</b></p>
                                <p class="text-capitalize">{{ date_format($program -> created_at, 'd/m/Y') }}</p>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Modificación</b></p>
                                <p class="text-capitalize">{{ date_format($program -> updated_at, 'd/m/Y') }}</p>
                            </div>
                        </li>  
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel filterable">
                        <div class="panel-heading clearfix">
                          {{--   <div class="btns-header pull-left">
                                <button  data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-1" type="button" class="btn btn-labeled btn-meritop-primary">
                                    <span class="btn-label"><i class="glyphicon glyphicon-chevron-down"></i></span> Filtros
                                </button>
                            </div> --}}
                            <div class="panel-faq">
                               {{--  <form method="post" action=" {{ route('objetives.filter') }} ">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="get">
                                    <div id="faq-cat-1-sub-1" class="collapse">
                                        <div class="panel-body">
                                            <div class="form-inline" style="display: block; overflow: hidden;">
                                                <div class="form-group pull-left">
                                                    <label class="control-label" for="dateranges">Rango de fecha: </label>
                                                    <input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" class="form-control" id="dateranges" readonly="readonly" value="{{ date('01/m/Y') }} - {{ date('d/m/Y') }}"/>
                                                </div>
                                                <div class="form-group pull-left">
                                                    <button type="submit" class="btn btn-meritop-primary btn-md" id="addButton">Buscar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form> --}}

                            {{-- <form action="{{ route('objetives.create') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <input type="hidden" name="id" value="{{ $program -> id }}">
                                <input type="submit" class="btn btn-primary pull-right" value="Agregar Variante">
                            </form> --}}
                            <div class="pull-right">
                                <a href="{{ url('admin/objetives/create/'. $program -> id) }}" class="btn btn-success" id="addButton">Nuevo</a>
                            </div>
                        </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Nombre</th>
                                            <th>Perspectiva</th>
                                            <th>Fecha inicio</th>
                                            <th>Fecha fin</th>
                                            <th>Estatus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($objetives) > 0)
                                            @foreach($objetives as $element)
                                                <tr>
                                                    <td><a href="{{route('objetives.show', $element -> id)}}">{{ $element -> code }}</a></td>
                                                    <td class="text-capitalize">{{ $element -> name }}</td>
                                                    <td class="text-capitalize">{{ $element -> perspective }}</td>
                                                    <td>{{ date_format( new DateTime($element -> start_date), 'd/m/Y') }}</td>
                                                    <td>{{ date_format(new DateTime($element -> due_date), 'd/m/Y') }}</td>
                                                    <td>@if ($element -> status == 1) <span class="label label-success">Activo</span> @else <span class="label label-danger">Inactivo</span> @endif</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9"><p class="text-center">No hay objetivos registrados para este programa</p></td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </article>
@stop
@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <form action="{{ route('programs.destroy', $program -> id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar programa</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro de eliminar el programa <b>{{ $program -> name }}</b>?</p>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
@stop
@section('scripts')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/datatables_custom.js') }}"></script>
    <!-- end page js -->
    <!-- DateRange Picked js -->
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/datetime/js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/vendors/airdatepicker/js/datepicker.en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('core-plus-theme/js/custom_js/advanceddate_pickers.js') }}"></script>

@endsection
