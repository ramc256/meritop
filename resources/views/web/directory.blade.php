@extends('layouts.web.default')

@section('title', 'Directorio')
@section('styles')
	<style>
		.jp-pagination .jp-current{
	    	background-color:{{ session('club') -> color}} !important;
	    }
	    .jp-pagination .jp-next, .jp-pagination .jp-previous{
		    color: {{ session('club') -> color}} !important;
		    border: solid 1px {{ session('club') -> color}} !important;
		}
	</style>
@stop
@section('content')
	<div class="shortcode-html">
		<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image}})">
		  	<div class="container g-bg-cover__inner">
			    <header class="g-mb-20">
		      		<h2 class="h1 g-font-weight-300 text-uppercase">Directorio</h2>
			    </header>

			    <ul class="u-list-inline">
			      	<li class="list-inline-item g-mr-7">
				        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Inicio</a>
				        <i class="fa fa-angle-right g-ml-7"></i>
			      	</li>
			      	<li class="list-inline-item g-color-white">
			        	<span>@yield('title')</span>
			      	</li>
			    </ul>
		  	</div>
		</section>
    </div>
	<!-- Sections -->
	<section class="u-shadow-v19">
		<div class="container text-center g-pb-20">
			<ul class="list-inline g-font-size-13 g-letter-spacing-1 mb-0">
				<li class="list-inline-item g-brd-right--lg g-brd-gray-light-v4 px-4 mx-0 g-py-10">
					<a class="js-go-to u-link-v5 g-color-black g-color-primary--hover text-uppercase" href="{{ route('directory') }}" data-target="#main">
						Miembros
					</a>
				</li>
				<li class="list-inline-item g-brd-right--lg g-brd-gray-light-v4 px-4 mx-0 g-py-10">
					<a class="js-go-to u-link-v5 g-color-black g-color-primary--hover text-uppercase" href="{{ route('ranking') }}" data-target="#onepages">
						Ranking
					</a>
				</li>
			</ul>
		 </div>
	</section>
	<!-- End Sections -->

	<!-- filter -->
	<section class="container g-mt-30">
		<div class="g-brd-gray-light-v4 g-brd-around text-center g-py-10">
			<ul class="list-inline g-font-size-13 g-letter-spacing-1 mb-0">
				<li class="list-inline-item g-brd-gray-light-v4 px-4 mx-0 g-py-10">
					<form class="form-inline" action="{{ route('directory.filter') }}" method="post">
						{{ csrf_field() }}
						<div class="form-group">
							<label class="form-label" for="departments">Departamentos: </label>
							<select class="form-control g-mx-15" name="department" id="directoryFilter">
									<option class="text-capitalize" value="all">Todos</option>
								@foreach ($departments as $element)
									<option @if ($element -> name == $department) selected = "selected" @endif class="text-capitalize" value="{{ $element -> name}}">{{ $element -> name}}</option>
								@endforeach
							</select>
							<button type="submit" class="btn u-btn-primary">Filtrar</button>
						</div>
					</form>
				</li>
				<li class="list-inline-item g-brd-gray-light-v4 px-4 mx-0 g-py-10">
					<form class="form-inline" action="{{ route('directory.search') }}" method="post">
						{{ csrf_field() }}
						<div class="form-group">
							<label class="form-label" for="departments"> </label>
							<input name="search" type="text" class="form-control g-mx-15" placeholder="Buscador..." required="required" maxlength="200" />
							<button type="submit" class="btn u-btn-primary">Buscar</button>
						</div>
					</form>
				</li>
			</ul>
		 </div>
	</section>
	<!-- End filter -->

	<!-- Directory -->
	<section class="container">
		<div class="row g-mb-30">
			@php($i=0)
			@foreach ($members as $element)
			@if ($i % 3 == 0)
				</div>
				<div class="row g-mb-30">
			@endif
			@php($i++)
			<!-- User Contacts -->
			<div class="col-lg-4 col-md-12 g-mb-30 g-mb-0--xl">
				<figure class="g-bg-white g-brd-around g-brd-gray-light-v4 g-brd-purple--hover g-transition-0_2 text-center">
					<div class="g-py-40 g-px-20">
						@if (empty($element -> image))
							@if ($element -> gender == 0)
								<img class="g-width-100 g-height-100 rounded-circle g-mb-20" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="Femenino">
							@else
								<img class="g-width-100 g-height-100 rounded-circle g-mb-20" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="Masculino">
							@endif
						@else
							<img class="g-width-100 g-height-100 rounded-circle g-mb-20" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="Image Description">
						@endif

						<h4 class="g-font-size-14 g-mb-5 g-color-gray-dark-v4 g-font-weight-500">{{ $element -> first_name . ' ' . $element -> last_name }}</h4>
						<div class="d-block">
							<span class="g-color-gray-dark-v5 g-brd-gray-light-v4 g-font-size-default g-mr-3">
								<i class="fa fa-envelope"></i>
							</span>
							<em class="g-color-gray-dark-v4 g-font-style-normal g-font-size-default">{{ $element -> email }}</em>
						</div>

						<div class="d-block">
							<span class="g-color-gray-dark-v5 g-brd-gray-light-v4 g-font-size-default g-mr-3">
								<i class="align-middle mr-1 icon-phone u-line-icon-pro"></i>
							</span>
							<em class="g-color-gray-dark-v4 g-font-style-normal g-font-size-default">{{ $element -> cell_phone }}</em>
						</div>
					</div>
					<hr class="g-brd-gray-light-v4 g-my-0">
					<ul class="row list-inline g-py-20 g-ma-0">
                      <li class="col g-brd-right g-brd-gray-light-v4">
                        <i class="fa fa-briefcase g-pos-rel g-top-1 g-color-gray-dark-v5 g-mr-5"></i>  {{ $element -> title }}
                      </li>
                      <li class="col g-brd-right g-brd-gray-light-v4">
                        <i class="fa fa-building  g-top-1 g-color-gray-dark-v5 g-mr-5"></i><span class="filter-column">{{ $element -> department }}</span>
                      </li>
                    </ul>
				</figure>
			</div>
			<!-- User Contacts -->
			@endforeach
		</div>
		{{ $members -> links() }}
	</section>
	<!-- End directory -->
@endsection
