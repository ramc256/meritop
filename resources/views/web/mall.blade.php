@extends('layouts.web.default')

@section('title', 'Mall')
@section('content')
<!-- ============= BREADCRUMBS ============= -->
<div class="shortcode-html">
	<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image}})">
	  	<div class="container g-bg-cover__inner">
		    <header class="g-mb-20">
	      		<h2 class="h1 g-font-weight-300 text-uppercase">@yield('title')</h2>
		    </header>

		    <ul class="u-list-inline">
		      	<li class="list-inline-item g-mr-7 text-capitalize">
			        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Inicio</a>
			        <i class="fa fa-angle-right g-ml-7"></i>
		      	</li>
		      	@if (empty($category) && empty($subcategory))
		      	<li class="list-inline-item g-mr-7 text-capitalize g-color-white">
		        	<span>@yield('title')</span>
		      	</li>
		      	@else
		      	<li class="list-inline-item g-mr-7 text-capitalize">
			        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('mall') }}">Mall</a>
			        <i class="fa fa-angle-right g-ml-7"></i>
		      	</li>

		      		@if (empty($subcategory))
		      	<li class="list-inline-item g-mr-7 text-capitalize g-color-white">
			        <span>{{ $category -> name }}</span>
		      	</li>
		      		@else
		      	<li class="list-inline-item g-mr-7 text-capitalize">
			        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('mall/'.$category -> slug) }}">{{ $category -> name }}</a>
			        <i class="fa fa-angle-right g-ml-7"></i>
		      	</li>
		      	<li class="list-inline-item text-capitalize">
			        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('mall/'.$category -> slug.'/'.$subcategory -> slug) }}"></a>
			        <span>{{ $subcategory -> name }}</span>
		      	</li>
		      		@endif
		      	@endif
		    </ul>
	  	</div>
	</section>
</div>
<!-- ============= END BREADCRUMBS ============= -->

<!-- ============= CONTAINER ============= -->
<div class="container">
	<div class="row">
		<!-- ============= CONTENT ============= -->
		<div class="col-md-9 flex-md-unordered">
			<div class="g-pl-15--lg">
				<!-- ============= FILTERS ============= -->
				{{-- <div class="d-flex justify-content-end align-items-center g-brd-bottom g-brd-gray-light-v4 g-pt-40 g-pb-20">

					<!-- Sort By -->
					<div class="g-mr-60">
						<h2 class="h6 align-middle d-inline-block g-font-weight-400 text-uppercase g-pos-rel g-top-1 mb-0">Ordenado por:</h2>

						<!-- Secondary Button -->
						<div class="d-inline-block btn-group">
							<button type="button" class="btn btn-secondary dropdown-toggle h6 align-middle g-brd-none g-color-gray-dark-v5 g-color-black--hover g-bg-transparent text-uppercase g-font-weight-300 g-font-size-12 g-pa-0 g-pl-10 g-ma-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Mas vendidos
							</button>
							<div class="dropdown-menu rounded-0">
								<a class="dropdown-item g-color-gray-dark-v4 g-font-weight-300" href="#">Mas vendidos</a>
								<a class="dropdown-item g-color-gray-dark-v4 g-font-weight-300" href="#">Menor precio</a>
								<a class="dropdown-item g-color-gray-dark-v4 g-font-weight-300" href="#">Mayor precio</a>
							</div>
						</div>
						<!-- End Secondary Button -->
					</div>
					<!-- End Sort By -->
				</div> --}}
				<!-- ============= END FILTERS ============= -->

				<!-- ============= PRODUCTS ============= -->
				@include('layouts.form-errors')
				<!-- parents_products -->
				<div class="row g-pt-30 g-mb-50">

					@foreach ($parents_products as $element)
						<!-- ============= PRODUCT ============= -->
						<div class="col-6 col-lg-4 g-mb-30">
							<figure>
								<a  href="{{ route('store.product', $element -> id_product) }}" }}>
									<div class="g-pos-rel g-mb-20">
									@if (empty($element -> image))
										<img class="img-fluid" src="{{ Storage::disk('images') -> url('img-default.png') }}" alt="{{ $element -> name }}">
									@else
										<img class="img-fluid" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> name  }}">
									@endif
										<!-- Ribbon -->
										{{-- <figcaption>
											<span class="u-ribbon-v1 g-width-40 g-height-40 g-color-white g-bg-primary g-font-size-11 text-center text-uppercase g-rounded-50x g-top-10 g-left-10 g-px-2 g-py-12">Nuevo</span>
										</figcaption> --}}
									</div>

									<div class="media">
										<!-- Product Info -->
										<div class="d-flex flex-column text-center" style="width: 100%;">
											<h4 class="h6 g-color-black mb-1">
												{{ ucwords($element -> name) }}
											</h4>
											<span class="d-inline-block g-color-gray-dark-v5 g-font-size-13 text-capitalize">{{ $element -> brand }}</span>
											<span class="d-block g-color-black g-font-size-17">{{ number_format($element -> points, 0, ',', '.') . ' ' . session('club') -> system_currency }}</span>
										</div>
									</div>
								</a>
							</figure>
						</div>
						<!-- ============= END PRODUCT ============= -->
					@endforeach
				</div>
				<hr class="g-mb-60">
				<!-- ============= END PRODUCTS ============= -->
			</div>
		</div>
		<!-- ============= END CONTENTS ============= -->

		<!-- ============= FILTERS ============= -->
		<div class="col-md-3 flex-md-first g-brd-right--lg g-brd-gray-light-v4 g-pt-40">
		<form action="{{ route('store.filter') }}" method="post">
			{{ csrf_field() }}
			<div class="g-pr-15--lg g-pt-60">
				<!-- ============= CATEGORIES ============= -->
				<div class="g-mb-30">
					<h3 class="h5 mb-3">Categorias</h3>
					<ul class="list-unstyled">
						@foreach ($categories as $element)
								<li>
									<label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
										<input name="category[{{ $element -> id }}]" class="hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" value="{{ $element -> name }}">
										<div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
											<i class="fa" data-check-icon="&#xf00c"></i>
										</div>
										<span class="text-capitalize">{{ $element -> name }}</span>{{--  <span class="float-right g-font-size-13">{{ $element -> total }}</span> --}}
									</label>
								</li>
						@endforeach
					</ul>
				</div>
				<hr>
				<!-- ============= END CATEGORIES ============= -->
				<!-- ============= BRANDS ============= -->
				<div class="g-mb-30">
					<h3 class="h5 mb-3">Marcas</h3>
					<ul class="list-unstyled">
						@foreach ($brands as $element)
							<li class="my-2">
								<label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
									<input name= "brand[{{ $element -> id }}]" class="hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" value="{{ $element -> name }}">
									<div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
										<i class="fa" data-check-icon="&#xf00c"></i>
									</div>
									<span class="text-capitalize">{{ $element -> name }}</span>{{--  <span class="float-right g-font-size-13">{{ $element -> total }}</span> --}}
								</label>
							</li>
						@endforeach
					</ul>
				</div>
				<hr>
				<!-- ============= END BRANDS ============= -->

				<button class="btn btn-block u-btn-primary g-font-size-default text-uppercase g-py-10" type="submit">Filtrar</button>
			</div>
		</form>
		</div>
		<!-- END FILTERS -->
	</div>
</div>
@endsection
