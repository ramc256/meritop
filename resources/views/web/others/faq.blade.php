@extends('layouts.web.default')

@section('title', 'Preguntas frecuentes')
@section('styles')
@stop
@section('content')

	<!-- Header -->
	<div class="shortcode-html">
		<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image}})">
		  	<div class="container g-bg-cover__inner">
			    <header class="g-mb-20">
		      		<h2 class="h1 g-font-weight-300 text-uppercase">Directorio</h2>
			    </header>

			    <ul class="u-list-inline">
			      	<li class="list-inline-item g-mr-7">
				        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Inicio</a>
				        <i class="fa fa-angle-right g-ml-7"></i>
			      	</li>
			      	<li class="list-inline-item">
			        	<span>@yield('title')</span>
			      	</li>
			    </ul>
		  	</div>
		</section>
    </div>
	<!-- End header -->

	<!-- Accordion -->
	<section class="">
		<div class="container g-py-100">
			<div class="row justify-content-center">
				<div class="col-lg-10">

					<!-- Heading -->
					<header class="row justify-content-center text-center g-my-50">
						<div class="col-lg-9">
							<h2 class="h2 g-color-black g-font-weight-600 mb-2">Preguntas frecuentes</h2>
							<div class="d-inline-block g-width-30 g-height-2 g-bg-primary mb-2"></div>
							<p class="lead mb-0">Apuntamos alto en centrarnos en construir relaciones con nuestros clientes y comunidad.</p>
						</div>
					</header>
					<!-- End Heading -->

					<div id="accordion" class="u-accordion u-accordion-color-primary" role="tablist" aria-multiselectable="true">
						<!-- Card -->
						<div class="card g-brd-none rounded g-mb-20">
							<div id="accordion-heading-01" class="g-pa-0" role="tab">
								<h5 class="mb-0">
									<a class="collapsed d-flex justify-content-between u-shadow-v19 g-color-main g-text-underline--none--hover rounded g-px-30 g-py-20" href="#accordion-body-01" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="accordion-body-01">
										Guarantee?
										<span class="u-accordion__control-icon g-color-primary">
											<i class="fa fa-angle-down"></i>
											<i class="fa fa-angle-up"></i>
										</span>
									</a>
								</h5>
							</div>
							<div id="accordion-body-01" class="collapse" role="tabpanel" aria-labelledby="accordion-heading-01">
								<div class="u-accordion__body g-color-gray-dark-v4 g-pa-30">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
							</div>
						</div>
						<!-- End Card -->

						<!-- Card -->
						<div class="card g-brd-none rounded g-mb-20">
							<div id="accordion-heading-02" class="g-pa-0" role="tab">
								<h5 class="mb-0">
									<a class="collapsed d-flex justify-content-between u-shadow-v19 g-color-main g-text-underline--none--hover rounded g-px-30 g-py-20" href="#accordion-body-02" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="accordion-body-02">
										Do you have any built-in caching?
										<span class="u-accordion__control-icon g-color-primary">
											<i class="fa fa-angle-down"></i>
											<i class="fa fa-angle-up"></i>
										</span>
									</a>
								</h5>
							</div>
							<div id="accordion-body-02" class="collapse" role="tabpanel" aria-labelledby="accordion-heading-02">
								<div class="u-accordion__body g-color-gray-dark-v4 g-pa-30">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
							</div>
						</div>
						<!-- End Card -->

						<!-- Card -->
						<div class="card g-brd-none rounded g-mb-20">
							<div id="accordion-heading-03" class="g-pa-0" role="tab">
								<h5 class="mb-0">
									<a class="collapsed d-flex justify-content-between u-shadow-v19 g-color-main g-text-underline--none--hover rounded g-px-30 g-py-20" href="#accordion-body-03" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="accordion-body-03">
										Can I add/upgrade my plan at any time?
										<span class="u-accordion__control-icon g-color-primary">
											<i class="fa fa-angle-down"></i>
											<i class="fa fa-angle-up"></i>
										</span>
									</a>
								</h5>
							</div>
							<div id="accordion-body-03" class="collapse" role="tabpanel" aria-labelledby="accordion-heading-03">
								<div class="u-accordion__body g-color-gray-dark-v4 g-pa-30">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
							</div>
						</div>
						<!-- End Card -->

						<!-- Card -->
						<div class="card g-brd-none rounded g-mb-20">
							<div id="accordion-heading-04" class="g-pa-0" role="tab">
								<h5 class="mb-0">
									<a class="collapsed d-flex justify-content-between u-shadow-v19 g-color-main g-text-underline--none--hover rounded g-px-30 g-py-20" href="#accordion-body-04" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="accordion-body-04">
										What access comes with my hosting plan?
										<span class="u-accordion__control-icon g-color-primary">
											<i class="fa fa-angle-down"></i>
											<i class="fa fa-angle-up"></i>
										</span>
									</a>
								</h5>
							</div>
							<div id="accordion-body-04" class="collapse" role="tabpanel" aria-labelledby="accordion-heading-04">
								<div class="u-accordion__body g-color-gray-dark-v4 g-pa-30">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
							</div>
						</div>
						<!-- End Card -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Accordion -->

	
	<!-- Icons Block -->
	<section class="container g-pt-100 g-pb-70">

			<div class="g-mb-30">
				<!-- Icon Blocks -->
				<div class="g-bg-blue text-center rounded g-pos-rel g-z-index-1 g-px-20 g-py-30">
					<span class="u-icon-v1 u-icon-size--xl g-color-white g-mb-10">
						<i class="icon-communication-058 u-line-icon-pro"></i>
					</span>
					<h3 class="h4 g-color-white mb-2">Soporte</h3>
					<span class="d-block h5 g-color-white-opacity-0_7 mb-4">Soporte tecnico</span>
					<button class="btn btn-md u-btn-white g-color-white g-bg-white-opacity-0_2 g-brd-white--hover g-color-blue--hover g-bg-white--hover g-rounded-25"   data-toggle="modal" data-target="#newMessage">Preguntar</button>
				</div>
				<!-- End Icon Blocks -->
			</div>
	</section>
	<!-- End Icons Block -->

@endsection
