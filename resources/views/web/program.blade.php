@extends('layouts.web.default')

@section('title', $program -> name)
@section('styles')
@stop 
@section('content')

<!-- Breadcrumbs -->
<div class="shortcode-html">
	<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image}})">
	  	<div class="container g-bg-cover__inner">
		    <header class="g-mb-20">
	      		<h2 class="h1 g-font-weight-300 text-uppercase">@yield('title')</h2>
		    </header>

		    <ul class="u-list-inline">
		      	<li class="list-inline-item g-mr-7">
			        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Inicio</a>
			        <i class="fa fa-angle-right g-ml-7"></i>
		      	</li>
		      	<li class="list-inline-item g-mr-7">
			        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('programs') }}">Programas</a>
			        <i class="fa fa-angle-right g-ml-7"></i>
		      	</li>
		      	<li class="list-inline-item g-color-white">
		        	<span>@yield('title')</span>
		      	</li>
		    </ul>
	  	</div>
	</section>
</div>
<!-- End Breadcrumbs -->

<!-- Section -->
<section class="container g-py-100 g-pb-40">
	<div class="row">
		<div class="col-lg-7 g-mb-60">
			<div class="mb-4">
				<h2 class="h3 text-uppercase mb-3">{{ $program -> name }}</h2>
				<div class="g-width-60 g-height-1 g-bg-black"></div>
			</div>
			<div class="mb-5">
				<p>{!! $program -> description !!}</p>
			</div>
		</div>

		<div class="col-lg-5 g-mb-60">
			<img class="img-fluid" src="{{ $_ENV['STORAGE_PATH'] . $program -> image }}" alt="Image Description">
			<p class="g-py-10 g-brd-gray-light-v4 g-font-size-11">{{ date_format($program -> created_at, 'd/m/Y') }}</p>
		</div>
	</div>
</section>
<!-- End Section -->

<!-- News Block 04 -->
<section class="g-bg-secondary g-py-100">
	<div class="container">
		<!-- Heading -->
		<header class="row justify-content-center text-center g-my-50">
			<div class="col-lg-9">
				<h2 class="h2 g-color-black g-font-weight-600 mb-2">Objetivos</h2>
				<div class="d-inline-block g-width-30 g-height-2 g-bg-primary mb-2"></div>
				<p class="lead mb-0">Cumple los objetivos dispuestos para tí y SUMA!</p>
			</div>
		</header>
		<!-- End Heading -->

		<div id="shortcode4">
			<div class="row">
				@foreach ($objetives as $element)								
				<div class="col-lg-6 g-mb-30">
					<article class="u-shadow-v21 g-bg-white rounded">
						<div class="g-px-30 g-pt-30" >
							<h3 class="g-font-weight-300">{{ $element -> name }}</h3>
							<p>{{ $element -> description }}</p>
						</div>
						<div class="media g-font-size-12 g-brd-top g-brd-gray-light-v4 g-px-30 g-pt-15">
							@php($date = date_create($element -> due_date))
							<p><b>Finaliza:</b> {{ date_format($date, 'd/m/Y') }}</p>
						</div>
					</article>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>
<!-- End News Block 04 -->
@stop
@section('scripts')
@endsection
