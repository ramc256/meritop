@extends('layouts.web.myaccount')

@section('title', 'Ordenes')
@section('styles')

@stop
@section('content')

<!-- Product Table Panel -->
<div class="card border-0">
	<div class="card-header d-flex align-items-center justify-content-between g-bg-gray-light-v5 border-0 g-mb-15">
		<h3 class="h6 mb-0">
			<i class="icon-directions g-pos-rel g-top-1 g-mr-5"></i> Ordenes
		</h3>
	</div>
	<div class="card-block g-pa-0">
		@if (count($orders))
			<!-- Product Table -->
			<div class="table-responsive">
				<table class="table table-bordered u-table--v2">
					<thead class="text-uppercase g-letter-spacing-1">
						<tr>
							<th class="g-font-weight-300 g-color-black">Fecha</th>
							<th class="g-font-weight-300 g-color-black">Orden # </th>
							<th class="g-font-weight-300 g-color-black">Sede</th>
							{{-- <th class="g-font-weight-300 g-color-black">N° Productos</th> --}}
							<th width="100" class="g-font-weight-300 g-color-black">Estatus</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($orders as $element)
							<tr>
								<td class="align-middle text-nowrap">
									{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}
								</td>
								<td class="align-middle text-nowrap">
									<a class="g-mb-2" href="{{ route('my-account.order', $element -> id) }}">{{ str_pad( $element -> id, 10 , 0, STR_PAD_LEFT) }}</a>
								</td>
								<td class="align-middle">
									{{ $element -> branch }}
								</td>
								{{-- <td class="align-middle">
									<div class="d-flex">
										{{ $element -> products }}
									</div>
								</td> --}}
								<td class="align-middle">
									<div class="d-flex">
										@if ($element -> status == 0) <span class="bg-color-default g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase">Abierto</span>
		                                @elseif($element -> status == 1) <span class="bg-color-primary g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase">Enviado </span>
		                                @elseif($element -> status == 2) <span class="bg-color-success g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase">Recibido </span>
		                                @elseif($element -> status == 3) <span class="bg-color-warning g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase">Devuelto</span>
		                                @elseif($element -> status == 4) <span class="bg-color-danger g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase">Cancelado</span>
										@endif
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			{{ $orders -> links() }}
		@else
			<p class="text-center">Usted no posee ordenes</p>
		@endif
	</div>
</div>

@endsection
