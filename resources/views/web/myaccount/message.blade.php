@extends('layouts.web.myaccount')

@section('title', 'Mensaje')
@section('content')
	@php($iname = Auth::user() -> first_name . ' ' . Auth::user() -> last_name)
<!-- Product Table Panel -->
<div class="card border-0">
	<div class="card-header d-flex align-items-center justify-content-between g-bg-gray-light-v5 border-0 g-mb-15">
		<h3 class="h6 mb-0">
			<i class="icon-directions g-pos-rel g-top-1 g-mr-5"></i> Mensaje
		</h3>
	</div>
	<div class="card-block g-py-20">
		<div class="g-mb-15">
	      	<h5 class="d-flex justify-content-between align-items-center h4 g-color-gray-dark-v1 mb-0">
		        <span class="d-block g-mr-10">{{ $message -> subject }}</span>
	      	</h5>
	      	<span class="g-color-gray-dark-v4 g-font-size-12">{{ date_format($message -> created_at, 'd/m/Y h:i:s A') }} @if ($message -> status == 1) <i class="fa fa-circle text-success  g-font-size-10 g-ml-10 g-mr-5"></i> Abierto @else <i class="fa fa-circle text-danger  g-font-size-10"></i> Cerrado @endif</span> 
	    </div>
    	<p>{{ $message -> message }}</p>
	</div>

	@if (count($message) > 0)	
	<div class="g-py-20 g-brd-top g-brd-gray-light-v4">
		@foreach ($responses as $element)
		<div class="media g-mb-30 @if ($element -> name != $iname) g-ml-20 @endif">
			@if ($element -> name == $iname)
		  	<div class="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-20u-shadow-v22 g-bg-primary g-color-white g-mr-15 h6"><span style="display: block; margin: auto;">YO</span></div>
		  	@endif
		  	<div class="media-body g-brd-around g-brd-gray-light-v4 g-pa-30">
			    <div class="g-mb-15">
			      	<h5 class="d-flex justify-content-between align-items-center h6 g-color-gray-dark-v1 mb-0">
				        <span class="d-block g-mr-10 text-capitalize">{{ $element -> name }}</span>
			      	</h5>
			      	<span class="g-color-gray-dark-v4 g-font-size-12">{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}</span>
			    </div>
			    <p>{{ $element -> message }}</p>
		  	</div>		  	
			@if ($element -> name != $iname)
		  	<div class="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-20u-shadow-v22 g-bg-primary g-color-white g-ml-15 h6"><span style="display: block; margin: auto;">RE</span></div>
		  	@endif
		</div>  
		@endforeach	
	</div>   
	@endif

	@if ($message -> status == 1)
		<form action="{{ route('imessage.response', $message -> id) }}" method="post">
			<div class="g-py-20 g-brd-top g-brd-gray-light-v4">
					{{ csrf_field() }}
					<div class="form-group">
						<textarea name="message" id="message" rows="8" class="form-control r-brd-none no-resize" required="required" placeholder="Escriba su respuesta acá..."></textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Responder</button>
					</div>
			</div> 
		</form>
	@endif
</div>    
@stop

@section('scripts')
    <!-- page js -->
    <script>
        $(document).ready(function(){

            // Obtiene el ID de la imagen y la inserta en el formulario para posteriormente enviarlo al metodo de cambio de imagen
            $('.btnResponse').click(function(){
                value = $(this).children('.data').text();
                $('.id_response').val(value);
            });
        });
    </script>
@endsection
