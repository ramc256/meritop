@extends('layouts.web.default')

@section('title', 'Mi cuenta')
@section('styles')
	<!-- CSS Page Style -->
	<link rel="stylesheet" href="{{ asset('css/pages/profile.css') }}">

@stop
@section('content')
	<!--=== Breadcrumbs v4 ===-->
	<div class="breadcrumbs-v4  bg-mall">
		<div class="container">
			<h1>@yield('title')</h1>
			<ul class="breadcrumb-v4-in">
				<li><a href="index.html">Inicio</a></li>
				<li class="active">@yield('title')</li>
			</ul>
		</div><!--/end container-->
	</div>
	<!--=== End Breadcrumbs v4 ===-->



		<!--=== Profile ===-->
		<div class="container content profile">
			<div class="row">
				<!--Left Sidebar-->
				<div class="col-md-3 md-margin-bottom-40">
					<img class="img-responsive profile-img margin-bottom-20" src="{{ asset('images/members/6.jpg') }}" alt="">
					
					<ul class="list-group sidebar-nav-v1 margin-bottom-40" id="sidebar-nav-1">
						<li class="list-group-item active">
							<a href="{{ url('pages/dashboard') }}"><i class="fa fa-tachometer "></i> Dashboard</a>
						</li>
						<li class="list-group-item ">
							<a href="{{ url('pages/myaccount') }}"><i class="fa fa-bar-chart-o"></i> Balance de puntos</a>
						</li>
						<li class="list-group-item">
							<a href="{{ url('pages/orders') }}"><i class="fa fa-cubes"></i> Ordenes</a>
						</li>
						<li class="list-group-item">
							<a href="{{ url('pages/ranking') }}"><i class="fa fa-trophy"></i> Ranking</a>
						</li>
						<li class="list-group-item">
							<a href="{{ url('pages/perfil') }}"><i class="fa fa-user"></i> Perfil</a>
						</li>
					</ul>


					<div class="margin-bottom-50"></div>

					<!--Datepicker-->
					<form action="#" id="sky-form2" class="sky-form">
						<div id="inline-start"></div>
					</form>
					<!--End Datepicker-->
				</div>
				<!--End Left Sidebar-->

				<div class="col-md-9">

					<!-- Info Blokcs -->
					<div class="row margin-bottom-30">
						<!-- Welcome Block -->
						<div class="col-md-12 md-margin-bottom-40">
							<div class="headline"><h2>Ranking</h2></div>


						</div>
					</div>
					<div class="row news-v2">
						<div class="col-md-4 md-margin-bottom-30">
							<div class="news-v2-badge text-center" style="height: 100px; line-height: 100px;">
								<img class="img-responsive" src="{{ asset('images/clubs/meritop.jpg') }}" alt="" height="150">
							</div>
							<div class="news-v2-desc bg-color-light">
								<h3>Club Meritop</h3>
								<small>Posición: 5 | Total: 310</small>
							</div>
						</div>
						<div class="col-md-4 md-margin-bottom-30">
							<div class="news-v2-badge text-center" style="height: 100px; line-height: 100px;">
								<img class="img-responsive" src="{{ asset('images/clubs/mercantil.jpg') }}" alt="" height="150">
							</div>
							<div class="news-v2-desc bg-color-light">
								<h3>Club Mercantil</h3>
								<small>Posición: 18 | Total: 120</small>
							</div>
						</div>
					</div>

				</div>	
			</div>
		</div><!--/container-->
		<!--=== End Profile ===-->
	

@stop
@section('scripts')
@endsection