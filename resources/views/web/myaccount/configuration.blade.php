@extends('layouts.web.myaccount')

@section('title', 'Configuración')
@section('styles')
	<style>
		.pruebaxz{
		    position: relative;
		    z-index: 2;
		    -webkit-box-flex: 1;
		    -webkit-flex: 1 1 auto;
		    -ms-flex: 1 1 auto;
		    flex: 1 1 auto;
		    width: 1%;
		    margin-bottom: 0;
	            border: solid 1px #ccc;
	        color: #000;
	        padding: .8rem 1rem .6rem;
		    background-color: #fff;
		    background-image: none;
		    height: calc(2.25rem + 2px);

			}

		    input[type=date]::-webkit-inner-spin-button,
			input[type=date]::-webkit-outer-spin-button {
			  -webkit-appearance: none;
			  margin: 0;
			}
			input[type=date] { -moz-appearance:textfield; }

</
		}
	</style>
@stop
@section('content')
		<!-- Nav tabs -->
		@include('flash::message')
		@include('layouts.form-errors')
		<ul class="nav nav-justified u-nav-v1-1 u-nav-primary g-brd-bottom--md g-brd-bottom-2 g-brd-primary g-mb-20" role="tablist" data-target="nav-1-1-default-hor-left-underline" data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-primary g-mb-20">
			<li class="nav-item">
				<a class="nav-link g-py-10 active" data-toggle="tab" href="#datos-personales" role="tab">Datos personales</a>
			</li>
			<li class="nav-item">
				<a class="nav-link g-py-10" data-toggle="tab" href="#datos-clubs" role="tab">Datos de Clubs</a>
			</li>
			<li class="nav-item">
				<a class="nav-link g-py-10" data-toggle="tab" href="#modificar-contrasena" role="tab">Modificar contraseña</a>
			</li>
			<li class="nav-item">
				<a class="nav-link g-py-10" data-toggle="tab" href="#sedes" role="tab">Direcciones de envío</a>
			</li>
		</ul>
		<!-- End Nav tabs -->

		<!-- Tab panes -->
		<div id="nav-1-1-default-hor-left-underline" class="tab-content">
			<!-- Edit Profile -->
			<div class="tab-pane fade show active" id="datos-personales" role="tabpanel">
				<form class="" action="{{ route('my-account.update') }}" method="post">
					{{ csrf_field() }}
					<div class="row">

						<!-- Input -->
						<div class="form-group col-md-4 g-mb-25">
							<label for="dni" class="g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">DNI</label>
							<div class="input-group g-brd-primary--focus">
								<input id="dni" name="dni" value="{{ $member -> dni }}" type="text" class="form-control form-control-md" placeholder="Documento de identificación" required="required" readonly="readonly">
							</div>
						</div>
						<!-- End Input -->
					</div>
					<div class="row">
						<!-- Input -->
						<div class="form-group col-md-6 g-mb-25">
							<label for="first_name" class="g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Nombres</label>
							<div class="input-group g-brd-primary--focus">
								<input id="first_name" name="first_name" value="{{ $member -> first_name }}" type="text" class="form-control form-control-md" placeholder="Escríba sus nombres" required="required">
							</div>
						</div>
						<!-- End Input -->

						<!-- Input -->
						<div class="form-group col-md-6 g-mb-25">
							<label for="last_name" class="g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Apellidos</label>
							<div class="input-group g-brd-primary--focus">
								<input id="last_name" name="last_name" value="{{ $member -> last_name }}" type="text" class="form-control form-control-md" placeholder="Escríba sus apellidos" required="required">
							</div>
						</div>
						<!-- End Input -->
					</div>


					<div class="row">
						<!-- Input -->
						<div class="form-group col-md-6 g-mb-25">
							<label for="email" class="g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Email</label>
							<div class="input-group g-brd-primary--focus">
								<input id="email" name="email" value="{{ $member -> email }}" type="text" class="form-control form-control-md" placeholder="Correo electronico" required="required">
							</div>
						</div>
						<!-- End Input -->

						<!-- Input -->
						<div class="form-group col-md-6 g-mb-25">
							<label for="cell_phone" class="g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Teléfono</label>
							<div class="input-group g-brd-primary--focus">
								<input id="cell_phone" name="cell_phone" value="{{ $member -> cell_phone }}" type="text" class="form-control form-control-md" placeholder="Numero telefonico" required="required">
							</div>
						</div>
						<!-- End Input -->
					</div>
					<div class="row">
						<!-- Input -->
						<div class="form-group col-md-6 g-mb-25">
							<label for="gender" class="g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Genero</label>
							<div class="input-group g-brd-primary--focus">
								<select id="gender" name="gender" class="form-control form-control-md" required="required">
									<option @if ($member -> gender == 0) selected="selected" @endif value="0">Femenino</option>
									<option @if ($member -> gender == 1) selected="selected" @endif value="1">Mascuino</option>
								</select>
							</div>
						</div>
						<!-- End Input -->
						<!-- Input -->
						<div class="form-group col-md-6 g-mb-25">
							<label for="birthdate" class="g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Fecha de nacimiento</label>
							<div class="input-group g-brd-primary--focus">
								<input id="birthdate" name="birthdate" value="{{$member->birthdate}}" type="date" class="pruebaxz" placeholder="DD/MM/YYYY" required="required">
							</div>
						</div>
						<!-- End Input -->
					</div>

					<div class="form-group g-mb-25">
						<label for="biography" class="g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Biografia</label>
						<div class="input-group g-brd-primary--focus">
							<textarea id="biography" name="biography" class="form-control form-control-md" placeholder="Habla un poco de tí" rows="4" maxlength="250">{{ $member -> biography }}</textarea>
						</div>
					</div>
					<!-- End Input -->

					<hr class="g-brd-gray-light-v4 g-my-25">

					<div class="text-sm-right">
						<button class="btn u-btn-primary rounded-0 g-py-12 g-px-25" type="submit">Guardar cambios</button>
					</div>
				</form>
			</div>
			<!-- End Edit Profile -->

			<!-- Datos de Clubs -->
			<div class="tab-pane fade" id="datos-clubs" role="tabpanel">
				<h2 class="h4 g-font-weight-300">Datos de clubs</h2>
				<ul class="list-unstyled g-mb-30">
					@foreach ($clubs_data as $key => $element)

					<li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                      	<div class="col-md-6 g-pr-10">
	                        <strong class="d-block g-color-gray-dark-v2 g-width-200 g-pr-10">Carnet</strong>
	                        <span class="align-top">{{$element -> carnet}}</span>
                      	</div>

                      	<div class="col-md-6 g-pr-10">
	                        <strong class="d-block g-color-gray-dark-v2 g-width-200 g-pr-10">Supervisor</strong>
	                        <span class="align-top">{{$element -> supervisor_code}}</span>
                      	</div>
                    </li>
					<li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                      	<div class="col-md-6 g-pr-10">
	                        <strong class="d-block g-color-gray-dark-v2 g-width-200 g-pr-10">Departamento</strong>
	                        <span class="align-top">{{$element -> department}}</span>
                      	</div>
                      	<div class="col-md-6 g-pr-10">
	                        <strong class="d-block g-color-gray-dark-v2 g-width-200 g-pr-10">Cargo</strong>
	                        <span class="align-top">{{$element -> title}}</span>
                      	</div>
                    </li>
                    <li>
                      	<div class="col-md-6 g-pr-10">
	                        <strong class="d-block g-color-gray-dark-v2 g-width-200 g-pr-10">Email</strong>
	                        <span class="align-top">{{$element -> email}}</span>
                      	</div>
                    </li>

					@endforeach
				</ul>
			</div>
			<!-- End Datos de Clubs -->


			<!-- Change Passowrd -->
			<div class="tab-pane fade" id="modificar-contrasena" role="tabpanel">
				<h2 class="h4 g-font-weight-300">Modificar contraseña</h2>

				<form action="{{ route('my-account.change-password') }}" method="post">
					{{ csrf_field() }}
					<!-- Current Password -->
					<div class="form-group row g-mb-25">
						<label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Contraseña anterior</label>
						<div class="col-sm-9">
							<div class="input-group g-brd-primary--focus">
								<input name="old_password" class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" placeholder="Escriba su contraseña anterior">
								<div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
									<i class="icon-lock"></i>
								</div>
							</div>
						</div>
					</div>
					<!-- End Current Password -->

					<!-- New Password -->
					<div class="form-group row g-mb-25">
						<label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Nueva contraseña</label>
						<div class="col-sm-9">
							<div class="input-group g-brd-primary--focus">
								<input name="new_password" class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" placeholder="Escriba su nueva contraseña">
								<div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
									<i class="icon-lock"></i>
								</div>
							</div>
						</div>
					</div>
					<!-- End New Password -->

					<!-- Verify Password -->
					<div class="form-group row g-mb-25">
						<label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Repetir contraseña</label>
						<div class="col-sm-9">
							<div class="input-group g-brd-primary--focus">
								<input name="re_passowrd" class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" placeholder="Repita la contraseña anterior">
								<div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
									<i class="icon-lock"></i>
								</div>
							</div>
						</div>
					</div>
					<!-- End Verify Password -->

					<hr class="g-brd-gray-light-v4 g-my-25">

					<div class="text-sm-right">
						<button class="btn u-btn-primary rounded-0 g-py-12 g-px-25 g-mr-10" type="submit">Guardar cambios</button>
					</div>
				</form>
			</div>
			<!-- End Change Passowrd -->

			<!-- Direcciones de envio -->
			<div class="tab-pane fade" id="sedes" role="tabpanel">
				<h2 class="h4 g-font-weight-300">Direcciones de envio</h2>

				<form action="{{ route('my-account.edit-branch') }}" method="post">
					{{ csrf_field() }}
					<!-- Current Password -->
					<div class="form-group row g-mb-25">
						@foreach ( $branches as $key => $element)
							<label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">{{ ucwords($key) }}</label>
							<div class="col-sm-9">
								<div class="input-group g-brd-primary--focus">
									<select name="fk_id_branch[]" class="form-control form-control-md rounded-0 pr-0 address">
										<option value="">Seleccione una sede ...</option>
									@foreach ( $element as $value)
										<option @if (in_array($value -> id, array_column($member_branches,'fk_id_branch'))) selected="selected" @endif value="{{ $value -> id }}">{{ $value -> name }}</option>
									@endforeach
									</select>
								</div>
								<ul class="data-branch list-unstyled g-my-20"></ul>
							</div>
						@endforeach
					</div>
					<!-- End Current Password -->


					<hr class="g-brd-gray-light-v4 g-my-25">

					<div class="text-sm-right">
						<button class="btn u-btn-primary rounded-0 g-py-12 g-px-25 g-mr-10" type="submit">Guardar cambios</button>
					</div>
				</form>
			</div>
			<!-- End Direcciones de envio -->
		</div>
		<!-- End Tab panes -->
@stop
@section('scripts')
	<script>
		data($('.address').val());
		$('.address').change(function(){
			data($(this).val());
		});

		function data(id){
			$.ajax({
				type: 'get',
				url: '{{ url("/shopping/branches") }}/'+id,
				success: function(data) {
					console.log(data[0]);
					if(data[0]['name'] == null || data == undefined){
						$('.data-branch').empty();
					}else{
						$('.data-branch').empty().append(
							"<li>"+data[0]['name']+"</li><li>"+data[0]['address']+". "+data[0]['city']+", "+data[0]['state']+", "+data[0]['postal_code']+"</li><li>"+data[0]['phone']+"</li>"
						);
					}
				}
			});
		}
	</script>
@endsection
