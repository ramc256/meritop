@extends('layouts.web.myaccount')

@section('title', 'Orden')
@section('styles')
<style >
	@media print {

		#aside-page, #header-page, #js-header, #footer-page, #general-info, #button-print {
			display: none;
		}
	}
</style>
@stop
@section('content')

@if (count($order) > 0 && count($order_products) > 0)
<!-- General Info -->
<section id="general-info" class="container g-pt-100 g-pb-30">
	<div class="row">
		<div class="col-sm-4 col-md-6 g-mb-30">
			<h2 class="h4 g-font-weight-700 text-uppercase">{{ Auth::user()-> first_name . ' ' . Auth::user() -> last_name }}</h2>
			<p class="g-color-gray-dark-v4">{{ $order -> branch }}</p>
		</div>
		<div class="col-sm-8 col-md-6">
			<div class="row justify-content-end no-gutters">
				<div class="col-sm-4 mb-1">
					<div class="g-bg-gray-light-v5 text-center g-pa-15">
						<h3 class="h6 g-color-black g-font-weight-600 text-uppercase"># Orden</h3>
						<p class="g-font-size-13 mb-0">{{ $order -> id }}</p>
					</div>
				</div>
				<div class="col-sm-4 mb-1">
					<div class="g-bg-gray-light-v5 text-center g-pa-15">
						<h3 class="h6 g-color-black g-font-weight-600 text-uppercase"> Fecha</h3>
						<p class="g-font-size-13 mb-0">{{ date_format($order -> created_at, 'd/m/Y') }}</p>
					</div>
				</div>
				<div class="col-sm-4 mb-1">
					<div class="g-bg-gray-light-v5 text-center g-pa-15">
					<h3 class="h6 g-color-black g-font-weight-600 text-uppercase"> Estatus</h3>
					<p class="g-font-size-13 mb-0">
						@if ($order -> status == 0) <span class="bg-color-default g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase"><i class="fa fa-circle-o"></i> Abierto</span>
			            @elseif($order -> status == 1) <span class="bg-color-primary g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase"><i class="fa fa-share"></i> Enviado </span>
			            @elseif($order -> status == 2) <span class="bg-color-success g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase"><i class="fa fa-check"></i> Recibido </span>
			            @elseif($order -> status == 3) <span class="bg-color-warning g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase"><i class="fa fa-mail-reply"></i> Devuelto</span>
			            @elseif($order -> status == 4) <span class="bg-color-danger g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase"><i class="fa fa-close"></i> Cancelado</span>
						@endif
					</p>
				</div>
			</div>
			</div>
		</div>
	</div>
</section>
<!-- End General Info -->

<!-- Product Table Panel -->
<div class="card border-0">
	<table class="text-center w-100">
		<thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-black text-uppercase">
			<tr>
				<th class="g-font-weight-400 text-left g-pb-20">Producto</th>
				<th class="g-font-weight-400 g-width-130 g-pb-20">Precio</th>
				<th class="g-font-weight-400 g-width-50 g-pb-20">Cantidad</th>
				<th class="g-font-weight-400 g-width-130 g-pb-20">Total</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
		@php($total = 0)
		@foreach ($order_products as $element)
			<!-- Item-->
			<tr class="g-brd-bottom g-brd-gray-light-v3">
				<td class="text-left g-py-25">
					@if (empty($element -> image))
						<img class="d-inline-block g-width-100 mr-4" src="{{ Storage::disk('images')->url('product-default.jpg') }}" alt="Image Description">
					@else
						<img class="d-inline-block g-width-100 mr-4" src="{{ $_ENV['STORAGE_PATH'].$element -> image }}" alt="Image Description">
					@endif
					<div class="d-inline-block align-middle">
						<h4 class="h6 g-color-black text-capitalize">{{ $element -> parent}} <span class="g-color-gray-dark-v2">{{ $element -> feature  }}</span></h4>
						<p class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_6 mb-0">
							{{ $element -> excerpt }}
						</p>
					</div>
				</td>
				<td class="g-color-gray-dark-v2 g-font-size-13">
					{{ number_format($element -> points, 0, ',', '.') }}</td>
				<td class="g-color-gray-dark-v2 g-font-size-13">
					{{ $element -> quantity }}
				</td>
				<td class="g-color-gray-dark-v2 g-font-size-13">
					{{ number_format($element -> points * $element -> quantity, 0, ',', '.') }}
				</td>
			</tr>
			<!-- End Item-->
		@php($total += ($element -> points * $element -> quantity) )
		@endforeach
			<tr>
				<td></td>
				<td></td>
				<td class="g-color-gray-dark-v2 g-font-size-16 g-py-30">Total</td>
				<td class="g-color-gray-dark-v2 g-font-size-16 g-py-30">{{ number_format($total, 0, ',', '.') }}</td>
			</tr>
		</tbody>
	</table>

	@foreach ($guide as $element)
	<div class="row g-mb-30">
		<div class="col-sm-4 mb-1">
			<div class="g-bg-gray-light-v5 text-center g-py-15">
				<h3 class="h6 g-color-black g-font-weight-600 text-uppercase"> Agente</h3>
				<p class="g-font-size-13 mb-0">
					{{ $element -> name }}
				</p>
			</div>
		</div>
		<div class="col-sm-4 mb-1">
			<div class="g-bg-gray-light-v5 text-center g-py-15">
				<h3 class="h6 g-color-black g-font-weight-600 text-uppercase"> Guía de envio</h3>
				<p class="g-font-size-13 mb-0">
					{{ $element -> num_guide }}
				</p>
			</div>
		</div>
		<div class="col-sm-4 mb-1">
			<div class="g-bg-gray-light-v5 text-center g-py-15">
				<h3 class="h6 g-color-black g-font-weight-600 text-uppercase"> Fecha</h3>
				<p class="g-font-size-13 mb-0">
					{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}
				</p>
			</div>
		</div>
	</div>
	@endforeach
	<div class="text-right">
		<button id="button-print" type="button" class="btn u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4" onclick="javascript:window.print();"> <i class="fa fa-fw fa-print"></i>Imprimir</button>
	</div>
</div>
@else
<h3 class="text-center h4 py-5">La orden solicitada no existe</h3>
@endif

@endsection
