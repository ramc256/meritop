@extends('layouts.web.myaccount')

@section('title', 'Mi Cuenta')
@section('styles')

@stop
@section('content')
	<!-- Overall Statistics -->
	<div class="row g-mb-40">
		<div class="col-md-4 ">
			<div class="bg-color g-color-white g-pa-25">
				<header class="d-flex text-uppercase">
					<i class="fa fa-trophy g-font-size-60 align-self-center display-4 g-mr-20"></i>
					<div class="g-line-height-1">
						<h4 class="h5">Ganados</h4>
						<div class="js-counter g-font-size-20" data-comma-separated="true">{{ number_format($total_acquired, 0 , '', '.')}}</div>
					</div>
				</header>
			</div>
		</div>

		<div class="col-md-4">
			<div class="bg-color g-color-white g-pa-25">
				<header class="d-flex text-uppercase">
					<i class="fa fa-gift g-font-size-60 align-self-center display-4 g-mr-20"></i>
					<div class="g-line-height-1">
						<h4 class="h5">Canjeados</h4>
						<div class="js-counter g-font-size-20" data-comma-separated="true">{{ number_format($total_trades, 0 , '', '.') }}</div>
					</div>
				</header>
			</div>
		</div>

		<div class="col-md-4">
			<div class="bg-color g-color-white g-pa-25">
				<header class="d-flex text-uppercase">
					<i class="fa fa-circle-o  g-font-size-60 align-self-center display-4 g-mr-20"></i>
					<div class="g-line-height-1">
						<h4 class="h5">Disponibles</h4>
						
						<div class="js-counter g-font-size-20" data-comma-separated="true">{{ number_format($account -> points, 0 , '', '.') }}</div>
					</div>
				</header>
			</div>
		</div>
	</div>
	<!-- End Overall Statistics -->

	<!-- Product Table Panel -->
	<div class="card border-0">
		<div class="card-header d-flex align-items-center justify-content-between g-bg-gray-light-v5 border-0 g-mb-15">
			<h3 class="h6 mb-0">
				<i class="icon-directions g-pos-rel g-top-1 g-mr-5"></i> Transacciones
			</h3>
		</div>

		<div class="card-block g-pa-0">
			@if (count($transactions))
				<!-- Product Table -->
			<div class="table-responsive">
				<table class="table table-bordered u-table--v2">
					<thead class="text-uppercase g-letter-spacing-1">
						<tr>
							<th class="g-font-weight-300 g-color-black">Fecha</th>
							<th class="g-font-weight-300 g-color-black">Descripción</th>
							<th width="100" class="g-font-weight-300 g-color-black">Operación</th>
							<th class="g-font-weight-300 g-color-black">{{ session('club') -> system_currency }}</th>
							<th class="g-font-weight-300 g-color-black">Saldo</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($transactions as $element)
							<tr>
								<td class="align-middle text-nowrap">
									{{ date_format($element -> created_at, 'd/m/Y h:i:s A') }}
								</td>
								<td class="align-middle">
									<h4 class="h6 g-mb-2">{{ $element -> description }}</h4>
									<div class="js-rating g-font-size-12 g-color-primary" data-rating="3.5"></div>
								</td>
								<td class="align-middle text-center">
									@if ($element -> operation == 1)
										<span class="bg-color-success g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase"> Aporte </span>
									@else
										<span class="bg-color-danger g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase"> Canje </span>
									@endif
								</td>
								<td class="align-middle">
									<div class="d-flex">
										{{ number_format($element -> points, 0, ',', '.') }}
									</div>
								</td>
								<td class="align-middle">
									<div class="d-flex">
										{{ number_format($element -> balance, 0, ',', '.') }}
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			{{ $transactions -> links() }}
			@else
				<p class="text-center">Usted no posee transacciones</p>
			@endif
		</div>
	</div>
	</section>

@stop
@section('scripts')
@endsection