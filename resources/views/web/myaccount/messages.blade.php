@extends('layouts.web.myaccount')

@section('title', 'Mensajes')
@section('styles')
	<link  rel="stylesheet" href="{{ asset('vendor/custombox/custombox.min.css')}}">
@stop
@section('content')

<!-- Product Table Panel -->
<div class="card border-0">
	<div class="card-header d-flex align-items-center justify-content-between g-bg-gray-light-v5 border-0 g-mb-15">
		<h3 class="h6 mb-0">
			<i class="icon-directions g-pos-rel g-top-1 g-mr-5"></i> Mensajes
		</h3>
		<button type="button" class="btn btn-success"  data-toggle="modal" data-target="#newMessage"><i class="fa fa-plus"></i> Nuevo</button>
	</div>
	<div class="card-block g-pa-0">
	@php($message = null)
		@if (count($messages) > 0)
			<!-- Product Table -->
			<div >
				<table class="table table-bordered u-table--v2">
					<thead class="text-uppercase g-letter-spacing-1">
						<tr>
							<th class="g-font-weight-300 g-color-black">Fecha</th>
							<th class="g-font-weight-300 g-color-black">Asunto</th>
							<th class="g-font-weight-300 g-color-black">Mensaje</th>
							<th class="g-font-weight-300 g-color-black">Estatus</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($messages as $element)
							<tr>
								<td class="align-middle text-nowrap">
									{{ date_format($element -> created_at, 'd-m-Y') }}
								</td>
								<td class="align-middle text-nowrap">
									<a class="g-color-black g-mb-2" href="{{ route('imessages.show', $element -> id) }}">{{ $element -> subject }}</a>
								</td>
								<td class="align-middle">
									{{ substr($element -> message, 0,200) }}...
								</td>
								<td class="align-middle">
									@if ($element -> status == 1)
										<span class="bg-color-success g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase">Activo</span>
	                                @else
	                                	<span  class="bg-color-danger g-py-5 g-px-10 g-color-white g-font-size-11 text-uppercase">Cerrado</span>
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			{{ $messages -> links() }}
		@else
			<p class="text-center">Usted no posee mensajes</p>
		@endif
	</div>
</div>
@endsection
