@extends('layouts.web.default')

@section('title', 'Ranking')
@section('styles')
	<style>
		.jp-pagination .jp-current{
	    	background-color:{{ session('club') -> color}} !important;
	    }
	    .jp-pagination .jp-next, .jp-pagination .jp-previous{
		    color: {{ session('club') -> color}} !important;
		    border: solid 1px {{ session('club') -> color}} !important;
		}
		.btn-brd, .btn-brd:hover{
			border-color: #999;
			color: #999;
			margin-left: auto;
			margin-right: auto;
		}
	</style>
@stop
@section('content')
	<div class="shortcode-html">
		<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image}})">
		  	<div class="container g-bg-cover__inner">
			    <header class="g-mb-20">
		      		<h2 class="h1 g-font-weight-300 text-uppercase">Ranking</h2>
			    </header>

			    <ul class="u-list-inline">
			      	<li class="list-inline-item g-mr-7">
				        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Inicio</a>
				        <i class="fa fa-angle-right g-ml-7"></i>
			      	</li>
			      	<li class="list-inline-item g-color-white">
			        	<span>@yield('title')</span>
			      	</li>
			    </ul>
		  	</div>
		</section>
    </div>
	<!-- Sections -->
	<section class="u-shadow-v19">
		<div class="container text-center g-pb-20">
			<ul class="list-inline g-font-size-13 g-letter-spacing-1 mb-0">
				<li class="list-inline-item g-brd-right--lg g-brd-gray-light-v4 px-4 mx-0 g-py-10">
					<a class="js-go-to u-link-v5 g-color-black g-color-primary--hover text-uppercase" href="{{ route('directory') }}" data-target="#main">
						Miembros
					</a>
				</li>
				<li class="list-inline-item g-brd-right--lg g-brd-gray-light-v4 px-4 mx-0 g-py-10">
					<a class="js-go-to u-link-v5 g-color-black g-color-primary--hover text-uppercase" href="{{ route('ranking') }}" data-target="#onepages">
						Ranking
					</a>
				</li>
			</ul>
		 </div>
	</section>
	<!-- End Sections -->

	<!-- Data -->
	<div class="container g-py-30">
		<div class="g-brd-gray-light-v4 g-brd-around text-center g-py-10">
			<div class="row">
				<div class="col-lg-2 text-center">
					@if (empty(Auth::user() -> image))
						@if (Auth::user() -> gender == 0)
						<img width="90%" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name  }}">
						@else
						<img width="90%" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name  }}">
						@endif
					@else
					<img width="90%" src="{{ $_ENV['STORAGE_PATH'] . Auth::user() -> image }}" alt="{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name  }}">
					@endif
				</div>
				<div class="col-lg-10">
					<div class="row">
						<div class="col-lg-4">
							<!-- User Details -->
							<div class="d-flex align-items-center justify-content-sm-between mb-2">
								<h2 class="g-color-black g-font-weight-300 h4 g-mr-10">{{ Auth::user() -> first_name . ' ' . Auth::user() -> last_name  }}</h2>
							</div>
							<!-- End User Details -->

							<!-- User Position -->
							<h4 class="h6 g-font-weight-100 g-mb-10 g-color-gray-dark-v5">
								<i class="fa fa-envelope g-pos-rel g-top-1 g-mr-5"></i> {{ Auth::user() -> email}}
							</h4>
							<!-- End User Position -->

							<!-- User Info -->
							<ul class="list-inline g-font-weight-300 g-color-gray-dark-v5">
								<li class="list-inline-item g-mr-20">
									<i class="fa fa-building g-pos-rel g-top-1 g-mr-5"></i> {{ session('club') -> department }}
								</li>
								<li class="list-inline-item g-mr-20">
									<i class="fa fa-briefcase g-pos-rel g-top-1 g-mr-5"></i> {{ session('club') -> title }}
								</li>
							</ul>
							<!-- End User Info -->
						</div>
						<div class="col-lg-4 text-center g-brd-left--lg g-brd-gray-light-v4">
							<h2 class="g-font-weight-300 g-mr-10 h4">{{ ucwords(session('club') -> system_currency) }} disponibles</h2>
							<p><span class="h2 g-color-gray-dark-v5 g-font-weight-300">{{ number_format($account -> points, 0, '', '.') }}</span></p>

						</div>
						<div class="col-lg-4 text-center g-brd-left--lg g-brd-gray-light-v4">
							<h2 class="g-font-weight-300 h4">Ranking</h2>
							<!-- Counter Pie Chart -->
							<div class=" g-mb-20 g-mb-0--xl">
								<i class="fa fa-trophy g-font-size-50 text-center g-color-gray-dark-v5"></i>
							</div>
							<p><span class="h6 g-color-gray-dark-v5 g-font-weight-300">{{ $rank -> rank . ' / ' . $total_ranking }} </span></p>
							<!-- End Counter Pie Chart -->
						</div>
						<!-- End User Block -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End data -->

	<!-- filter -->
	<section class="container g-mb-30">
		<div class="g-brd-gray-light-v4 g-brd-around text-center g-py-10">
			<ul class="list-inline g-font-size-13 g-letter-spacing-1 mb-0">
				<li class="list-inline-item g-brd-gray-light-v4 px-4 mx-0 g-py-10">
					<div class="form-inline">
						<label for="departments">Departamentos: </label>
						<select class="form-control g-ml-15" name="departments" id="directoryFilter">
								<option class="text-capitalize" value="all">Todos</option>
							@foreach ($departments as $element)
								<option class="text-capitalize" value="{{ $element -> name}}">{{ $element -> name}}</option>
							@endforeach
						</select>
					</div>
				</li>
			</ul>
		 </div>
	</section>
	<!-- End filter -->

	<!-- Products -->
	<section class="container">
		<div class="card-block g-pa-0">
			<!-- Product Table -->
			<div class="table-responsive">
				<table id="filter-table" class="table table-bordered g-brd-primary u-table--v2">
					<thead class="text-uppercase">
						<tr>
							<th class="g-font-weight-300 g-color-black">Posición</th>
							<th class="g-font-weight-300 g-color-black">Nombre</th>
							{{-- <th class="g-font-weight-300 g-color-black ">{{ session('club') -> system_currency }}</th> --}}
							<th class="g-font-weight-300 g-color-black">Cargo</th>
							<th class="g-font-weight-300 g-color-black">Departamento</th>
						</tr>
					</thead>

					<tbody>
						@php($i=1)
						@foreach ($ranking as $element )
						<tr class="g-pl-20 @if($element['id'] == Auth::user()->id) g-bg-primary-opacity-0_1 g-color-primary @elseif($i==1) g-bg-green-opacity-0_1 g-color-green @elseif($i==2) g-bg-cyan-opacity-0_1 g-color-cyan @elseif($i==3) g-bg-orange-opacity-0_1 g-color-orange @else g-color-gray-dark-v5 @endif">
							<td class="align-middle">
								<!-- Ribbon -->
								<figcaption>
									<span class="g-font-size-20 g-font-weight-900" style="display: inline-block;">{{ $element['rank'] }}</span>
									@if ($i == 1)
										<span><img class="g-width-30 g-height-30 g-ml-20" src="{{ asset('images/cup-oro.svg') }}" alt="Gold Cup"></span>
									@elseif ($i == 2)
										<span><img class="g-width-30 g-height-30 g-ml-20" src="{{ asset('images/cup-plata.svg') }}" alt="Gold Cup"></span>
									@elseif ($i == 3)
										<span><img class="g-width-30 g-height-30 g-ml-20" src="{{ asset('images/cup-bronce.svg') }}" alt="Gold Cup"></span>
									@endif
								</figcaption>
								<!-- End Ribbon -->
								<div class="js-rating g-font-size-12 g-color-primary" data-rating="5"></div>
							</td>
							<td class="align-middle">
								@if (empty($element['image']))
									@if ($element['gender'] == 0)
										<img class="g-width-50 g-height-50 rounded-circle" src="{{ Storage::disk('images') -> url('img-default-female.jpg') }}" alt="Femenino">
									@else
										<img class="g-width-50 g-height-50 rounded-circle" src="{{ Storage::disk('images') -> url('img-default-male.jpg') }}" alt="Masculino">
									@endif
								@else
									<img class="g-width-50 g-height-50 rounded-circle" src="{{ $_ENV['STORAGE_PATH'] . $element['image'] }}" alt="Image Description">
								@endif
								<span class="g-ml-20">	{{ $element['first_name'] . ' ' . $element['last_name'] }}</span>
							</td>{{--
							<td class="align-middle">
								<span class="">	{{ number_format($element -> total, 0, ',', '.') . ' ' .session('club') -> system_currency}}</span>
							</td> --}}
							<td class="align-middle">{{ $element['title'] }}</td>
							<td class="align-middle text-nowrap filter-column">{{ $element['department'] }}</td>
						</tr>
						@php($i++)
						@endforeach
					</tbody>
				</table>
			</div>
			<!-- End Product Table -->
		</div>
	</section>
@stop
@section('scripts')
	<script src="{{ asset('assets/jquery.filters.js') }}"></script>
	<script>
		$(document).ready(function(){
			$('#directoryFilter').filterTables();
		});
	</script>
@endsection
