@extends('layouts.web.default')

@section('title', 'Programas')
@section('styles')
	<!-- CSS Style Switcher -->
		<link rel="stylesheet" href="{{ asset('style-switcher/style-switcher.css') }}">
@stop
@section('content')

<div class="shortcode-html">
	<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image}})">
	  	<div class="container g-bg-cover__inner">
		    <header class="g-mb-20">
		      	<h3 class="h5 g-font-weight-300 g-mb-5">Nuestros</h3>
	      		<h2 class="h1 g-font-weight-300 text-uppercase">@yield('title') de incentivo</h2>
		    </header>

		    <ul class="u-list-inline">
		      	<li class="list-inline-item g-mr-7">
			        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Inicio</a>
			        <i class="fa fa-angle-right g-ml-7"></i>
		      	</li>
		      	<li class="list-inline-item g-color-white">
		        	<span>@yield('title')</span>
		      	</li>
		    </ul>
	  	</div>
	</section>
</div>

	<!-- Blog Masonry Blocks -->
<div class="container g-pt-100 g-pb-70">
	@if (count($programs) < 1)
		<h2 class="h4 text-center">Actualmente no hay programas registrados para este club</h2>
	@else
		<div class="masonry-grid row">
			<div class="masonry-grid-sizer col-sm-1"></div>
			@php
				$rand_quote = mt_rand(0, count($quotes)-1);
				$rand_program = mt_rand(1, count($programs));
				if ($rand_program != 1) {
					$rand_program--;
				}
				$count = 1;
			@endphp
			@foreach ($programs as $element)
				@if ($count == $rand_program )
					<div class="masonry-grid-item col-sm-6 col-lg-4 g-mb-30">
						<!-- Blog Grid Modern Blocks -->
						<article class="u-shadow-v21 u-shadow-v21--hover g-transition-0_3 g-bg-primary text-center g-rounded-5 g-pa-30 g-py-100">
							<span class="g-color-white-opacity-0_8 g-font-size-60 g-line-height-0">
								&#8220;
							</span>
							<h2 class="h3 g-color-white g-font-weight-600 mb-4"> {{ $quotes[$rand_quote] -> content }}</h2>
							<h3 class="g-color-white-opacity-0_8 g-font-size-13 text-uppercase">{{ $quotes[$rand_quote] -> author }}</h3>						
						</article>
						<!-- End Blog Grid Modern Blocks -->
					</div>
					@php
						$rand_quote = mt_rand(1, count($quotes));
						$rand_program = mt_rand(1, count($programs));
						$count = 0;
					@endphp
				@endif
				@php($count++)
				<div class="masonry-grid-item col-sm-6 col-lg-4 g-mb-30">
					<!-- Blog Grid Modern Blocks -->
					<article class="u-shadow-v21 u-shadow-v21--hover g-transition-0_3">
						<a style="text-decoration: none;" href="{{ url('/programs', $element -> id) }}">
							@if (empty($element -> image))
							<img class="img-fluid w-100" src="{{ asset('images/product-default.jpg') }}" alt="{{ $element -> name}}">
							@else
								<img class="img-fluid w-100" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> name}}">
							@endif
							<div class="g-bg-white g-pa-30 g-rounded-bottom-5">
								<ul class="list-inline g-color-gray-dark-v4 g-font-weight-600 g-font-size-12">
									<li class="list-inline-item">{{ date_format($element -> created_at, 'd/m/Y') }}</li>
								</ul>
								<h2 class="h5 g-color-black g-font-weight-600 mb-4">
									{{ $element -> name }}
								</h2>
								<p>{!! substr($element -> description, 0, 200) !!}...</p>
							</div>
						</a>
					</article>
					<!-- End Blog Grid Modern Blocks -->
				</div>

				@php
					$count++;
				@endphp
			@endforeach
		</div>
	@endif
</div>
	<!-- End Blog Masonry Blocks -->
@stop
@section('scripts')
	<script src="{{ asset('vendor/masonry/dist/masonry.pkgd.min.js') }}"></script>
	<script>
		$(document).on('ready', function () {
			// Initialization of masonry.js
			$('.masonry-grid').imagesLoaded().then(function () {
				$('.masonry-grid').masonry({
					// options
					columnWidth: '.masonry-grid-sizer',
					itemSelector: '.masonry-grid-item',
					percentPosition: true
				});
			});

		});
	</script>
@endsection
