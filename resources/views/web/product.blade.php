@extends('layouts.web.default')

@section('title', $product -> name)
@section('styles')
<style>
	input[type=number]::-webkit-outer-spin-button,
	input[type=number]::-webkit-inner-spin-button {
	    -webkit-appearance: none;
	    margin: 0;
	}
	input[type=number] {
	    -moz-appearance:textfield;
	}

/*Tab v6
------------------------------------*/
.tab-v6 .nav-tabs {
  border-bottom-color: #dedede;
}

.tab-v6 .nav-tabs > li {
  margin-right: 30px;
}

.tab-v6 .nav-tabs > li > a {
  border: none;
  color: #687074;
  padding: 6px 0;
  font-size: 18px;
  margin-right: 0;
  background: none;
  text-transform: uppercase;
  border-bottom: solid 1px transparent;
}

.tab-v6 .nav-tabs > li.active > a,
.tab-v6 .nav-tabs > li.active > a:hover,
.tab-v6 .nav-tabs > li.active > a:focus {
  border-top: none;
  border-left: none;
  border-right: none;
  border-bottom: 1px solid #18ba9b;
}

.tab-v6 .nav-tabs > li > a:hover {
  border-bottom: 1px solid #18ba9b;
}

.tab-v6 .nav-tabs > li > a:focus {
  border: none;
}

.tab-v6 .tab-content {
  padding: 30px 0;
}
</style>
@stop
@section('content')
	{{-- <div class="shortcode-html">
		<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image }})">
		  	<div class="container g-bg-cover__inner">
			    <header class="g-mb-20">
			      	<h3 class="h5 g-font-weight-300 g-mb-5">{{ $product -> name }}</h3>
			    </header>

			    <ul class="u-list-inline">
			      	<li class="list-inline-item g-mr-7">
				        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Mall</a>
				        <i class="fa fa-angle-right g-ml-7"></i>
			      	</li>
			      	<li class="list-inline-item g-mr-7">
				        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('mall/' . $product -> category ) }}">{{ $product -> category }}</a>
				        <i class="fa fa-angle-right g-ml-7"></i>
			      	</li>
			      	<li class="list-inline-item g-color-primary">
			        	<span>@yield('title')</span>
			      	</li>
			    </ul>
		  	</div>
		</section>
    </div> --}}
	<!-- Product Description -->
	<div class="container">
		<div class="row">
			<!-- Carousel -->
			<div class="col-lg-7">
				<div id="carousel1" class="js-carousel g-pt-10 g-mb-10" data-infinite="1" data-fade="1" data-arrows-classes="u-arrow-v1 g-brd-around g-brd-primary g-absolute-centered--y g-width-45 g-height-45 g-font-size-14 g-color-primary g-color-primary--hover rounded-circle" data-arrow-left-classes="fa fa-angle-left g-left-40" data-arrow-right-classes="fa fa-angle-right g-right-40" data-nav-for="#carousel2">
					@foreach ($images as $element)
					<div class="js-slide g-bg-black-opacity-0_1--after">
						<img class="img-fluid w-100" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> slug }}">
					</div>
					@endforeach
					<div class="js-slide g-bg-black-opacity-0_1--after">
						<img class="img-fluid w-100" src="{{ $_ENV['STORAGE_PATH'] . $product -> image }}" alt="{{ $product -> feature }}">
					</div>
				</div>
				<div id="carousel2" class="js-carousel text-center u-carousel-v3 g-mx-minus-5 g-mb-10" data-infinite="1"
						 data-center-mode="1" data-slides-show="3" data-is-thumbs="1" data-nav-for="#carousel1">		
					@foreach ($images as $element)
					<div class="js-slide g-cursor-pointer g-px-5">
						<img class="img-fluid" src="{{ $_ENV['STORAGE_PATH'] . $element -> image }}" alt="{{ $element -> slug }}">
					</div>
					@endforeach			
					<div class="js-slide g-cursor-pointer g-px-5">
						<img class="img-fluid" src="{{ $_ENV['STORAGE_PATH'] . $product -> image }}" alt="{{ $product -> feature }}">
					</div>
				</div>
			</div>
			<!-- End Carousel -->

			<div class="col-lg-5">
				<div class="g-px-40--lg g-py-70">
					<!-- Product Info -->
					<div class="g-mb-20">
						<h1 class="g-font-weight-300 ">{{ $product -> name}} <small class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-20 text-uppercase">{{ $product -> feature }}</small></h1>
						
						<h2 class="g-font-weight-300 h3 g-mb-10">{{ $product -> brand }}</h2>
						<hr class="g-mt-10">
						<p>{{ $product -> excerpt }}</p>
					</div>
					<!-- End Product Info -->

					<!-- Price -->
					<div class="g-mb-20">
						<span class="g-color-black g-font-weight-500 g-font-size-30 mr-2">{{ number_format($product -> points, 0, ',', '.') }}</span>
						<span  class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-12 text-uppercase">{{session('club') -> system_currency}}</span>
					</div>
					<!-- End Price -->

					<!-- Productos -->
					<div class="g-mb-20 g-brd-y g-brd-gray-light-v3  pt-3 g-mb-30">
						<h2 class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-12 text-uppercase mb-2">Modelos</h2>
						<select onchange="window.location=+this.value;" class="form-control g-font-size-13 rounded-0 g-color-gray-dark-v5 g-font-weight-400 g-cursor-pointer"  id="">
							@foreach ($products as $element)
								<option @if($element -> id == $product -> id) selected="selected" @endif value="{{ $element -> id}}">{{ $element -> feature}}</option>
							@endforeach
						</select>
					</div>
					<!-- End Productos -->


					<form action="{{ route('cart.add', $product -> id) }}" method="post">
						<!-- Quantity -->
						<div class="d-flex justify-content-between align-items-center g-brd-bottom g-brd-gray-light-v3 py-3 g-mb-30" role="tab">
							<h5 class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-default mb-0">Cantidad</h5>

							<div class="js-quantity input-group u-quantity-v1 g-width-100 g-brd-primary--focus">
								<input id="qty" name="qty" class="js-result form-control text-center g-font-size-13 rounded-0" type="number" value="1" readonly="readonly" required="required">
								<input class="js-limit" type="hidden" readonly="readonly" required="required" value="{{ $product -> quantity_current }}">
								<div class="input-group-addon d-flex align-items-center g-width-30 g-bg-white g-font-size-13 rounded-0 g-pa-5">
									<i class="js-plus g-color-gray g-color-primary--hover fa fa-angle-up"></i>
									<i class="js-minus g-color-gray g-color-primary--hover fa fa-angle-down"></i>
								</div>
							</div>
						</div>
						<!-- End Quantity -->

						<!-- Buttons -->
						<div class="row g-mx-minus-5 g-mb-20">
							<div class="col g-px-5 g-mb-10">
								{{ csrf_field() }}
								<button  type="submit" class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-15 g-px-25" >
									Agregar al carrito <i class="align-middle ml-2 icon-finance-100 u-line-icon-pro"></i>
								</button>
							</div>
						</div>
						<!-- End Buttons -->
					</form>
				</div>
			</div>
		</div>
	</div>
	

	<!-- Features -->
{{-- 	<div class="g-brd-y g-brd-gray-light-v4 g-my-30">
		<div class="container g-py-30">
			<div class="row justify-content-center">
				<div class="col-md-4 mx-auto g-py-15">
					<div class="media g-px-50--lg">
						<i class="d-flex g-color-black g-font-size-30 g-pos-rel g-top-3 mr-4 icon-real-estate-048 u-line-icon-pro"></i>
						<div class="media-body">
							<span class="d-block g-font-weight-400 g-font-size-default text-uppercase">Envio gratis</span>
							<span class="d-block g-color-gray-dark-v4">En 2-3 dias</span>
						</div>
					</div>
				</div>

				<div class="col-md-4 mx-auto g-brd-x--md g-brd-gray-light-v3 g-py-15">
					<div class="media g-px-50--lg">
						<i class="d-flex g-color-black g-font-size-30 g-pos-rel g-top-3 mr-4 icon-real-estate-040 u-line-icon-pro"></i>
						<div class="media-body">
							<span class="d-block g-font-weight-400 g-font-size-default text-uppercase">Devoluciones gratis</span>
							<span class="d-block g-color-gray-dark-v4">Sin cargos</span>
						</div>
					</div>
				</div>

				<div class="col-md-4 mx-auto g-py-15">
					<div class="media g-px-50--lg">
						<i class="d-flex g-color-black g-font-size-30 g-pos-rel g-top-3 mr-4 icon-hotel-restaurant-062 u-line-icon-pro"></i>
						<div class="media-body text-left">
							<span class="d-block g-font-weight-400 g-font-size-default text-uppercase">24/7</span>
							<span class="d-block g-color-gray-dark-v4">Días de almacenamiento</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> --}}
	<!-- End Features -->

	<div class="container g-my-30">
		<p>{!! $product -> description !!}</p><br>

		{{-- <h3 class="heading-md margin-bottom-20">Especificaciones</h3>
		<div class="row g-color-gray-dark-v5">
			<div class="col-sm-6">
				<ul class="list-unstyled specifies-list">
					<li><i class="fa fa-caret-right"></i> Mercado: <span>{{ $product -> market }}</span></li>
					<li><i class="fa fa-caret-right"></i> Color: <span>{{ $product -> color }}</span></li>
					<li><i class="fa fa-caret-right"></i> Tamaño: <span>{{ $product -> size }}</span></li>
					<li><i class="fa fa-caret-right"></i> Medida: <span>{{ $product -> measure }}</span></li>
					<li><i class="fa fa-caret-right"></i> Peso: <span>{{ $product -> weight }}</span></li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="list-unstyled specifies-list">
					<li><i class="fa fa-caret-right"></i> Ancho: <span>{{ $product -> width }}</span></li>
					<li><i class="fa fa-caret-right"></i> Alto: <span>{{ $product -> heigth }}</span></li>
					<li><i class="fa fa-caret-right"></i> Profundidad: <span>{{ $product -> depth }}</span></li>
					<li><i class="fa fa-caret-right"></i> Volumen: <span>{{ $product -> volume }}</span></li>
				</ul>
			</div>
		</div> --}}
	</div>

@stop
@section('scripts')

	<script src="{{ asset('js/components/hs.carousel.js') }}"></script>
	<script src="{{ asset('vendor/slick-carousel/slick/slick.js') }}"></script>
	<script src="{{ asset('js/components/hs.count-qty.js') }}"></script>
	<script>
		$.HSCore.components.HSCarousel.init('[class*="js-carousel"]');	
		$.HSCore.components.HSCountQty.init('.js-quantity');	
	</script>
@endsection
