@extends('layouts.web.default')

@section('title', 'Resumen de orden')
@section('content')	
			
<!-- Promo Block -->
<section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall">
	<div class="divimage dzsparallaxer--target w-100 g-bg-pos-top-center g-bg-cover g-bg-black-opacity-0_1--after" style="height: 140%; background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image}})"></div>

	<div class="container g-color-white g-pt-100 g-pb-40">
		<div class="g-mb-50">
			<h3 class="g-color-white g-font-size-50 g-font-size-90--md g-line-height-1_2 mb-0">@yield('title')</h3>
			<p class="g-color-white g-font-weight-600 g-font-size-20 text-uppercase">Resultado de la orden</p>
		</div>

		<div class="d-flex justify-content-end">
			<ul class="u-list-inline g-bg-gray-dark-v1 g-font-weight-300 g-rounded-50 g-py-5 g-px-20">
				<li class="list-inline-item g-mr-5">
					<a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Inicio</a>
					<i class="g-color-white-opacity-0_5 g-ml-5">/</i>
				</li>
				<li class="list-inline-item g-mr-5">
					<a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ route('cart.show') }}">Carrito de compra</a>
					<i class="g-color-white-opacity-0_5 g-ml-5">/</i>
				</li>
				<li class="list-inline-item g-color-white g-font-weight-400">
					<span>@yield('title')</span>
				</li>
			</ul>
		</div>
	</div>
</section>
<!-- End Promo Block -->

<table class="text-center w-100">
	<thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-black text-uppercase">
		<tr>
			<th class="g-font-weight-400 text-left g-pb-20">Producto</th>
			<th class="g-font-weight-400 g-width-130 g-pb-20">Precio</th>
			<th class="g-font-weight-400 g-width-50 g-pb-20">Cantidad</th>
			<th class="g-font-weight-400 g-width-130 g-pb-20">Total</th>
			<th></th>
		</tr>
	</thead>

	<tbody>
	@foreach (Cart::content() as $element)
		<!-- Item-->
		<tr class="g-brd-bottom g-brd-gray-light-v3">
			<td class="text-left g-py-25">
				@if (empty($element -> options -> image))
					<img class="d-inline-block g-width-100 mr-4" src="{{ Storage::disk('images')->url('product-default.jpg') }}" alt="Image Description">
				@else										
					<img class="d-inline-block g-width-100 mr-4" src="{{ Storage::disk('products')->url($element -> options -> image) }}" alt="Image Description">
				@endif
				<div class="d-inline-block align-middle">
					<h4 class="h6 g-color-black text-capitalize">{{ $element -> name }} <span class="g-color-gray-dark-v2">{{ $element -> feature }}</span></h4>
					<p class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_6 mb-0">
						{{ $element -> options -> description }}
					</p>
				</div>
			</td>
			<td class="g-color-gray-dark-v2 g-font-size-13">
				{{ $element -> price }}</td>
			<td class="g-color-gray-dark-v2 g-font-size-13">
				{{ $element -> qty }}
			</td>
			<td class="g-color-gray-dark-v2 g-font-size-13">
				{{ $element -> price * $element -> qty }}
			</td>
		</tr>
		<!-- End Item-->
	@endforeach	
		<tr>
			<td></td>
			<td></td>
			<td class="g-color-gray-dark-v2 g-font-size-16 g-py-30">Total</td>
			<td class="g-color-gray-dark-v2 g-font-size-16 g-py-30">{{ Cart::subtotal() }}</td>
		</tr>		
	</tbody>
</table>

@endsection