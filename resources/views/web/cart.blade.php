@extends('layouts.web.default')

@section('title', 'Carrito de compras')
@section('styles')
@stop
@section('content')

<div class="shortcode-html">
	<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{ $_ENV['STORAGE_PATH'] . session('bg')-> image}})">
	  	<div class="container g-bg-cover__inner">
		    <header class="g-mb-20">
	      		<h2 class="h1 g-font-weight-300 text-uppercase">@yield('title')</h2>
		    </header>

		    <ul class="u-list-inline">
		      	<li class="list-inline-item g-mr-7">
			        <a class="u-link-v5 g-color-white g-color-primary--hover" href="{{ url('/') }}">Inicio</a>
			        <i class="fa fa-angle-right g-ml-7"></i>
		      	</li>
		      	<li class="list-inline-item g-color-primary">
		        	<span>@yield('title')</span>
		      	</li>
		    </ul>
	  	</div>
	</section>
</div>


<div class="container g-pt-100 g-pb-70">
	<div class="g-mb-100">
		<!-- Step Titles -->
		<ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
			<li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
				<span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-primary g-color-primary g-color-white--active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
					<i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
					<i class="fa fa-check g-show-check"></i>
				</span>
				<h4 class="g-font-size-16 text-uppercase mb-0">Items</h4>
			</li>

			<li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
				<span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
					<i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
					<i class="fa fa-check g-show-check"></i>
				</span>
				<h4 class="g-font-size-16 text-uppercase mb-0"> Dirección de envío</h4>
			</li>

			<li class="col-3 list-inline-item">
				<span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
					<i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
					<i class="fa fa-check g-show-check"></i>
				</span>
				<h4 class="g-font-size-16 text-uppercase mb-0">Resumen</h4>
			</li>
		</ul>
		<!-- End Step Titles -->
	</div>
	@include('flash::message')
	@if (sizeof(Cart::content()) > 0)
		<div id="stepFormSteps">
			<!-- Shopping Cart -->
			<div id="step1" class="active">
				<h3 class="h6 text-uppercase mb-2">Items</h3>
				<hr>
				<!-- Products Block -->
				<div class="g-overflow-x-scroll g-overflow-x-visible--lg">
					<form id="cart-update" action="{{ route('cart.update') }}" class="js-validate js-step-form" data-progress-id="#stepFormProgress" data-steps-id="#stepFormSteps" method="post">
						{{ csrf_field() }}
						<table class="text-center w-100">
							<thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-black text-uppercase">
								<tr>
									<th class="g-font-weight-400 text-left g-pb-20">Producto</th>
									<th class="g-font-weight-400 g-width-130 g-pb-20">Precio</th>
									<th class="g-font-weight-400 g-width-50 g-pb-20">Cantidad</th>
									<th class="g-font-weight-400 g-width-130 g-pb-20">Subtotal</th>
									<th></th>
								</tr>
							</thead>

							<tbody id="items">
							@foreach (Cart::content() as $element)
								<!-- Item-->
								<tr class="js-item g-brd-bottom g-brd-gray-light-v3 ">
									<td class="text-left g-py-25">
										@if ($element -> options -> has('image'))
											<img class="d-inline-block g-width-100 mr-4" src="{{ $_ENV['STORAGE_PATH'] . $element -> options -> image }}" alt="Image Description">
										@else
											<img class="d-inline-block g-width-100 mr-4" src="{{ Storage::disk('images')->url('product-default.jpg') }}" alt="Image Description">
										@endif
										<div class="d-inline-block align-middle">
											<h4 class="h6 g-color-black text-capitalize">{{ $element -> name }} <span class="g-color-gray-dark-v2">{{ $element -> options -> feature }}</span></h4>
											<p class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_6 mb-0">
												{{ $element -> options -> description }}
											</p>
										</div>
									</td>
									<td class="g-color-gray-dark-v2 g-font-size-13">{{ number_format($element -> price, 0, ',', '.') }}</td>
									<td>
										<div class="input-group u-quantity-v1 g-width-100 g-brd-primary--focus">
											<input class="js-quantity form-control text-center g-font-size-13 rounded-0 g-pa-0" type="number" name="product[{{ $element -> rowId }}]" value="{{ $element -> qty }}" required="required" readonly="readonly">
											@if (array_key_exists($element -> id, $quantity_current))
												<input class="js-limit" type="hidden" readonly="readonly" required="required" value="{{ $quantity_current[$element -> id]['quantity_current']  }}">
											@endif
											<input class="js-price" type="hidden" readonly="readonly" required="required" value="{{ $element -> price }}">
											<div class="input-group-addon d-flex align-items-center g-width-30 g-bg-white g-font-size-12 rounded-0 g-px-5 g-py-6">
												<i class="js-plus g-color-gray g-color-primary--hover fa fa-angle-up sum"></i>
												<i class="js-minus g-color-gray g-color-primary--hover fa fa-angle-down res"></i>
											</div>
										</div>
									</td>
									<td class="text-right g-color-black">
										<span class="js-subtotal g-color-gray-dark-v2 g-font-size-13 mr-4">{{ number_format($element -> price * $element -> qty, 0, ',', '.') }}</span>
										<span class="g-color-gray-dark-v4 g-color-black--hover g-cursor-pointer">
											<a href="{{ route('cart.delete', $element -> rowId) }}"><i class="mt-auto fa fa-trash g-color-primary"></i></a>
										</span>
									</td>
								</tr>
								<!-- End Item-->
							@endforeach			
								<tr>
									<td class="g-pt-20">
										<div class="g-width-200 g-bg-gray-light-v5 text-center g-py-15">
											<h3 class="h6 g-color-black g-font-weight-600 text-uppercase"> Saldo</h3>
											<p class="g-font-size-13 mb-0">
												{{ number_format($balance =  $account -> points, 0, ',', '.') . ' ' . session('club') -> system_currency }}
											</p>
										</div>
									</td>
									<td></td>
									<td class="g-color-gray-dark-v2 g-font-size-16 g-mr-20 g-font-weight-500 g-py-25">Total</td>
									<td id="js-subtotal-order" class="g-color-gray-dark-v2 g-font-size-16 g-px-10  g-py-5">{{ number_format($cost = Cart::subtotal(0, '.', ''), 0, ',', '.') }}</td>
								</tr>						
							</tbody>
						</table>
					</form>
				</div>
				<!-- End Products Block -->
				<div class="g-my-30 text-right">
					<button type="button" class="btn u-btn-black g-font-size-13 text-uppercase g-py-15 mb-4"  onclick="event.preventDefault(); document.getElementById('cart-update').submit();"><i class="fa fa-refresh"></i> Actualizar</button>
					<button class="btn u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4" type="button" data-next-step="#step2"><i class="fa fa-arrow-right"></i> Siguiente</button>
				</div>
			</div>
			<!-- End Shopping Cart -->

			<!-- Shipping -->
			<div id="step2">
				<form id="cart-order" action="{{ route('cart.order') }}" method="post">
					{{ csrf_field() }}
					<div class="g-overflow-x-scroll g-overflow-x-visible--lg">
						<h3 class="h6 text-uppercase mb-2">Direcciones del Club</h3>
						<hr>
						@foreach ( $branches as $key => $element)
							<!-- Current clubs -->
							<div class="form-group row g-mb-25">
								<div class="col-md-6">
									<label class="form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">{{ ucwords($key) }}</label>
									<select id="address" name="fk_id_branch[]" class="form-control rounded-0 pr-0">
									@foreach ( $element as $value)
										<option @if (in_array($value -> id, array_column($member_branches,'fk_id_branch'))) selected="selected" @endif value="{{ $value -> id }}">{{ $value -> name }}</option>
									@endforeach
									</select>
								</div>
								<ul class="list-unstyled col-md-6" id="data-branch"></ul>
							</div>
							<hr>
						@endforeach
					</div>
					<!-- End Products Block -->
					<div class="g-my-30 text-right">
						<button class="btn u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4" type="button" data-next-step="#step3"><i class="fa fa-arrow-right"></i> Siguiente</button>
					</div>
				</form>
			</div>
			<!-- End Shipping -->

			<!-- Resumen -->
			<div id="step3">
				<h3 class="h6 text-uppercase">Resumen de orden</h3>
				<hr>
				<!-- General Info -->
				<section id="general-info" class="g-pb-30">
					<div class="row">
						<div class="col-sm-4 col-md-6 g-mb-30">
							<h2 class="h4 g-font-weight-700 text-uppercase">{{ Auth::user()-> first_name . ' ' . Auth::user() -> last_name }}</h2>
							<p id="resum-branch" class="g-color-gray-dark-v4"></p>
						</div>
						<div class="col-sm-8 col-md-6">
							<div class="row justify-content-end no-gutters">
								<div class="col-sm-4 mb-1">
									<div class="g-bg-gray-light-v5 text-center g-pa-15">
										<h3 class="h6 g-color-black g-font-weight-600 text-uppercase">Fecha</h3>
										<p class="g-font-size-13 mb-0">{{ date('d/m/Y') }}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- End General Info -->

				<section>
					<table class="text-center w-100">
						<thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-black text-uppercase">
							<tr>
								<th class="g-font-weight-400 text-left g-pb-20">Producto</th>
								<th class="g-font-weight-400 g-width-130 g-pb-20">Precio</th>
								<th class="g-font-weight-400 g-width-50 g-pb-20">Cantidad</th>
								<th class="g-font-weight-400 g-width-130 g-pb-20">Total</th>
								<th></th>
							</tr>
						</thead>

						<tbody>
						@foreach (Cart::content() as $element)
							<!-- Item-->
							<tr class="g-brd-bottom g-brd-gray-light-v3">
								<td class="text-left g-py-25">
									@if ($element -> options -> has('image'))
										<img class="d-inline-block g-width-100 mr-4" src="{{ $_ENV['STORAGE_PATH'] . $element -> options -> image }}" alt="Image Description">
									@else
										<img class="d-inline-block g-width-100 mr-4" src="{{ Storage::disk('images')->url('product-default.jpg') }}" alt="Image Description">
									@endif
									<div class="d-inline-block align-middle">
										<h4 class="h6 g-color-black text-capitalize">{{ $element -> name }} <span class="g-color-gray-dark-v2">{{ $element -> options -> feature  }}</span></h4>
										<p class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_6 mb-0">
											{{ $element -> options -> description }}
										</p>
									</div>
								</td>
								<td class="g-color-gray-dark-v2 g-font-size-13">
									{{ number_format($element -> price, 0, ',', '.') }}</td>
								<td class="g-color-gray-dark-v2 g-font-size-13">
									{{ $element -> qty }}
								</td>
								<td class="g-color-gray-dark-v2 g-font-size-13">
									{{ number_format($element -> price * $element -> qty, 0, ',', '.') }}
								</td>
							</tr>
							<!-- End Item-->
						@endforeach
							<tr>
								<td class="g-pt-20">
									<div class="g-width-200 g-bg-gray-light-v5 text-center g-py-15">
										<h3 class="h6 g-color-black g-font-weight-600 text-uppercase"> Saldo</h3>
										<p class="g-font-size-13 mb-0">
											{{ number_format($balance =  $account -> points, 0, ',', '.') . ' ' . session('club') -> system_currency }}
										</p>
									</div>
								</td>
								<td></td>
								<td class="g-color-gray-dark-v2 g-font-size-16 g-mr-20 g-font-weight-500 g-py-25">Total</td>
								<td id="js-subtotal-order" class="g-color-gray-dark-v2 g-font-size-16 g-px-10  g-py-5">{{ number_format($cost = Cart::subtotal(0, '.', ''), 0, ',', '.') }}</td>
							</tr>		
						</tbody>
					</table>
				</section>
				<footer class=" g-my-30 text-right">
					<button type="button" class="btn u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4"  onclick="event.preventDefault(); document.getElementById('cart-order').submit();"><i class="fa fa-check"></i> Realizar pedido</button>
				</footer>
				<!-- End Resumen -->
			</div>
		</div>

		<a href="{{ url('mall') }}" class="btn u-btn-primary  g-color-white g-font-size-13 text-uppercase g-py-15 mb-4"> Ir al mall</a>
	@else
	<div class="text-center">
		<h5 class=" g-color-gray-dark-v2 g-mb-20">El carrito está vacio</h5>
		<a href="{{ url('mall') }}" class="btn u-btn-primary  g-color-white g-font-size-13 text-uppercase g-py-15 mb-4"> Ir al mall</a>
	</div>
	@endif
</div>
<!-- End Checkout Form -->


<!-- JS Global Compulsory -->
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/tether.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>

<!-- JS Implementing Plugins -->
<script src="{{ asset('vendor/hs-megamenu/src/hs.megamenu.js') }}"></script>
<script src="{{ asset('vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<!-- jQuery UI Core -->
<script src="{{ asset('vendor/jquery-ui/ui/widget.js') }}"></script>
<!-- jQuery UI Widgets -->
<script src="{{ asset('vendor/jquery-ui/ui/widgets/autocomplete.js') }}"></script>
<script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<!-- End jQuery UI Widgets -->
<script src="{{ asset('vendor/chosen/chosen.jquery.js') }}"></script>

<!-- JS Unify -->
<script src="{{ asset('js/hs.core.js') }}"></script>
<script src="{{ asset('js/components/hs.header.js') }}"></script>
<script src="{{ asset('js/helpers/hs.hamburgers.js') }}"></script>
<script src="{{ asset('js/components/hs.dropdown.js') }}"></script>
<script src="{{ asset('js/components/hs.scrollbar.js') }}"></script>
<script src="{{ asset('js/components/hs.tabs.js') }}"></script>
<script src="{{ asset('js/components/hs.count-qty.js') }}"></script>


<script src="{{ asset('js/helpers/hs.not-empty-state.js') }}"></script>
<script src="{{ asset('js/helpers/hs.focus-state.js') }}"></script>
<script src="{{ asset('js/components/hs.select.js') }}"></script>
<script src="{{ asset('js/helpers/hs.file-attachments.js') }}"></script>
<script src="{{ asset('js/components/hs.autocomplete.js') }}"></script>
<script src="{{ asset('js/components/hs.datepicker.js') }}"></script>
<script src="{{ asset('js/components/hs.step-form.js') }}"></script>
<!-- JS Customization -->
<script src="{{ asset('js/custom.js') }}"></script>

<!-- JS Plugins Init. -->
<script>
	;(function ($) {
		'use strict';
		$(document).on('ready', function () {
			// Header
			$.HSCore.components.HSHeader.init($('#js-header'));
			$.HSCore.helpers.HSHamburgers.init('.hamburger');

			// Initialization of HSDropdown component
			$.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
				afterOpen: function(){
					$(this).find('input[type="search"]').focus();
				}
			});

			// Initialization of HSScrollBar component
			$.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

			// Initialization of HSMegaMenu plugin
			$('.js-mega-menu').HSMegaMenu({
				event: 'hover',
				pageContainer: $('.container'),
				breakpoint: 991
			});
			$.HSCore.components.HSTabs.init('[data-tabs-mobile-type]');
		});
	})(jQuery);
</script>
<script>



	(function ($) {
		'use strict';
		$(document).on('ready', function () {
			$.HSCore.helpers.HSNotEmptyState.init();
			$.HSCore.helpers.HSFocusState.init();
			$.HSCore.components.HSSelect.init('.js-custom-select');
			$.HSCore.helpers.HSFileAttachments.init();
			$.HSCore.components.HSAutocomplete.init('#autocomplete1');
			$.HSCore.components.HSDatepicker.init('#datepickerDefault');
			// $.HSCore.components.HSCountQty.init('.js-quantity');
			$.HSCore.components.HSStepForm.init('.js-step-form');
		});
	})(jQuery);

	$(document).on('ready', function () {

		$('[data-toggle="tooltip"]').tooltip();

		data($('#address').val());
		$('#address').change(function(){
			data($(this).val());

		});

		function data(id){
			$.ajax({
				type: 'get',
				url: '{{ url("/shopping/branches") }}/'+id,
				success: function(data) {
					$('#data-branch').empty().append(
						"<li>"+data[0]['name']+"</li><li>"+data[0]['address']+", "+data[0]['city']+", "+data[0]['state']+", "+data[0]['postal_code']+"</li><li>"+data[0]['phone']+"</li>"
					);

					$('#resum-branch').empty().append(
						data[0]['name']+"<br>"+data[0]['address']
					);
				}
			});
		}

		$('.js-item').each(function () {
			$this = $(this);

			var
			plus = $this.find('.js-plus'),
			minus = $this.find('.js-minus'),
			quantity = $this.find('.js-quantity'),
			limit = $this.find('.js-limit'),
			price = $this.find('.js-price'),
			subtotal = $this.find('.js-subtotal'),
			limitVal = parseInt(limit.val()),
			quantityVal = parseInt(quantity.val()),
			priceVal = parseInt(price.val());

            function calculator(){
            	var subtotalOrder = 0,
            	saldo = parseInt($('#js-saldo').text());

            	$('.js-item').each(function () {
            		subtotals = parseInt($(this).find('.js-subtotal').text());
            		subtotalOrder += subtotals;
            	});
            	$('#js-subtotal-order').text(subtotalOrder);
            	$('#js-total-order').text(saldo-subtotalOrder);
            }

			plus.click(function(){
				if (quantityVal < limitVal) {
						quantityVal += 1;
					quantity.val(quantityVal);
				}
                subtotal.text(quantityVal * priceVal);
                calculator();
			});

			minus.click(function(){
				if (quantityVal > 1) {
						quantityVal -= 1;
					quantity.val(quantityVal);
				}
                subtotal.text(quantityVal * priceVal);
                calculator();
			});

		});

	});


</script>
@endsection
