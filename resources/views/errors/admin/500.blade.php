@extends('layouts.admin.default')

@section('title', ' Error 404')
@section('styles')

    <link href="{{ asset('assets/core-plus-theme/css/404.css') }}" rel="stylesheet">
    <!-- end of page level styles-->
@stop
@section('content')
        <div class="err-cont">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-push-6">
                        <div class="error_type text-center hidden-lg hidden-md hidden-sm">500</div>
                        <p class="error text-center hidden-lg hidden-md hidden-sm">error</p>
                        <div class="text-center server_img"><img src="{{ asset('assets/core-plus-theme/images/500.png') }}" alt="server break"></div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-sm-pull-6">
                        <div class="text-center">
                            <div class="error_type hidden-xs">500</div>
                            <p class="error hidden-xs">error</p>
                            <div class="error_msg"><p>Oops! Ah ocurrido un error Interno en el Servidor</p></div>
                            <hr class="seperator">
                            <a href="{{ url('admin') }}" class="btn btn-primary">Ir al inicio</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
@section('script')
@endsection