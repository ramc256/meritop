<?php

namespace Meritop\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMemberclub extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name_club;
    public $logo;
    public $alias;
    public $url;
    
    public function __construct($name_club, $alias, $logo, $url)
    {
      $this->name_club = $name_club;
      $this->alias = $alias;
      $this->logo = $logo;
      $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.member.newMemberclub' );
    }
}
