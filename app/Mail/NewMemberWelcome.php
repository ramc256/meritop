<?php

namespace Meritop\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Meritop\Member;

class NewMemberWelcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $password;
    public $first_name;
    public $alias;
    public $logo;
    public $url;
    public function __construct( $password, $first_name, $alias, $logo, $url)
    {
        $this->password = $password;
        $this->first_name = $first_name;
        $this->alias = $alias;
        $this->logo = $logo;
        $this->url = $url;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->markdown('emails.member.new_member_welcome');
    }
}
