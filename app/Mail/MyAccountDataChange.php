<?php

namespace Meritop\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MyAccountDataChange extends Mailable
{
    use Queueable, SerializesModels;

    public $first_name;
    public $last_name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct($first_name, $last_name)
     {
        $this -> first_name    = $first_name;
        $this -> last_name     = $last_name;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.myaccount.myaccountdatachange');
    }
}
