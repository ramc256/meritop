<?php

namespace Meritop\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeliveredStatusOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public $data_order;
     public $items;
     public $logo;

     public function __construct( $data_order, $items, $logo)
     {
         $this->data_order = $data_order;
         $this->items = $items;
         $this->logo = $logo;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.order.deliveredstatusorder');
    }
}
