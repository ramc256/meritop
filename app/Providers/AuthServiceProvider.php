<?php

namespace Meritop\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Meritop\Http\Assets\ResourceFunctions;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Meritop\Model' => 'Meritop\Policies\ModelPolicy',
        'Meritop\Model' => 'Meritop\Policies\PrivilegePolicy',

        // User::class => PrivilegePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('privileges', function ($user, $module) {
            $comprober=false;

            $module = explode('/', $module);
            $role = session('role');

            foreach ($role as $value) {
                if ( ( ($value->fk_id_module == $module[0]) && ($value->fk_id_action == $module[1])  )) {
                     $comprober = true;
                  
                    // dd("hola");
                }
            }

            return $comprober;
        });
    }
}
