<?php

namespace Meritop\Providers;

use \Meritop\Brokers\PasswordBrokerManager;

class PasswordResetServiceProvider extends \Illuminate\Auth\Passwords\PasswordResetServiceProvider
{
	/**
	 * @inheritdoc
	 */
	protected function registerPasswordBroker()
    {
        parent::registerPasswordBroker();

        $this->app->singleton('auth.password', function ($app) {
            return new PasswordBrokerManager($app);
        });
    }
}