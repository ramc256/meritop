<?php

namespace Meritop;
use Meritop\Notifications\MyResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Meritop\Notifications\UserResetPasswordNotification;
use Meritop\Notifications\CustomResetPasswordNotification;
use Meritop\Models\Media;
use Mail;
use Meritop\Mail\NewMemberClub;

class User extends Authenticatable
{
    use Notifiable;

    protected $guard = 'user';
    protected $table = 'users';
    protected $primarikey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','first_name','last_name','user_name','email', 'password','kind','status','image', 'fk_id_role','fk_id_country'
    ];


    public function role(){
        //relacion de de uno a muchos entre role y user
        return $this->belonTo(Role::class);
    }

    public function country(){
        //relacion de de uno a muchos entre country y user
        return $this->belongTo(Country::class);
    }

    public function HistoricalImport(){
        //relacion de de uno a muchos entre country y user
        return $this->belongTo(HistoricalImport::class);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPasswordNotification($token));
    }
}
