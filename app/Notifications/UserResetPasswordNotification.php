<?php

namespace Meritop\Notifications;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Meritop\Models\Media;
use Meritop\Models\UserClub;

class UserResetPasswordNotification extends ResetPassword
{

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public $token;
    public function __construct($token)
   {
      $this->token = $token;
   }

    public function toMail($notifiable)
    {
        /*
            *consult the image related club
            */
           $club = UserClub::where('fk_id_user', $notifiable->id)->first();

            $logo_image = Media::where([
                'identificator' => $club->fk_id_club,
                'table'         => 'clubs'
            ])
            ->first();

            $logo = $_ENV['STORAGE_PATH'].$logo_image->image;

        return (new MailMessage)
                    ->subject('Recuperar contraseña')
                    ->greeting('Hola')
                    ->line('Estás recibiendo este correo porque hiciste una solicitud de recuperación de contraseña para tu cuenta.')
                    ->action('Recuperar contraseña', route('admin.password.reset', $this->token))
                    ->line('Si no realizaste esta solicitud, no se requiere realizar ninguna otra acción.')
                    ->salutation($logo);
    }

}
