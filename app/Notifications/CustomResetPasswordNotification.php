<?php

namespace Meritop\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CustomResetPasswordNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $logo;
    public function __construct($logo)
    {
      $this->logo = $logo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {   
        dd($notifiable);
        return (new MailMessage)
                    ->subject('Recuperar contraseña')
                    ->greeting('Hola')
                    ->line('Estás recibiendo este correo porque hiciste una solicitud de recuperación de contraseña para tu cuenta.')
                    ->action('Recuperar contraseña', route('admin.password.reset', $this->token))
                    ->line('Si no realizaste esta solicitud, no se requiere realizar ninguna otra acción.')
                    ->salutation('Saludos, '.$this->logo );

        public function build()
    {
        
        return $this->markdown('emails.member.new_member_welcome');
    }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'logo' => $logo
        ];
    }
}
