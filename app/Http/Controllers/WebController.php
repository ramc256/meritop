<?php

namespace Meritop\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\ParentProduct;
use Meritop\Models\Media;
use Meritop\Models\Brand;
use Meritop\Models\Account;
use Meritop\Models\Category;
use Meritop\Models\Subcategory;
use Meritop\Models\Program;
use Meritop\Models\Product;
use Meritop\Models\Club;
use Meritop\Models\MemberClub;
use Meritop\Models\Branch;
use Meritop\Models\Quote;
use Meritop\Member;

class WebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Auth::guard('web')->logout();
         $parents_products = ParentProduct::select('parents_products.*', 'products.id as id_product', 'products.points', 'media.image', 'media.slug',
         'subcategories.name as subcategory', 'subcategories.slug as slug_subcategory', 'brands.name as brand')
         ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
         ->join('products', 'products.fk_id_parent_product', 'parents_products.id')
         ->join('inventory', 'inventory.fk_id_product', 'products.id')
         ->join('media', 'media.identificator', 'parents_products.id')
         ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
         ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
         ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
         ->where([
            'media.table' => 'parents_products',
            'clubs_parents_products.fk_id_club' => session('club')->id,
            'parents_products.deleted' => false,
            'products.status' => true,
            ['inventory.quantity_current', '>', 0]
         ])
         ->orderBy('parents_products.created_at', 'desc')
         ->distinct()
         ->get();

        $parents_products = ParentProduct::getDistinctParentProduct($parents_products, "id");

        $categories = Category::select('categories.*', 'media.image', 'media.slug as slug_media')
        ->join('media', 'media.identificator', 'categories.id')
        ->join('subcategories', 'subcategories.fk_id_category', 'categories.id')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_subcategory', 'subcategories.id')
        ->join('parents_products', 'parents_products.id', 'parents_products_subcategories.fk_id_parent_product')
        ->join('products', 'products.fk_id_parent_product', 'parents_products.id')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->where([
            'clubs_parents_products.fk_id_club' => session('club')->id,
            ['inventory.quantity_current', '>', 0],
            'products.status' => true,
            'media.table' => 'categories'
        ])
        ->distinct()
        ->get();

        $three_first_ranking = ResourceFunctions::threeFirstRanking(session('club')->id);

        $members_last = Member::select('members.*', 'members_clubs.*', 'departments.name as department')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->where('members_clubs.fk_id_club', session('club')->id)
        ->orderby('members.created_at', 'desc')
        ->take(3)
        ->get();

        $next_birthdate = ResourceFunctions::nextBirthday(session('club')->id);

        $quote = Quote::inRandomOrder()->first();

        $account = Account::where('fk_id_member', Auth::user()->id)->first();
        $rank = MemberClub::select('rank')->where('fk_id_member', Auth::user()->id)->first();
        $total = Member::all()->count();

        return view('index')->with(
            [
                'club' => session('club'),
                'parents_products' => $parents_products,
                'categories' => $categories,
                'rank' => $rank,
                'members_last' => $members_last,
                'next_birthdate' => $next_birthdate,
                'quote' => $quote,
                'total_ranking' => $total,
                'account' => $account
            ]);
    }


    public function branchCart($branch_id){

        $branches_array = Branch::select('branches.*', 'cities.name as city', 'states.name as state')
        ->join('cities', 'cities.id', 'branches.fk_id_city')
        ->join('states', 'states.id', 'cities.fk_id_state')
        ->where('branches.id', $branch_id)
        ->get();

        return $branches_array;
    }

    // public function contact()
    // {
    //     return view('web.contact');
    // }

}
