<?php

namespace Meritop\Http\Controllers\Web;


use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Requests\Web\MyAccount\ChangePasswordRequest;
use Meritop\Http\Requests\Web\MyAccount\UpdateRequest;
use Meritop\Http\Requests\Web\MyAccount\UpdateBranchRequest;
use Meritop\Http\Requests\Web\MyAccount\ChangeImageRequest;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Mail\MyAccountDataChange;
use Meritop\Models\Order;
use Meritop\Models\ParentProduct;
use Meritop\Models\Product;
use Meritop\Models\OrderProduct;
use Meritop\Models\Guide;
use Meritop\Models\Branch;
use Meritop\Models\AccountTransaction;
use Meritop\Models\MemberBranch;
use Meritop\Models\MemberClub;
use Meritop\Models\Account;
use Meritop\Models\Media;
use Meritop\Member;
use Storage;
use Mail;
use Google\Cloud\Storage\StorageClient;
use League\Flysystem\Filesystem;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;

class MyAccountController extends Controller
{
    public function index()
    {
        Log::info('Ingreso exitoso a MyAccountController - index(), del miembro: '.Auth::user()->user_name);

        $id = Auth::user()->id;

        $account = Account::where('fk_id_member', $id)
        ->first();

        $contributions = AccountTransaction::select('accounts_transactions.*')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->where('accounts_transactions.fk_id_club', session('club')->id)
        ->where([
            ['accounts.fk_id_member', $id],
            ['accounts_transactions.operation', 1]
        ])
        ->get();

        $total_acquired = $contributions->sum('points');

        $trades = AccountTransaction::select('accounts_transactions.*')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->where('accounts_transactions.fk_id_club', session('club')->id)
        ->where([
            ['accounts.fk_id_member', $id],
            ['accounts_transactions.operation', 0]
        ])
        ->get();

        $total_trades = $trades->sum('points');

        $transactions = AccountTransaction::select('accounts_transactions.*', 'clubs.name as club')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->where('accounts_transactions.fk_id_club', session('club')->id)
        ->where([['accounts.fk_id_member', $id]])
        // ->whereYear('accounts_transactions.created_at', date('Y'))
        // ->whereMonth('accounts_transactions.created_at', date('m'))
        ->orderBy('accounts_transactions.created_at', 'desc')
        ->orderBy('accounts_transactions.balance', 'desc')
        ->paginate(100);

        return view('web.myaccount.index')->with([
            'account' => $account,
            'total_acquired' => $total_acquired,
            'total_trades' => $total_trades,
            'transactions' => $transactions
        ]);
    }

    public function update(UpdateRequest $request)
    {
        try {
            DB::beginTransaction();

            Member::FindOrFail(Auth::user()->id)
            ->update($request->all());

            Mail::to($request -> email)->send(new MyAccountDataChange($request -> first_name, $request -> last_name));

            ResourceFunctions::createHistoricalActionMember('actualizar','Se ha realizado una actualización sobre el registro: '.$request->first_name.' '.$request->last_name);

            DB::commit();

            ResourceFunctions::messageSuccess('MyAccountController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('MyAccountController','actualizar',$e);
        }

        return back();
    }

    public function queryConfiguration(){

        $member = Member::FindOrFail(Auth::user()->id);

        $member_branches = MemberBranch::select('members_branches.*')
        ->where('members_branches.fk_id_member' , Auth::user()->id)
        ->get()->toArray();

        $clubs = MemberBranch::select('clubs.id')
        ->join('branches', 'members_branches.fk_id_branch', 'branches.id')
        ->join('clubs', 'clubs.id', 'branches.fk_id_club')
        ->where('members_branches.fk_id_member', Auth::user()->id)
        ->distinct()
        ->get();

        if (!empty($clubs->id)) {
            $branches_array = Branch::select('branches.*', 'clubs.name as club')
            ->join('clubs', 'clubs.id', 'branches.fk_id_club')
            ->whereIn('branches.fk_id_club' , $clubs)
            ->get();
        }else {
            $branches_array = Branch::select('branches.*', 'clubs.name as club')
            ->join('clubs', 'clubs.id', 'branches.fk_id_club')
            ->where('branches.fk_id_club' , session('club')->id)
            ->get();
        }

        $aux = null;
        $branches = [];
        foreach ($branches_array as $key) {

            if ($aux != $key -> club) {
                $aux = $key -> club;
            }

            $branches[$aux][] = $key;
        }

        $clubs_data = MemberClub::select('members_clubs.*', 'clubs.name as club', 'departments.name as department')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->join('members_departments', 'members_departments.fk_id_member', 'members_clubs.fk_id_member')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->where('members_clubs.fk_id_member', Auth::user()->id)
        ->get();

        return view('web.myaccount.configuration')->with([
            'member' => $member,
            'branches' => $branches,
            'member_branches' => $member_branches,
            'clubs' => $clubs,
            'clubs_data' => $clubs_data
        ]);
    }

    public function configuration()
    {
        Log::info('Ingreso exitoso a MyAccountController - configuration(), del miembro: '.Auth::user()->user_name);

        return $this -> queryConfiguration();
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        try {
            DB::beginTransaction();

            $member = Member::FindOrFail(Auth::user()->id);

            if (Hash::check($request -> old_password, $member -> password)) {
                Member::where('id',Auth::user()->id)
                ->update([
                    'password'    => bcrypt($request -> new_password),
                ]);
            }

            Mail::to(Auth::user()-> email)->send(new MyAccountDataChange(Auth::user()->first_name, Auth::user()->last_name));

            ResourceFunctions::createHistoricalAction('change','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('MyAccountController','change');

        } catch (Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('MyAccountController','change',$e);
        }

        return $this -> queryConfiguration();
    }

    public function changeImage(ChangeImageRequest $request, $id)
    {
         try {
            DB::beginTransaction();

            if (!empty($file = $request -> file('image'))) {

    	        $filesystem = ResourceFunctions::filesystemGoogle();

                $file = $request -> file('image'); // Asignamos el archivo a una valiable
                $file_name = time() . mt_rand() .'.'. $file -> getClientOriginalExtension(); //Asignamos un nuevo nombre al archivo

    			$url_cdn = $_ENV["STORAGE_ENV"];
    	        $url_image = 'image/members/'.$file_name;

                $member = Member::FindOrFail($id);

                if ($filesystem->has($url_cdn . $member -> image)) {
                    $filesystem->delete($url_cdn . $member -> image);
                }

                // write a file
    	        $filesystem->write($url_cdn . 'image/members/'. $file_name, file_get_contents($file));

                Member::where('id', $id) -> update(['image' => $url_image]);

            }
            ResourceFunctions::createHistoricalActionMember('actualizar','Se ha realizado una actualización sobre el registro: ');

            DB::commit();

            ResourceFunctions::messageSuccess('MyAccountController','changeImage');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('MyAccountController','changeImage',$e);
        }

        return back();
    }

    public function updateBranch(UpdateBranchRequest $request)
    {
        $clubs = ResourceFunctions::getClubs();

        $old_members_branches = MemberBranch::select('branches.id')
        ->join('branches', 'branches.id', 'members_branches.fk_id_branch')
        ->whereIn('branches.fk_id_club', $clubs)
        ->where('fk_id_member', Auth::user()->id)
        ->get()->toArray();

        try {
            DB::beginTransaction();

            for($i = 0 ; $i < count($old_members_branches) ; $i++) {
                MemberBranch::where([
                    'members_branches.fk_id_member' => Auth::user()->id,
                    'members_branches.fk_id_branch' => $old_members_branches[$i]
                ])
                ->update([
                    'members_branches.fk_id_branch'  => $request -> fk_id_branch[$i],
                ]);
            }

            Mail::to(Auth::user()-> email)->send(new MyAccountDataChange(Auth::user()->first_name, Auth::user()->last_name));

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.Auth::user()->first_name.' '.Auth::user()->last_name);

            DB::commit();

            ResourceFunctions::messageSuccess('MyAccountController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('MyAccountController','actualizar',$e);
        }

        return back();
    }

    public function filter(Request $request)
    {
        Log::info('Ingreso exitoso a MyAccountController - filter(), del miembro: '.Auth::user()->user_name);

        $id = Auth::user()->id;

        if ($request->operation == null) {
            $operations = [0,1];
        }else {
            $operations = [$request->operation];
        }

        $aux = session('club')->id;
        $i = 0;

        foreach ($aux as $key) {
            $clubs[$i] = $key->fk_id_club;
            $i++;
        }

        $account = Account::select('accounts.*', 'members.dni', 'members.first_name', 'members.last_name', 'members_clubs.carnet')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->where('accounts.fk_id_member', $id)
        ->first();

        $contributions = AccountTransaction::select('accounts_transactions.*')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->whereIn('accounts_transactions.fk_id_club', $clubs)
        ->where([
            ['accounts.fk_id_member', $id],
            ['accounts_transactions.points', '>', 0],
            ['accounts_transactions.operation', 1]
        ])
        ->get();

        $total_acquired = $contributions->sum('points');

        $trades = AccountTransaction::select('accounts_transactions.*')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->whereIn('accounts_transactions.fk_id_club', $clubs)
        ->where([
            ['accounts.fk_id_member', $id],
            ['accounts_transactions.points', '>', 0],
            ['accounts_transactions.operation', 0]
        ])
        ->get();

        $total_trades = $trades->sum('points');

        $dates = ResourceFunctions::getDates($request->date);
        $start_date = $dates[0];
        $due_date = $dates[1];
        $valid_dates = ResourceFunctions::validDates($start_date, $due_date);

        $transactions = [];

        if ($valid_dates == 1) {
                $transactions = AccountTransaction::select('accounts_transactions.*')
                ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
                ->join('members', 'members.id', 'accounts.fk_id_member')
                ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
                ->whereIn('accounts_transactions.fk_id_club', $clubs)
                ->where([
                    ['accounts.fk_id_member', $id],
                    ['accounts_transactions.points', '>', 0]
                ])
                ->whereBetween('accounts_transactions.created_at',  [$start_date, $due_date])
                ->whereIn('accounts_transactions.operation', $operations)
                ->paginate(15);;
        }elseif ($valid_dates == 0) {
            flash('El rango de búsqueda entre fechas es de maximo 6 meses', '¡Error!')->error();
        }else {
            flash('Solo es posible buscar registros con antiguedad máxima a 1 año', '¡Error!')->error();
        }

        return view('admin.accounts.show' )->with([
            'account' => $account,
            'total_acquired' => $total_acquired,
            'total_trades' => $total_trades,
            'transactions' => $transactions
        ]);
    }

    public function orders()
    {
        Log::info('Ingreso exitoso a MyAccountController - showOrders(), del miembro: '.Auth::user()->user_name);

        $id = Auth::user()->id;

        $orders_query = Order::select('orders.*', 'branches.name as branch', 'clubs.name as club')
        ->join('branches','branches.id', 'orders.fk_id_branch')
        ->join('clubs','clubs.id', 'branches.fk_id_club')
        ->orderBy('orders.updated_at', 'desc')
        ->where([
            'branches.fk_id_club' => session('club')->id,
            'orders.fk_id_member' => $id
        ]);

        $orders = $orders_query->paginate(15);

        $total_orders = $orders_query->count();

        return view('web.myaccount.orders')->with(['orders' => $orders, 'total_orders' => $total_orders]);
    }

    public function order(Request $request, $id){

        $order = Order::select('orders.*', 'branches.name as branch')
        ->join('branches','branches.id', 'orders.fk_id_branch')
        ->where([
            'orders.id' => $id,
            'orders.fk_id_member' => Auth::user()->id
        ])
        ->first();

        $order_products = OrderProduct::select('orders_products.points', 'orders_products.quantity', 'products.feature',
        'products.id', 'products.excerpt', 'parents_products.name as parent')
        ->join('products', 'products.id', 'orders_products.fk_id_product')
        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        ->where([
            'orders_products.fk_id_order' => $id,
        ])
        ->get();

        $order_products = ParentProduct::getDistinctParentProduct($order_products, "id");

        foreach ($order_products as $key) {
            $image = Media::where(['identificator' => $key->fk_id_product, 'table' => 'products'])->first();

            if (empty($image->image)) {
                $parent_product = ParentProduct::select('parents_products.*')
                ->join('products', 'products.fk_id_parent_product', 'parents_products.id')
                ->where('products.id', $key->id)
                ->first();

                $image = Media::where(['identificator' => $parent_product->id, 'table' => 'parents_products'])->first();
            }

            $key['image'] = $image->image;
            $key['slug'] = $image->slug;
        }

        $guide = Guide::select('guides.*', 'agents.name')
        ->join('agents', 'agents.id', 'guides.fk_id_agent')
        ->where('fk_id_order', $id)
        ->get();

        return view('web.myaccount.order')->with([
            'order' => $order,
            'order_products' => $order_products,
            'guide' => $guide
        ]);
        // }

        // flash('La orden a la que intenta acceder, no existe', '¡Error!')->error();
        // return view('nodata');
    }

}
