<?php

namespace Meritop\Http\Controllers\Web;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Member;
use Meritop\Models\Department;

class DirectoryController extends Controller
{
    public function index()
    {
    	$departments = Department::where('fk_id_club', session('club')->id)->get();
        $members = Member::getMembers()->paginate(30);

        return view('web.directory')->with([
            'members' => $members,
            'departments' => $departments,
            'department' => null
        ]);
    }

    public function filter(Request $request)
    {
        $departments = Department::where('fk_id_club', session('club')->id)->get();
        if ($request->department == 'all') {
            return redirect()->route('directory');
        }else {
            $members = Member::departmentMembers($request -> department)->paginate(30);
        }

        return view('web.directory')->with([
            'members' => $members,
            'departments' => $departments,
            'department' => $request->department
        ]);
    }

    public function search(Request $request)
    {
        $departments = Department::where('fk_id_club', session('club')->id)->get();
        $members = Member::searchName($request->search)->paginate(30);

        return view('web.directory')->with([
            'members' => $members,
            'departments' => $departments,
            'department' => null
        ]);
    }
}
