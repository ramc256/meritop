<?php

namespace Meritop\Http\Controllers\Web;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Models\Program;
use Meritop\Models\Objetive;
use Meritop\Models\Quote;

class ProgramController extends Controller
{
    public function index()
    {
        $programs = Program::select('programs.*', 'media.image', 'media.slug')
        ->join('objetives', 'objetives.fk_id_program', 'programs.id')
        ->join('media', 'media.identificator', 'programs.id')
        ->join('groups_programs', 'groups_programs.fk_id_program', 'programs.id')
        ->join('members_groups', 'members_groups.fk_id_group', 'groups_programs.fk_id_group')
        ->where([
            'programs.status' => true,
            'objetives.status' => true,
            'fk_id_club' => session('club')->id,
            'table' => 'programs',
            'members_groups.fk_id_member' => Auth::user()->id,
        ])
        ->whereDate('objetives.due_date', '>=', date('Y-m-d'))
        ->distinct()
        ->get();

        $quotes = Quote::all();

        return view('web.programs')->with(['programs' => $programs, 'quotes' => $quotes]);
    }

    public function show($id)
    {
        $program = Program::select('programs.*', 'media.image', 'media.slug')
        ->join('objetives', 'objetives.fk_id_program', 'programs.id')
        ->join('media', 'media.identificator', 'programs.id')
        ->where([
            'programs.id' => $id,
            'media.table' => 'programs',
            'programs.status' => true,
            'objetives.status' => true,
        ])
        ->whereDate('objetives.due_date', '>=', date('Y-m-d'))
        ->first();

        if (empty($program)) {
            return redirect('/programs');
        }

        $objetives = Objetive::select('objetives.*', 'perspectives.name as perspective')
        ->join('perspectives', 'perspectives.id', 'objetives.fk_id_perspective')
        ->where([
            'objetives.fk_id_program' => $id,
            'objetives.status' => true,
        ])
        ->whereDate('objetives.due_date', '>=', date('Y-m-d'))
        ->get();

        return view('web.program')->with(['program' => $program, 'objetives' => $objetives]);
    }

}
