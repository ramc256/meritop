<?php

namespace Meritop\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\MemberBranch;
use Meritop\Models\Branch;
use Meritop\Models\ParentProduct;
use Meritop\Models\Product;
use Meritop\Models\Order;
use Meritop\Models\Media;
use Meritop\Models\OrderProduct;
use Meritop\Models\Inventory;
use Meritop\Models\InventoryTransaction;
use Meritop\Models\Account;
use Meritop\Models\AccountTransaction;
use Cart;
use Exception;

class CartController extends Controller
{
    // Ver los artículos del carrito
    public function show()
    {
        $member_branches = MemberBranch::select('members_branches.*')
        ->where('members_branches.fk_id_member' , Auth::user()->id)
        ->get()->toArray();

        $clubs = MemberBranch::select('clubs.id')
        ->join('branches', 'members_branches.fk_id_branch', 'branches.id')
        ->join('clubs', 'clubs.id', 'branches.fk_id_club')
        ->where('members_branches.fk_id_member', Auth::user()->id)
        ->distinct()
        ->get();

        if (!empty($clubs->id)) {
            $branches_array = Branch::select('branches.*', 'clubs.name as club')
            ->join('clubs', 'clubs.id', 'branches.fk_id_club')
            ->whereIn('branches.fk_id_club' , $clubs)
            ->get();
        }else {
            $branches_array = Branch::select('branches.*', 'clubs.name as club')
            ->join('clubs', 'clubs.id', 'branches.fk_id_club')
            ->where('branches.fk_id_club' , session('club')->id)
            ->get();
        }
        
        $aux = null;
        $branches = [];
        foreach ($branches_array as $key) {

            if ($aux != $key -> club) {
                $aux = $key -> club;
            }

            $branches[$aux][] = $key;
        }

        $id_products = [];
        foreach ($cart = Cart::content() as $element) {
            $id_products[] = $element -> id;
        }

        $quantity_current_select = Inventory::select('fk_id_product', 'quantity_current')
        ->whereIn('fk_id_product', $id_products)
        ->get()->toArray();

        $aux = null;
        $quantity_current = [];
        foreach ($quantity_current_select as $key) {

            if ($aux != $key['fk_id_product']) {
                $aux = $key['fk_id_product'];
            }

            $quantity_current[$aux] = $key;
        }

        $account = Account::where('fk_id_member', Auth::user()->id)->first();
// dd(Cart::content());
        return view('web.cart')->with([
            'branches' => $branches,
            'member_branches' => $member_branches,
            'quantity_current' => $quantity_current,
            'account' => $account
        ]);
    }

    // Ver los artículos del carrito
    public function add(Request $request, $id)
    {
        $product = Product::select('products.*', 'parents_products.name as parent', 'parents_products.description')
        ->join('parents_products', 'products.fk_id_parent_product', 'parents_products.id')
        ->where('products.id',$id)
        ->first();

        $image = Media::where(['media.identificator' => $id, 'media.table' => 'products'])->first();

        if (empty($image->image)) {
            $image = Media::where(['media.identificator' => $product->fk_id_parent_product, 'media.table' => 'parents_products'])->first();
        }

        Cart::add(['id' => $product -> id, 'name' => $product->parent .' '. $product -> name, 'qty' => $request -> qty, 'price' => $product -> points,
            'options' => [
            'sku' => $product -> sku,
            'slug' => $product -> slug,
            'description' => $product -> excerpt,
            'image' => $image -> image,
            'id_parents_product' => $id,
            'feature' => $product->feature
        ]]);

        return redirect()->route('cart.show');
    }

    public function update(Request $request)
    {
        foreach ($request -> product as $key => $element) {
            Cart::update($key , ['qty' => $element]);
        }

        return back();
    }

    // Ver los artículos del carrito
    public function delete($id)
    {
        Cart::remove($id);

        return back();
    }

    // Ver los artículos del carrito
    public function destroy()
    {
        if (sizeof(Cart::content())>0) {
            Cart::destroy();
        }
        return back();
    }

    public function generateOrder(Request $request)
    {
        try {
            DB::beginTransaction();

            $total = 0;
            $id_products = [];
            $iterator = -1;
            $qty = [];
            foreach ($cart = Cart::content() as $element) {
                $products[$element -> id] = [
                    'fk_id_product' => $element -> id,
                    'quantity' 		=> $element -> qty,

                ];
                $id_products[] = $element -> id;
                $total = $total + ($element -> qty * $element -> price);
                $qty[] = $element -> qty;
                $iterator++;
            }

            $total_inventory = Inventory::whereIn('inventory.fk_id_product', $id_products)
            ->get();

            $total_points = Account::select('accounts.points')
            ->where('accounts.fk_id_member', Auth::user() -> id)
            ->first();

            if (ResourceFunctions::ValidateQuantityproduct($products,$total_inventory)) {
                flash('En una de las productes coloco una cantidad mayor a la existente en inventario', '¡Error!')->error();
                return back();
            }

            /*
            *allows consulted the points to the user
            */
            if (ResourceFunctions::validatePointMember($products, $total_points->points)  ) {
                flash('La suma del costo de los parents_productos debe ser igual o menor a la cantidad de puntos que posee el miembro', '¡Error!')->error();

                return back();
            }

            // Armamos el array para crear la nueva orden
            $order_data = [
                'status' => 0,
                'note' => ' ',
                'internal_note' => ' ',
                'id_order_reference' => 0,
                'discount' => 0,
                'fk_id_member' => Auth::user() -> id,
                'fk_id_branch' => $request->fk_id_branch[0],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];

            // Creamos la orden
            $order = Order::create($order_data);

            foreach ($cart = Cart::content() as $element) {
                $order_products[] = [
                    'quantity' => $element -> qty,
                    'points' => $element -> price,
                    'fk_id_order' => $order->id,
                    'fk_id_product' => $element -> id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }

            // Insertamos los parents_productos de la orden
            OrderProduct::insert($order_products);

            $order = Order::FindOrFail($order->id);

            $order_products = OrderProduct::where('fk_id_order', $order->id)
            ->join('products', 'products.id', 'orders_products.fk_id_product')
            ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
            ->get();

            $i = $iterator;
            foreach ($total_inventory as $key) {

                Inventory::where('fk_id_product', $key -> fk_id_product)
                ->update([
                    'quantity_current' => ($key->quantity_current - $qty[$i])
                ]);

                InventoryTransaction::create([
                    'quantity_update' => $qty[$i],
                    'description' => 'Compra de artículos',
                    'fk_id_inventory' => $key->id
                ]);
                $i--;
            }

            Account::where(['fk_id_member' => Auth::user() -> id, 'fk_id_kind_account' => 1])
            ->update([
                'points' => ($total_points->points - $total)
            ]);

            $account = Account::where(['fk_id_member' => Auth::user() -> id, 'fk_id_kind_account' => 1])
            ->first();

            AccountTransaction::create([
                'description' => "Compra de artículos - Orden #$order->id",
                'operation' => 0,
                'points' => $total,
                'balance' => $total_points->points - $total,
                'fk_id_account' => $account->id,
                'fk_id_club' => session('club')->id
            ]);

            Cart::destroy();

            ResourceFunctions::createHistoricalActionMember('generar','Se ha generado una orden por el miembro: '.$request->email);

            DB::commit();

            ResourceFunctions::messageSuccess('MyAccountController','generar');

        } catch (\Exception $e) {
            DB::rollBack();

            ResourceFunctions::messageError('MyAccountController','generar',$e);
        }

        return redirect()->route('my-account.order', $order->id);
    }


}
