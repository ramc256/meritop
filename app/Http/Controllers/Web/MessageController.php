<?php

namespace Meritop\Http\Controllers\Web;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\Message;
use Meritop\Models\Response;
use Meritop\Models\Club;
use Mail;
use Meritop\Mail\NewMemberMessage;
use Meritop\Mail\NewMemberTicket;
use Meritop\Http\Requests\Web\Message\CreateRequest;
use Meritop\Http\Requests\Web\Response\ResponseCreateRequest;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::select('messages.*', 'members.id as id_member', 'members.first_name', 'members.last_name', 'members.cell_phone', 'members.email')
        ->join('members', 'members.id', 'messages.fk_id_member')
        ->where('members.id', Auth::user()->id)
        ->orderBy('messages.created_at', 'Desc')
        ->paginate(10);

        return view('web.myaccount.messages')->with(['messages' => $messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        try {
            DB::beginTransaction();

            $request->request->add(['fk_id_member' => Auth::user() -> id]);

            Message::create( $request -> all() );

            $message = Message::where('fk_id_member', Auth::user() -> id)->get()->last();

            ResourceFunctions::createHistoricalActionMember('store','Se ha generado una orden por el miembro: '.$request->email);

            DB::commit();

            /*
            *Allows to send an email when the new registered user
            */

            $url = $_ENV['APP_URL'].'/admin/login';
 
            Mail::to(Auth::user() -> email)->send(new NewMemberMessage(Auth::user() -> first_name . ' ' . Auth::user() -> last_name, $url));

            Mail::to('cliente@meritop.com')->send(new NewMemberTicket(Auth::user() -> first_name . ' ' . Auth::user() -> last_name,
            Auth::user() -> email, $url));

            ResourceFunctions::messageSuccess('MessageController','store');

        } catch (\Exception $e) {
            DB::rollBack();

            ResourceFunctions::messageError('MessageController','store',$e);

            return back();
        }

        return redirect()->route('imessages.show', $message->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = Message::select('messages.*', 'members.id as id_member', 'members.first_name', 'members.last_name', 'members.cell_phone', 'members.email')
        ->join('members', 'members.id', 'messages.fk_id_member')
        ->where(['messages.id' => $id, 'messages.fk_id_member' => Auth::user() -> id]) -> first();

        $responses = Response::where(['fk_id_message' => $message -> id])->orderBy('created_at', 'asc')->paginate(10);

        return view('web.myaccount.message')->with(['message' => $message, 'responses' => $responses]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            Response::where('fk_id_message', $id)->delete();

            Message::destroy($id);

            ResourceFunctions::createHistoricalAction('destroy','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('MessageController','destroy');

        } catch (\Exception $e) {

            DB::rollBack();

            ResourceFunctions::messageError('MessageController','destroy',$e);
        }

        return redirect()->route('imessages.index');
    }

    public function response(ResponseCreateRequest $request, $id)
    {
        try {
            DB::beginTransaction();

            $request->request->add(['fk_id_message' => $id]);
            $request->request->add(['name' => Auth::user() -> first_name . ' ' . Auth::user() -> last_name]);

            Response::create( $request -> all() );

            ResourceFunctions::createHistoricalActionMember('store','Se ha generado una orden por el miembro: '.$request->email);

            DB::commit();

            ResourceFunctions::messageSuccess('MessageController','store');

        } catch (\Exception $e) {
            DB::rollBack();

            ResourceFunctions::messageError('MessageController','store',$e);
        }

        return redirect()->route('imessages.show', $id);
    }

    public function responseDestroy(Request $request)
    {
        try {
            DB::beginTransaction();

            $message = Response::FindOrFail($request -> id_response);
            Response::destroy($request -> id_response);

            ResourceFunctions::createHistoricalAction('destroy','Se ha eliminado el registro: '.$request -> id_response);

            DB::commit();

            ResourceFunctions::messageSuccess('MessageController','destroy');

        } catch (\Exception $e) {

            DB::rollBack();

            ResourceFunctions::messageError('MessageController','destroy',$e);
        }

        return redirect()->route('imessages.show', $message->fk_id_message);
    }
}
