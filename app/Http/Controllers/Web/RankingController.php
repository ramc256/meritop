<?php

namespace Meritop\Http\Controllers\Web;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Member;
use Meritop\Models\MemberClub;
use Meritop\Models\Department;
use Meritop\Models\Account;
use Illuminate\Support\Collection as Collection;

class RankingController extends Controller
{
    public function index()
    {
    	$departments = Department::where('fk_id_club', session('club') -> id) -> get();
    	$account = Account::where('fk_id_member', Auth::user()->id)->first();

        $rank = MemberClub::select('rank')->where('fk_id_member', Auth::user()->id)->first();
        $ranking = Member::select('members.*', 'members_clubs.rank', 'members_clubs.title', 'departments.name as department')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->where('members_clubs.fk_id_club', session('club') -> id)
        ->orderBy('rank', 'asc')
        ->offset(0)
        ->limit(50)
        ->get()->toArray();

        foreach ($ranking as $key => $value) {
            if ($value['title'] == '') {
                $ranking[$key]['title'] = "N/A";
            }
        }
        
        foreach ($departments as $key => $value) {
            if ( !in_array( $value->name, array_column($ranking, 'department') ) ) {
                unset($departments[$key]);
            }
        }

        $total = Member::all()->count();

        return view('web.ranking')->with(['ranking' => $ranking, 'rank' => $rank,  'departments' => $departments, 'account' => $account, 'total_ranking' => $total]);
    }

}
