<?php

namespace Meritop\Http\Controllers\Web;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Assets\ResourceFunctions;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Models\ParentProduct;
use Meritop\Models\Product;
use Meritop\Models\Category;
use Meritop\Models\Subcategory;
use Meritop\Models\Brand;
use Meritop\Models\Media;
use Meritop\Http\Controllers\Web\WebController;
use Meritop\Http\Requests\Web\Store\FilterRequest;

class StoreController extends Controller
{
    public function index()
    {
        $parents_products = ParentProduct::parentProductStore();

        return $this->indexStore($parents_products, null, null);
    }

    public function product($id)
    {
        $product = Product::select('products.*', 'parents_products.name', 'parents_products.description',
        'parents_products.model', 'inventory.quantity_current', 'categories.name as category', 'brands.name as brand',
        'markets.name as market', 'media.image')
        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('markets', 'markets.id', 'parents_products.fk_id_market')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('media', 'media.identificator', 'parents_products.id')
        ->where([
            'products.deleted' => false,
            'products.status' => true,
            'clubs_parents_products.fk_id_club' => session('club')->id,
            ['inventory.quantity_current', '>', 0],
            'products.id' => $id,
            'media.table' => 'parents_products'
        ])->first();

        if (empty($product)) {
            return redirect('/mall');
            // exit(0);
        }

        $products = Product::select('products.*', 'parents_products.name as parent', 'inventory.quantity_current')
        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->where([
            'products.deleted' => false,
            'products.status' => true,
            'clubs_parents_products.fk_id_club' => session('club')->id,
            ['inventory.quantity_current', '>', 0],
            'products.fk_id_parent_product' => $product->fk_id_parent_product
        ])
        ->distinct()
        ->get();

        $images = Media::where(['identificator' => $id, 'table' => 'products'])->get();

        return view('web.product')->with(['product' => $product, 'images' => $images, 'products' => $products]);
    }

    /**
     * Recibe datos por post
     */
    public function filter(Request $request)
    {
        $parents_products = ParentProduct::select('parents_products.*', 'products.points', 'products.id as id_product',
        'media.image', 'media.slug', 'brands.name as brand')
        ->join('products', 'products.fk_id_parent_product', 'parents_products.id')
        ->join('markets', 'markets.id', 'parents_products.fk_id_market')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('media', 'media.identificator', 'parents_products.id')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->parentProductFilter(session('club')->id, $request->market, $request->brand, $request->category)
        ->get();
        // ->paginate(15);

        $parents_products = ParentProduct::getDistinctParentProduct($parents_products, "id");

        return $this->indexStore($parents_products, null, null);
    }

    public function category($category)
    {
        $parents_products = ParentProduct::select('parents_products.*', 'products.points', 'products.id as id_product',
        'media.image', 'media.slug', 'brands.name as brand')
        ->join('products', 'products.fk_id_parent_product', 'parents_products.id')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('media', 'media.identificator', 'parents_products.id')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->parentProductCategory(session('club')->id, $category)
        ->get();

        $parents_products = ParentProduct::getDistinctParentProduct($parents_products, "id");

        return $this->indexStore($parents_products, $category, null);
    }

    public function subcategory($category, $subcategory)
    {
        $parents_products = ParentProduct::select('parents_products.*', 'products.points', 'products.id as id_product',
        'media.image', 'media.slug', 'brands.name as brand')
        ->join('products', 'products.fk_id_parent_product', 'parents_products.id')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('media', 'media.identificator', 'parents_products.id')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->parentProductSubcategory(session('club')->id, $subcategory)
        ->get();

        $parents_products = ParentProduct::getDistinctParentProduct($parents_products, "id");

        return $this->indexStore($parents_products, $category, $subcategory);
    }

    public function market($market, $category, $subcategory)
    {
        $parents_products = ParentProduct::select('parents_products.*', 'products.points', 'products.id as id_product',
        'media.image', 'media.slug', 'brands.name as brand')
        ->join('products', 'products.fk_id_parent_product', 'parents_products.id')
        ->join('media', 'media.identificator', 'parents_products.id')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->parentProductMarket(session('club')->id, $market)
        ->get();

        $parents_products = ParentProduct::getDistinctParentProduct($parents_products, "id");

        return $this->indexStore($parents_products, null, null);
    }

    public function indexStore($parents_products, $category, $subcategory)
    {
        // $categories = DB::table('categories')
        // ->select(DB::raw("categories.*, (select distinct count(products.id)
		// 	from parents_products_subcategories
		// 	join parents_products on parents_products.id = parents_products_subcategories.fk_id_parent_product
		// 	join products on products.fk_id_parent_product = parents_products.id
		// 	join inventory on inventory.fk_id_product = products.id
		// 	join subcategories on subcategories.id = parents_products_subcategories.fk_id_subcategory
		// 	join categories as cate on cate.id = subcategories.fk_id_category
		// 	where inventory.quantity_current > 0
		// 	and cate.id = categories.id
		// 	and parents_products.deleted = false
        //     and clubs_parents_products.fk_id_club =  ".session('club')->id.
		// 	" group by products.id) as total"))
        // ->join('subcategories', 'subcategories.fk_id_category', 'categories.id')
        // ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_subcategory', 'subcategories.id')
        // ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products_subcategories.fk_id_parent_product')
        // ->where('clubs_parents_products.fk_id_club', session('club')->id)
        // ->distinct()
        // ->get();
        $categories = Category::select('categories.*')
        ->join('subcategories', 'subcategories.fk_id_category', 'categories.id')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_subcategory', 'subcategories.id')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products_subcategories.fk_id_parent_product')
        ->where('clubs_parents_products.fk_id_club', session('club')->id)
        ->distinct()
        ->get();
        
        $brands = DB::table('brands')
        ->select(DB::raw("brands.*, (select count(parents_products.id)
             from parents_products
             join brands as brand on brand.id = parents_products.fk_id_brand
             where brand.id = brands.id) as total"))
        ->join('parents_products', 'parents_products.fk_id_brand', 'brands.id')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->where('clubs_parents_products.fk_id_club', session('club')->id)
        ->distinct()
        ->get();

        $markets = DB::table('markets')
        ->select(DB::raw("markets.name, (select count(parents_products.fk_id_market)
							             from parents_products
							             where parents_products.fk_id_market = markets.id) as total"))
        ->join('parents_products', 'parents_products.fk_id_market', 'markets.id')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->where('clubs_parents_products.fk_id_club', session('club')->id)
        ->distinct()
        ->get();

        $category = Category::where('slug', $category)->first();

        $subcategory = Subcategory::where('slug', $subcategory)->first();

        return view('web.mall')->with([
            'parents_products' => $parents_products,
            'categories' => $categories,
            'brands' => $brands,
            'markets' => $markets,
            'category' => $category,
            'subcategory' => $subcategory
        ]);
    }

}
