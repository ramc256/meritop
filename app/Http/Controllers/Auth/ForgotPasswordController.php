<?php

namespace Meritop\Http\Controllers\Auth;

use Meritop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Response;
use Meritop\Models\Club;
use Meritop\Models\Media;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm($gateway = null)
    {   
       if (empty($gateway)) {
           return view('auth.message');
       }else{
            $club = Club::where('slug', $gateway)->first();
            
            if (sizeof($club) < 1) {
                flash('El club al que está intentando acceder no existe','danger');
                return view('auth.message');
            }else{
                $media = Media::where(['identificator' => $club -> id, 'table' => 'clubs'])->first();

                $bg = Media::where(['identificator' => $club -> id, 'table' => 'login'])->first();

                if (count($media) > 0) {
                    $image = $media -> image;
                }else{
                    $image = null;
                }
                // return view('auth.passwords.email')->with(['club' => $club, 'image' => $image, 'bg' => $bg]);
                return view('auth.passwords.email')->with(['club' => $club, 'image' => $image, 'bg' => $bg ]);
            }
       }

    }


    /**
     * Send a password reset link to a user.
     *
     * @param  array  $credentials
     * @return string
     */
    public function sendResetLink(array $credentials)
    {

        // First we will check to see if we found a user at the given credentials and
        // if we did not we will redirect back to this current URI with a piece of
        // "flash" data in the session to indicate to the developers the errors.
        $user = $this->getUser($credentials);

        if (is_null($user)) {
            return static::INVALID_USER;
        }

        // Once we have the reset token, we are ready to send the message out to this
        // user with a link to reset their password. We will then redirect back to
        // the current URI having nothing set in the session to indicate errors.
        $user->sendPasswordResetNotification(
            $this->tokens->create($user)
        );

        return static::RESET_LINK_SENT;
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(Request $request)
    { 
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $var = ['email' => $request->email, 'gateway' => $request->gateway];
       
        $response = $this->broker()->sendResetLink(
            $var
        ); 
         
          return $response == Password::RESET_LINK_SENT
                                      ? $this->sendResetLinkResponse($response)
                                      : $this->sendResetLinkFailedResponse($request, $response);

                   
    }


    

    
}
