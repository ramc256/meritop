<?php

namespace Meritop\Http\Controllers\Auth;

use Meritop\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Meritop\Models\Club;
use Meritop\Models\Media;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetResponse($response)
    {
        // return view('auth.passwords.message');
        return redirect()->route('member.login', [$_POST['club']]);
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $club
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $club = null, $token = null)
    {   
        // dd('Stop');
        if (empty($club)) {
            return view('auth.message');
        }else{
            $query_club = Club::where('slug', $club)->first();
            
            if (sizeof($query_club) < 1) {
                flash('El club al que está intentando acceder no existe','danger');
                return view('auth.message');
            }else{
                $media = Media::where(['identificator' => $query_club -> id, 'table' => 'clubs'])->first();

                $bg = Media::where(['identificator' => $query_club -> id, 'table' => 'login'])->first();

                if (count($media) > 0) {
                    $image = $media -> image;
                }else{
                    $image = null;
                }
                return view('auth.passwords.reset')->with(
                    ['token' => $token, 'email' => $request->email, 'club' => $club, 'query_club' => $query_club,'image' => $image, 'bg' => $bg]
                );
            }
        }
    }


    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        // modifica la contraseña del usuario
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();

        // $url = $_ENV['APP_URL'].'/'.$data_club->slug;
        // $d= \Request::
        // dd(\Request::ip());

        //$this->guard()->login($user);
    }

    

}






