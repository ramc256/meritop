<?php

namespace Meritop\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\MemberClub;
use Meritop\Models\Subcategory;
use Meritop\Models\Club;
use Meritop\Models\Account;
use Meritop\Models\Banner;
use Meritop\Models\Media;
use Meritop\Member;
use Cart;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    // public function showLoginForm()
    // {

    // }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm($gateway = null)
    {        

        if (empty($gateway)) {
           return view('auth.message');
        }else{
            $club = Club::where('slug', $gateway)->first();
            
            if (sizeof($club) < 1) {
                flash('El club al que está intentando acceder no existe','danger');

                return view('auth.message');
            }else{
                $media = Media::where(['identificator' => $club -> id, 'table' => 'clubs'])->first();

                $bg = Media::where(['identificator' => $club -> id, 'table' => 'login'])->first();

                if (count($media) > 0) {
                    $image = $media -> image;
                }else{
                    $image = null;
                }
                return view('auth.login')->with(['club' => $club, 'image' => $image, 'bg' => $bg]);
            }
        }
        // return view('auth.message');
    }

    public function logout()
    {
        Cart::destroy();
        Auth::guard('web')->logout();
        $url = session('club')->slug.'/';
        session()->forget('club');

        return redirect($url);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $member = Member::where(['email' => $request -> email])->first();

        if (count($member) > 0) {
            $club = MemberClub::select('clubs.*')
            ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
            ->where([
                'fk_id_member' => $member -> id,
                'fk_id_club' => $request -> club,
            ])
            ->first();

            if (count($club) > 0) {
                $validator = $this->guard()->attempt(
                    ['email' => $request -> email, 'password' => $request -> password, 'status'  => 1], $request->has('remember')
                );
            }else{
                $validator = false;
            }
        }else{
            $validator = false;
        }

        if($validator == false)
        {
            flash('Usuario y/o contraseña incorrecta', 'danger');
        }

        return $validator;
    }

     /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        //consulta los clubs relacionados al usuario
        $club = MemberClub::select('clubs.*', 'media.image', 'members_clubs.carnet',
        'members_clubs.email', 'members_clubs.title', 'members_clubs.supervisor_code', 'departments.name as department')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->join('media', 'media.identificator', 'clubs.id')
        ->join('members_departments', 'members_departments.fk_id_member', 'members_clubs.fk_id_member')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->where([
            'members_clubs.fk_id_member' => Auth::user() -> id,
            'members_clubs.fk_id_club' => $request -> club,
            'media.table' => 'clubs'
        ])
        ->first();

        $subcategories_array = DB::table('parents_products_subcategories')
        ->select(DB::raw("subcategories.*, categories.name as category, categories.slug as slug_category,
           (select count(*)
			  from parents_products_subcategories
			  join subcategories as subc on subc.id = parents_products_subcategories.fk_id_subcategory
			  where subc.id = subcategories.id) as total"))
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->join('parents_products', 'parents_products.id', 'parents_products_subcategories.fk_id_parent_product')
        ->join('products', 'products.fk_id_parent_product', 'parents_products.id')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->where([
            ['inventory.quantity_current', '>', 0],
            'clubs_parents_products.fk_id_club' => $club->id,
            'parents_products.deleted' => false
        ])
        ->distinct()
        ->get();

        $aux = null;
        $subcategories = [];
        foreach ($subcategories_array as $key) {

            if ($aux != $key -> category) {
                $aux = $key -> category;
            }

            $subcategories[$aux][] = $key;
        }

        $banner = Banner::select('banners.*', 'media.image', 'media.slug')
        ->join('media', 'media.identificator', 'banners.id')
        ->where(['media.table' => 'banners', 'banners.fk_id_club' => $request -> club])
        ->distinct()
        ->get();

        $bg = Media::where(['identificator' => $club -> id, 'table' => 'login'])->first();

        session([
            'club' => $club,
            'subcategories' => $subcategories,
            'banner' => $banner,
            'bg' => $bg
        ]);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

}
