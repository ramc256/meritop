<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Product;
use Meritop\Models\ParentProduct;
use Meritop\Models\Brand;
use Meritop\Models\Subcategory;
use Meritop\Models\Country;
use Meritop\Models\Club;
use Meritop\Models\Inventory;
use Meritop\Models\InventoryTransaction;
use Meritop\Models\Media;
use Meritop\Models\Market;
use Meritop\Models\ParentProductSubcategory;
use Meritop\Models\ClubParentProduct;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\ParentProduct\CreateRequest;
use Meritop\Http\Requests\Admin\ParentProduct\UpdateRequest;
use Storage;

class ParentProductController extends Controller
{
    public $module = 7;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {        /*
        * Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a ParentProductController - index(), del usuario: '.Auth::user()->user_name);

        $parents_products = ParentProduct::select('parents_products.*', 'brands.name as brand')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->where('deleted', false)
        ->get();

        return view('admin.parents_products.index')->with([
            'parents_products' => $parents_products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.parents_products.index');
        }

        Log::info('Ingreso exitoso a ParentProductController - create(), del usuario: '.Auth::user()->user_name);

        $brands = Brand::all();
        $clubs = Club::all();
        $countries = Country::orderBy('name')->get();
        $markets = Market::all();
        $subcategories = Subcategory::select('subcategories.id', 'subcategories.name', 'categories.name as category')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')->get();

        return view('admin/parents_products/create')->with([
            'brands'        => $brands,
            'countries'     => $countries,
            'clubs'         => $clubs,
            'markets'       => $markets,
            'subcategories' => $subcategories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            $fk_id_parent_product = 'fk_id_parent_product';

            /*---------------------------------------------Create a new parents_product---------------------------------------------*/

            /**
             * [$request->warranty heck whether it was checked or not.
             *  and change its value to 1 for be inserted into db]
             * @var [bool] to @var [int]
             */
            ($request->warranty == 'on') ? $request['warranty'] = true : $request['warranty'] = false;

            $request['fk_id_country'] = Auth::user()->fk_id_country;

            $parent_product = ParentProduct::create($request->all());                //insert a new parents_product with the values corresponding to the produc model

            $request[$fk_id_parent_product] = $parent_product->id;         //it's |aggregated a new position in request for be inserted through the model

            /*---------------------------------------------Create a new product---------------------------------------------*/
            ($request->sell_out_stock == 'on') ? $request['sell_out_stock'] = true : $request['sell_out_stock'] = false;
            $product = Product::create($request->all());


            /**
             * [Store the every images of the parents_product]
             * @var [file]
             */
            $file = $request->file('parent_image');

            if (!empty($file)) {
                  ResourceFunctions::imageChange($parent_product->id, null, $file, 'parents_products');
            }

            /**
             * [Store the every images of the parents_product]
             * @var [file]
             */
            $files = $request->file('product_image');

            if (!empty($files)) {
                foreach ($request->product_image as $file) {
                  ResourceFunctions::imageChange($product->id, null, $file, 'products');
                }
            }



            /*---------------------------------------------Create a new record into inventory---------------------------------------------*/
            $inventory = Inventory::create([
                'quantity_current' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'fk_id_product' => $product->id
            ]);

            /*---------------------------------------------Create a new record into historical---------------------------------------------*/
            InventoryTransaction::create([
                'quantity_update' => 0,
                'description' => 'cantidad inicial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'fk_id_inventory' => $inventory->id
            ]);


            /*---------------------------------------------Create multiple records of parents_products_subcategories---------------------------------------------*/
            /*
            Format a vector to make multiple inserts to the table
            Of the different checkboxes selected from the matrix.
            The name of those fields is compound by:
                suffix fk_id_subcategor and the id of the subcategory:
                fk_id_subcategory/id
            */
            $i=0;
            $parents_products_subcategories = null;
            foreach ($request->fk_id_subcategory as $campo => $dato) {

                $parents_products_subcategories[$i] = [
                    'fk_id_subcategory' => $dato,
                    $fk_id_parent_product => $parent_product->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $i++;
             }

            DB::table('parents_products_subcategories')->insert($parents_products_subcategories);

            /*---------------------------------------------Create multiple records of clubs_parents_products---------------------------------------------*/
            $i=0 ;
            $clubs_parents_products = null;
            foreach ($request->fk_id_club as $campo => $dato) {
                        $clubs_parents_products[$i] = [
                            'fk_id_club' => $dato,
                            $fk_id_parent_product => $parent_product->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ];
                        $i++;
             }

            DB::table('clubs_parents_products')->insert($clubs_parents_products);

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ParentProductController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ParentProductController','store',$e);
        }

        return redirect()->route('parents-products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a ParentProductController - show(), del usuario: '.Auth::guard('user')->user()->user_name);

        $parent_product = ParentProduct::select('parents_products.*', 'brands.name as brand', 'subcategories.id as id_subcategory', 'subcategories.name as subcategory', 'categories.id as id_category', 'categories.name as category', 'markets.name as market')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->join('markets', 'markets.id', 'parents_products.fk_id_market')
        ->where([
            'parents_products.id' => $id,
            'parents_products.deleted' => false
        ])
        ->first();

        $products = Product::select('products.id', 'products.feature', 'products.sku', 'products.barcode', 'products.points', 'products.status')
        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        ->where([
            'products.fk_id_parent_product' => $id,
            'products.deleted' => false,
            'parents_products.deleted' => false
        ])
        ->get();

        $clubs_parents_products = ClubParentProduct::select('clubs.*')
        ->join('clubs', 'clubs.id', 'clubs_parents_products.fk_id_club')
        ->where('clubs_parents_products.fk_id_parent_product', $id)
        ->get();

        $parent_image = Media::where(['identificator' => $id, 'table' => 'parents_products'])
        ->first();

        $product_image = Media::where(['identificator' => $id, 'table' => 'products'])
        ->get();

        $parents_products_subcategories = ParentProductSubcategory::join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->where('fk_id_parent_product', $id)
        ->get();

        return view('admin.parents_products.show')
        ->with([
            'parent_product' => $parent_product,
            'products' => $products,
            'clubs_parents_products' => $clubs_parents_products,
            'parent_image' => $parent_image,
            'product_image' => $product_image,
            'parents_products_subcategories' => $parents_products_subcategories
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a ParentProductController - show(), del usuario: '.Auth::user()->user_name);

        $parent_product = ParentProduct::select('parents_products.*', 'brands.id as id_brand', 'brands.name as brand', 'subcategories.id as id_subcategory',
        'subcategories.name as subcategory', 'categories.id as id_category', 'categories.name as category', 'markets.name as market')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->join('markets', 'markets.id', 'parents_products.fk_id_market')
        ->where([
            'parents_products.id' => $id,
            'parents_products.deleted' => false
        ])
        ->first();

        $brands = Brand::all();
        $clubs = Club::all();
        $subcategories = Subcategory::select('subcategories.id', 'subcategories.name', 'categories.name as category')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->get();

        $clubs_parents_products = ClubParentProduct::select('clubs.*')
        ->join('clubs', 'clubs.id', 'clubs_parents_products.fk_id_club')
        ->where('clubs_parents_products.fk_id_parent_product', $id)
        ->get();

        $parent_image = Media::where(['identificator' => $id, 'table' => 'parents_products'])
        ->first();

        $product_image = Media::where(['identificator' => $id, 'table' => 'products'])
        ->get();

        $parents_products_subcategories = ParentProductSubcategory::where('fk_id_parent_product', $id)
        ->get()->toArray();

        $markets = Market::all();

        return view('admin.parents_products.edit')->with([
            'parent_product' => $parent_product,
            'brands' => $brands,
            'clubs' => $clubs,
            'subcategories' => $subcategories,
            'clubs_parents_products' => $clubs_parents_products,
            'parent_image' => $parent_image,
            'product_image' => $product_image,
            'parents_products_subcategories' => array_column($parents_products_subcategories,'fk_id_subcategory'),
            'markets' => $markets
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            $fk_id_parent_product = 'fk_id_parent_product';

            /*---------------------------------------------Create a new parents_product---------------------------------------------*/

            /**
             * [$request->warranty heck whether it was checked or not.
             *  and change its value to 1 for be inserted into db]
             * @var [bool] to @var [int]
             */
            if ($request->warranty == 'on')
            {
                $request['warranty'] = true;
            }else{
                $request['warranty'] = false;
                $request['warranty_conditions'] = null;
                $request['warranty_day'] = null;
            }

            ParentProduct::FindOrFail($id)
            ->update($request->all());

            /**
             * [Store the every images of the parents_product]
             * @var [file]
             */
            $files = $request->file('parent_image');

            $i=0;
            $data_insert =  array();

            if (!empty($files)) {
                  ResourceFunctions::imageChange($parent_product->id, null, $file, 'parents_products');
            }

            /*---------------------------------------------Create multiple records of parents_products_subcategories---------------------------------------------*/
            /*
            Format a vector to make multiple inserts to the table
            Of the different checkboxes selected from the matrix.
            The name of those fields is compound by:
                suffix fk_id_subcategor and the id of the subcategory:
                fk_id_subcategory/id
            */
            ParentProductSubcategory::where($fk_id_parent_product, $id)->delete();

            $i=0;
            $parents_products_subcategories = null;
            foreach ($request->fk_id_subcategory as $campo => $dato) {

                $parents_products_subcategories[$i] = [
                    'fk_id_subcategory' => $dato,
                    $fk_id_parent_product => $id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $i++;
             }

            DB::table('parents_products_subcategories')->insert($parents_products_subcategories);


            /*---------------------------------------------Create multiple records of clubs_parents_products---------------------------------------------*/
            ClubParentProduct::where($fk_id_parent_product, $id)->delete();

            $i=0 ;
            $clubs_parents_products = null;
            foreach ($request->fk_id_club as $campo => $dato) {
                        $clubs_parents_products[$i] = [
                            'fk_id_club'    => $dato,
                            $fk_id_parent_product  => $id,
                            'created_at'    => date('Y-m-d H:i:s'),
                            'updated_at'    => date('Y-m-d H:i:s')
                        ];
                        $i++;
             }

            DB::table('clubs_parents_products')->insert($clubs_parents_products);

            if (!empty($request -> file('image'))) {
                ResourceFunctions::imageChange($id, $request -> id_image, $request -> file('image'), 'parents_products');
            }

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ParentProductController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ParentProductController','actualizar',$e);
        }

        return redirect()->route('parents-products.show',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }
        try {
            DB::beginTransaction();

            ParentProduct::where('id', $id)
            ->update([
                    'deleted' => true
                    ]);

            Product::where('fk_id_parent_product', $id)
            ->update([
                    'deleted' => true
                    ]);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('ParentProductController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ParentProductController','eliminar',$e);
        }

        return redirect()->route('parents-products.index');
    }
}
