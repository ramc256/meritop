<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Brand;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Brand\BrandRequest;
use FontLib\OpenType\File;

class BrandController extends Controller
{
    public $module = 8;
    /**
     * Display a listing of the brand.
     *
     *  @return the view(index)
     */

     public function __construct()
    {
       $this->middleware('auth:user');
    }

    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        /**
         * [Log description] Create a new log message for indicate the controller name,
         * the method name, and the user_name
         * @var [Log]
         * @param [String message]
         */
        Log::info('Ingreso exitoso a BrandController - index(), del usuario: '.Auth::guard('user')->user()->user_name);

        /*
        *It consult all brands
        */
        $brands = Brand::all();

        /*
        *Obtains the number of brands
        */
        $num_brands = $brands -> count();

        return view('admin.brands.index')->with(['brands' => $brands, 'num_brands' => $num_brands]);
    }

    /**
     * Show the form for creating a new brand.
     *
     * @return the view(create)
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('brands.index');
        }

        /**
         * [Log description] Create a new log message for indicate the controller name,
         * the method name, and the user_name
         * @var [Log]
         * @param [String message]
         */
        Log::info('Ingreso exitoso a BrandController - create(), del usuario: '.Auth::guard('user')->user()->user_name);

        return view('admin.brands.create');
    }

    /**
     * Store a newly created brand in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @return redirect view(index)
     */
    public function store(BrandRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('brands.index');
        }

        /**
         * [try store a new Brand and register its hisotrical]
         */
        try {
            /**
             * [DB beginTransaction] Start a new transactions for to store data into all
             * tables of the proceses if there not errors
             * @var [DB]
             */
            DB::beginTransaction();

            /**
             * [This sentense store through the Brand model a new brand from the form]
             * @var [Request]
             */
            Brand::create( $request->all() );

            /**
             * [ResourceFunctions createHistoricalAction] From ResourceFunctions File
             * the createHistoricalAction is called for store the historical of the operation
             * @param [String operation]
             * @param [String message]
             */
            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            /**
             * [DB commit] If all transactions into tables was correct, The changes are saved
             * in DataBase
             * @var [DB]
             */
            DB::commit();

            /**
             * [ResourceFunctions messageSuccess] From ResourceFunctions File
             * the function messageSuccess is called for generate a success message and log message
             * @param [String Controller name]
             * @param [String Metod name]
             */
            ResourceFunctions::messageSuccess('BrandController','store');

        } catch (\Exception $e) {
            /**
             * [DB rollBack] This sentence applies for run a rollback if any exception occurs
             * @var [DB]
             */
            DB::rollBack();

            /**
             * [ResourceFunctions messageError] From ResourceFunctions File
             * the function messageError is called for generate a success message and log message
             * @param [String Controller name]
             * @param [String Metod name]
             * @param [Exception]
             */
            ResourceFunctions::messageError('BrandController','store',$e);
        }

        return redirect()->route('brands.index');
    }

    /**
     * Display the specified brand.
     *
     * @param  int  $id - (id of the particular brand)
     * @return the view(show)
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        /**
         * [Log description] Create a new log message for indicate the controller name,
         * the method name, and the user_name
         * @var [Log]
         * @param [String message]
         */
        Log::info('Ingreso exitoso a BrandController - show(), del usuario: '.Auth::guard('user')->user()->user_name);

        /**
         * [$brand this sentence Search through Brand model the register with a
         * specific id]
         * @var [Brand]
         * @param [Integer register id]
         */
        $brand = Brand::FindOrFail($id);

        return view('admin.brands.show')->with(['brand' => $brand]);
    }

    /**
     * Show the form for editing the specified brand.
     *
     * @param  int  $id (id of the particular brand)
     * @return the view(edit)
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        /**
         * [Log description] Create a new log message for indicate the controller name,
         * the method name, and the user_name
         * @var [Log]
         * @param [String message]
         */
        Log::info('Ingreso exitoso a BrandController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);

        /**
         * [$brand this sentence Search through Brand model the register with a
         * specific id]
         * @var [Brand]
         * @param [Integer register id]
         */
        $brand = Brand::FindOrFail($id);

        return view('admin.brands.edit')->with(['brand' => $brand]);
    }

    /**
     * Update the specified brand in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @param  int  $id (id of the particular brand)
     * @return redirect view(index)
     */
    public function update(BrandRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        /**
         * [try store a new Brand and register its hisotrical]
         * @var [type]
         */
        try {
            /**
             * [DB beginTransaction] Start a new transactions for to store data into all
             * tables of the proceses if there not errors
             * @var [DB]
             */
            DB::beginTransaction();

            /**
             * [Brand description] This sentence update the all data from the form
             * through Brand model in DataBase
             * @param [Integer register id]
             * @param [Request]
             */
            Brand::FindOrFail($id)
            ->update( $request->all() );

            /**
             * [ResourceFunctions createHistoricalAction] From ResourceFunctions File
             * the createHistoricalAction is called for store the historical of the operation
             * @param [String operation]
             * @param [String message]
             */
            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            /**
             * [DB commit] If all transactions into tables was correct, The changes are saved
             * in DataBase
             * @var [DB]
             */
            DB::commit();

            /**
             * [ResourceFunctions messageSuccess] From ResourceFunctions File
             * the function messageSuccess is called for generate a success message and log message
             * @param [String Controller name]
             * @param [String Metod name]
             */
            ResourceFunctions::messageSuccess('BrandController','actualizar');

        } catch (\Exception $e) {
            /**
             * [DB rollBack] This sentence applies for run a rollback if any exception occurs
             * @var [DB]
             */
            DB::rollBack();

            /**
            * [ResourceFunctions messageError] From ResourceFunctions File
            * the function messageError is called for generate a success message and log message
            * @param [String Controller name]
            * @param [String Metod name]
            * @param [Exception]
            */
            ResourceFunctions::messageError('BrandController','actualizar',$e);
        }

        return redirect()->route('brands.index');
    }

    /**
     * Remove the specified brand from storage.
     *
     * @param  int  $id (id of the particular brand)
     * @return redirect view(index)
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        /**
         * [try store a new Brand and register its hisotrical]
         * @var [type]
         */
        try {
            /**
             * [DB beginTransaction] Start a new transactions for to store data into all
             * tables of the proceses if there not errors
             * @var [DB]
             */
            DB::beginTransaction();

            /**
             * [Brand description] This sentence delet the register for a register
             * specific id
             * @var [Integer register id]
             */
            Brand::destroy($id);

            /**
             * [ResourceFunctions createHistoricalAction] From ResourceFunctions File
             * the createHistoricalAction is called for store the historical of the operation
             * @param [String operation]
             * @param [String message]
             */
            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);


           /**
            * [DB commit] If all transactions into tables was correct, The changes are saved
            * in DataBase
            * @var [DB]
            */
            DB::commit();

           /**
            * [ResourceFunctions messageSuccess] From ResourceFunctions File
            * the function messageSuccess is called for generate a success message and log message
            * @param [String Controller name]
            * @param [String Metod name]
            */
            ResourceFunctions::messageSuccess('BrandController','eliminar');

        } catch (\Exception $e) {
            /**
             * [DB rollBack] This sentence applies for run a rollback if any exception occurs
             * @var [DB]
             */
            DB::rollBack();

           /**
            * [ResourceFunctions messageError] From ResourceFunctions File
            * the function messageError is called for generate a success message and log message
            * @param [String Controller name]
            * @param [String Metod name]
            * @param [Exception]
            */
            ResourceFunctions::messageError('BrandController','eliminar',$e);
        }

        return redirect()->route('brands.index');
    }
}
