<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Meritop\Models\Order;
use Meritop\Models\Branch;
use Meritop\Models\Inventory;
use Meritop\Models\Guide;
use Meritop\Models\Club;
use Meritop\Models\MemberClub;
use Meritop\Models\UserClub;
use Meritop\Models\Account;
use Meritop\Models\InventoryTransaction;
use Meritop\Models\Agent;
use Meritop\Models\Product;
use Meritop\Models\Media;
use Meritop\Member;
use Meritop\User;
use Meritop\Models\OrderProduct;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\AccountTransaction;
use Meritop\Http\Requests\Admin\Order\FilterRequest;
use Meritop\Http\Requests\Admin\Order\CreateRequest;
use Meritop\Http\Requests\Admin\Order\UpdateRequest;
use Mail;
use Meritop\Mail\DeliveredStatusOrder;



class OrderController extends Controller
{

	public $module = 12;

		public function index()
		{
			/*
			*Method that allows the validation of privileges
			*/
			if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
				return redirect()->route('admin.dashboard');
			}

			Log::info('Ingreso exitoso a UserController - index(), del usuario: '.Auth::user()->user_name);

			$open = []; $sent = []; $delivered = []; $refounds = []; $canceled = [];
			/*
            *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
            */
			$clubs = ResourceFunctions::getClubs();

			$orders = Order::select('orders.*', 'members.dni','members.first_name','members.last_name','clubs.name as club','cities.name as city','states.name as state')
			->join('members','members.id', 'orders.fk_id_member')
			->join('branches','branches.id', 'orders.fk_id_branch')
			->join('cities','cities.id', 'branches.fk_id_city')
			->join('states','states.id', 'cities.fk_id_state')
			->join('clubs','clubs.id', 'branches.fk_id_club')
			->whereIn( 'branches.fk_id_club', $clubs)
			->get();

			$orders_products = OrderProduct::select('orders_products.*')
			->join('products', 'products.id', 'orders_products.fk_id_product')
			->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
			->get();

			$users_clubs = Club::select('clubs.*')
			->whereIn( 'clubs.id', $clubs)
			->get();
			/*
			*Relates the number of products to each order
			*/
			$id_aux = '';
			foreach ($orders as $orderDate) {
				$quantity = 0;
				foreach ($orders_products as $gpdate) {

					if ( $orderDate -> id == $gpdate ->fk_id_order) {
						$quantity += $gpdate ->quantity ;
					}
				}

				if ($id_aux != $orderDate -> id) {
					$orderDate['quantity'] = $quantity;
					$id_aux = $orderDate -> id;
				}
			}
			/*
			*Sort orders by status type
			*/
			foreach ($orders as $value) {
				switch ($value->status) {
					case 0:
						$open[] = $value;
						break;
					case 1:
						$sent[] = $value;
						break;
					case 2:
						$delivered[] = $value;
						break;
					case 3:
						$refounds[] = $value;
						break;
					case 4:
						$canceled[] = $value;
						break;
				}
			}

		return view('admin.orders.index' )->with([
			'open' 			=> $open,
			'sent' 			=> $sent,
			'delivered' 	=> $delivered,
			'refounds' 		=> $refounds,
			'canceled' 		=> $canceled,
			'users_clubs' 	=> $users_clubs,
		]);

    }

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function create()
		{
				/*
				*Method that allows the validation of privileges
				*/
				if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
						return back();
				}
				/*
		        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
		        */
				$clubs = ResourceFunctions::getClubs();

				Log::info('Ingreso exitoso a OrderController - create(), del usuario: '.Auth::user()->user_name);

				/*
				*Consultation of branch the branches related to the club
				*/
				$branches = Branch::select('branches.*')
				->whereIn('fk_id_club',$clubs)
				->get();
				/*
				*Consult of branch the members related to the club
				*/
				$members = MemberClub::select('members.*')
				->join('members', 'members.id', 'members_clubs.fk_id_member')
				->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
				->whereIn('members_clubs.fk_id_club', $clubs)
				->where('members.deleted', false)
				->get();
				/*
				*Consult of clubs
				*/
				$clubs = Club::all()->whereIn('id', $clubs);
				/*
				*Consult of products
				*/
				$products = Product::select('products.*','parents_products.name as product','inventory.quantity_current')
				->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
				->join('inventory', 'inventory.fk_id_product', 'products.id')
				->where('parents_products.deleted', 0)
				->where('parents_products.fk_id_country', Auth::user()->fk_id_country)
				->get();

				return view('admin.orders.create')->with([
					'branches'	=>$branches,
					'members'	=> $members,
					'products'	=> $products,
					'clubs'	=> $clubs
				]);
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(CreateRequest $request)
		{
			/*
			*Method that allows the validation of privileges
			*/
			if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
					return redirect()->route('users.index');
			}

			try{
				DB::beginTransaction();
				$content=0; $flag =true;
				/**
				 * Validates that when processing an order has selected items, otherwise executes the validation
				 * @var [boolean] if is true executes the redirection
				 */
				if (empty($request->quantities)) {
					flash('Para generar una orden debe tener productos registrados', '¡Error!')->error();
					return back();
				}
				/*
				*Allows you to calculate the items to be refunded
				*/
				$i =0;

				foreach ($request->quantities as $key => $valueQuantities) {
					// dd($valueQuantities);
					if ($valueQuantities > 0) {
						$products[$key] = [
							'fk_id_product' => $key,
							'quantity' 		=> $valueQuantities,
						];
						$p[$i] = $key;
						$flag = false;
						$i++;
					}
				}
				// dd("gggg");
				/**
				 * Validates that when processing an order has selected items, otherwise executes the validation
				 * @var [boolean] if is true executes the redirection
				 */
				if ($flag) {
					flash('Para procesar la orden la cantidad de items debe ser mayor a 0', '¡Error!')->error();
					return back();
				}
				/*
				*Check the number of products found in inventory
				*/
				$total_inventory = Inventory::select('inventory.quantity_current','inventory.fk_id_product')
			    ->whereIn('inventory.fk_id_product', $p)
			    ->get();
			    /*
				*Check how many points the user has in their account
				*/
				$total_points = Account::select('accounts.points')
				->where('accounts.fk_id_member', $request->fk_id_member)
				->first();
				/**
				 * Validates that the amount of items to be processed is equal to or less than the inventory
				 * @var [array] [$products] array with key and quantity of products
				 * @var [array] [$total_inventory] array with quantity of products in the inventory
				 */
				if (ResourceFunctions::validateQuantityproduct($products,$total_inventory)) {
					flash('En uno de los productos coloco una cantidad mayor a la existente en inventario', '¡Error!')->error();
					return back();
				}
				/**
				 * Validates that the total of points added by each item, is equal to or less than that in the member's account
				 * @var [array] [$products] array with key and quantity of products
				 * @var [array] [$total_inventory] array with quantity of points of account member
				 */
				if ( ResourceFunctions::validatePointMember($products, $total_points->points)  ) {
					flash('La suma del costo de los productos es mayor a los puntos que posee el miembro', '¡Error!')->error();
					return back();
				}

				$order = Order::create([
					'note'            => $request -> note,
					'internal_note'   => $request -> internal_note,
					'discount'        => $request -> discount,
					'status'          => 0,
					'fk_id_member'    => $request -> fk_id_member,
					'fk_id_branch'    => $request -> fk_id_branch,		
				]);

				if ($order) {
					/*
					*Check the product data
					*/
					$consult_products = Product::select('products.*')
		      		->whereIn('products.id', $p )
		      		->get();
		      		/*
		      		*Creates an array with the data of the products and the quantity of the same
		      		*/
		      		// dd($consult_products);
					foreach ($consult_products as $key => $valueproducts) {
						foreach ($products as $key1 => $valueOrders_products) {
							// dd($valueproducts->id, $valueOrders_products['fk_id_product']);
							if ($valueproducts->id === $valueOrders_products['fk_id_product']) {
								$data_products[] = [
									'quantity' 		=> $valueOrders_products['quantity'],
									'points' 		=> $valueproducts->points,
									'fk_id_product' => $valueproducts->id,
									'fk_id_order' 	=> $order->id,
									'created_at'    => date('Y-m-d H:i:s'),
									'updated_at'    => date('Y-m-d H:i:s'),
								];
							}
						}
					}
					/*
					*insert products in the orders_products table
					*/

					DB::table('orders_products')->insert($data_products);
					/*
					*Returns parents_products and products to the inventory and account member
					*/
					foreach ($data_products as $value) {
						/*
						*Calculates the number of points related to the order
						*/
						$content +=  $value['points'] * $value['quantity'];
						/*
						*Check the existing quantity of a product in inventory
						*/
						$inventory_quantity = Inventory::select('inventory.quantity_current','inventory.id')
						->where('inventory.fk_id_product', $value['fk_id_product'])
						->first();

							if($inventory_quantity){
								/*
								*update the inventory with items refunded
								*/
								$inventory= Inventory::where('fk_id_product',$value['fk_id_product'])
								->update ([
									'quantity_current'  => ($inventory_quantity->quantity_current - $value['quantity']  ),
									'updated_at'        => date('Y-m-d H:i:s')
								]);
								/*
								*Register the new transaction
								*/
								$inventory_transaction = InventoryTransaction::create([
										'quantity_update' 	=> $value['quantity'],
										'fk_id_inventory'	=> $inventory_quantity->id,
										'description'     	=> 'devolucion de productos',
								]);

							}//if close

						}//foreach close


					/*
					*update the inventory with items refunded
					*/
					$account = Account::select('accounts.id')
					->where('fk_id_member', $request->fk_id_member)
					->first();

					Account::where('fk_id_member', $request->fk_id_member)
					->update ([
						'points'  		=> abs( $total_points->points - $content),
						'updated_at'	=> date('Y-m-d H:i:s')
					]);

					$club = Branch::select('clubs.id')
					->join('clubs','clubs.id','branches.fk_id_club')
					->where('branches.id', $request->fk_id_branch)
					->first();

					$d = AccountTransaction::create([
		                'description' 	=> "orden generada desde el administrador",
		                'operation' 	=> 0,
		                'points' 		=> $content,
		                'balance'       => abs( $total_points->points - $content),
		                'fk_id_account' => $account->id,
		                'fk_id_club' 	=> $club->id,
		            ]);


				}//if close order


				ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro de una orden: '.$request->name);

				DB::commit();

				ResourceFunctions::messageSuccess('OrderController','store');

			} catch (\Exception $e) {
					DB::rollBack();
					ResourceFunctions::messageError('OrderController','store',$e);
			}

			return redirect()->route('orders.show', $order->id);

		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function show($id)
		{
			/*
			*Method that allows the validation of privileges
			*/
			if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
					return redirect()->route('admin.dashboard');
			}
			$subtotal= 0; $discount= 0; $total = 0; $quantity =0;
			/*
			*Check order and related data
			*/
			$order = Order::where( 'orders.id', $id)
			->select('orders.*', 'members.dni', 'members.first_name', 'members.last_name', 'members.email','branches.postal_code','branches.name','branches.address','branches.phone','clubs.name as club','states.name as state','cities.name as city','members_clubs.carnet')
			->join('members','members.id', 'orders.fk_id_member')
			->join('members_clubs','members_clubs.fk_id_member','members.id')
			->join('clubs','clubs.id', 'members_clubs.fk_id_club')
			->join('branches','branches.id', 'orders.fk_id_branch')
			->join('cities','cities.id', 'branches.fk_id_city')
			->join('states','states.id', 'cities.fk_id_state')
			->first();
			/*
			*check all agent
			*/
			$agents = Agent::all();
			/*
			*check all products to show
			*/
			$orders_products = OrderProduct::where('fk_id_order', $id)->select('products.sku','products.feature','parents_products.name','orders_products.*','orders.discount')
			->join('products', 'products.id', 'orders_products.fk_id_product')
			->join('orders', 'orders.id', 'orders_products.fk_id_order')
			->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
			->get();
			/*
			*See the guides related to the order
			*/
			$guides = Guide::where('fk_id_order', $id)->select('guides.*','agents.name as agent')
			->join('agents', 'agents.id', 'guides.fk_id_agent')
			->get();

			/*
			*Calculates in subtotal discount and the amount d points
			*/
			foreach ($orders_products as $value) {

				$subtotal += $value -> points * $value->quantity;
				$discount =  $value -> discount;
				$quantity += $value -> quantity ;
			}

			$total = (($subtotal) + ($discount));

			return view('admin.orders.show')->with([
				'order' 			=> $order,
				'orders_products'	=>$orders_products,
				'agents'			=>$agents,
				'guides'			=>$guides,
				'subtotal'			=>$subtotal,
				'total'				=>$total,
				'discount'			=>$discount,
				'quantity'			=>$quantity]);
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function edit($id)
		{
				/*
				*Method that allows the validation of privileges
				*/
				if ( (ResourceFunctions::validationReturn($this->module,4)) !=true) {
						 return back();
				}

				Log::info('Ingreso exitoso a OrderController - edit(), del usuario: '.Auth::user()->user_name);
				/*
		        *Captures the id of the session variable
		        */
				$id_club = session('clubs')->first()->fk_id_club;
				/*
				*Consult the order
				*/
				$order = Order::select('orders.*', 'members.id as member','members.dni','members.first_name','members.last_name','members_clubs.carnet','branches.name as branch')
				->join('members','members.id', '=', 'orders.fk_id_member')
				->join('members_clubs','members_clubs.fk_id_member', '=', 'members.id')
				->join('branches','branches.id', '=', 'orders.fk_id_branch')
				->where( 'orders.id', $id)
				->first();
				/*
				*Consult the branches related to a specific club
				*/
				$branches = Branch::select('branches.*')
				->where('fk_id_club',$id_club)
				->get();
				/*
				*Consult the guides related to the order
				*/
				$guides = Guide::select('guides.*')
				->where('fk_id_order',$id)
				->get();
				/*
				*Consult the products related to the order
				*/
				$orders_products = OrderProduct::where('fk_id_order', $id)->select('products.*','parents_products.name','orders_products.quantity')
				->join('products', 'products.id', 'orders_products.fk_id_product')
				->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
				->get();

				return view('admin.orders.edit')
				->with([
						'order' 			=> $order,
						'branches' 			=> $branches,
						'guides' 			=> $guides,
						'orders_products' 	=> $orders_products,
				]);

		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function update(UpdateRequest $request, $id)
		{
			/*
			*Method that allows the validation of privileges
			*/
			if ( (ResourceFunctions::validationReturn($this->module,4)) !=true) {
					 return back();
			}

			try {
				DB::beginTransaction();
				/*
				*update data order
				*/
				$order = Order::where('id',$id)
				->update([
					'note'           => $request -> note,
					'internal_note'  => $request -> internal_note,
					'fk_id_branch'   => $request -> fk_id_branch,
					'updated_at'     => date('Y-m-d H:i:s')
				]);

				ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

				DB::commit();

				ResourceFunctions::messageSuccess('OrderController','actualizar');

			} catch (\Exception $e) {
				DB::rollBack();
				ResourceFunctions::messageError('OrderController','actualizar',$e);
			}

			return redirect()->route('orders.show', $id);

		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id)
		{
			/*
	        *Method that allows the validation of privileges
	        */
	        if ( (ResourceFunctions::validationReturn($this->module,5)) !=true) {
	             return back();
	        }
			try {
				DB::beginTransaction();

				$content_points=0;
				/*
				*Allows you to consult the products of the orders
				*/
				$orders_products = OrderProduct::where('fk_id_order', $id)->select('products.points','products.id as product','orders_products.quantity','orders.fk_id_member','orders.fk_id_branch')
					->join('products', 'products.id', 'orders_products.fk_id_product')
					->join('orders', 'orders.id', 'orders_products.fk_id_order')
					->get();

					foreach ($orders_products as $value) {
						/*
						*Calculates the points that will be returned to the member
						*/
						$content_points += $value->points * $value->quantity;
						/*
						*Allows you to consult the total inventory
						*/
						$total_inventory = Inventory::select('inventory.quantity_current','inventory.id as inventory')
						->where('inventory.fk_id_product', $value->product)
						->first();

						if($total_inventory){
							/*
							*Update the member's account by returning the points of the products refunded
							*/
							$inventory= Inventory::where('fk_id_product',$value->product)
							->update ([
								'quantity_current'  => abs($value->quantity + $total_inventory->quantity_current),
								'updated_at'        => date('Y-m-d H:i:s')
							]);

							$in =InventoryTransaction::create([
								'quantity_update' 	=> $value->quantity,
								'fk_id_inventory'   => $total_inventory->inventory,
								'description'       => 'devolucion de products',
							]);

						}//if close
					}// forech close

					if ($inventory) {

						Order::where('id',$id)
						->update([
								'status'  => 4,
						]);

						$order= Order::select('orders.*')->where('id',$id)->first();

						/*
						*update the inventory with items refunded
						*/
						$account = Account::select('accounts.*')
						->where('fk_id_member',$order->fk_id_member)
						->first();

						 Account::where('fk_id_member',$order->fk_id_member)
						->update([
								'points'  => abs( $account->points + $content_points),
						]);
						// dd($account);
						$club = Branch::select('clubs.id')
						->join('clubs','clubs.id','branches.fk_id_club')
						->where('branches.id', $order->fk_id_branch)
						->first();

						AccountTransaction::create([
			                'description' 	=> "Anulacion de la orden",
			                'operation' 	=> 0,
			                'points' 		=> -($content_points),
			                'balance'       => abs( $account->points + $content_points),
			                'fk_id_account' => $account->id,
			                'fk_id_club' 	=> $club->id,
			            ]);


					}

			ResourceFunctions::createHistoricalAction('cancelar','Se ha realizado el proceso de cancelar una orden: ');

			DB::commit();

			ResourceFunctions::messageSuccess('OrderController','cancelar');

			} catch (\Exception $e) {
				DB::rollBack();
				ResourceFunctions::messageError('OrderController','cancelar',$e);
			}

			return redirect()->route('orders.index');

		}


		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function refund_create(Request $request, $id){

			/*
	        *Method that allows the validation of privileges
	        */
	        if ( (ResourceFunctions::validationReturn($this->module,3)) !=true) {
	             return back();
	        }

	        Log::info('Ingreso exitoso a RefundController - edit(), del usuario: '.Auth::user()->user_name);
	        /*
	        *Initialization of arrays
	        */
	        $open = []; $sent = []; $delivered = []; $refounds = []; $canceled = [];
	        /*
	        *Captures the id of the session variable
	        */
	        $id_club = session('clubs')->first()->fk_id_club;
	        /*
			*Consult the order
			*/
	        $order = Order::select('orders.*', 'members.id as member','members.dni','members.first_name','members.last_name','members_clubs.carnet','branches.name as branch')
	        ->join('members','members.id', 'orders.fk_id_member')
	        ->join('members_clubs','members_clubs.fk_id_member','members.id')
	        ->join('branches','branches.id','orders.fk_id_branch')
	        ->where( 'orders.id', $id)
	        ->first();
	        /*
			*Consult the guides related to the order
			*/
	        $guides = Guide::select('guides.*')
	        ->where('fk_id_order',$id)
	        ->get();
	        /*
			*Consult the products related to the order
			*/
	        $orders_products = OrderProduct::where('fk_id_order', $id)->select('products.*','parents_products.name','orders_products.quantity')
	        ->join('products', 'products.id', 'orders_products.fk_id_product')
	        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
	        ->get();

	        return view('admin.orders.refund-create')
	        ->with([
	            'order' 			=> $order,
	            'guides' 			=> $guides,
	            'orders_products' 	=> $orders_products,
	        ]);

		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @return \Illuminate\Http\Response
		 */
		public function refund_store(Request $request, $id){

			/*
	        *Method that allows the validation of privileges
	        */
	        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
	            return redirect()->route('users.index');
	        }

	        try{

	            DB::beginTransaction();
	            /*
	            *consult to the id of order referenced
	            */
	            $order_reference = Order::select('*')->where('id',$id)->first();

	            /*
	            *insert data fields in the request and then insert into the database
	            */
	            $request -> request -> add([
					'status' 			=> 3,
					'id_order_reference' => $id,
					'fk_id_branch' 		=> $order_reference->fk_id_branch,
					'fk_id_member' 		=> $order_reference->fk_id_member
				]);
				$order = Order::create($request -> all());

				if ($order) {
	            	/*
		            *consult to the id of order referenced
		            */
	            	// $id_order = Order::all()->last()->id;
	            		/*
			            *consult data of order referenced
			            */
						$products_references = OrderProduct::select('orders_products.*')
						->where('fk_id_order',$id)
						->get();
						/*
						*Allows you to calculate the items to be refunded
						*/

						$i=0;
						foreach ($request->quantity as $key1 => $valueQuantity) {
							foreach ($request->points as $key2 =>  $valuePoints) {
								if ($key1==$key2) {
									$products[$key1] = [
										'quantity' 		=> $valueQuantity,
										'points' 		=> $valuePoints,
										'fk_id_product' => $key1,
									];
									$i++;
									break;
								}

							}
						}

					/*
					*Allows you to sort the received arrangement
					*/
					$refunds_products = [];

						foreach ($products_references as $key1 => $valueReferences) {

							foreach ($products as $key2 => $valueRefunds) {

								if( ($valueReferences->fk_id_product== $valueRefunds['fk_id_product'])	&& (($valueRefunds['quantity'] >$valueReferences->quantity)  ) && ( $valueRefunds['quantity'] !=0) ){
										flash('La cantidad de parents_productsos que devolvera debe ser igual o menor a la cantidad encontrada en la orden que gestiona', '¡Error!')->error();
											return back();
								}elseif (	( ($valueReferences->fk_id_product== $valueRefunds['fk_id_product'])	&&	( $valueRefunds['quantity'] <= $valueReferences->quantity  ) && ( $valueRefunds['quantity'] !=0)  ) ) {
											/*
											*Arrangement that will have the products that will be refunded
											*/
											$refunds_products[$key1]	 = [
												'quantity' 		=>  abs($valueRefunds['quantity']),
												'points' 		=> $valueRefunds['points'],
												'fk_id_product' => $valueRefunds['fk_id_product'],
												'fk_id_order' 	=> $order->id,
												'created_at'    => date('Y-m-d H:i:s'),
												'updated_at'    => date('Y-m-d H:i:s'),
											];
									}
							}
						}

					/*
					*Inserts in the database the products related to the order
					*/
	            	DB::table('orders_products')->insert($refunds_products);

	            	/*
	            	*Returns parents_products and products to the inventory and account member
	            	*/
	            	$content=0;

	            	foreach ($refunds_products as $value) {
	            		/*
						*Calculates the points that will be returned to the member
						*/
						$content += $value['points'] * $value['quantity'];
						/*
						*Allows you to consult the total inventory
						*/
						$total_inventory = Inventory::select('inventory.quantity_current','inventory.id as inventory')
						->where('inventory.fk_id_product', $value['fk_id_product'])
						->first();

							if($total_inventory){
								/*
								*update the inventory with items refunded
								*/
								$inventory= Inventory::where('fk_id_product',$value['fk_id_product'])
								->update ([
									'quantity_current'  => abs($value['quantity'] + $total_inventory->quantity_current),
									'updated_at'        => date('Y-m-d H:i:s')
								]);

								/*
								*Register the new transaction
								*/
								$inventory_transaction = InventoryTransaction::create([
										'quantity_update' => $value['quantity'],
										'fk_id_inventory' => $total_inventory->inventory,
										'description'     => 'devolucion de products',
								]);

							}//if close

					}//foreach close


					/*
					*Code that allows returning the points to the user
					*/
					$total_points = Account::select('accounts.points')
					->where('accounts.fk_id_member', $order_reference->fk_id_member)
					->first();

					$points = 0;
					if ($total_points !=null) {
						$points = $total_points->points;
					}

					/*
					*Update the member's account by returning the points of the products refunded
					*/
					Account::where('fk_id_member', $order_reference->fk_id_member)
					->update ([
						'points'  		=> ( $points + $content),
						'updated_at'	=> date('Y-m-d H:i:s')
					]);

					$account = Account::select('accounts.*')
					->where('fk_id_member',$order->fk_id_member)
					->first();

					$club = Branch::select('clubs.id')
					->join('clubs','clubs.id','branches.fk_id_club')
					->where('branches.id', $order->fk_id_branch)
					->first();

					$account_transaction = AccountTransaction::create([
		                'description' 	=> "devolucion de una orden",
		                'operation' 	=> 0,
		                'points' 		=> -($content),
		                'balance'       => ( $points + $content),
		                'fk_id_account' => $account->id,
		                'fk_id_club' 	=> $club->id,
		            ]);


	            }//if close order

	            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro de una devolucion: '.$request->name);

	            DB::commit();

	            ResourceFunctions::messageSuccess('OrderController','store');

	        } catch (\Exception $e) {
	            DB::rollBack();
	            ResourceFunctions::messageError('OrderController','store',$e);
	        }

	        return redirect()->route('orders.show', $order->id);

		}


		public function filter(FilterRequest $request){

		/*
		*Method that allows the validation of privileges
		*/
		if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
				return redirect()->route('admin.dashboard');
		}

			Log::info('Ingreso exitoso a OrderController - filter(), del usuario: '.Auth::user()->user_name);
			/*
			*Initialization of arrays
			*/
			$open = []; $sent = []; $delivered = []; $refounds = []; $canceled = [];
			/*
			*Method that allows processing the range of dates
			*/
			$fechas = ResourceFunctions::getDates($request->date);
			$fecha_inicio = $fechas[0];
			$fecha_fin = $fechas[1];
			// dd($fecha_inicio,$fecha_fin);
			/*
			*Function that takes the id of the related clubs and turns them into an arrangement
			*/
			if ($request->fk_id_club == null) {
	            $clubs = ResourceFunctions::getClubs();
	        }else {
	            $clubs = [$request->fk_id_club];
	        }
			/*
			*consult to the orders
			*/
			$orders = Order::select('orders.*', 'members.dni','members.first_name','members.last_name','clubs.name as club' ,'cities.name as city','states.name as state')
			->join('members','members.id', 'orders.fk_id_member')
			->join('branches','branches.id', 'orders.fk_id_branch')
			->join('cities','cities.id', 'branches.fk_id_city')
			->join('states','states.id', 'cities.fk_id_state')
			->join('clubs','clubs.id', 'branches.fk_id_club')
			->whereIn( 'branches.fk_id_club', $clubs)
			->whereBetween('orders.created_at', [$fecha_inicio, $fecha_fin])
			->get();
			/*
			*consult to the products of orders
			*/
			$orders_products = OrderProduct::select('orders_products.*')
			->join('products', 'products.id', 'orders_products.fk_id_product')
			->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
			->get();
			/*
			*colsult the clubs related to the user
			*/
			$users_clubs = Club::select('clubs.*')
			->whereIn( 'clubs.id', $clubs)
			->get();

			/*
			*Relates the number of products to each order
			*/
			$id_aux = '';
			foreach ($orders as $orderDate) {
				$quantity = 0;
				foreach ($orders_products as $gpdate) {

					if ( $orderDate -> id == $gpdate ->fk_id_order) {
						$quantity += $gpdate ->quantity ;
					}
				}

				if ($id_aux != $orderDate -> id) {
					$orderDate['quantity'] = $quantity;
					$id_aux = $orderDate -> id;
				}
			}
			/*
			*Sort orders by status type
			*/
			foreach ($orders as $value) {
				switch ($value->status) {
					case 0:
						$open[] = $value;
						break;
					case 1:
						$sent[] = $value;
						break;
					case 2:
						$delivered[] = $value;
						break;
					case 3:
						$refounds[] = $value;
						break;
					case 4:
						$canceled[] = $value;
						break;
				}
			}

		return view('admin.orders.index' )->with([
			'open' 			=> $open,
			'sent' 			=> $sent,
			'delivered' 	=> $delivered,
			'refounds' 		=> $refounds,
			'canceled' 		=> $canceled,
			'users_clubs' 	=> $users_clubs,
		]);

		}


		/**
		 * Update the specified resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function status(Request $request, $id)
		{

			/*
			*Method that allows the validation of privileges
			*/
			if ( (ResourceFunctions::validationReturn($this->module,4)) !=true) {
					 return back();
			}
			try {
				DB::beginTransaction();

				$order = Order::where('id',$id)
				->update([
					'status'        => 2,
					'updated_at'	=> date('Y-m-d H:i:s')

				]);

				if ($order) {
					/*
					*Check the orders, and the data related to it
					*/
					$data_order = Order::where( 'orders.id', $id)
					->select('orders.*', 'members.email','members.first_name','members.last_name','guides.num_guide','agents.name as agent','branches.fk_id_club')
					->join('members','members.id', 'orders.fk_id_member')
					->join('guides','guides.fk_id_order', 'orders.id')
					->join('agents','agents.id', 'guides.fk_id_agent')
					->join('branches','branches.id','orders.fk_id_branch')
					->first();
					/*
					*Check the items of the order
					*/
					$items = OrderProduct::where('fk_id_order', $id)->select('products.*','parents_products.name','orders_products.quantity','orders.discount')
					->join('products', 'products.id', 'orders_products.fk_id_product')
					->join('orders', 'orders.id', 'orders_products.fk_id_order')
					->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
					->get();
					/*
		            *consult the image related club
		            */
		            $logo_image = Media::where([
		                'identificator' => $data_order->fk_id_club,
		                'table'         => 'clubs'
		            ])
		            ->first();
		            $logo = $_ENV['STORAGE_PATH'].$logo_image->image;
		            /*
					*Code that allows the sending of emails
					*/
					Mail::to($data_order->email)->send(new DeliveredStatusOrder($data_order, $items, $logo));
					Mail::to("order@meritop.com")->send(new DeliveredStatusOrder($data_order, $items, $logo));
				}

				ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

				DB::commit();

				ResourceFunctions::messageSuccess('OrderController','actualizar');

			} catch (\Exception $e) {
				DB::rollBack();
				ResourceFunctions::messageError('OrderController','actualizar',$e);
			}

			return redirect()->route('orders.show', $id);

		}


}
