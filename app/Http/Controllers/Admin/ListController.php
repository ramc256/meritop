<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\State;
use Meritop\Models\City;
use Meritop\Member;

class ListController extends Controller
{
    
     /**
     * get state To generate nested lists.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id country
     * @return \Illuminate\Http\Response
     */
    public function getState(Request $request, $id)
    {
        if ($request->ajax()) {
            /*
            *consult the states Depending on the id of the country
            */
            $state = State::where('fk_id_country', $id)
            ->get();
            return response()->json($state);
        }
    }


    /**
     * get cities To generate nested lists.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id state
     * @return \Illuminate\Http\Response
     */
    public function getCity(Request $request, $id)
    {
        if ($request->ajax()) {
            /*
            *consult the cities Depending on the id of the state
            */
            $city = City::where('fk_id_state', $id)
            ->get();
            return response()->json($city);
        }
    }


   
}
