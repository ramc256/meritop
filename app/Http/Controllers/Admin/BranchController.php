<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Branch;
use Meritop\Models\Club;
use Meritop\Models\City;
use Meritop\Models\Country;
use Meritop\Models\State;
use Meritop\Models\MemberBranch;
use Meritop\Models\HistoricalImport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Assets\ResourceFunctions;
use Illuminate\Support\Facades\Input;
use Excel;
use Meritop\Http\Requests\Admin\Branch\BrachCreateRequest;
use Meritop\Http\Requests\Admin\Branch\BrachUpdateRequest;
use Meritop\Http\Requests\Admin\Branch\BrachImportRequest;

class BranchController extends Controller
{
    public $module = 14;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
         /*
         *Method that allows the validation of privileges
         */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a BranchesController - index(), del usuario: '.Auth::user()->user_name);

        /**
         * [$clubs description] Variable that contain all user's clubs login
         * @var [type] Integer Array
         */
        $clubs = ResourceFunctions::getClubs();
        /*
        *consult the branches
        */
        $branches = Branch::select('branches.*', 'countries.name as country','states.name as state','cities.name as city')
        ->join('cities','cities.id','branches.fk_id_city')
        ->join('states','states.id','cities.fk_id_state')
        ->join('countries','countries.id','states.fk_id_country')
        ->whereIn( 'branches.fk_id_club', $clubs)
        ->get();

        return view('admin.branches.index')->with([
            'branches' => $branches
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.branches.index');
        }

        Log::info('Ingreso exitoso a BranchesController - create(), del usuario: '.Auth::user()->user_name);
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $id_clubs = ResourceFunctions::getClubs();
        /*
        *consult the clubs related to the user
        */
        $clubs = Club::whereIn('id', $id_clubs)->get();
        /*
        *consult the countries
        */
        $countries = Country::orderBy('name', 'asc')->get();

        return view('admin.branches.create')->with([
            'clubs'     => $clubs,
            'countries' => $countries,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrachCreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }
        $clubs = ResourceFunctions::getClubs();

        try{
        /*
        * add a field to the request and then insert in the branch table
        */
        $request -> request -> add(['fk_id_club' => session('clubs')->first()->fk_id_club]);

        $branch = Branch::create($request -> all());

        ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

        DB::commit();

        ResourceFunctions::messageSuccess('BrachController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('BranchController','store',$e);
        }

        return redirect()->route('branches.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a BrachController - show(), del usuario: '.Auth::user()->user_name);
        /*
        *consult the branches
        */
        $branch = Branch::select('branches.*', 'countries.name as country','states.name as state','cities.name as city','clubs.name as club')
        ->join('cities','cities.id', 'branches.fk_id_city')
        ->join('states','states.id', 'cities.fk_id_state')
        ->join('clubs','clubs.id', 'branches.fk_id_club')
        ->join('countries','countries.id', 'states.fk_id_country')
        ->where( 'branches.id', $id)
        ->first();

        return view('admin.branches.show')->with([
            'branch' => $branch
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a BranchesController - edit(), del usuario: '.Auth::user()->user_name);
        /*
        *consult the branch realted to the id passed by parameter
        */
        $branch = Branch::select('branches.*', 'countries.id as id_country','states.id as id_state','cities.id as id_city')
        ->join('cities','cities.id', 'branches.fk_id_city')
        ->join('states','states.id', 'cities.fk_id_state')
        ->join('countries','countries.id', 'states.fk_id_country')
        ->where( 'branches.id', $id)
        ->first();
        /*
        *consult the countries
        */
        $countries = Country::orderBy('name', 'asc')->get();
        /*
        *consult the cities
        */
        $cities = City::orderBy('name', 'asc')->get();
        /*
        *consult the states
        */
        $states = State::orderBy('name', 'asc')->get();

        return view('admin.branches.edit')->with([
            'branch'    => $branch,
            'states'    => $states,
            'cities'    => $cities,
            'countries' => $countries
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BrachUpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();
            /*
            *update data to the branch
            */
            $branch=Branch::where('id',$id)
             ->update([
                'name'              => $request->name ,
                'address'           => $request->address,
                'postal_code'       => $request->postal_code,
                'dispatch_route'    => $request->dispath_route,
                'phone'             => $request->phone,
                'secondary_phone'   => $request->secondary_phone,
                'fk_id_city'        => $request->fk_id_city,
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('BrachController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('BrachController','actualizar',$e);
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();
            /*
            *Allows verifit if the seat has related members
            */
            $member_Branches = MemberBranch::select('members_branches.*')
            ->where( 'members_branches.fk_id_branch', $id)
            ->get();
            /*
            *Validates that the headquarters does not have related members
            */
            if (($member_Branches->count()!=0)) {
                 flash('No es posible eliminar la sede debido ha que tiene miembros relacionados', '¡Alert!')->warning();
                 return back();
            }

            Branch::destroy($id);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('MemberController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('MemberController','eliminar',$e);
        }

        return redirect()->route('branches.index');
    }


    /**
     * method that allows to importing files.
     *
     *@param Meritop\Http\Requests\Admin\Member\MemberImportRequest  $request.
     *@param $branches_registers: Arrangement with the data of the file branches.
     *@param $registers: Variable that contains the number of records with correct data, to register.
     *@param $no_registers: Variable that contains the number of records with incorrect data.
     *@param $status_registers: Allows to identify if the data of the file are acts or not to save in the database.
     * @return \Illuminate\Http\Response
     */
    public function import(BrachImportRequest $request){

        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,6)) !=true) {
            return back();
        }

        $file = $request -> file('import_file');

        /*
        *File header indices
        */
        $keys=[
            0=>"code",1=>"name", 2=>"address", 3=>"postal_code", 5=>"fk_id_city"
        ];
        /*
        *Method to load the file
        */
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
                $reader->formatDates(true, 'Y-m-d');
            })->get();

            $dataColumn = Excel::load($path, function($reader) {
                $w=$reader->select(array('code','name','address','postal_code','fk_id_city'))->get();
            })->get();

            /*
            *Validate the correct file structure
            */
            if ( (empty($data) ) 
                || ($data->count()==0)  
                || ((ResourceFunctions::validateHead($keys, $dataColumn)!=5)==true)  
                || (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"code")==false)  
                || (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"name")==false)  
                || (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"address")==false) 
                || (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"postal_code")==false) 
                || (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"fk_id_city")==false)  
                || (ResourceFunctions::validateColumnNumeric($dataColumn->toArray(),"postal_code") ) 
                || (ResourceFunctions::validateColumnNumeric($dataColumn->toArray(),"fk_id_city") )
                // || ( ResourceFunctions::validateSheetExcel($data) ) 
            ) {
                 

                if ((empty($data) ) || ($data->count()==0)) {
                   ResourceFunctions::messageErrorExportNull('BrachController','store',null);
                }
                /*
                *allows you to validate that the file has only one sheet
                */
                // if( ResourceFunctions::validateSheetExcel($data) ){
                // flash('Para procesar el archivo debe tener solo una hoja', '¡Error!')->error();
                // }
                /*
                *Validate that the fk_id_city column is not void
                */
                if (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"fk_id_city")==false) {
                    ResourceFunctions::messageErrorColumnNull('BrachController', "fk_id_city");
                }
                /*
                *Validate that the postal_code column is not void
                */
                if (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"postal_code")==false) {
                    ResourceFunctions::messageErrorColumnNull('BrachController', "postal_code");
                }
                /*
                *Validate that the address column is not void
                */
                if (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"address")==false) {
                    ResourceFunctions::messageErrorColumnNull('BrachController', "address");
                }
                /*
                *Validate that the name column is not void
                */
                if (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"name")==false) {
                    ResourceFunctions::messageErrorColumnNull('BrachController', "name");
                }
                /*
                *Validate that the code column is not void
                */
                if (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"code")==false) {
                    ResourceFunctions::messageErrorColumnNull('BrachController', "code");                
                }
                /*
                *Validate that the file header is correct
                */
                /*
                *Validate that the fk_id_city column is just numbers
                */
                if (ResourceFunctions::validateColumnNumeric($dataColumn->toArray(),"fk_id_city")) {
                      ResourceFunctions::messageErrorColumnNumeric('BrachController',"fk_id_city");
                }
                /*
                *Validate that the fk_id_city column is just numbers
                */
                if (ResourceFunctions::validateColumnNumeric($dataColumn->toArray(),"postal_code")) {
                      ResourceFunctions::messageErrorColumnNumeric('BrachController',"postal_code");
                }
                if ( (ResourceFunctions::validateHead($keys, $data)!=5)==true  ) {
                    ResourceFunctions::messageStructureHeadboard('BrachController');
                }
                /*
                *Saves a history of the files that matter
                */
                HistoricalImport::insert([
                    'file_name'        => $file ->getClientOriginalName(),
                    'status'           => 0,
                    'ip_address'       => $request->ip(),
                    'total_records'    => 0,
                    'success_records'  => 0,
                    'fail_records'     => 0,
                    'kind'             => 'import_Branches',
                    'fk_id_user'       => Auth::user()->id,
                    'created_at'       => date('Y-m-d H:i:s'),
                    'updated_at'       => date('Y-m-d H:i:s'),
                ]);
                return back();

            }else{

                    
                $registers=0;$no_registers=0; $aux=false;
                /*
                *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
                */
                $clubs = ResourceFunctions::getClubs();

                $id_cities = City::select('cities.id')
                ->join('states', 'states.id', 'cities.fk_id_state')
                ->join('countries', 'countries.id', 'states.fk_id_country')
                ->where('countries.id', Auth::user()->fk_id_country)
                ->get()->toArray();

                $code_branches = Branch::select('branches.code')
                ->where('fk_id_club', $clubs)
                ->get()->toArray();

                foreach ($data as $key => $valueData) {
                    /*
                    *Allows to validate that the code of the city is in the db
                    */
                    // dd( in_array($valueData->code, array_column($code_branches, 'code') )==false );
                    if ( (in_array($valueData->fk_id_city, array_column($id_cities, 'id') ))  && ( in_array($valueData->code, array_column($code_branches, 'code') )==false )  ) {
                        /*
                        *Load the records with the correct information
                        */
                        $branches_registers[] = [
                            'code'      => $valueData->code,
                            'name'      => $valueData->name,
                            'address'   => $valueData->address,
                            'phone'     => $valueData->phone,
                            'status'    => 1
                        ];
                        $registers++;

                    }else{
                        /*
                        *Load records with incorrect information
                        */
                        $branches_registers[] = [
                            'code'      => $valueData->code,
                            'name'      => $valueData->name,
                            'address'   => $valueData->address,
                            'phone'     => $valueData->phone,
                            'status'    => 0
                        ];
                        $no_registers++;
                        $aux = true;
                    }
                }//cierre del forech

                /*
                *Save a history of the uploaded file
                */
                HistoricalImport::create([
                    'file_name'        => $file ->getClientOriginalName(),
                    'status'           => 0,
                    'ip_address'       => $request->ip(),
                    'total_records'    => 0,
                    'success_records'  => 0,
                    'fail_records'     => 0,
                    'kind'             => 'import_Branches',
                    'fk_id_user'       => Auth::user()->id,
                ]);
                // dd($branches_registers);
                /*
                *Allows to validate that all the records comply with the parameters to register them
                */
                // dd($branches_registers,$aux);
                if ($aux==true) {

                    return view('admin.branches.import' )
                    ->with([
                        'branches_registers'    => $branches_registers,
                        'registers'             => $registers,
                        'no_registers'          => $no_registers,
                        'status_branches'       => 0,
                    ]);

                }else{

                    foreach ($data as $key => $value) {
                        try{

                            DB::beginTransaction();

                            $branches = Branch::create([
                                'code'              => $value->code,
                                'name'              => $value->name,
                                'address'           => $value->address,
                                'postal_code'       => $value->postal_code,
                                'dispatch_route'    => $value->dispatch_route,
                                'phone'             => $value->phone,
                                'fk_id_club'        => session('clubs')->first()->fk_id_club,
                                'fk_id_city'        => $value->fk_id_city,
                            ]);
                            /*
                            *function that allows register the action Performed
                            */
                            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

                            DB::commit();

                        } catch (\Exception $e) {
                            DB::rollBack();
                            ResourceFunctions::messageError('BranchController','store',$e);
                        }
                    }//foreach clouse

                    return view('admin.branches.import' )
                    ->with([
                        'branches_registers'   => $branches_registers,
                        'registers'            => $registers,
                        'no_registers'         => $no_registers,
                        'status_branches'      => 1,
                    ]);

                }
            }//else close
        }
    }//import method close


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_import($id)
    {

        


        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a BrachController - show(), del usuario: '.Auth::user()->user_name);
        /*
        *consult the branches
        */
        $branch = Branch::select('branches.*', 'countries.name as country','states.name as state','cities.name as city','clubs.name as club')
        ->join('cities','cities.id', 'branches.fk_id_city')
        ->join('states','states.id', 'cities.fk_id_state')
        ->join('clubs','clubs.id', 'branches.fk_id_club')
        ->join('countries','countries.id', 'states.fk_id_country')
        ->where( 'branches.id', $id)
        ->first();

        return view('admin.branches.import' )
        ->with([
            'branches_registers'   => $branches_registers,
            'registers'            => $registers,
            'no_registers'         => $no_registers,
            'status_branches'      => 1,
        ]);

        
    }

}
