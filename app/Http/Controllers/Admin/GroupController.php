<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Models\Group;
use Meritop\Models\Club;
use Meritop\Models\Program;
use Meritop\Models\MemberGroup;
use Meritop\Models\Department;
use Meritop\Member;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Group\GroupRequest;
use Meritop\Http\Requests\Admin\Group\FilterRequest;

class GroupController extends Controller
{
    public $module = 5;
    /**
     * Display a listing of the Group.
     *
     *  @return the view(index)
     */

    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
       if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
           return redirect()->route('admin.dashboard');
       }

       Log::info('Ingreso exitoso a GroupController - index(), del usuario: '.Auth::user()->user_name);
       /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
       $clubs = ResourceFunctions::getClubs();

       $groups = Group::select('groups.*')
       ->join('members_groups', 'members_groups.fk_id_group', 'groups.id')
       ->join('members', 'members_groups.fk_id_member', 'members.id')
       ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
       ->where('members_clubs.fk_id_club', $clubs)
       ->distinct()
       ->get();

       $num_groups = $groups -> count();

       return view('admin/groups/index')->with(['groups' => $groups, 'num_groups' => $num_groups]);
    }

    /**
     * Show the form for creating a new Group.
     *
     * @return the view(create)
     */
    public function create()
    {
       /*
        *Method that allows the validation of privileges
        */
       if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
           return redirect()->route('admin.dashboard');
       }

        Log::info('Ingreso exitoso a GroupController - create(), del usuario: '.Auth::user()->user_name);

        $clubs = ResourceFunctions::getClubs();

        $members = Member::select('members.*','departments.name')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->distinct()
        ->get();

        $departments = Department::select('departments.*')
        ->whereIn('departments.fk_id_club', $clubs)
        ->distinct()
        ->get();

        return view('admin.groups.create')->with([
            'members'       => $members,
            'departments'   => $departments
        ]);
    }

    /**
     * Store a newly created Group in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @return redirect view(index)
     */
    public function store(GroupRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            /*---------------------------------------------Create a new Group---------------------------------------------*/
            Group::create($request->all());
            $fk_id_group = Group::all()->last()->id;


            /*---------------------------------------------Create multiple records of members_groups---------------------------------------------*/
            /*
                Format a vector to make multiple inserts to the table
                Of the different checkboxes selected from the matrix.
            */
            $i=0;
            $members_groups = null;

            foreach ($request->members as $campo => $dato) {
                $members_groups[$i] = [
                    'fk_id_member' => $dato,
                    'fk_id_group' => $fk_id_group,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $i++;
             }

            DB::table('members_groups')->insert($members_groups);

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('GroupController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('GroupController','store',$e);
        }

        return redirect()->route('groups.index');
    }

    /**
     * Display the specified Group.
     *
     * @param  int  $id - (id of the particular Group)
     * @return the view(show)
     */
    public function show($id)
    {
       /*
        *Method that allows the validation of privileges
        */
       if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
           return redirect()->route('admin.dashboard');
       }

        Log::info('Ingreso exitoso a GroupController - show(), del usuario: '.Auth::user()->user_name);

        $group = Group::FindOrFail($id);
        $members = MemberGroup::select('members_groups.*', 'members.first_name', 'members.last_name', 'members.dni')
        ->join('members', 'members.id', 'members_groups.fk_id_member')
        ->where('fk_id_group', $id)
        ->get();

        return view('admin/groups/show')->with(['group' => $group, 'members' => $members]);
    }

    /**
     * Show the form for editing the specified Group.
     *
     * @param  int  $id (id of the particular Group)
     * @return the view(edit)
     */
    public function edit($id)
    {
       /*
        *Method that allows the validation of privileges
        */
       if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
           return back();
       }

        Log::info('Ingreso exitoso a GroupController - edit(), del usuario: '.Auth::user()->user_name);

        $clubs = ResourceFunctions::getClubs();

        $group = Group::FindOrFail($id);

        $members = Member::select('members.*','departments.name')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->distinct()
        ->get();

         $departments = Department::select('departments.*')
        ->whereIn('departments.fk_id_club', $clubs)
        ->distinct()
        ->get();

        $members_groups = MemberGroup::where('fk_id_group', $id)->get();

        return view('admin/groups/edit')->with([
            'group'             => $group,
            'members'           => $members,
            'members_groups'    => $members_groups,
            'departments'       => $departments
        ]);
    }

    /**
     * Update the specified Group in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @param  int  $id (id of the particular Group)
     * @return redirect view(index)
     */
    public function update(GroupRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
       if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
           return redirect()->route('admin.dashboard');
       }

        try {
            DB::beginTransaction();

            Group::where('id', $id)
            ->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    ]);

            /*---------------------------------------------Create multiple records of members_groups---------------------------------------------*/
            MemberGroup::where('fk_id_group', $id)->delete();
            /*
                Format a vector to make multiple inserts to the table
                Of the different checkboxes selected from the matrix.
            */
            $i=0;
            $members_groups = null;
            foreach ($request->members as $campo => $dato) {
                $members_groups[$i] = [
                    'fk_id_member' => $dato,
                    'fk_id_group' => $id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $i++;
            }

            DB::table('members_groups')->insert($members_groups);

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('GroupController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('GroupController','actualizar',$e);
        }

        return redirect()->route('groups.index');
    }

    /**
     * Remove the specified Group from storage.
     *
     * @param  int  $id (id of the particular Group)
     * @return redirect view(index)
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
       if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
           return back();
       }

        try {
            DB::beginTransaction();

            $members = MemberGroup::select('members_groups.*', 'members.first_name', 'members.last_name', 'members.dni')
            ->join('members', 'members.id', 'members_groups.fk_id_member')
            ->where('fk_id_group', $id)
            ->get();

            if (count($members) > 0) {

                Log::critical('Ha ocurrido un error al intentar realizar el proceso: '. $metode . '  - El grupo seleccionado con miembros'.Auth::guard('user')->user()->user_name);
                flash('Error: El grupo seleccionado posee miembros y no se puede eliminar', '¡Error!')->error();
                return back();

            }else{
                Group::destroy($id);
            }

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('GroupController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('GroupController','eliminar',$e);
        }

        return redirect()->route('groups.index');
    }


    public function filter(FilterRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }
        Log::info('Ingreso exitoso a GroupController - filter(), del usuario: '.Auth::user()->user_name);

        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();
        /*
        *Consult users and club related data
        */
        $members = Member::select('members.*')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->where('members_departments.fk_id_department', $department)
        ->distinct()
        ->get();

        $departments = Department::select('departments.*')
        ->whereIn('departments.fk_id_club', $clubs)
        ->distinct()
        ->get();
        return redirect()->route('admin.groups.create')->with([
            'members' => $members,
            'departments' => $departments
        ]);

    }


}
