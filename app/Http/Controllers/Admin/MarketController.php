<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Market;
use  Meritop\Http\Requests\Admin\Market\CreateRequest;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class MarketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $market = Market::all();
        Log::info('Ingreso exitoso a MarketController - index(), del usuario: '.Auth::user()->user_name);
        return view('admin.markets.index')->with(['markets' => $market]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Log::info('Ingreso exitoso a MarketController - create(), del usuario: '.Auth::user()->user_name);
        return view('admin.markets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        //
        if(!empty($request)){
            try{
                Market::create($request->all());
                ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);
                DB::commit();
                ResourceFunctions::messageSuccess('MarketController','store');
            }catch (\Exception $e) {
                DB::rollBack();
                ResourceFunctions::messageError('MarketController','store',$e);
            }

            
        }
        Log::info('Ingreso exitoso a MarketController - index(), del usuario: '.Auth::user()->user_name);
        return redirect()->route('markets.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $market = Market::where('id', $id)->orderBy('name','description')->first();
        Log::info('Ingreso exitoso a MarketController - index(), del usuario: '.Auth::user()->user_name);
        return view('admin.markets.show', ['market' => $market]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $market = Market::where('id', $id)->orderBy('name','description')->first();
        Log::info('Ingreso exitoso a MarketController - index(), del usuario: '.Auth::user()->user_name);
        return view('admin.markets.edit', ['market' => $market]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateRequest $request, $id)
    {
        //
        try{
            $market = Market::FindOrFail($id)->update($request->all());
            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);
            DB::commit();
            ResourceFunctions::messageSuccess('MarketController','actualizar');

        }catch(\Exception $e){
            DB::rollBack();
            ResourceFunctions::messageError('MarketController','actualizar',$e);
        }
        Log::info('Ingreso exitoso a MarketController - update(), del usuario: '.Auth::user()->user_name);
        return redirect()->route('markets.show', ['market' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try{
            Market::where('id', $id)->delete();
            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);
            DB::commit();
            ResourceFunctions::messageSuccess('MarketController','eliminar');
        }catch(\Exception $e){
            DB::rollBack();
            ResourceFunctions::messageError('MarketController','eliminar',$e);
        }

        Log::info('Ingreso exitoso a MarketController - index(), del usuario: '.Auth::user()->user_name);
        return redirect()->route('markets.index');
    }
}
