<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\HistoricalImport;
use Excel;
use Illuminate\Support\Facades\Input;
use Meritop\Member;
use Meritop\Models\Objetive;
use Meritop\Models\Account;
use Meritop\Models\Club;
use Meritop\Models\AccountTransaction;
use Meritop\Models\Contribution;
use Meritop\Models\FailedRecord;
use Meritop\Http\Requests\Admin\Payload\PayloadImportRequest;
use Meritop\Http\Requests\Admin\Payload\FilterRequest;
use Illuminate\Support\Collection as Collection;


class PayloadController extends Controller
{
		public $module = 3;


		public function index()
		{
			/*
			*Method that allows the validation of privileges
			*/
			if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
					return redirect()->route('admin.dashboard');
			}

			Log::info('Ingreso exitoso a PayloadController - index(), del usuario: '.Auth::user()->user_name);
			/*
			*Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
			*/
			$clubs = ResourceFunctions::getClubs();

			$contributions = Contribution::select('contributions.*', 'users.first_name', 'users.last_name')
			->join('users', 'users.id', 'contributions.fk_id_user')
			->join('users_clubs', 'users_clubs.fk_id_user', 'users.id')
			->whereIn('users_clubs.fk_id_club', $clubs)
			->whereYear('contributions.created_at', date('Y'))
			->whereMonth('contributions.created_at', date('m'))
			->distinct()
			->get();
			
			$sum_points = $contributions->sum('total_points');

			return view('admin.payload.index')->with([
			'contributions' => $contributions,
			'sum_points' => $sum_points
			]);
		}




	public function filter(FilterRequest $request)
	{
			/*
			*Method that allows the validation of privileges
			*/
			if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
					return redirect()->route('admin.dashboard');
			}

			Log::info('Ingreso exitoso a AccountController - filter(), del usuario: '.Auth::user()->user_name);

			/**
			 * [$date description] Variable that contains the dates of the form with
			 * correct format
			 * @var [type] Date Array
			 */
			$date = ResourceFunctions::getDates($request->date);

			/**
			 * [$date description] Variable that contains the filter start date
			 * @var [type] Date
			 */
			$start_date = $date[0];

			/**
			 * [$due_date description] Variable that contains the filter due date
			 * @var [type] Date
			 */
			$due_date = $date[1];

			/**
			 * [$transactions description] Variable that contains all user's account
			 * transactions with the filters
			 * @var [type] Objetc Array
			 */
				/**
				* [$clubs description] Variable that contain all user's clubs login
				* @var [type] Integer Array
				*/

				 /*
			*Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
			*/
			$clubs = ResourceFunctions::getClubs();
			/*
			*consult the clubs related to the user
			*/
			$users_clubs = Club::select('clubs.*')
			->whereIn( 'clubs.id', $clubs)
			->get();

				 $contributions = Contribution::select('contributions.*', 'users.first_name', 'users.last_name')
				->join('users', 'users.id', 'contributions.fk_id_user')
				->join('users_clubs', 'users_clubs.fk_id_user', 'users.id')
				->whereIn('users_clubs.fk_id_club', $clubs)
				->whereBetween('contributions.created_at',  [$start_date, $due_date])
				->distinct()
				->get();

				return view('admin.payload.index')->with([
						'contributions' => $contributions,
						'users_clubs' => $users_clubs

				]);

	}


	public function store(PayloadImportRequest $request){

		


		/*
		*Method that allows the validation of privileges
		*/

		session()->forget('payload');//
		session()->forget('data');
		session()->forget('file_name');

		if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
				return redirect()->route('admin.dashboard');
		}
			/*
			*capture the file and its name
			*/
			$file = $request -> file('import_file');
			$file_name = $file ->getClientOriginalName();
			/*
			*arrays for saves the records
			*/
			$payload_registers= [];
			$failed_records= [];
			/*
			*consult the file format for the points contribution
			*/
			$club = session('clubs')->first();
			$club_format = Club::select('clubs.member_format_import')
			->where('id',$club->fk_id_club)
			->first();



			if ( ($file -> getClientOriginalExtension() == 'txt') && ($club_format->member_format_import == 1) ) {

				$file = fopen($request ->import_file, "r");
				$i=0;
				/*
				*while there is a row
				*/
				while(!feof($file)){
					/*
					*method that allows to capture each line of the file
					*/
					$linea = fgets($file,1000);
					/*
					*generate array via the separator "|" which is the one with the txt file
					*/
					$data [$i]= explode("|",trim($linea));

					$i++;
				}//while close
				/*
				*arrangement with the header that the file must have
				*/
				$validator = [
					0 =>0,
					1 =>3,
					2 =>25
				];

				$identification= 3;
				$id = "carnet";
				$points ="25";
				$objetive_code="0";
				/*
				*deletes the last record of the file, this record is blank
				*/
				$ultimo = end($data);
				if ($ultimo[0]=="") {
					array_pop($data);
				}

			}else if( ($file -> getClientOriginalExtension() != 'txt') && ($club_format->member_format_import == 1)){
				flash('Su configuracion solo le permite cargar archivos con extensiones txt', '¡Error!')->error();
				return back();
			}else if( ($file -> getClientOriginalExtension() == 'txt') && ($club_format->member_format_import == 0)){
				flash('Solo tiene permitido cargar archivos con extensiones xls, xlsx, cvs', '¡Error!')->error();
				return back();
			}else{
				$path = Input::file('import_file')->getRealPath();
				$data = Excel::load($path, function($reader) {
						$reader->formatDates(true, 'Y-m-d');
				})->get();
				$data = $data->toArray();
				// if( ResourceFunctions::validateSheetExcel($data) ){
				// flash('Para procesar el archivo debe tener solo una hoja', '¡Error!')->error();
				// return back();
				// }

				$validator = [
					0=>"dni",
					1=>"objetive_code",
					2=>"points"
				];
				$identification= "dni";
				$points ="points";
				$id = "dni";
				$objetive_code="objetive_code";
			}

				if(Input::hasFile('import_file')){

					/*
					*Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
					*/
					$clubs = ResourceFunctions::getClubs();
					/*
					*Consult users and club related data
					*/

					$historical_imports = HistoricalImport::select('historical_imports.file_name')
					->join('users', 'users.id', 'historical_imports.fk_id_user')
					->join('users_clubs', 'users_clubs.fk_id_user', 'users.id')
					->whereIn('users_clubs.fk_id_club', $clubs)
					->get()->toArray();

					if (     in_array($file_name, array_column($historical_imports, 'file_name'))  ) {
						flash('El archivo ya ha sido cargado anteriormente', '¡Error!')->error();
						return back();
					}

					if ( (sizeOf($data)==0)
						||  (empty($data) )
						|| (ResourceFunctions::validateHead($validator, $data)!=3)
						|| (ResourceFunctions::validateColumnNumeric($data,$identification))
						|| (ResourceFunctions::validateColumnVoid($data, $identification)==false)
						|| (ResourceFunctions::validateColumnNumeric($data,$points) )
						|| (ResourceFunctions::validateColumnVoid($data,$objetive_code)==false)
						|| (ResourceFunctions::validateColumnVoid($data,$points)==false)
					) {
						/**
						*Verify if the file is empty
						*/
						if ( (empty($data) ) || (sizeOf($data)==0) ) {
							 ResourceFunctions::messageErrorExportNull('payloadController','store',null);
						}
						/*
						*Validate that the dni column is just numbers
						*/
						if (ResourceFunctions::validateColumnNumeric(array_column($data, $identification), $identification )) {
							ResourceFunctions::messageErrorColumnNumeric('payloadController', $identification);
						}
						/*
						*Validate that the points column is not void
						*/
						if (ResourceFunctions::validateColumnVoid($data,"points")==false) {
							ResourceFunctions::messageErrorColumnNull('payloadController', "points");
						}
						/*
						*Validate that the points column is just numbers
						*/
						if (ResourceFunctions::validateColumnNumeric($data,$points)) {
							ResourceFunctions::messageErrorColumnNumeric('payloadController',"points");
						}
						/*
						*Validate that the dni column is not void
						*/
						if (ResourceFunctions::validateColumnVoid($data,$identification)==false) {
							ResourceFunctions::messageErrorColumnNull('payloadController', $identification);
						}
						/*
						*Validate that the objetive_code column is not void
						*/
						if (ResourceFunctions::validateColumnVoid($data,$objetive_code)==false) {
							ResourceFunctions::messageErrorColumnNull('payloadController', $objetive_code);
						}
						/*
						*Validate that the file header is correct
						*/
						if ( (ResourceFunctions::validateHead($validator, $data)!=3) ) {
							ResourceFunctions::messageStructureHeadboard('payloadController');
						}

						return back();

				}else{
						$registers=0;
						$no_registers=0;
						$no_code_status=0;
						$points_register=0;
						$points_no_code_status=0;
						$points_no_registers=0;
						$aux=false;
						/*
						*Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
						*/
						$clubs = ResourceFunctions::getClubs();
						/*
						*Allows consult the members According to a club
						*/
						$members = Member::select('members.*','members_clubs.carnet')
						->join('members_clubs','members_clubs.fk_id_member', 'members.id')
						->whereIn('members_clubs.fk_id_club',$clubs)
						->get()->toArray();
						/*
						*Allows consult the members related to the club id passed by parameter
						*/
						$objetives = Objetive::select('objetives.code','objetives.status')
						->join('programs','programs.id','objetives.fk_id_program')
						->where('programs.fk_id_club', session('clubs')->first()->fk_id_club)
						->get()->toArray();


						if (  (empty($objetives) ) || (sizeof($objetives)==0) ) {
							flash('Para cargar los puntos debe tener objetivos registrados en la base de datos', '¡Error!')->error();
							return back();
						}

						foreach ($data as $key => $valueData) {
							/*
							*Validate that the member is registered in the database
							*/
							if ( in_array($valueData[$identification], array_column($members, $id ) )) {
										$i=0;

										foreach ($objetives as $key => $value) {
												/*
												*Validate that the objetives is registered in the database and status is active
												*/
												if ( ( in_array($valueData[$objetive_code], array_column($objetives, 'code')) )  &&   ( $value['code']== $valueData[$objetive_code] )   ) {
													/*
													* Save records where the target code is incorrect
													*/
													if (($value['status']==1) ) {
														$payload_registers[] = [
														'dni'           => $valueData[$identification],
														'carnet'        => $valueData[$identification],
														'objetive_code' => $valueData[$objetive_code],
														'points'        => $valueData[$points],
														'status'        => 1
													];
													$points_register +=$valueData[$points];
													$registers++;
													break;
													}else{
														/*
														* Save records where the target code is incorrect
														*/
														$payload_registers[] = [
															'dni'           => $valueData[$identification],
															'carnet'        => $valueData[$identification],
															'objetive_code' => $valueData[$objetive_code],
															'points'        => $valueData[$points],
															'status'        => 2
														];

														$no_registers++;
														$points_no_registers += $valueData[$points];
														$aux = true;
														break;
													}

											    }
											     $i++;
										}//foreach close

							}else{
								/**
								* Save records where the user is incorrect
								*/
								$payload_registers[] = [
									'dni'           => $valueData[$identification],
									'carnet'        => $valueData[$identification],
									'objetive_code' => $valueData[$objetive_code],
									'points'        => $valueData[$points],
									'status'        => 0
								];
								$no_registers++;
								$points_no_registers += $valueData[$points];
								$aux = true;

							}

						}//forech close of the data

					$data_aporte[] = [

						"rejected_users" => $no_registers,
						"rejected_points" => $points_no_registers,
						"registers" => $registers,
						"processed_points" => $points_register,
						"total_points" => $points_no_registers + $points_register,
						"total_users" => $registers + $no_registers


					];

					$club = session('clubs')->first();
					$puntos = session(['payload' => $payload_registers, 'file_name' => $file_name, 'data' => $data_aporte]);

					$format_import = Club::select('clubs.member_format_import')
					->where('id',$club->fk_id_club)
					->first();
					return view('admin.payload.shows')->with([
									'contributions' 	=> $data_aporte,
									'payload'  	=> $payload_registers,
									'format_import'		=> $format_import,
									'file_name' => $file_name
								]);

				}
		}

}//cierre del store

public function InsertFile(Request $request){
	$status_payload = 0;
	$id_kind_account  = 1;
	$file_name = session('file_name');
	$data = session('data');
	if ($request['payload'] == 1) {
		if (!empty(session('payload'))) {
			$payload_registers = session('payload');
		}
	}
	try {
	 	if ( !((empty($payload_registers) ) || (sizeof($payload_registers)==0)) ) {

	 			foreach ($payload_registers as $key => $value) {

	 				if ($value['status'] == 1) {

	 					$status_payload = 1;
	 						/*
	 						*consult the dni or carnet of the members
	 						*/

	 					$club = session('clubs')->first();
						$club_format = Club::select('clubs.member_format_import')
						->where('id',$club->fk_id_club)
						->first();
	 					if ($club_format->member_format_import == 0 ) {

	 						$member = Member::select('members.id')
	 						->where( 'dni' , $value['dni'])
	 						->first();

	 					}else{

	 						$member = Member::select('members.id')
	 						->join('members_clubs','members_clubs.fk_id_member','members.id')
	 						->where( 'members_clubs.carnet' , $value['carnet'])
	 						->first();
	 						}

	 						/*
	 						*Query the data to create the description of the transaction
	 						*/
	 						$description = Objetive::select('objetives.name as objetive','objetives.description as objetive_description','programs.name as programs','programs.description as program_description')
	 						->where( 'objetives.code', $value['objetive_code'])
	 						->join('programs', 'programs.id', 'objetives.fk_id_program')
	 						->first();

	 						/*
	 						*consult the points of tha member account
	 						*/
	 						$points_account = Account::where( [ ['accounts.fk_id_kind_account' , 1] , ['accounts.fk_id_member' , $member->id] ] )
	 						->select('id','points','code')
	 						->first();

	 						/**
	 						* Allows to validate if the user has a cuena on the specified currency type
	 						* @var [type]
	 						*/
	 						if ($points_account) {
	 							/*
	 							*calculo de la cantidad de puntos a aportar
	 							*/
	 							$total_points = abs( $value['points'] + $points_account->points);
	 							/**
	 							* [update the points]
	 							*/
	 							Account::where('code', $points_account->code)
	 							->update([
	 							'points' => $total_points,
	 							'updated_at'       => date('Y-m-d H:i:s')
	 							]);
	 							/*
	 							*Records the transaction
	 							*/
	 							// dd($description->objetive, $points_account->points ,$total_points, $points_account->id, session('clubs')->first()->fk_id_club);
	 							$a = AccountTransaction::create([
	 								'description'   => $description->objetive,
	 								'operation'     => 1,
	 								'points'        => $value['points'],
	 								'balance'       => $total_points,
	 								'fk_id_account' => $points_account->id,
	 								'fk_id_club'    => session('clubs')->first()->fk_id_club
	 							]);

	 						}else{
	 							/*
	 							*function that allows create a new account
	 							*/
	 							// ResourceFunctions::createAccount(session('clubs')->first()->fk_id_club, $member->id, $id_kind_account, $value['points']);
	 							/*
	 							*
	 							*/
	 							$account = Account::where( [ ['accounts.fk_id_kind_account' , 1] , ['accounts.fk_id_member' , $member->id] ] )
	 							->select('id')
	 							->first();
	 							/*
	 							*Records the transaction
	 							*/
	 							AccountTransaction::create([
	 								'description'   => $description->objetive,
	 								'operation'     => 1,
	 								'points'        => $value['points'],
	 								'balance'       => $total_points,
	 								'fk_id_club'    => session('clubs')->first()->fk_id_club,
	 								'fk_id_account' => $account->id,
	 							]);

	 						}

	 					}

	 				}//forech close

	 	}//if close

	 	foreach ($data as $value) {

	 		HistoricalImport::create([
	 		'file_name'        => $file_name,
	 		'status'           => $status_payload,
	 		'ip_address'       => $request->ip(),
	 		'total_records'    => ($value['registers'] + $value['rejected_users'] ),
	 		'success_records'  => $value['registers'],
	 		'fail_records'     => ($value['rejected_users']),
	 		'kind'             => 'import_payload',
	 		'fk_id_user'       => Auth::user()->id,
	 		]);

		 	$contribution = Contribution::create([
		 		'total_users'      	=> ($value['registers'] + $value['rejected_users'] ),
		 		'processed_users'  	=> $value['registers'],
		 		'rejected_users'   	=> ( $value['rejected_users'] ),
		 		'total_points'    	=> ( $value['processed_points'] + $value['rejected_points'] ),
		 		'processed_points'  => $value['processed_points'],
		 		'rejected_points'   => ($value['rejected_points']),
		 		'ip_address'        => $request->ip(),
		 		'file_name'   		=> $file_name,
		 		'status'   			=> 1,
		 		'fk_id_user'        => Auth::user()->id,
		 		'fk_id_club'        => session('clubs')->first()->fk_id_club,
		 	]);
	 	}



	 	if ($contribution)  {

	 		$i=0;
	 		$data_insert = null;

	 		if ( !((empty($payload_registers) ) || (sizeof($payload_registers)==0)) ) {
	 			foreach ($payload_registers as $failed ) {
	 				if ($failed['status'] != 1) {

	 					if (empty($failed['dni'])) {

	 							$failed['dni'] = '0';
	 					}

	 					if (empty($failed['carnet'])) {
	 							$failed['carnet'] = 0;
	 					}

	 					$data_insert[$i] =([
	 						'dni'                   => $failed['dni'],
	 						'carnet'                => $failed['carnet'],
	 						'objetive_code'         => $failed['objetive_code'],
	 						'points'                => $failed['points'],
	 						'status'                => $failed['status'],
	 						'fk_id_contribution'	=> $contribution->id,
	 						'created_at'            => date('Y-m-d H:i:s'),
	 						'updated_at'            => date('Y-m-d H:i:s'),
	 					]);
	 				}
	 			$i++;
	 			}
	 		}

	 		if ( !((empty($data_insert) ) || (sizeof($data_insert)==0)) ) {
	 			DB::table('failed_records')->insert($data_insert);
	 		}

	 	}

	 session()->forget('payload');
	 DB::commit();
	}catch (Exception $e){
	 	DB::rollBack();
	 	ResourceFunctions::messageError('BrandController','store',$e);
	}catch (\Illuminate\Database\QueryException $e) {
	 	DB::rollBack();
	 	ResourceFunctions::messageError('BrandController','store',$e);
	}

	return redirect()->route('payload.show', $contribution->id);


}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
	/*
	*Method that allows the validation of privileges
	*/
	if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
			return redirect()->route('admin.index');
	}
	Log::info('Ingreso exitoso a PayloadController - show(), del usuario: '.Auth::user()->user_name);
	/*
	*consult the historical_imports
	*/
	$contributions = Contribution::select('contributions.*')
	->where([
			'contributions.id' => $id,
	])
	->first();
	/**
	*consult the historical_imports failed
	*/
	$failed_records = FailedRecord::select('failed_records.*')
	->where([
			'fk_id_contribution' => $id,
	])
	->get();
	/*
	*consult the file format for the points contribution
	*/
	$club = session('clubs')->first();
	$format_import = Club::select('clubs.member_format_import')
	->where('id',$club->fk_id_club)
	->first();

	return view('admin.payload.show')->with([
		'contributions' 	=> $contributions,
		'failed_records'  	=> $failed_records,
		'format_import'		=> $format_import,
	]);

}


	public function filter(FilterRequest $request)
	{
		/*
		*Method that allows the validation of privileges
		*/
		if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
				return redirect()->route('admin.dashboard');
		}

		Log::info('Ingreso exitoso a AccountController - filter(), del usuario: '.Auth::user()->user_name);

		/**
		 * [$date description] Variable that contains the dates of the form with
		 * correct format
		 * @var [type] Date Array
		 */
		$date = ResourceFunctions::getDates($request->date);

		/**
		 * [$date description] Variable that contains the filter start date
		 * @var [type] Date
		 */
		$start_date = $date[0];

		/**
		 * [$due_date description] Variable that contains the filter due date
		 * @var [type] Date
		 */
		$due_date = $date[1];

		/**
		 * [$transactions description] Variable that contains all user's account
		 * transactions with the filters
		 * @var [type] Objetc Array
		 */
			/**
			* [$clubs description] Variable that contain all user's clubs login
			* @var [type] Integer Array
			*/

			 $clubs = ResourceFunctions::getClubs();


			 $contributions = Contribution::select('contributions.*', 'users.first_name', 'users.last_name')
			->join('users', 'users.id', 'contributions.fk_id_user')
			->join('users_clubs', 'users_clubs.fk_id_user', 'users.id')
			->whereIn('users_clubs.fk_id_club', $clubs)
			->whereBetween('contributions.created_at',  [$start_date, $due_date])
			->distinct()
			->get();

			return view('admin.payload.index')->with([
					'contributions' => $contributions

			]);

	}

}//cierre de la clase

