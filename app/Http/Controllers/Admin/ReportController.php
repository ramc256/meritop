<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\UserClub;
use Meritop\Models\Club;
use Meritop\Models\Order;
use Meritop\Models\InventoryTransaction;
use Meritop\Http\Requests\Admin\Report\ExchangeUserFilter;
use Meritop\Http\Requests\Admin\Report\ExchangeFilter;
use Meritop\Member;
use Excel;

class ReportController extends Controller
{
    public function index()
    {
        return view('admin.reports.index');
    }

    public function inventoryOutput(Request $request)
    {
        $users_clubs = UserClub::getUserClubs(Auth::guard('user')->user()->id)->get();

        $inventory_report  = InventoryTransaction::inventoryOutputIndex(Auth::guard('user')->user()->id)->get();

        $total = $this->getTotal($inventory_report);

        return view('admin.reports.inventory-output')->with([
            'inventory_report' => $inventory_report,
            'total' => $total,
            'users_clubs' => $users_clubs
        ]);
    }

    public function inventoryOutputFilter(Request $request)
    {
        $fechas = ResourceFunctions::getDates($request->date);

        $fecha_inicio = $fechas[0];
        $fecha_fin = $fechas[1];

        $users_clubs = UserClub::getUserClubs(Auth::guard('user')->user()->id)->get();

        $inventory_report  = InventoryTransaction::inventoryOutputFilter($request->fk_id_club, $fecha_inicio, $fecha_fin)->get();

        $inventory_report_canceled = InventoryTransaction::inventoryOutputCQuery($request->fk_id_club, $fecha_inicio, $fecha_fin)->get();

        // dd($inventory_report, $inventory_report_canceled);

        $total = $this->getTotal($inventory_report);

        // $report = Order::select(DB::raw('products.sku, parents_products.name as product, parents_products.model, brands.name as brand, clubs.name as club, SUM(orders_products.quantity) as quantity'))
        // ->join('orders_products', 'orders_products.fk_id_order', 'orders.id')
        // ->join('products', 'products.id', 'orders_products.fk_id_product')
        // ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        // ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        // ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        // ->join('clubs', 'clubs.id', 'clubs_parents_products.fk_id_club')
        // ->whereNotIn('orders.status', [3,4])
        // ->whereBetween('orders.created_at', ['2017-10-01 00:00:00' , '2017-11-05 11:59:59'])
        // ->groupBy('products.sku', 'parents_products.name', 'parents_products.model', 'brands.name', 'clubs.name')
        // ->get();
        //  return Excel::create('Salida_de_Inventario', function($excel) use ($report) {
        //     $excel->sheet('mySheet', function($sheet) use ($report)
        //     {
        //         $sheet->fromArray($report);
        //
        //     });
        //
        //     })->export('csv');
        return view('admin.reports.inventory-output')->with([
            'inventory_report' => $inventory_report,
            'total' => $total,
            'users_clubs' => $users_clubs
        ]);
    }

    public function getTotal($inventory_report)

    {
        $total = 0;
        foreach ($inventory_report as $key) {
            $total = $total + $key->quantity;
        }

        return $total;
    }


    public function exchangeUser(){


        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();

        $users_clubs = Club::select('clubs.*')
        ->whereIn( 'clubs.id', $clubs)
        ->get();

       $exchanges_users = DB::table('orders')
        ->select(DB::raw(
            'orders.created_at, members.dni, members_clubs.carnet, members.first_name, members.last_name, clubs.name as club, branches.name as branch, cities.name as city,states.name as state, parents_products.name as product, products.feature, sum(orders_products.quantity) as quantity, sum(orders_products.quantity*orders_products.points) as points'))
        ->join('members','members.id', 'orders.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('branches','branches.id', 'orders.fk_id_branch')
        ->join('cities','cities.id', 'branches.fk_id_city')
        ->join('states','states.id', 'cities.fk_id_state')
        ->join('clubs','clubs.id', 'branches.fk_id_club')
        ->join('orders_products','orders_products.fk_id_order', 'orders.id')
        ->join('products','products.id', 'orders_products.fk_id_product')
        ->join('parents_products','parents_products.id', 'products.fk_id_parent_product')
        ->whereMonth('orders_products.created_at', date('m'))
        ->whereNotIn( 'orders.status', [3,4])
        ->groupBy('members.dni','members_clubs.carnet', 'members.first_name', 'members.last_name', 'clubs.name','branches.name','cities.name','states.name','parents_products.name','orders_products.points','products.feature','orders.created_at')
        ->orderBy('orders.created_at', 'DESC')
        ->get();

        $total_points   = 0;
        $total_quantity = 0;

        foreach ($exchanges_users as $key => $value) {
            $total_points   += $value->points;
            $total_quantity += $value->quantity;
        }

        return view('admin.reports.exchanges-users')->with([
            'exchanges_users'   => $exchanges_users,
            'users_clubs'       => $users_clubs,
            'total_points'      => $total_points,
            'total_quantity'    => $total_quantity
        ]);

    }

    public function exchangeItem(){

        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();

        $users_clubs = Club::select('clubs.*')
        ->whereIn( 'clubs.id', $clubs)
        ->get();

        $orders = DB::table('orders')
        ->select(DB::raw('orders.id, orders.created_at, parents_products.name as product, products.feature, parents_products.model, brands.name as brand, sum(orders_products.quantity) as quantity, sum(orders_products.quantity*orders_products.points) as points, members.dni, members_clubs.carnet, members.first_name,members.last_name, clubs.name as club, branches.name as branch, cities.name as city, states.name as state'))
        ->join('members','members.id', 'orders.fk_id_member')
        ->join('members_clubs','members_clubs.fk_id_member', 'members.id')
        ->join('branches','branches.id', 'orders.fk_id_branch')
        ->join('cities','cities.id', 'branches.fk_id_city')
        ->join('states','states.id', 'cities.fk_id_state')
        ->join('clubs','clubs.id', 'branches.fk_id_club')
        ->join('orders_products','orders_products.fk_id_order', 'orders.id')
        ->join('products','products.id', 'orders_products.fk_id_product')
        ->join('parents_products','parents_products.id', 'products.fk_id_parent_product')
        ->join('brands','brands.id', 'parents_products.fk_id_brand')
        ->whereMonth('orders.created_at', date('m'))
        ->whereNotIn( 'orders.status', [3,4])
        ->groupBy('brands.name','parents_products.model','orders.id','orders.created_at','members.dni','members_clubs.carnet', 'members.first_name', 'members.last_name', 'clubs.name','branches.name','cities.name','states.name','parents_products.name','orders_products.points','products.feature')
        ->get();

        $total_points   = 0;
        $total_quantity = 0;
        foreach ($orders as $key => $value) {
            $total_points   += $value->points;
            $total_quantity += $value->quantity;
        }

        return view('admin.reports.exchange-item')->with([
            'orders'            => $orders,
            'users_clubs'       => $users_clubs,
            'total_points'      => $total_points,
            'total_quantity'    => $total_quantity,
        ]);
    }





    public function exchangeFilter (ExchangeFilter $request){
        /**
         * [$date description] Variable that contains the dates of the form with
         * correct format
         * @var [type] Date Array
         */
        $date = ResourceFunctions::getDates($request->date);

        /**
         * [$date description] Variable that contains the filter start date
         * @var [type] Date
         */
        $start_date = $date[0];

        /**
         * [$due_date description] Variable that contains the filter due date
         * @var [type] Date
         */
        $due_date = $date[1];
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        if ($request->fk_id_club == null) {
            $clubs = ResourceFunctions::getClubs();
        }else {
            $clubs = [$request->fk_id_club];
        }
        $users_clubs = Club::select('clubs.*')
        ->whereIn( 'clubs.id', $clubs)
        ->get();

        $orders = DB::table('orders')
        ->select(DB::raw('orders.id, orders.created_at, parents_products.name as product,products.feature,parents_products.model, brands.name as brand, sum(orders_products.quantity) as quantity, sum(orders_products.quantity*orders_products.points) as points, members.dni, members_clubs.carnet, members.first_name,members.last_name, clubs.name as club, branches.name as branch, cities.name as city, states.name as state'))
        ->join('members','members.id', 'orders.fk_id_member')
        ->join('members_clubs','members_clubs.fk_id_member', 'members.id')
        ->join('branches','branches.id', 'orders.fk_id_branch')
        ->join('cities','cities.id', 'branches.fk_id_city')
        ->join('states','states.id', 'cities.fk_id_state')
        ->join('clubs','clubs.id', 'branches.fk_id_club')
        ->join('orders_products','orders_products.fk_id_order', 'orders.id')
        ->join('products','products.id', 'orders_products.fk_id_product')
        ->join('parents_products','parents_products.id', 'products.fk_id_parent_product')
        ->join('brands','brands.id', 'parents_products.fk_id_brand')
        ->whereBetween('orders.created_at', [$start_date, $due_date])
        ->whereNotIn( 'orders.status', [3,4])
        ->whereIn( 'branches.fk_id_club', $clubs)
        ->groupBy('brands.name','parents_products.model','orders.id','orders.created_at','members.dni','members_clubs.carnet', 'members.first_name', 'members.last_name', 'clubs.name','branches.name','cities.name','states.name','parents_products.name','orders_products.points','products.feature')
        ->get();

        $total_points   = 0;
        $total_quantity = 0;
        foreach ($orders as $key => $value) {
            $total_points   += $value->points;
            $total_quantity += $value->quantity;
        }

        return view('admin.reports.exchange-item')->with([
            'orders'            => $orders,
            'users_clubs'       => $users_clubs,
            'total_points'      => $total_points,
            'total_quantity'    => $total_quantity,
        ]);

    }


    public function exchangeUserFilter(ExchangeUserFilter $request){

        /**
         * [$date description] Variable that contains the dates of the form with
         * correct format
         * @var [type] Date Array
         */
        $date = ResourceFunctions::getDates($request->date);

        /**
         * [$date description] Variable that contains the filter start date
         * @var [type] Date
         */
        $start_date = $date[0];

        /**
         * [$due_date description] Variable that contains the filter due date
         * @var [type] Date
         */
        $due_date = $date[1];
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        if ($request->fk_id_club == null) {
            $clubs = ResourceFunctions::getClubs();
        }else {
            $clubs = [$request->fk_id_club];
        }
        $users_clubs = Club::select('clubs.*')
        ->whereIn( 'clubs.id', $clubs)
        ->get();

        $exchanges_users = DB::table('orders')
        ->select(DB::raw(
            'orders.created_at, members.dni, members_clubs.carnet, members.first_name, members.last_name, clubs.name as club, branches.name as branch, cities.name as city,states.name as state, parents_products.name as product, products.feature, sum(orders_products.quantity) as quantity, sum(orders_products.quantity*orders_products.points) as points'))
        ->join('members','members.id', 'orders.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('branches','branches.id', 'orders.fk_id_branch')
        ->join('cities','cities.id', 'branches.fk_id_city')
        ->join('states','states.id', 'cities.fk_id_state')
        ->join('clubs','clubs.id', 'branches.fk_id_club')
        ->join('orders_products','orders_products.fk_id_order', 'orders.id')
        ->join('products','products.id', 'orders_products.fk_id_product')
        ->join('parents_products','parents_products.id', 'products.fk_id_parent_product')
        ->whereBetween('orders_products.created_at', [$start_date, $due_date])
        ->whereIn('members_clubs.fk_id_club' , $clubs)
        ->whereNotIn( 'orders.status', [3,4])
        ->groupBy('members.dni','members_clubs.carnet', 'members.first_name', 'members.last_name', 'clubs.name','branches.name','cities.name','states.name','parents_products.name','orders_products.points','products.feature', 'orders.created_at')
        ->orderBy('orders.created_at', 'DESC')
        ->get();
        
        $total_points   = 0;
        $total_quantity = 0;

        foreach ($exchanges_users as $key => $value) {
            $total_points   += $value->points;
            $total_quantity += $value->quantity;
        }

        return view('admin.reports.exchanges-users')->with([
            'exchanges_users'   => $exchanges_users,
            'users_clubs'       => $users_clubs,
            'total_points'      => $total_points,
            'total_quantity'    => $total_quantity
        ]);

    }

    public function reportContributionAndExchangeFilter (Request $request){
        $clubs = ResourceFunctions::getClubs();

        $users_clubs = Club::select('clubs.*')
        ->whereIn( 'clubs.id', $clubs)
        ->get();
        /**
         * [$date description] Variable that contains the dates of the form with
         * correct format
         * @var [type] Date Array
         */
        $date = ResourceFunctions::getDates($request->date);

        /**
         * [$date description] Variable that contains the filter start date
         * @var [type] Date
         */
        $start_date = $date[0];
        $due_date = $date[1];

        /**
         * [$due_date description] Variable that contains the filter due date
         * @var [type] Date
         */
        // $due_date = $date[1];
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */

        $members = Member::select('members.dni','members.first_name','members.last_name','members_clubs.carnet','clubs.name','accounts.code')
                ->join('accounts','accounts.fk_id_member','members.id' )
                ->join('members_clubs','members_clubs.fk_id_member','members.id')
                ->join('clubs', 'members_clubs.fk_id_club', 'clubs.id')
                ->where('clubs.id',2)
                ->get();

        $canjes = DB::table('members')
                ->select(DB::raw('members.dni, SUM(accounts_transactions.points) as total'))
                ->join('accounts','accounts.fk_id_member','members.id' )
                ->join('members_clubs','members_clubs.fk_id_member','members.id')
                ->join('clubs', 'members_clubs.fk_id_club', 'clubs.id')
                ->join('accounts_transactions', 'accounts_transactions.fk_id_account','accounts.id')
                ->where('accounts_transactions.operation', 0)
                ->whereBetween('accounts_transactions.created_at', [$start_date ,$due_date])
                ->groupBy('members.dni')
                ->get()->toArray();

        $aporte = DB::table('members')
                ->select(DB::raw('members.dni,SUM(accounts_transactions.points) as total'))
                ->join('accounts','accounts.fk_id_member','members.id' )
                ->join('members_clubs','members_clubs.fk_id_member','members.id')
                ->join('clubs', 'members_clubs.fk_id_club', 'clubs.id')
                ->join('accounts_transactions', 'accounts_transactions.fk_id_account','accounts.id')
                ->where('accounts_transactions.operation', 1)
                ->whereBetween('accounts_transactions.created_at', [$start_date ,$due_date])
                ->groupBy('members.dni')
                ->get()->toArray();

        $total_exchange = 0;
        $total_contribution = 0;
        foreach ($members as  $value) {
            $user_canjes = 0;
            $user_aporte = 0;
            if (in_array($value->dni, array_column($canjes, 'dni'))) {
                foreach ($canjes as $key => $val) {
                    if ($val->dni == $value->dni) {
                        $user_canjes = $val->total;
                         break;
                    }

                }

            }
            if (in_array($value->dni, array_column($aporte, 'dni'))) {
                foreach ($aporte as $key => $val) {
                    if ($val->dni == $value->dni) {
                        $user_aporte = $val->total;
                         break;
                    }

                }

            }
            $total_exchange += $user_canjes;
            $total_contribution += $user_aporte;

            $data_aporte [] = [

                'first_name' => $value->first_name,
                'last_name' => $value->last_name,
                'dni' => $value->dni,
                'carnet' => $value->carnet,
                'club'  => $value->name,
                'n_cuenta' => $value->code,
                'exchange' => $user_canjes,
                'contribution' => $user_aporte
            ];

        }

        return view('admin.reports.contribution-exchange')->with([ 'data_aporte' => $data_aporte,
                                                                   'users_clubs' => $users_clubs,
                                                                   'total_exchange' => $total_exchange,
                                                                   'total_contribution' => $total_contribution ]);
    }

    public function reportContributionAndExchange(){

        $clubs = ResourceFunctions::getClubs();

        $users_clubs = Club::select('clubs.*')
        ->whereIn( 'clubs.id', $clubs)
        ->get();

        $members = Member::select('members.dni','members.first_name','members.last_name','members_clubs.carnet','clubs.name','accounts.code')
                ->join('accounts','accounts.fk_id_member','members.id' )
                ->join('members_clubs','members_clubs.fk_id_member','members.id')
                ->join('clubs', 'members_clubs.fk_id_club', 'clubs.id')
                ->where('clubs.id',2)
                ->get();

        $canjes = DB::table('members')
                ->select(DB::raw('members.dni, SUM(accounts_transactions.points) as total'))
                ->join('accounts','accounts.fk_id_member','members.id' )
                ->join('members_clubs','members_clubs.fk_id_member','members.id')
                ->join('clubs', 'members_clubs.fk_id_club', 'clubs.id')
                ->join('accounts_transactions', 'accounts_transactions.fk_id_account','accounts.id')
                ->where('accounts_transactions.operation', 0)
                ->whereMonth('accounts_transactions.created_at', date('m'))
                ->groupBy('members.dni')
                ->get()->toArray();

        $aporte = DB::table('members')
                ->select(DB::raw('members.dni,SUM(accounts_transactions.points) as total'))
                ->join('accounts','accounts.fk_id_member','members.id' )
                ->join('members_clubs','members_clubs.fk_id_member','members.id')
                ->join('clubs', 'members_clubs.fk_id_club', 'clubs.id')
                ->join('accounts_transactions', 'accounts_transactions.fk_id_account','accounts.id')
                ->where('accounts_transactions.operation', 1)
                ->whereMonth('accounts_transactions.created_at', date('m'))
                ->groupBy('members.dni')
                ->get()->toArray();

        $total_exchange = 0;
        $total_contribution = 0;
        foreach ($members as  $value) {
            $user_canjes = 0;
            $user_aporte = 0;
            if (in_array($value->dni, array_column($canjes, 'dni'))) {
                foreach ($canjes as $key => $val) {
                    if ($val->dni == $value->dni) {
                        $user_canjes = $val->total;
                         break;
                    }

                }

            }
            if (in_array($value->dni, array_column($aporte, 'dni'))) {
                foreach ($aporte as $key => $val) {
                    if ($val->dni == $value->dni) {
                        $user_aporte = $val->total;
                         break;
                    }

                }

            }

            $total_exchange += $user_canjes;
            $total_contribution += $user_aporte;

            $data_aporte [] = [

                'first_name' => $value->first_name,
                'last_name' => $value->last_name,
                'dni' => $value->dni,
                'carnet' => $value->carnet,
                'club'  => $value->name,
                'n_cuenta' => $value->code,
                'exchange' => $user_canjes,
                'contribution' => $user_aporte
            ];

        }

        return view('admin.reports.contribution-exchange')->with([ 'data_aporte' => $data_aporte,
                                                                   'users_clubs' => $users_clubs,
                                                                   'total_exchange' => $total_exchange,
                                                                   'total_contribution' => $total_contribution ]);
    }



}
