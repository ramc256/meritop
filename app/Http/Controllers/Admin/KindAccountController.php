<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\KindAccount;
use Meritop\Models\Account;

class KindAccountController extends Controller
{
    public $module = 19;


    /**
     * Display a listing of the kind account.
     *
     * @return the view(index)
     */
    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a KindAccountController - index(), del usuario: '.Auth::user()->user_name);

        $kinds_accounts = KindAccount::all();

        return view('admin/kind_account/index')->with([
            'kinds_accounts' => $kinds_accounts
        ]);
    }

    /**
     * Show the form for creating a new kind account.
     *
     * @return the view(create)
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.kind_account.index');
        }

        Log::info('Ingreso exitoso a KindAccountController - create(), del usuario: '.Auth::user()->user_name);

        return view('admin/kind_account/create');
    }

    /**
     * Store a newly created kind account in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @return redirect view(index)
     */
    public function store(Request $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            KindAccount::create($request -> all());

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('KindAccountController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('KindAccountController','store',$e);
        }

        return redirect()->route('kind_account.index');
    }

    /**
     * Display the specified kind account.
     *
     * @param  int  $id - (id of the particular kind account)
     * @return the view(show)
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a KindAccountController - show(), del usuario: '.Auth::guard('user')->user()->user_name);

        $kind_account = KindAccount::FindOrFail($id);

        return view('admin/kind_account/show')->with(['$kind_account' => $kind_account]);
    }

    /**
     * Show the form for editing the specified kind account.
     *
     * @param  int  $id (id of the particular kind account)
     * @return the view(edit)
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a KindAccountController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);

        $kind_account = KindAccount::FindOrFail($id);

        return view('admin/kind_account/edit')->with(['$kind_account' => $kind_account]);
    }

    /**
     * Update the specified kind account in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @param  int  $id (id of the particular kind account)
     * @return redirect view(index)
     */
    public function update(Request $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            KindAccount::where('id', $id)
            ->update([
                    'name' => $request->name,
                    'description' => $request->description
                    ]);

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('KindAccountController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('KindAccountController','actualizar',$e);
        }

        return redirect()->route('kind_account.index');
    }

    /**
     * Remove the specified kind account from storage.
     *
     * @param  int  $id (id of the particular kind account)
     * @return redirect view(index)
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }
        try {
            DB::beginTransaction();

            $accounts = Account::where('fk_id_kind_account', $id)->get();

            if (count($accounts) > 0) {
                KindAccount::destroy($id);

                ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

                DB::commit();

                ResourceFunctions::messageSuccess('KindAccountController','eliminar');
            }else {
                flash('No se puede eliminar el tipo de cuenta ya que ya existen cuentas de este tipo', '¡Error!')->error();
                Log::error('Eror en KindAccountController - destroy(), no se pueden eliminar este tipo de cuenta ya que ya ha sido asociada - del usuario: '.Auth::guard('user')->user()->user_name);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('KindAccountController','eliminar',$e);
        }

        return redirect()->route('kind_account.index');
    }
}
