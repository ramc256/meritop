<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Product;
use Meritop\Models\ParentProduct;
use Meritop\Models\Inventory;
use Meritop\Models\InventoryTransaction;
use Meritop\Models\ClubParentProduct;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Inventory\InventoryRequest;
use Storage;


class InventoryController extends Controller
{
    public $module = 9;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
       if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
       }

       Log::info('Ingreso exitoso a InventoryController - index(), del usuario: '.Auth::user()->user_name);

       $parents_products = ParentProduct::select('parents_products.*', 'brands.name as brand')
       ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
       ->where('deleted', false)
       ->get();

        foreach ($parents_products as $key) {
            $t = Inventory::select('inventory.*')
            ->join('products', 'products.id', 'inventory.fk_id_product')
            ->where('products.fk_id_parent_product', $key->id)
            ->get();

            $key['quantity_current'] = $t->sum('quantity_current');
        }

        return view('admin.inventory.index')->with(['parents_products' => $parents_products]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
                return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a InventoryController - show(), del usuario: '.Auth::user()->user_name);

        $parent_product = ParentProduct::select('parents_products.*', 'brands.name as brand', 'subcategories.id as id_subcategory',
        'subcategories.name as subcategory', 'categories.id as id_category', 'categories.name as category', 'clubs.id as id_club',
        'clubs.alias as club', 'markets.name as market')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('clubs', 'clubs.id', 'clubs_parents_products.fk_id_club')
        ->join('markets', 'markets.id', 'parents_products.fk_id_market')
        ->where([
            'parents_products.id' => $id,
            'parents_products.deleted' => false
        ])
        ->orderBy('club','asc')
        ->first();

        $product = Product::where('fk_id_parent_product', $id)
        ->first();

        $products = Product::select('products.*', 'inventory.quantity_current')
        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->where([
            'products.fk_id_parent_product' => $id,
            'products.deleted' => false
        ])
        ->get();

        $clubs_parents_products = ClubParentProduct::where('fk_id_prouct', $id);

        return view('admin/inventory/show')->with([
            'parent_product' => $parent_product,
            'products' => $products,
            'product' => $product,
            'clubs_parents_products' => $clubs_parents_products
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InventoryRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.inventory.index');
        }

        try {
            DB::beginTransaction();

            $quantities_currents = Inventory::whereIn('fk_id_product',  $request -> products)->get();

            for ($i=0; $i < count($request -> quantities) ; $i++) {

                if (($request -> quantities[$i] + $quantities_currents[$i]['quantity_current']) >= 0) {
                    Inventory::where('fk_id_product',$request -> products[$i])
                    ->update([
                            'quantity_current' => ($request -> quantities[$i] + $quantities_currents[$i]['quantity_current'])
                            ]);

                    InventoryTransaction::create([
                        'quantity_update' => $request -> quantities[$i],
                        'description' => 'Ingreso a inventario',
                        'fk_id_inventory' => $quantities_currents[$i]['id']
                    ]);

                    ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request -> products[$i]);
                }
            }

            DB::commit();

            ResourceFunctions::messageSuccess('InventoryController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();

            ResourceFunctions::messageError('InventoryController','actualizar',$e);
        }

        return redirect()->route('inventory.index');
    }

}
