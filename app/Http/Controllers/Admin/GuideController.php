<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\Guide;
use Meritop\Models\Order;
use Meritop\Models\Media;
use Meritop\Models\OrderProduct;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Mail;
use Meritop\Mail\SentStatusOrder;

class GuideController extends Controller
{
	public $module=11;
   /**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(Request $request, $id)
		{
			/*
			*Method that allows the validation of privileges
			*/
			if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
					return redirect()->route('users.index');
			}
			// dd($request->all());
			try{
				DB::beginTransaction();
				/*
				*Code that allows you to join a new field to the request and then generate a new record
				*/
				$request -> request -> add(['fk_id_order' => $id]);
				$guide = Guide::create($request -> all());

				/*
				*Updates the status of the order to be sent
				*/
				$order = Order::where('id',$id)
				->update([
					'status'        => 1,
					'updated_at'	=> date('Y-m-d H:i:s')
				]);

				if( !empty($order) && !empty($guide) ){
					/*
					*Check the orders, and the data related to it
					*/
					
					$data_order = Order::where( 'orders.id', $id)
					->select('orders.*', 'members.email','members.first_name','members.last_name','guides.num_guide','agents.name as agent')
					->join('members','members.id', 'orders.fk_id_member')
					->join('guides','guides.fk_id_order', 'orders.id')
					->join('agents','agents.id', 'guides.fk_id_agent')
					// ->join('members_clubs','members_clubs.fk_id_member','members.id')
					->first();

					/*
					*Relates the number of variants to each order
					*/
					
					$items = OrderProduct::select('products.*','parents_products.name','orders_products.quantity','orders.discount')
					->join('products', 'products.id', 'orders_products.fk_id_product')
					->join('orders', 'orders.id', 'orders_products.fk_id_order')
					->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
					->where('orders_products.fk_id_order', $id)
					->get();
					/*
		            *consult the image related club
		            */
		            $logo_image = Media::where([
		                'identificator' => session('clubs')->first()->fk_id_club,
		                'table'         => 'clubs'
		            ])
		            ->first();

		            $logo = $_ENV['STORAGE_PATH'].$logo_image->image;
					/*
					*Code that allows the sending of emails
					*/
					Mail::to($data_order->email)->send(new SentStatusOrder($data_order, $items, $logo));
					Mail::to("order@meritop.com")->send(new SentStatusOrder($data_order, $items, $logo));
				}
				// .Auth::user()->user_name
				$r = ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro de una orden: ');
				// dd($r);

				DB::commit();

				ResourceFunctions::messageSuccess('OrderController','store');

			} catch (\Exception $e) {
					DB::rollBack();
					ResourceFunctions::messageError('OrderController','store',$e);
			}

			return redirect()->route('orders.show', $id);

		}

}
