<?php

namespace Meritop\Http\Controllers\Admin;

use Meritop\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Meritop\Models\HistoricalLogin;
use Meritop\Models\BannedMember;
use Meritop\Models\RoleModuleAction;
use Meritop\Models\UserClub;
use Meritop\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:user');
    }

    protected function broker()
    {
      return Password::broker('users');
    }

    protected function guard()
    {
      return Auth::guard('user');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    { 
        return view('admin.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {   
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();

        if ($user) {
            /**
             * Comprobation if the user that try to login is blocked
             * @var boolean
             */

            $encontrado = false;
            $banned_members = BannedMember::all();
            foreach ($banned_members as $ip_address_blocked) {
                        if ($ip_address_blocked->ip_address == \Request::ip()) {
                                $encontrado = true;
                        }
            }
            // dd(\Request::ip());
            if (!$encontrado) {
                Log::info('Ingreso exitoso a ResetPasswordControllerAdmin , del usuario: '.$user->user_name);

                /**
                 * Store a newly created historical of ip address by user in storage.
                 */
                $w=HistoricalLogin::create([
                        'ip_address'    => \Request::ip(),
                        'user_name'     => $user->user_name,
                ]);

                /*
                *Check the permissions that the user has
                */
                
                $role = RoleModuleAction::where('fk_id_role',$user->fk_id_role)
                ->select('*')
                ->get();

                /*
                *Consult the clubs related to the user
                */
                $clubs = UserClub::select('fk_id_club')
                ->where('fk_id_user', $user->id)
                ->get();

                /*
                *Saves in the session variable an arrangement with the data consulted, role and clubs related to the user
                */
                session(['role' => $role, 'clubs'=>$clubs]);

                $this->guard()->login($user);
            }else{

                 return back();
            }

        }

        
    }



  }
