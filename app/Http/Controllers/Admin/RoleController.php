<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Role;
use Meritop\Models\Module;
use Meritop\Models\Action;
use Meritop\Models\RoleModuleAction;
use Meritop\Models\KindRole;
use Meritop\User;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Role\RoleCreateRequest;
use Meritop\Http\Requests\Admin\Role\RoleUpdateRequest;

class RoleController extends Controller
{
    public $module = 18;
    /**
     * Display a listing of the roles.
     *
     * @return the view(index)
     */


    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a RoleController - index(), del usuario: '.Auth::guard('user')->user()->user_name);

        $roles = Role::where('id', '>', 1)->get();

        return view('admin.roles.index')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new role.
     *
     * @return the view(create)
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.roles.index');
        }

        Log::info('Ingreso exitoso a RoleController - create(), del usuario: '.Auth::user()->user_name);

        $modules = Module::orderBy('order')->get();
        $actions = Action::all();
        $kind_roles = KindRole::all();

        return view('admin.roles.create')->with(['modules' => $modules, 'actions' => $actions, 'kind_roles' => $kind_roles]);
    }

    /**
     * Store a newly created role in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @return redirect view(index)
     */
    public function store(RoleCreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            $role = Role::create($request->all());

            /*
            Format a vector to make multiple inserts to the table
            Of the different checkboxes selected from the matrix
             */
            $i=0;
            $data_insert = null;
            foreach ($request->privileges as $campo ) {
                /*
                    It filter the parameters from the view ignoring
                        The tocken of the form.
                 */
                $campo = explode('/', $campo);
                if (is_numeric($campo[0])) {
                    $data_insert[$i] = [
                        'fk_id_role' => $role->id,
                        'fk_id_module' => $campo[0],
                        'fk_id_action' => $campo[1],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                }
                $i++;
            }

            DB::table('roles_modules_actions')->insert($data_insert);

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('RoleController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('RoleController','store',$e);
        }

        return redirect()->route('roles.index');
    }

    /**
     * Display the specified role.
     *
     * @param  int  $id - (id of the particular role)
     * @return the view(show)
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a RoleController - show(), del usuario: '.Auth::user()->user_name);

        $modules = Module::orderBy('order')->get();
        $actions = Action::all();
        $role = Role::select('roles.*', 'kind_roles.id as id_kind_role', 'kind_roles.name as kind')
        ->join('kind_roles', 'kind_roles.id', 'roles.fk_id_kind_role')
        ->where('roles.id', $id)
        ->first();
        $rma = RoleModuleAction::select('roles_modules_actions.*')
        ->join('modules', 'modules.id', 'roles_modules_actions.fk_id_module')
        ->where('fk_id_role', $id)
        ->orderBy('order')
        ->orderBy('fk_id_action')
        ->get()
        ->toArray();

        return view('admin.roles.show')->with(['modules' => $modules, 'actions' => $actions, 'role' => $role, 'rma' => $rma]);
    }

    /**
     * Show the form for editing the specified role.
     *
     * @param  int  $id (id of the particular role)
     * @return the view(edit)
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a RoleController - edit(), del usuario: '.Auth::user()->user_name);

        $modules = Module::orderBy('order')->get();
        $actions = Action::all();
        $role = Role::FindOrFail($id);
        $rma = RoleModuleAction::select('roles_modules_actions.*', 'modules.order')
        ->join('modules', 'modules.id', 'roles_modules_actions.fk_id_module')
        ->where('fk_id_role', $id)
        ->orderBy('order')
        ->orderBy('fk_id_action')
        ->get()
        ->toArray();
        $kind_roles = KindRole::all();

        return view('admin.roles.edit')->with([
            'modules' => $modules,
            'actions' => $actions,
            'role' => $role,
            'rma' => $rma,
            'kind_roles' => $kind_roles
        ]);
    }

    /**
     * Update the specified role in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @param  int  $id (id of the particular role)
     * @return redirect view(index)
     */
    public function update(RoleUpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {

            DB::beginTransaction();

            Role::FindOrFail($id)
                ->update($request->all());

            $rma = RoleModuleAction::where('fk_id_role', $id)->delete();

            $i=0;
            $data_insert = null;
            foreach ($request->privileges as $campo) {
                $campo = explode('/', $campo);
                if (is_numeric($campo[0])) {
                    $data_insert[$i] = [
                        'fk_id_role' => $id,
                        'fk_id_module' => $campo[0],
                        'fk_id_action' => $campo[1],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                }
                $i++;
            }

            DB::table('roles_modules_actions')->insert($data_insert);

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('RoleController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('RoleController','actualizar',$e);
        }

     return redirect()->route('roles.index');
    }

    /**
     * Remove the specified role from storage.
     *
     * @param  int  $id (id of the particular role)
     * @return redirect view(index)
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            $users_roles = User::where('fk_id_role', $id)->get();

            if ( count($users_roles) < 1 ) {

                RoleModuleAction::where('fk_id_role', $id)->delete();

                Role::destroy($id);

                ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

                DB::commit();

                ResourceFunctions::messageSuccess('RoleController','eliminar');
            }else {
                flash('No es posible eliminar el rol ya que se encuentra asignado a un(os) usuario(s)')->error();
            }

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('RoleController','eliminar',$e);
        }

        return redirect()->route('roles.index');
    }

}
