<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Models\Program;
use Meritop\Models\Club;
use Meritop\Models\Group;
use Meritop\Models\GroupProgram;
use Meritop\Models\Objetive;
use Meritop\Models\Media;
use Meritop\Models\Perspective;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Program\ProgramCreateRequest;
use Meritop\Http\Requests\Admin\Program\ProgramUpdateRequest;

class ProgramController extends Controller
{
    public $module = 6;

     public function __construct()
    {
       $this->middleware('what_kind', ['except' => ['index','show']]);
    }

    /**
     * Display a listing of the Programs.
     *
     *  @return the view(index)
     */
    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a ProgramController - index(), del usuario: '.Auth::guard('user')->user()->user_name);

        $clubs = ResourceFunctions::getClubs();

        $programs = Program::where('deleted', false)
        ->whereIn('fk_id_club', $clubs)
        ->get();

        $groups_programs = GroupProgram::select('groups_programs.*', 'groups.id', 'groups.name')
        ->join('groups', 'groups.id', 'groups_programs.fk_id_group')
        ->join('programs', 'programs.id', 'groups_programs.fk_id_program')
        ->distinct()
        ->get();

        $programs = ResourceFunctions::getGroups($programs, $groups_programs, 'fk_id_program');

        return view('admin/programs/index')->with(['programs' => $programs]);
    }

    /**
     * Show the form for creating a new Programs.
     *
     * @return the view(create)
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.programs.index');
        }

        Log::info('Ingreso exitoso a ProgramController - create(), del usuario: '.Auth::user()->user_name);
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $club = session('clubs')->first();

        $groups = Group::select('groups.*')
        ->join('members_groups', 'members_groups.fk_id_group', 'groups.id')
        ->join('members', 'members.id', 'members_groups.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->where('members_clubs.fk_id_club', $club->fk_id_club)
        ->distinct()
        ->get();

        $perspectives = Perspective::all();

        return view('admin/programs/create')->with(['groups' => $groups, 'perspectives' => $perspectives]);
    }

    /**
     * Store a newly created Programs in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @return redirect view(index)
     */
    public function store(ProgramCreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            /*------------------------------------Create a new Program------------------------------------*/

            $request -> request -> add(['fk_id_club' => session('clubs')->first()->fk_id_club]);
            $request -> request -> add(['status' => '0']);

             //This get the unique club for the user of kind Club
            Program::create($request ->  all());
            $id_program = Program::all()->last()->id;
            // dd($request->all());

            /**
             * [Store the every images of the product]
             * @var [file]
             */
            $file = $request->file('image');

            if (!empty($file)) {
                ResourceFunctions::imageChange($id_program, null, $file, 'programs');
            }                         //This do multiple inserts in the specificed table


            /*------------------------------------Create a new Objetive------------------------------------*/
            /**
             * Create a new Objetive
             * @var integer
             */
            if ($id_program) {

                $request -> merge(['name' => $request->objetive_name]);
                $request -> merge(['description' => $request->objetive_description]);
                $request -> request -> add(['fk_id_program' => $id_program ]);
                $objetive = Objetive::create($request -> all());

            }

            /*------------------------------------Create a new Groups_Programs------------------------------------*/
            /**
             * Create a new register in groups_programs table
             * @var integer
             */
            $i=0;
            $groups_programs = null;
            foreach ($request->all() as $campo => $dato) {

                $encontrado = strpos($campo, 'fk_id_group');                       //Search the chain 'fk_id_group' into $campo and return true or false

                if ($encontrado !== false) {
                    for ($i=0; $i < count($dato); $i++) {
                        $groups_programs[$i] = [
                            $campo => $dato[$i],
                            'fk_id_program'  => $id_program,
                            'created_at'    => date('Y-m-d H:i:s'),
                            'updated_at'    => date('Y-m-d H:i:s')
                        ];
                    }
                }
             }

            DB::table('groups_programs')->insert($groups_programs);

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ProgramController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ProgramController','store',$e);
        }

        return redirect()->route('programs.index');
    }

    /**
     * Display the specified Programs.
     *
     * @param  int  $id - (id of the particular Programs)
     * @return the view(show)
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a ProgramController - show(), del usuario: '.Auth::user()->user_name);

        $program = Program::select('programs.*',  'clubs.id as id_club', 'clubs.name as Club', 'media.image')
        ->join('clubs', 'clubs.id', '=', 'programs.fk_id_Club')
        ->join('media', 'media.identificator', 'programs.id')
        ->where([
            'programs.id' => $id,
            'media.table' => 'programs'
        ])
        ->first();

        $groups_programs = GroupProgram::select('groups_programs.*', 'groups.id', 'groups.name')
        ->join('groups', 'groups.id', 'groups_programs.fk_id_group')
        ->where('fk_id_program', $id)
        ->get();

        $objetives = Objetive::select('objetives.*', 'perspectives.name as perspective')
        ->join('perspectives', 'perspectives.id', 'objetives.fk_id_perspective')
        ->where(['fk_id_program' => $id, 'objetives.deleted' => false])
        ->get();

        return view('admin/programs/show')->with([
            'program'           => $program,
            'groups_programs'   => $groups_programs,
            'objetives'         => $objetives]);
    }

    /**
     * Show the form for editing the specified Programs.
     *
     * @param  int  $id (id of the particular Programs)
     * @return the view(edit)
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a ProgramController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);

        $club = session('clubs')->first();

        $program = Program::select('programs.*',  'clubs.id as id_club', 'clubs.name as Club', 'media.image')
        ->join('clubs', 'clubs.id', '=', 'programs.fk_id_Club')
        ->join('media', 'media.identificator', 'programs.id')
        ->where([
            'programs.id' => $id,
            'media.table' => 'programs'
        ])
        ->first();

        $groups = Group::select('groups.*')
        ->join('members_groups', 'members_groups.fk_id_group', 'groups.id')
        ->join('members', 'members.id', 'members_groups.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->where('members_clubs.fk_id_club', $club->fk_id_club)
        ->distinct()
        ->get();

        $groups_programs = GroupProgram::select('groups_programs.*', 'groups.id', 'groups.name')
        ->join('groups', 'groups.id', 'groups_programs.fk_id_group')
        ->where('fk_id_program', $id)
        ->get();

        $clubs = Club::all();

        return view('admin/programs/edit')->with([
            'program'           => $program,
            'groups_programs'   => $groups_programs,
            'clubs'             => $clubs,
            'groups'            => $groups
        ]);
    }

    /**
     * Update the specified Programs in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @param  int  $id (id of the particular Programs)
     * @return redirect view(index)
     */
    public function update(ProgramUpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            $fk_id_program = 'fk_id_program';

            /*------------------------------------Create a new Program------------------------------------*/
            /**
             * Create a new Program and catch the record id
             * @var [$id_program]
             */
            Program::where('id', $id)
            ->update([
                'name' => $request -> name,
                'description' => $request -> description,
                'status' => $request -> status,
            ]);

            /*------------------------------------Create a new Groups_Programs------------------------------------*/
            /**
             * Create a new register in groups_programs table
             * @var integer
             */
            GroupProgram::where($fk_id_program, $id)->delete();

            $i=0;
            $groups_programs = null;
            foreach ($request->all() as $campo => $dato) {

                $encontrado = strpos($campo, 'fk_id_group');                       //Search the chain 'fk_id_group' into $campo and return true or false

                if ($encontrado !== false) {
                    for ($i=0; $i < count($dato); $i++) {
                        $groups_programs[$i] = [
                            $campo => $dato[$i],
                            $fk_id_program => $id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ];
                    }
                }
             }

            DB::table('groups_programs')->insert($groups_programs);

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ProgramController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ProgramController','store',$e);
        }

        return redirect()->route('programs.show', $id);
    }

    /**
     * Remove the specified Programs from storage.
     *
     * @param  int  $id (id of the particular Programs)
     * @return redirect view(index)
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            Program::where('id',$id)
            ->update([
                'status' => false,
                'deleted' => true
            ]);

            Objetive::where('fk_id_program', $id)
            ->update([
                'status' => false,
                'deleted' => true
            ]);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('ProgramController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ProgramController','eliminar',$e);
        }

        return redirect()->route('programs.index');
    }

    public function filter()
    {
        // // Method that allows the validation of privileges
        // if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
        //     return redirect()->route('admin.dashboard');
        // }
        //
        // Log::info('Ingreso exitoso a ProgramController - index(), del usuario: '.Auth::guard('user')->user()->user_name);
        //
        // $programs = Program::all();
        //
        // $groups_programs = GroupProgram::select('groups_programs.*', 'groups.id', 'groups.name')
        // ->join('groups', 'groups.id', 'groups_programs.fk_id_group')
        // ->get();
        //
        // $programs = ResourceFunctions::getGroups($programs, $groups_programs, 'fk_id_program');
        //
        // return view('admin/programs/index')->with(['programs' => $programs]);
    }
}
