<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Perspective;
use Meritop\Http\Assets\ResourceFunctions;

class PerspectiveController extends Controller
{
    /**
     * Display a listing of the Perspective.
     *
     *  @return the view(index)
     */

     public function __construct()
    {
       $this->middleware('auth:user');
    }

    public function index()
    {
        Log::info('Ingreso exitoso a PerspectiveController - index(), del usuario: '.Auth::guard('user')->user()->user_name);

        $perspectives = Perspective::all();
        $num_perspectives = $perspectives -> count();

        return view('admin/perspectives/index')->with(['perspectives' => $perspectives, 'num_perspective' => $num_perspectives]);
    }

    /**
     * Show the form for creating a new Perspective.
     *
     * @return the view(create)
     */
    public function create()
    {
        Log::info('Ingreso exitoso a PerspectiveController - create(), del usuario: '.Auth::guard('user')->user()->user_name);

        return view('admin/perspectives/create');
    }

    /**
     * Store a newly created Perspective in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @return redirect view(index)
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            Perspective::create( $request->all() );

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('PerspectiveController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('PerspectiveController','store',$e);
        }

        return redirect()->route('perspectives.index');
    }

    /**
     * Display the specified Perspective.
     *
     * @param  int  $id - (id of the particular Perspective)
     * @return the view(show)
     */
    public function show($id)
    {
        Log::info('Ingreso exitoso a PerspectiveController - show(), del usuario: '.Auth::guard('user')->user()->user_name);

        $perspective = Perspective::FindOrFail($id);

        return view('admin/perspectives/show')->with(['perspective' => $perspective]);
    }

    /**
     * Show the form for editing the specified Perspective.
     *
     * @param  int  $id (id of the particular Perspective)
     * @return the view(edit)
     */
    public function edit($id)
    {
        Log::info('Ingreso exitoso a PerspectiveController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);

        $perspective = Perspective::FindOrFail($id);

        return view('admin/perspectives/edit')->with(['perspective' => $perspective]);
    }

    /**
     * Update the specified Perspective in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @param  int  $id (id of the particular Perspective)
     * @return redirect view(index)
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            Perspective::where('id', $id)
            ->update(['name' => $request->name]);

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('PerspectiveController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('PerspectiveController','actualizar',$e);
        }

        return redirect()->route('perspectives.index');
    }

    /**
     * Remove the specified Perspective from storage.
     *
     * @param  int  $id (id of the particular Perspective)
     * @return redirect view(index)
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            Perspective::destroy($id);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('PerspectiveController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('PerspectiveController','eliminar',$e);
        }

        return redirect()->route('perspectives.index');
    }
}
