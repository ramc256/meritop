<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Member;
use Meritop\Models\MemberClub;
use Meritop\Models\MemberGroup;
use Meritop\Models\Department;
use Meritop\Models\Club;
use Meritop\Models\Branch;
use Meritop\Models\MemberBranch;
use Meritop\Models\MemberDepartment;
use Meritop\Models\City;
use Meritop\Models\Media;
use Meritop\Models\Account;
use Meritop\Models\HistoricalImport;
use Excel;
use Illuminate\Support\Facades\Input;
use Meritop\Mail\NewMemberWelcome;
use Meritop\Mail\NewMemberClub;
use Mail;
use Meritop\Http\Requests\Admin\Member\MemberImportRequest;
use Meritop\Http\Requests\Admin\Member\MemberCreateRequest;
use Meritop\Http\Requests\Admin\Member\MemberUpdateRequest;
use Meritop\Http\Requests\Admin\Member\FilterRequest;


class MemberController extends Controller
{
    public $module = 4;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
       /*
       *Method that allows the validation of privileges
       */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();
        /*
        *Consult users and club related data
        */
        $members = MemberClub::select('members.*', 'clubs.alias as club','members_clubs.status as status_clubs','members_clubs.carnet')
        ->join('members', 'members.id', 'members_clubs.fk_id_member')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->where('members.deleted', false)
        ->get();
        /*
        *Consult group related data
        */
        $members_groups = MemberGroup::select('members_groups.*', 'groups.id', 'groups.name')
        ->join('groups', 'groups.id', 'members_groups.fk_id_group')
        ->get();
        /*
        *Relates the member to the groups
        */
        $members = ResourceFunctions::getGroups($members, $members_groups, 'fk_id_member');

        return view('admin.members.index' )->with([
            'members' => $members
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return back();
        }
        /*
        *Capture the club id of the session variable
        */
        $club = session('clubs')->first();

        Log::info('Ingreso exitoso a MemberController - create(), del usuario: '.Auth::user()->user_name);
        /*
        *Consult the branches related to the club
        */
        $branches = Branch::select('branches.*')
        ->whereIn('fk_id_club',$club)
        ->get();
        /*
        *Consult the departments related to the club
        */
        $departments = Department::select('departments.*')
        ->whereIn('fk_id_club',$club)
        ->get();

        return view('admin.members.create')->with([
            'branches'      =>$branches,
            'departments'   => $departments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberCreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('users.index');
        }

        try{
            DB::beginTransaction();
            /*
            *Code that modify the password that comes in the request by the same password but encrypted, it inserts all the request in the database
            */

            $existing_member = Member::select('*')
            ->join('members_clubs','members_clubs.fk_id_member','members.id')
            ->where('dni',$request->dni)
            ->first();
            $data_club = Club::select('clubs.*')
            ->where('id',session('clubs')->first()->fk_id_club)
            ->first();
            /*
            *consult the image related club
            */
            $logo_image = Media::where([
                'identificator' => session('clubs')->first()->fk_id_club,
                'table'         => 'clubs'
            ])
            ->first();

            $logo = $_ENV['STORAGE_PATH'].$logo_image->image;
            $url = $_ENV['APP_URL'].'/'.$data_club->slug;
            
            // dd($data_club, $logo, $url);
            if ($existing_member) {

                $members_clubs = MemberClub::select('*')
                ->where('members_clubs.fk_id_member', $existing_member->id )
                ->first();

                if ($members_clubs) {
                    /**
                     * Validates that the total of points added by each item, is equal to or less than that in the member's account
                     * @var [array] [$products] array with key and quantity of products
                     * @var [array] [$total_inventory] array with quantity of points of account member
                     */
                        flash('Miembro ya registrado en la plataforma', '¡Alert!')->error();
                        return back();

                }else{ 
                    /*
                    *Save in related table memberClub
                    */
                    $members_clubs = MemberClub::create([
                        'carnet'          => $request -> carnet,
                        'email'           => $request -> email_corporate,
                        'title'           => $request -> title,
                        'supervisor_code' => $request -> supervisor_code,
                        'status'          => $request -> status,
                        'fk_id_member'    => $member->id,
                        'fk_id_club'      => session('clubs')->first()->fk_id_club
                    ]);
                    /*
                    *Save in related table memberBranch
                    */
                    $members_branches = MemberBranch::create([
                        'fk_id_member' => $member->id,
                        'fk_id_branch' => $request -> fk_id_branch
                    ]);
                    /*
                    *Save in related table memberDepartment
                    */
                    $members_departments = MemberDepartment::create([
                        'fk_id_member'  => $member->id,
                        'fk_id_department'  => $request -> fk_id_department
                    ]);
                    /*
                    *Allows to send an email when the new registered user
                    */
                    Mail::to($request -> email )->send(new NewMemberClub($data_club->alias,$logo,$url) );

                }

            }else{
               
                $pass = substr( md5(microtime()), 1, 8);
                $request -> merge(['password' => bcrypt($pass)]);

                $member = Member::create($request -> all());

                if ($member==true) {
                    /*
                    *Save in related table memberClub
                    */
                    $members_clubs = MemberClub::create([
                        'carnet'          => $request -> carnet,
                        'email'           => $request -> email_corporate,
                        'title'           => $request -> title,
                        'supervisor_code' => $request -> supervisor_code,
                        'status'          => $request -> status,
                        'fk_id_member'    => $member->id,
                        'fk_id_club'      => session('clubs')->first()->fk_id_club
                    ]);
                    /*
                    *Save in related table memberBranch
                    */
                    $members_branches = MemberBranch::create([
                        'fk_id_member' => $member->id,
                        'fk_id_branch' => $request -> fk_id_branch
                    ]);
                    /*
                    *Save in related table memberDepartment
                    */
                    $members_departments = MemberDepartment::create([
                        'fk_id_member'  => $member->id,
                        'fk_id_department'  => $request -> fk_id_department
                    ]);
                    /*
                    *Function that allows to create a new account to the user
                    */
                    ResourceFunctions::createAccount( session('clubs')->first()->fk_id_club, $member->id, null, 0);
                    /*
                    *Allows to send an email when the new registered user
                    */
                    Mail::to($request -> email )->send(new NewMemberWelcome($pass, $member->first_name, $data_club->alias,$logo,$url) );

                    ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);
                    
            }

           

        }
            

            DB::commit();

            ResourceFunctions::messageSuccess('MemberController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('MemberController','store',$e);
        }

        return redirect()->route('members.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.index');
        }

        Log::info('Ingreso exitoso a MemberController - show(), del usuario: '.Auth::user()->user_name);
        /*
        *Query the data of the member related to the last id
        */
        $member = Member::FindOrFail($id);
        /*
        *Captures the id of the session variable
        */
        $id_club = session('clubs')->first()->fk_id_club;
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();
        /*
        *Consult users and club related data
        */
        $members_clubs = MemberClub::select('members_clubs.*','clubs.name as club','branches.name as branch', 'departments.code as department')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->join('members', 'members.id', 'members_clubs.fk_id_member')
        ->join('members_branches', 'members_branches.fk_id_member', 'members.id')
        ->join('branches', 'branches.id', 'members_branches.fk_id_branch')
        ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->where('members_clubs.fk_id_member', $id)
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->distinct()
        ->get();
        // dd($members_clubs);
        // $members_departments = MemberDepartment::select('members_departments.*')
        // ->where('members_departments.fk_id_member', $id)
        // ->first();

        return view('admin.members.show')->with(['member' => $member, 'members_clubs' => $members_clubs]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       /*
       *Method that allows the validation of privileges
       */
        if ( (ResourceFunctions::validationReturn($this->module,4)) !=true) {
             return back();
        }

        Log::info('Ingreso exitoso a UserController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);
        /*
        *Captures the id of the session variable
        */
        $id_club = session('clubs')->first()->fk_id_club;
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();
        /*
        *Query the data of the member related to the last id
        */
        $member = Member::FindOrFail($id);
        /*
        *Check the club's data related to the member
        */
        $members_clubs = MemberClub::select('members_clubs.*','clubs.name as club')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->join('members', 'members.id', 'members_clubs.fk_id_member')
        ->where('members_clubs.fk_id_member', $id)
        ->where('members_clubs.fk_id_club', $id_club)
        ->distinct()
        ->first();
        /*
        *Check the branch's data related to the member
        */
        $member_branch = MemberBranch::select('members_branches.*')
        ->join('branches', 'branches.id', 'members_branches.fk_id_branch')
        ->where('branches.fk_id_club',$id_club)
        ->where('members_branches.fk_id_member',$id)
        ->first();
        /*
        *Check the department's data related to the member
        */
        $member_department = MemberDepartment::select('members_departments.*')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->where('departments.fk_id_club',$id_club)
        ->where('members_departments.fk_id_member',$id)
        ->first();
        /*
        *Consult club-related venues in the settlement
        */
        $branches = Branch::select('branches.*')
        ->where('fk_id_club',$clubs)
        ->get();
        /*
        *Check club-related departmet in the settlement
        */
        $departments = Department::select('departments.*')
        ->where('departments.fk_id_club', $clubs)
        ->get();

        return view('admin.members.edit')
        ->with([
            'member'            => $member,
            'branches'          => $branches,
            'departments'       => $departments,
            'members_clubs'     => $members_clubs,
            'member_branch'     => $member_branch,
            'member_department' => $member_department,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MemberUpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ( (ResourceFunctions::validationReturn($this->module,4)) !=true) {
             return back();
        }

        try {
            DB::beginTransaction();
            /*
            *Updates the member's data
            */
            $member = Member::where('id',$id)
            ->update([
                'dni'           => $request -> dni,
                'first_name'    => $request -> first_name,
                'last_name'     => $request -> last_name,
                'birthdate'     => $request -> birthdate,
                'email'         => $request -> email,
                'biography'     => $request -> biography,
                'gender'        => $request -> gender,
                'cell_phone'    => $request -> cell_phone,
                'status'        => $request -> status,
            ]);

            /*
            *Two operations are performed that allow to delete the relation and to register a new one in the table
            *members_clubs.
            */
            $members_clubs_delete = MemberClub::where([
                'fk_id_club' => session('clubs')->first()->fk_id_club,
                'fk_id_member' => $id
            ])->delete();

            $members_clubs = MemberClub::insert([
                'carnet'          => $request -> carnet,
                'email'           => $request -> email_corporate,
                'supervisor_code' => $request -> supervisor_code,
                'title'           => $request -> title,
                'status'          => $request -> status,
                'fk_id_club'      => session('clubs')->first()->fk_id_club,
                'fk_id_member'    => $id,
                'created_at'      => date('Y-m-d H:i:s'),
                'updated_at'      => date('Y-m-d H:i:s'),
            ]);

            /*
            *Two operations are performed that allow to delete the relation and to register a new one in the table
            *members_department.
            */

            $members_department_delete = MemberDepartment::join('departments', 'departments.id', 'members_departments.fk_id_department')
            ->where('departments.fk_id_club', session('clubs')->first()->fk_id_club )
            ->where('members_departments.fk_id_member', $id)->delete();

            $members_departments = MemberDepartment::insert([
                'fk_id_member'      => $id,
                'fk_id_department'  => $request -> department,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ]);


            /*
            *Two operations are performed that allow to delete the relation and to register a new one in the table
            *members_braches.
            */
            $members_branches = MemberBranch::join('branches', 'branches.id', 'members_branches.fk_id_branch')
            ->where('branches.fk_id_club', session('clubs')->first()->fk_id_club )
            ->where('members_branches.fk_id_member', $id)->delete();

            $members_branches = MemberBranch::insert([
                'fk_id_member'  => $id,
                'fk_id_branch'  => $request -> branch,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);


            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('MemberController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('MemberController','actualizar',$e);
        }

        return redirect()->route('members.index');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();
            /*
            *Query that allows to verify if the user is related to clubs and is active
            */
            $members_clubs = MemberClub::select('*')
            ->where(['fk_id_member'=> $id],['fk_id_club'=> session('clubs')->first()->fk_id_club],['status'=> 1])
            ->get();
            /*
            *Alert to inform that the member is related to other clubs and is active
            */
            if ($members_clubs->count()!=0) {
               flash('El usuario no se puede eliminar debido a que esta relacionado a otros clubs', '¡Error!')->error();
                return back();

            }
            /*
            *update status of member
            */
            $member = Member::where('members.id', $id)
                ->update([
                 'status' => 0
            ]);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('MemberController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('MemberController','eliminar',$e);
        }

        return redirect()->route('members.index');
    }

    /**
     * method that allows to importing files.
     *
     *@param Meritop\Http\Requests\Admin\Member\MemberImportRequest  $request.
     *@param $members_registers: Arrangement with the data of the file members.
     *@param $registers: Variable that contains the number of records with correct data, to register.
     *@param $no_registers: Variable that contains the number of records with incorrect data.
     *@param $status_registers: Allows to identify if the data of the file are acts or not to save in the database.
     *@return \Illuminate\Http\Response
     */
    public function import(MemberImportRequest $request)
    {
    /*
    *Method that allows the validation of privileges
    */
    if ((ResourceFunctions::validationReturn($this->module,6)) !=true) {
        return back();
    }

    $clubs = session('clubs')->first();
    $file = $request -> file('import_file');

    /*
    *Indices that validate the file header
    */
        $keys=[
        0=>"dni", 1=>"first_name", 2=>"last_name",3=>"birthdate",4=>"email",5=>"cell_phone",6=>"gender",7=>"carnet",8=>"email_corporate",9=>"supervisor_code",10=>"branch",11=>"department",12=>"password"
        ];

        if(Input::hasFile('import_file')){
            /*
            *Code that allows you to take the data from the file and convert it into an array
            */
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
                $reader->formatDates(true, 'Y-m-d');
            })->get();
            /*
            *Code that allows to take the data of a column in specific
            */
            $dataColumn = Excel::load($path, function($reader) {
                $w=$reader->select(array('dni','email','department','password'))->get();
            })->get();

            $dataDNI = Excel::load($path, function($reader) {
                $w=$reader->select(array('dni'))->get();
            })->get();

            $dataEmail = Excel::load($path, function($reader) {
                $w=$reader->select(array('email'))->get();
            })->get();
            /*
            *Validate that the file structure is correct
            */
            if ( (empty($data) )
                || ($data->count()==0)
                || (ResourceFunctions::validateColumnNumeric($dataColumn->toArray(),"dni"))
                || (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"dni")==false)
                || (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"password")==false)
                || (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"email")==false)
                || (!ResourceFunctions::validateHead($keys, $data)==13)
                || ( ResourceFunctions::validateDuplicateValue(ResourceFunctions::arraySimple($dataDNI)) )
                || ( ResourceFunctions::validateDuplicateValue(ResourceFunctions::arraySimple($dataEmail)) )
                // || ( ResourceFunctions::validateSheetExcel($data) )
                 ) {


                if ((empty($data) ) || ($data->count()==0)) {
                   ResourceFunctions::messageErrorExportNull('DataController','store',null);
                }
                /*
                *allows you to validate that the file has only one sheet
                */
                // if( ResourceFunctions::validateSheetExcel($data) ){
                // flash('Para procesar el archivo debe tener solo una hoja', '¡Error!')->error();
                // }
                 /*
                *Validate that the dni column is just numbers
                */
                if (ResourceFunctions::validateColumnNumeric($dataColumn->toArray(),"dni")) {
                      ResourceFunctions::messageErrorColumnNumeric('DataController',"dni");
                }
                /*
                *Validate that the dni column is not void
                */
                if (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"dni")==false) {
                    ResourceFunctions::messageErrorColumnNull('MemberController', "dni");
                }
                /*
                *Validate that the email column is not void
                */
                if (ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"email")==false) {
                    ResourceFunctions::messageErrorColumnNull('MemberController', "email");
                }
                /*
                *Validate that the email column is not void
                */
                if ( ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"email")==false ) {
                    ResourceFunctions::messageErrorColumnNull('MemberController', "email");
                }
                /*
                *Validate that the password column is not void
                */
                if ( ResourceFunctions::validateColumnVoid($dataColumn->toArray(),"password")==false ) {
                    ResourceFunctions::messageErrorColumnNull('MemberController', "password");
                }
                /*
                *Validate that there are no duplicate values in the column dni
                */
                if ( ResourceFunctions::validateDuplicateValue(ResourceFunctions::arraySimple($dataDNI))  ) {
                    flash('En el archivo existen DNI duplicados', '¡Alert!')->warning();
                }
                /*
                *Validate that there are no duplicate values in the column Email
                */
                if ( ResourceFunctions::validateDuplicateValue(ResourceFunctions::arraySimple($dataEmail))  ) {
                    flash('En el archivo existen Email duplicados', '¡Alert!')->warning();
                }
                /*
                *Records a history of transactions
                */
                HistoricalImport::insert([
                    'file_name'        => $file ->getClientOriginalName(),
                    'status'           => 0,
                    'ip_address'       => $request->ip(),
                    'total_records'    => 0,
                    'success_records'  => 0,
                    'fail_records'     => 0,
                    'kind'             => 'import_members',
                    'fk_id_user'       => Auth::user()->id,
                    'created_at'       => date('Y-m-d H:i:s'),
                    'updated_at'       => date('Y-m-d H:i:s'),
                ]);

                return back();

            }else{

                $registers=0;$no_registers=0; $aux=false;
                /*
                *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
                */
                $clubs = ResourceFunctions::getClubs();
                /*
                *Consultation the members related to the club
                */
                $members = Member::select('members.*')
                ->join('members_clubs','members_clubs.fk_id_member', 'members.id')
                ->get()->toArray();
                /*
                *Consultation of venues the members related to the club
                */
                $members_clubs = MemberClub::select('members.dni')
                ->join('members', 'members.id', 'members_clubs.fk_id_member')
                ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
                ->whereIn('members_clubs.fk_id_club', $clubs)
                ->where('members.deleted', false)
                ->get()->toArray();
                /*
                *Consultation of branches related to the club
                */
                $branches = Branch::select('branches.code')
                ->whereIn('fk_id_club',$clubs)
                ->get()->toArray();
                /*
                *Consultation of departments related to the club
                */
                $departments = Department::select('departments.code')
                ->whereIn('fk_id_club',$clubs)
                ->get()->toArray();
                /*
                *Initialize the arrangements in case the query does not bring any value
                */
                if (empty($members)) {
                    $members=[];
                }

                if(empty($members_clubs)){
                    $members_clubs=[];
                }

                if(empty($branches)){
                    $branches[0]=[0=>'codigo'];
                }

                if(empty($departments)){
                    $departments[0]=[0=>'codigo'];
                }
                /*
                *status 0 cuando el usuario esta registrado en la base de datos y en el club no se ejecuta el registro NO SE REGISTRA
                *status 1 cuando el usuario existe en la base de datos no esta relacionado al club pero su codigo de sede es incorrecto NO SE REGISTRA
                *status 2 cuando el usuario existe en la base de datos no esta relacionado al club y su codigo de sede es correcto o no posee
                *status 3 guarda a los usuarios que no estan registrados en la base de datos, y si tiene una sede valida o el campo sin valores
                *status 4 guarda a los usuarios que tienen un correo que ya existe en la base de datos
                */
               
                foreach ($data as $key => $valueData) {
                    /*
                    *Allows to validate if the user is registered in the database
                    */
                    if ( in_array($valueData->dni, array_column($members, 'dni') ) ) {

                            if ( in_array($valueData->dni, array_column($members_clubs, 'dni') ) ) {
                                /*
                                *Saves users who are already related to the club and exist in the base detos
                                */
                                $members_registers[] = [
                                    'dni'               => $valueData->dni,
                                    'first_name'        => $valueData->first_name,
                                    'last_name'         => $valueData->last_name,
                                    'birthdate'        => $valueData->birthdate,
                                    'email'             => $valueData->email,
                                    'cell_phone'        => $valueData->cell_phone,
                                    'gender'            => $valueData->gender,
                                    'carnet'            => $valueData->carnet,
                                    'email_corporate'   => $valueData->email_corporate,
                                    'supervisor_code'   => $valueData->supervisor_code,
                                    'branch'            => $valueData->branch,
                                    'department'        => $valueData->department,
                                    'password'          => $valueData->password,
                                    'status'            => 0
                                ];
                                $no_registers++;
                                // $aux=true;
                            }else if( in_array($valueData->email, array_column($members, 'email') ) ){
                                    /*
                                    *save records with existing emails in the database
                                    */
                                    $members_registers[] = [
                                        'dni'               => $valueData->dni,
                                        'first_name'        => $valueData->first_name,
                                        'last_name'         => $valueData->last_name,
                                        'birthdate'         => $valueData->birthdate,
                                        'email'             => $valueData->email,
                                        'cell_phone'        => $valueData->cell_phone,
                                        'gender'            => $valueData->gender,
                                        'carnet'            => $valueData->carnet,
                                        'email_corporate'   => $valueData->email_corporate,
                                        'supervisor_code'   => $valueData->supervisor_code,
                                        'branch'            => $valueData->branch,
                                        'department'        => $valueData->department,
                                        'password'          => $valueData->password,
                                        'status'            => 4
                                    ];
                                    $no_registers++;
                                    // $aux=true;

                            }else if( (($valueData->branch !=null) && (in_array($valueData->branch, array_column($branches, 'code') )==false))  || (($valueData->department !=null) && (in_array($valueData->department, array_column($departments, 'code') )==false)) ){
                                    /*
                                    *Saves users who have an unsigned site code in the DB
                                    */ 
                                    $members_registers[] = [
                                        'dni'               => $valueData->dni,
                                        'first_name'        => $valueData->first_name,
                                        'last_name'         => $valueData->last_name,
                                        'birthdate'        => $valueData->birthdate,
                                        'email'             => $valueData->email,
                                        'cell_phone'        => $valueData->cell_phone,
                                        'gender'            => $valueData->gender,
                                        'carnet'            => $valueData->carnet,
                                        'email_corporate'   => $valueData->email_corporate,
                                        'supervisor_code'   => $valueData->supervisor_code,
                                        'branch'            => $valueData->branch,
                                        'department'        => $valueData->department,
                                        'password'          => $valueData->password,
                                        'status'            => 1
                                    ];
                                    $no_registers++;
                                    // $aux=true;

                            }else{
                                /*
                                *Relates to users who are registered in the database, are not related to the club and valid email.
                                */
                                $members_registers[] = [
                                    'dni'               => $valueData->dni,
                                    'first_name'        => $valueData->first_name,
                                    'last_name'         => $valueData->last_name,
                                    'birthdate'         => $valueData->birthdate,
                                    'email'             => $valueData->email,
                                    'cell_phone'        => $valueData->cell_phone,
                                    'gender'            => $valueData->gender,
                                    'carnet'            => $valueData->carnet,
                                    'email_corporate'   => $valueData->email_corporate,
                                    'supervisor_code'   => $valueData->supervisor_code,
                                    'branch'            => $valueData->branch,
                                    'department'        => $valueData->department,
                                    'password'          => $valueData->password,
                                    'status'            => 2
                                ];
                                $registers++;

                            }//else close

                    }if( in_array($valueData->email, array_column($members, 'email') ) ){
                                    /*
                                    *save records with existing emails in the database
                                    */                                    
                                    $members_registers[] = [
                                        'dni'               => $valueData->dni,
                                        'first_name'        => $valueData->first_name,
                                        'last_name'         => $valueData->last_name,
                                        'birthdate'         => $valueData->birthdate,
                                        'email'             => $valueData->email,
                                        'cell_phone'        => $valueData->cell_phone,
                                        'gender'            => $valueData->gender,
                                        'carnet'            => $valueData->carnet,
                                        'email_corporate'   => $valueData->email_corporate,
                                        'supervisor_code'   => $valueData->supervisor_code,
                                        'branch'            => $valueData->branch,
                                        'department'        => $valueData->department,
                                        'password'          => $valueData->password,
                                        'status'            => 4
                                    ];
                                    $no_registers++;
                                    // $aux=true;

                            }else if( (($valueData->branch !=null) && (in_array($valueData->branch,  array_column($branches, 'code'))==false))  || (($valueData->department !=null) && (in_array($valueData->department,  array_column($departments, 'code'))==false) ) ){
                            /*
                            *Saves the users that have a code of seat or department non-existent in the BD
                            // */ 
                            $members_registers[] = [
                                'dni'               => $valueData->dni,
                                'first_name'        => $valueData->first_name,
                                'last_name'         => $valueData->last_name,
                                'birthdate'         => $valueData->birthdate,
                                'email'             => $valueData->email,
                                'cell_phone'        => $valueData->cell_phone,
                                'gender'            => $valueData->gender,
                                'carnet'            => $valueData->carnet,
                                'email_corporate'   => $valueData->email_corporate,
                                'supervisor_code'   => $valueData->supervisor_code,
                                'branch'            => $valueData->branch,
                                'department'        => $valueData->department,
                                'password'          => $valueData->password,
                                'status'            => 1
                            ];
                            $no_registers++;
                            // $aux=true;

                    }else {
                        /*
                        *Saves the users that are not registered in the database, and if it has a valid headquarters or the field without values
                        */
                        $members_registers[] = [

                            'dni'               => $valueData->dni,
                            'first_name'        => $valueData->first_name,
                            'last_name'         => $valueData->last_name,
                            'birthdate'         => $valueData->birthdate,
                            'email'             => $valueData->email,
                            'cell_phone'        => $valueData->cell_phone,
                            'gender'            => $valueData->gender,
                            'carnet'            => $valueData->carnet,
                            'email_corporate'   => $valueData->email_corporate,
                            'supervisor_code'   => $valueData->supervisor_code,
                            'branch'            => $valueData->branch,
                            'department'        => $valueData->department,
                            'password'          => $valueData->password,
                            'status'            => 3
                        ];
                        $registers++;

                    }//close else

                }//Data forech close


            }//close else
               
                    $i=0; $r=0;

                    $data_club = Club::select('clubs.*')
                    ->where('id',session('clubs')->first()->fk_id_club)
                    ->first();
                    /*
                    *consult the image related club
                    */
                    $logo_image = Media::where([
                        'identificator' => session('clubs')->first()->fk_id_club,
                        'table'         => 'clubs'
                    ])
                    ->first();

                    $logo = $_ENV['STORAGE_PATH'].$logo_image->image;
                    $url = $_ENV['APP_URL'].'/'.$data_club->slug;

                    foreach ($members_registers as $key => $valueData) {

                        if ( ($valueData['status']==2) ) {

                            try {
                                /*
                                *querry  the id table Member
                                */
                                $id_member = Member::select('members.id')->where('members.dni',$valueData['dni'])->first();

                                DB::beginTransaction();
                                /*
                                *insert in the table MemberClub
                                */
                                $members_clubs = Memberclub::create([
                                    'carnet'            => $valueData['carnet'],
                                    'email'             => $valueData['email_corporate'],
                                    'status'            => 1,
                                    'fk_id_member'      => 3,
                                    'fk_id_club'        => session('clubs')->first()->fk_id_club
                                ]);
                                /*
                                *Validate that the field has a code to insert it into the db
                                */
                                if($valueData['branch']!=null){
                                    /*
                                    *querry  the id table Branch
                                    */
                                    $id_branch = Branch::select('branches.id')->where('branches.code',$valueData['branch'])->first();
                                    /*
                                    *insert in the table MemberBranch
                                     */
                                    $members_branches = MemberBranch::create([
                                        'fk_id_member'  => $id_member->id,
                                        'fk_id_branch'  => $id_branch
                                    ]);
                                }
                                /*
                                *Validate that the field has a code to insert it into the db
                                */
                                if($valueData['department']!=null){
                                    /**
                                    *insert in the table MemberDepartmet
                                    */
                                    $id_department = Department::select('departments.id')->where('departments.code',$valueData['department'])->first();

                                    $members_departments = MemberDepartment::create([
                                        'fk_id_member'      => $id_member->id,
                                        'fk_id_department'  => $id_department
                                    ]);
                                }

                                /*
                                *Query that allows validate if the user has an account related to the country that are
                                *registering
                                */
                                $department = Department::all()
                                ->where('fk_id_country', Auth::user()->fk_id_country )
                                ->where('.fk_id_member', $id_member)->first();
                                /*
                                *Allows to validate that the user does not have an account related to the country.
                                */
                                if ($department==null) {
                                    /*
                                    *Code that allows to create an account to the member
                                    */
                                    ResourceFunctions::createAccount( session('clubs')->first()->fk_id_club, $id_member->id, null, 0);
                                }
                                /*
                                *Valid that the user registered in the club to be able to send the mail
                                */
                                if ($members_clubs) {
                                    /*
                                    *Code that allows the sending of emails
                                    */
                                    Mail::to($valueData['email'])->send(new NewMemberclub($name_club->name));
                                     /*
                                    *Allows to send an email when the new registered user
                                    */
                                    Mail::to($request -> email )->send(new NewMemberClub($data_club->name,$logo,$url) );
                                }

                            DB::commit();
                            } catch (Exception $e) {
                            DB::rollBack();
                            ResourceFunctions::messageError('BrandController','store',$e);
                            } catch (\Illuminate\Database\QueryException $e) {
                            DB::rollBack();
                            ResourceFunctions::messageError('BrandController','store',$e);
                            }

                        }

                        if( ($valueData['status']==3) ){

                                try {

                                    DB::beginTransaction();

                                    // $pass = substr( md5(microtime()), 1, 8);
                                    /*
                                    *Insert data into the members table
                                    */
                                    Member::create([
                                        'dni'           => $valueData['dni'],
                                        'first_name'    => $valueData['first_name'],
                                        'last_name'     => $valueData['last_name'],
                                        'birthdate'     => $valueData['birthdate'],
                                        'password'      => bcrypt($valueData['password']),
                                        'email'         => $valueData['email'],
                                        'cell_phone'    => $valueData['cell_phone'],
                                        'gender'        => $valueData['gender'],
                                        'status'        => 1
                                    ]);

                                     $member = Member::all()->last();
                                     // dd($member);
                                    /*
                                    *Insert data into the membersClubs table
                                    */
                                    $members_clubs = Memberclub::create([
                                        'carnet'            => $valueData['carnet'],
                                        'email'             => $valueData['email_corporate'],
                                        'status'            => 1,
                                        'fk_id_member'      => $member->id,
                                        'fk_id_club'        => session('clubs')->first()->fk_id_club
                                    ]);

                                    /*
                                    *Validate that the field has a code to insert it into the db
                                    */
                                    if($valueData['branch']!=null){
                                        /*
                                        *insert in the table MemberBranch
                                        */
                                        $id_branch = Branch::select('branches.id')->where('branches.code',$valueData['branch'])->first();

                                        $members_branches = MemberBranch::create([
                                            'fk_id_member'  => $member->id,
                                            'fk_id_branch'  => $id_branch->id
                                        ]);
                                    }
                                    /*
                                    *Validate that the field has a code to insert it into the db
                                    */
                                    if($valueData['department']!=null){
                                        /*
                                        *consult id into the members
                                        */
                                        $id_department = Department::select('departments.id')->where('departments.code',$valueData['department'])->first();
                                        /*
                                        *insert in the table MemberBranch
                                        */
                                        $members_departments = MemberDepartment::create([
                                            'fk_id_member'  => $member->id,
                                            'fk_id_department'  => $id_department->id
                                        ]);
                                    }
                                    /*
                                    *Valid that the user registered in the club to be able to send the mail
                                    */
                                    if ($members_clubs) {

                                        ResourceFunctions::createAccount(session('clubs')->first()->fk_id_club, $member->id, null,0);

                                        Mail::to($valueData['email'])->send(new NewMemberWelcome($valueData['password'], $valueData['first_name'], $data_club->alias,$logo,$url) );
                                    }

                                    DB::commit();
                                    }catch (Exception $e) {
                                    DB::rollBack();
                                    ResourceFunctions::messageError('BrandController','store',$e);
                                    }catch (\Illuminate\Database\QueryException $e) {
                                    DB::rollBack();
                                    ResourceFunctions::messageError('BrandController','store',$e);
                                    }

                                }//close elseif
                                $i++;
                    }//close foreach
                        /*
                        *Inserted in the table when the transaction is successful
                        */
                        HistoricalImport::insert([
                        'file_name'        => $file ->getClientOriginalName(),
                        'status'           => 1,
                        'ip_address'       => $request->ip(),
                        'total_records'    => count($members_registers),
                        'success_records'  => $registers,
                        'fail_records'     => $no_registers,
                        'kind'             => 'import_members',
                        'fk_id_user'       => Auth::user()->id,
                        'created_at'       => date('Y-m-d H:i:s'),
                        'updated_at'       => date('Y-m-d H:i:s'),
                    ]);

                    return view('admin.members.import' )
                    ->with([
                        'members_registers' => $members_registers,
                        'registers'         => $registers,
                        'no_registers'      => $no_registers,
                        'status_registers'  => 1,
                    ]);


        }//close if validate import

    }//close method import


    public function filter(FilterRequest $request)
    {

        /*
       *Method that allows the validation of privileges
       */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();
        /*
        *Consult users and club related data
        */
        /*
        *Method that allows processing the range of dates
        */
        $fechas = ResourceFunctions::getDates($request->date);
        $fecha_inicio = $fechas[0];
        $fecha_fin = $fechas[1];
        /*
        *Consult users and club related data
        */
        $members = MemberClub::select('members.*', 'clubs.alias as club','members_clubs.status as status_clubs')
        ->join('members', 'members.id', 'members_clubs.fk_id_member')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->whereBetween('members.created_at', [$fecha_inicio, $fecha_fin])
        ->where('members.deleted', false)
        ->get();
        /*
        *Consult group related data
        */
        $members_groups = MemberGroup::select('members_groups.*', 'groups.id', 'groups.name')
        ->join('groups', 'groups.id', 'members_groups.fk_id_group')
        ->get();
        /*
        *Relates the member to the groups
        */
        $members = ResourceFunctions::getGroups($members, $members_groups, 'fk_id_member');

        return view('admin.members.index' )->with([
            'members' => $members
        ]);
    }

}//close class
