<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Affiliate;
use Meritop\Models\City;
use Meritop\Models\Media;
use Storage;


class AffiliateController extends Controller
{
    public $module = 5;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:user');
    }

    public function index()
    {
        // Method that allows the validation of privileges
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a AffiliateController - index(), del usuario: '.Auth::guard('user')->user()->user_name);
        $affiliates = Affiliate::select( 'id', 'alias', 'status')->get();

        return view('admin.affiliates.index' )->with(['affiliates'=> $affiliates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Method that allows the validation of privileges
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('affiliates.index');
        }

        Log::info('Ingreso exitoso a AffiliateController - create(), del usuario: '.Auth::guard('user')->user()->user_name);

        $cities = City::all();

        return view('admin.affiliates.create' )->with(['cities'=> $cities]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Method that allows the validation of privileges
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('affiliates.index');
        }

        Log::info('Ingreso exitoso a AffiliateController - store(), del usuario: '.Auth::guard('user')->user()->user_name);
        Affiliate::create($request->all());

        $files = $request->file('image');
        $id_affiliate = Affiliate::all()->last()->id;
        $i=0;
            if (!empty($files)) {
                foreach ($files as $file) {

                    $file_name = time().'_'.mt_rand();
                    $file_slug = time().'_'.$file_name;
                    Storage::disk('media')->put($file_slug, file_get_contents($file->getRealPath() ) );

                    $data_insert[$i]=[
                        'name'=>$file,
                        'kind'=>true,
                        'slug'=>$file_slug,
                        'identificator'=>$id_affiliate,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),

                    ];
                    $i++;

                }
            }

        Media::insert($data_insert);

        return redirect()->route('affiliates.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Method that allows the validation of privileges
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a StoreController - show(), del usuario: '.Auth::guard('user')->user()->user_name);
        $affiliate = Affiliate::select('affiliates.id as id','affiliates.alias','affiliates.name','affiliates.fiscal','affiliates.address', 'cities.name as city','affiliates.status','affiliates.latitude','affiliates.longitude','affiliates.phone', 'affiliates.contact_person','affiliates.open_hours','affiliates.facebook','affiliates.twitter', 'affiliates.instagram','affiliates.google_plus','affiliates.slug')
        ->join('cities','cities.id' ,'=', 'affiliates.fk_id_city')
        ->where('affiliates.id',$id)
        ->get();

        $media = Media::select('media.name')
        ->where('media.identificator',$id)
        ->get();

        return view('admin.affiliates.show')->with(['affiliate' => $affiliate, 'media'=>$media]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Method that allows the validation of privileges
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('affiliates.index');
        }

        Log::info('Ingreso exitoso a RoleController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);
        $affiliate = Affiliate::select('affiliates.id','affiliates.alias','affiliates.name','affiliates.fiscal','affiliates.address', 'cities.id as city','affiliates.status','affiliates.latitude','affiliates.longitude','affiliates.phone', 'affiliates.contact_person','affiliates.open_hours','affiliates.email','affiliates.facebook','affiliates.twitter', 'affiliates.instagram','affiliates.google_plus','affiliates.slug')
        ->join('cities','cities.id' ,'=', 'affiliates.fk_id_city')
        ->where('affiliates.id',$id)
        ->get();

        $cities = City::all();

        return view('admin.affiliates.edit')->with(['affiliate' => $affiliate, 'cities'=>$cities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Method that allows the validation of privileges
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a AfiliateController - update(), del usuario: '.Auth::guard('user')->user()->user_name);

        $afiliate = Affiliate::where('affiliates.id',$id)
        ->update([
                'alias'=>$request->alias,
                'name'=> $request->name,
                'fiscal'=>$request->fiscal,
                'address'=> $request->address,
                'status' =>$request->status,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'phone' => $request->phone,
                'contact_person' => $request->contact_person,
                'email' => $request->email,
                'open_hours' => $request->open_hours,
                'facebook' => $request->facebook,
                'twitter' => $request->twiter,
                'instagram' => $request->instagram,
                'google_plus' => $request->google_plus,
                'slug' => $request->slug,
                'fk_id_city' => $request->city,
                'updated_at' => date('Y-m-d H:i:s'),

                ]);

        return redirect()->route('affiliates.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Method that allows the validation of privileges
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a AfiliateController - destroy(), del usuario: '.Auth::guard('user')->user()->user_name);

        DB::table('affiliates')->where('id', $id)->delete();
        DB::table('media')->where('identificator', $id)->delete();

        return redirect()->route('affiliates.index');
    }
}
