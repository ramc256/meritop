<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Requests\Admin\Account\FilterRequest;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\Account;
use Meritop\Models\AccountTransaction;
use Meritop\Member;

class AccountController extends Controller
{
    public $module = 2;

    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a AccountController - index(), del usuario: '.Auth::user()->user_name);

        /**
         * [$clubs description] Variable that contain all user's clubs login
         * @var [type] Integer Array
         */
        $clubs = ResourceFunctions::getClubs();

        /**
         * [$accounts description] Variable that contains all the accounts belonging to the user's clubs
         * @var [type] Object array
         */
        $accounts = Account::select('accounts.*', 'members.dni', 'members.first_name', 'members.last_name', 'kind_accounts.name as kind')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('kind_accounts', 'kind_accounts.id', 'accounts.fk_id_kind_account')
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->distinct()
        ->get();

        return view('admin.accounts.index' )->with(['accounts' => $accounts]);
    }


    public function show($id)
    {
       /**
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a AccountController - show(), del usuario: '.Auth::user()->user_name);

        /**
         * [$clubs description] Variable that contain all user's clubs login
         * @var [type] Integer Array
         */
        $clubs = ResourceFunctions::getClubs();

        /**
         * [$transactions description] Variable that contains all user's account transactions
         * @var [type] Objetc Array
         */
        $transactions = AccountTransaction::select('accounts_transactions.*')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->whereIn('accounts_transactions.fk_id_club', $clubs)
        ->where('accounts.id', $id)
        ->whereYear('accounts_transactions.created_at', date('Y'))
        ->whereMonth('accounts_transactions.created_at', date('m'))
        ->orderBy('accounts_transactions.created_at', 'desc')
        ->get();

        
        return $this->getAccountData($id, $clubs, $transactions);
    }


    public function filter(FilterRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a AccountController - filter(), del usuario: '.Auth::user()->user_name);

        /**
         * [$date description] Variable that contains the dates of the form with
         * correct format
         * @var [type] Date Array
         */
        $date = ResourceFunctions::getDates($request->date);

        /**
         * [$date description] Variable that contains the filter start date
         * @var [type] Date
         */
        $start_date = $date[0];

        /**
         * [$due_date description] Variable that contains the filter due date
         * @var [type] Date
         */
        $due_date = $date[1];

        /**
         * This will create the filter, if the operation is null, the filters
         * will be both the contributions as traded, else will be the seleted
         */
        if ($request->operation == null) {
            $operations = [0,1];
        }else {
            $operations = [$request->operation];
        }

        /**
         * [$clubs description] Variable that contain all user's clubs login
         * @var [type] Integer Array
         */
        $clubs = ResourceFunctions::getClubs();

        /**
         * [$transactions description] Variable that contains all user's account
         * transactions with the filters
         * @var [type] Objetc Array
         */
        $transactions = AccountTransaction::select('accounts_transactions.*')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->whereIn('accounts_transactions.fk_id_club', $clubs)
        ->where([
            ['accounts.id', $id]
        ])
        ->whereBetween('accounts_transactions.created_at',  [$start_date, $due_date])
        ->whereIn('accounts_transactions.operation', $operations)
        ->orderBy('accounts_transactions.created_at', 'desc')
        ->get();

        return $this->getAccountData($id, $clubs, $transactions);
    }

    public function getAccountData($id, $clubs, $transactions){

        /**
         * [$account description] Variable that contains all user's account data
         * @var [type] Array
         */
        $account = Account::select('accounts.*', 'members.dni', 'members.first_name', 'members.last_name', 'members_clubs.carnet', 'kind_accounts.name as kind')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('kind_accounts', 'kind_accounts.id', 'accounts.fk_id_kind_account')
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->where('accounts.id', $id)
        ->first();

        /**
         * [$contributions description] Variable that contains all user's account
         * transactions whose operation is contribution
         * @var [type] Objetc Array
         */
        $contributions = AccountTransaction::select('accounts_transactions.*')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->whereIn('accounts_transactions.fk_id_club', $clubs)
        ->where([
            ['accounts.id', $id],
            ['accounts_transactions.operation', 1]
        ])
        ->get();

        /**
         * [$total_points description] Variable that contains the total points
         * obtained in his transactions record
         * @var [type] Integer
         */
        $total_points = $contributions->sum('points');

        /**
         * [$traded description] Variable that contains all user's account
         * transactions whose operation is trade
         * @var [type] Objetc Array
         */
        $traded = AccountTransaction::select('accounts_transactions.*')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->whereIn('accounts_transactions.fk_id_club', $clubs)
        ->where([
            ['accounts.id', $id],
            ['accounts_transactions.operation', 0]
        ])
        ->get();

        /**
         * [$total_points description] Variable that contains the total points
         * redeeming in his transactions record
         * @var [type] Integer
         */
        $traded_points = $traded->sum('points');

        return view('admin.accounts.show' )->with([
            'account' => $account,
            'total_points' => $total_points,
            'traded_points' => $traded_points,
            'transactions' => $transactions
            ]);
    }

}
