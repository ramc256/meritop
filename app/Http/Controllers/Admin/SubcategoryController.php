<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Models\Subcategory;
use Meritop\Models\Category;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Subcategory\SubcategoryCreateRequest;
use Meritop\Http\Requests\Admin\Subcategory\SubcategoryUpdateRequest;

class SubcategoryController extends Controller
{
    public $module = 11;
    /**
     * Display a listing of the subcategory.
     *
     *  @return the view(index)
     */

    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a SubcategoryController - index(), del usuario: '.Auth::guard('user')->user()->user_name);

        $subcategories = Subcategory::all();
        $num_subcategories = $subcategories -> count();

        return view('admin/subcategories/index')->with(['subcategories' => $subcategories, 'num_subcategories' => $num_subcategories]);
    }

    /**
     * Show the form for creating a new subcategory.
     *
     * @return the view(create)
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.subcategories.index');
        }

        Log::info('Ingreso exitoso a SubcategoryController - create(), del usuario: '.Auth::guard('user')->user()->user_name);

        $categories = Category::all();

        return view('admin/subcategories/create')->with(['categories' => $categories]);
    }

    /**
     * Store a newly created subcategory in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @return redirect view(index)
     */
    public function store(SubcategoryCreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            Subcategory::create($request->all());

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('SubcategoryController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('SubcategoryController','store',$e);
        }

        return redirect()->route('subcategories.index');
    }

    /**
     * Display the specified subcategory.
     *
     * @param  int  $id - (id of the particular subcategory)
     * @return the view(show)
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a SubcategoryController - show(), del usuario: '.Auth::guard('user')->user()->user_name);

        $subcategory = Subcategory::select('subcategories.*', 'categories.name as category')
        ->join('categories', 'categories.id', '=', 'subcategories.fk_id_category')
        ->where('subcategories.id', $id)
        ->get();

        return view('admin/subcategories/show')->with(['subcategory' => $subcategory]);
    }

    /**
     * Show the form for editing the specified subcategory.
     *
     * @param  int  $id (id of the particular subcategory)
     * @return the view(edit)
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a SubcategoryController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);

        $subcategory = Subcategory::FindOrFail($id);
        $categories = Category::all();

        return view('admin/subcategories/edit')->with(['subcategory' => $subcategory, 'categories' => $categories]);
    }

    /**
     * Update the specified subcategory in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @param  int  $id (id of the particular subcategory)
     * @return redirect view(index)
     */
    public function update(SubcategoryUpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            Subcategory::where('id', $id)
            ->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    'slug' => $request->slug,
                    'status' => $request->status,
                    'fk_id_category' => $request->fk_id_category
                    ]);

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('SubcategoryController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('SubcategoryController','actualizar',$e);
        }

        return redirect()->route('subcategories.index');
    }

    /**
     * Remove the specified subcategory from storage.
     *
     * @param  int  $id (id of the particular subcategory)
     * @return redirect view(index)
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            Subcategory::destroy($id);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('SubcategoryController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('SubcategoryController','eliminar',$e);
        }

        return redirect()->route('subcategories.index');
    }
}
