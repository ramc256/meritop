<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Assets\ResourceFunctions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Meritop\Models\Department;
use Meritop\Models\MemberDepartment;
use Meritop\Models\Club;

class DepartmentController extends Controller
{
    public $module = 15;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
         *Method that allows the validation of privileges
         */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a BranchesController - index(), del usuario: '.Auth::user()->user_name);
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();
        /*
        *allows consult the departments Related to a specific club
        */
        $departments = Department::select('departments.*', 'clubs.name as club')
        ->join('clubs','clubs.id', 'departments.fk_id_club')
        ->whereIn( 'departments.fk_id_club', $clubs)
        ->get();

        return view('admin.departments.index')->with([
            'departments' => $departments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*
         *Method that allows the validation of privileges
         */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.branches.index');
        }

        Log::info('Ingreso exitoso a BranchesController - create(), del usuario: '.Auth::user()->user_name);
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $id_clubs = ResourceFunctions::getClubs();
        /*
        *consult the data of a specific club
        */
        $clubs = Club::whereIn('id', $id_clubs)
        ->get();

        return view('admin.departments.create')->with(['clubs' => $clubs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        $club = session('clubs')->first();

        try{
            DB::beginTransaction();
            /*
            *Allows you to join some fields to the request, then generate the record
            */
            $request -> request -> add(['code' => substr( md5(microtime()), 1, 8)]);

            $request -> request -> add(['fk_id_club' => $club->fk_id_club]);

            $department = Department::create($request -> all());

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('DepartmentController','store');

            }catch (\Exception $e) {
                DB::rollBack();
                ResourceFunctions::messageError('DepartmentController','store',$e);
            }

        return redirect()->route('departments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a DepartmentController - show(), del usuario: '.Auth::user()->user_name);
        /*
        *consult the department data
        */
        $department= Department::select('departments.*', 'clubs.name as club')
        ->join('clubs','clubs.id', 'departments.fk_id_club')
        ->where( 'departments.id', $id)
        ->first();

        return view('admin.departments.show')->with([
            'department' => $department
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a BranchesController - edit(), del usuario: '.Auth::user()->user_name);
        /*
        *consult the department data
        */
        $department = Department::FindOrFail($id);

        return view('admin.departments.edit')->with([
            'department' => $department
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {

            DB::beginTransaction();
            /*
            *update department data
            */
            $department=Department::where('id',$id)
             ->update([
                'name'          => $request->name ,
                'updated_at'    => date('Y-m-d H:i:s')
            ]);

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('DepartmentController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('DepartmentController','actualizar',$e);
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();
            /*
            *Query that allows to verify if the department is related to a member
            */
            $member_departments = MemberDepartment::select('members_departments.*')
            ->where( 'members_departments.fk_id_department', $id)
            ->get();

            if (($member_departments->count()!=0)) {

                flash('No puede realizar la operacion, el departemento tiene miembros relacionados', '¡Alert!')->warning();

                return redirect()->route('departments.index');
            }
            /*
            *code line that allows destroy a database register
            */
            Department::destroy($id);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('DepartmentController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('DepartmentController','eliminar',$e);
        }

        return redirect()->route('departments.index');
    }
}
