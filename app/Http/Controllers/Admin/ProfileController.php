<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Meritop\Http\Controllers\Controller;
use Meritop\User;
use Meritop\Models\HistoricalAction;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Profile\ProfileRequest;
use Google\Cloud\Storage\StorageClient;
use League\Flysystem\Filesystem;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;
use Meritop\Http\Requests\Admin\Profile\ChangePasswordRequest;

class ProfileController extends Controller
{
    public function index()
    {
        $user = User::FindOrFail(Auth::user()->id);

        /**
         * Cakculate the Percentage actions by user logined
         * @var $total_registers        =>      all the registers in  historical_actions table
         * @var $total_user_registers   =>      the registers by user logined
         * @var $pua                    =>      Percentage the actions by user logined
         */
        $total_registers = HistoricalAction::all()->count();
        $total_user_registers = HistoricalAction::where('id_user', Auth::guard('user')->user()->id)->count();

        if ($total_registers != 0) {
            $pua = $total_user_registers / $total_registers;
        }else {
            $pua = 0;
        }

        $historical_actions = HistoricalAction::where('id_user', Auth::guard('user')->user()->id) -> paginate(6);
        return view('admin.profile')->with(['user' => $user, 'pua' => $pua, 'historical_actions' => $historical_actions]);
    }

    public function update(ProfileRequest $request, $id)
    {
        try {
            DB::beginTransaction();

            // User::where('id', Auth::guard('user')->user()->id)
            // ->update([
            //     'first_name'    => $request   -> first_name,
            //     'last_name'     => $request   -> last_name,
            //     'user_name'     => $request   -> user_name,
            //     'email'         => $request   -> email,
            //     'password'      => bcrypt($request ->password)                
            // ]);

            User::FindOrFail(Auth::user()->id)
            ->update($request->all());
            
            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ProfileController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ProfileController','actualizar',$e);
        }
        return back();
    }
// 
    public function changePassword(ChangePasswordRequest $request)
    {
        try {
            DB::beginTransaction();
            $user = User::FindOrFail(Auth::user()->id);
           
            if (Hash::check($request -> old_password, $user -> password)) {
                $u = user::where('id',Auth::user()->id)
                ->update([
                    'password'    => bcrypt($request -> new_password),
                ]);

            }
           
            ResourceFunctions::createHistoricalAction('change','Se ha realizado una actualización sobre el registro: '.Auth::user()->user_name);

            DB::commit();

            ResourceFunctions::messageSuccess('ProfileController','change');

        } catch (Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ProfileController','change',$e);
        }
        return back();
    }

    public function imageChange(Request $request, $id){
        try {

            DB::beginTransaction();

	        $filesystem = ResourceFunctions::filesystemGoogle();

            $file = $request -> file('image'); // Asignamos el archivo a una valiable
            $file_name = time() . mt_rand() .'.'. $file -> getClientOriginalExtension(); //Asignamos un nuevo nombre al archivo

			$url_cdn = $_ENV["STORAGE_ENV"];
	        $url_image = 'image/users/'.$file_name;

            //Recibimos el archivo enviado mediante el formulario

            $user = User::FindOrFail($id);
            
            if (!empty($user -> image)) {
                if ($filesystem->has($url_cdn . $user -> image)) {
                    $filesystem->delete($url_cdn . $user -> image);
                }
            }

            $result = User::where('id', $id) -> update(['image' => $url_image]);

            // write a file
	        $filesystem->write($url_cdn . 'image/users/' . $file_name, file_get_contents($file));

            Log::notice('Proceso exitoso: ProfileController -> imageChange(): '.Auth::user()->user_name);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ProfileController','imageChange',$e);
        }
        return back();
    }
}
