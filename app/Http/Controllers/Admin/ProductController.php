<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Product;
use Meritop\Models\Brand;
use Meritop\Models\Subcategory;
use Meritop\Models\Country;
use Meritop\Models\Club;
use Meritop\Models\Inventory;
use Meritop\Models\InventoryTransaction;
use Meritop\Models\Media;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Product\CreateRequest;
use Meritop\Http\Requests\Admin\Product\UpdateRequest;
use Storage;

class ProductController extends Controller
{
    public $module = 7;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(CreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            /*---------------------------------------------Create a new product---------------------------------------------*/
            ($request->sell_out_stock == 'on') ? $request['sell_out_stock'] = true : $request['sell_out_stock'] = false;

            $product = Product::create($request->all());

            /**
             * [Store the every images of the product]
             * @var [file]
             */
            $files = $request->file('image');

            if (!empty($files)) {
                foreach ($request->image as $file) {
                    ResourceFunctions::imageChange($product->id, null, $file, 'products');
                }
            }

            /*---------------------------------------------Create a new record into inventory---------------------------------------------*/
            $inventory = Inventory::create([
                'quantity_current' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'fk_id_product' => $product->id
            ]);

            /*---------------------------------------------Create a new record into InventoryTransaction---------------------------------------------*/
            InventoryTransaction::create([
                'quantity_update' => 0,
                'description' => 'cantidad inicial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'fk_id_inventory' => $inventory->id
            ]);

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ProductController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ProductController','store',$e);
        }

        return redirect()->route('parents-products.show',$product->fk_id_parent_product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a ProductController - show(), del usuario: '.Auth::user()->user_name);

        $product = Product::select('products.*')
        ->where(['products.id' => $id, 'products.deleted' => false])
        ->first();

        $images = Media::where(['identificator' => $id, 'table' => 'products'])->get();

        return view('admin/products/show')->with([
            'product' => $product,
            'images' => $images,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a ProductController - show(), del usuario: '.Auth::user()->user_name);

        $product = Product::select('products.*')
        ->where(['products.id' => $id, 'products.deleted' => false])
        ->first();
        $images = Media::where(['identificator' => $id, 'table' => 'products'])->get();

        return view('admin/products/edit')->with([
            'product'   => $product,
            'images'     => $images,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            /*---------------------------------------------Create a new product---------------------------------------------*/
            ($request->sell_out_stock == 'on') ? $request['sell_out_stock'] = true : $request['sell_out_stock'] = false;

            $product = Product::FindOrFail($id)
            ->update($request->all());

            if (!empty($request -> file('image'))) {
                ResourceFunctions::imageChange($id, $request -> id_image, $request -> file('image'), 'products');
            }

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ProductController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ProductController','actualizar',$e);
        }

        return redirect()->route('products.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            $product = Product::FindOrFail($id);

            Product::where('id', $id)
            ->update([
                    'deleted' => true
                    ]);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('ProductController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ProductController','eliminar',$e);
        }

        return redirect()->route('parents-products.show',$product->fk_id_parent_product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newCreate(Request $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a ProductController - create(), del usuario: '.Auth::user()->user_name);

        return view('admin.products.create') -> with(['parent_product' => $id]);
    }
}
