<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Models\Category;
use Meritop\Models\Subcategory;
use Meritop\Models\Media;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Category\CategoryCreateRequest;
use Meritop\Http\Requests\Admin\Category\CategoryUpdateRequest;

class CategoryController extends Controller
{
    public $module = 10;

    /**
     * Display a listing of the category.
     *
     *  @return the view(index)
     */
    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a CategoryController - index(), del usuario: '.Auth::guard('user')->user()->user_name);

        $categories = Category::all();

        /**
         * [$num_categories description] Total categories
         * @var [Integer]
         */
        $num_categories = $categories -> count();

        return view('admin/categories/index')->with(['categories' => $categories, 'num_categories' => $num_categories]);
    }

    /**
     * Show the form for creating a new category.
     *
     * @return the view(create)
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.categories.index');
        }

        Log::info('Ingreso exitoso a CategoryController - create(), del usuario: '.Auth::guard('user')->user()->user_name);

        return view('admin/categories/create');
    }

    /**
     * Store a newly created category in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @return redirect view(index)
     */
    public function store(CategoryCreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            Category::create($request -> all());

            /**
             * [$id_category description] This sentence search the id of last
             * register stored into DataBase
             * @var [Integer]
             */
            $id_category = Category::all()->last()->id;

            /**
             * [Store the every images of the product]
             * @var [file]
             */
            $file = $request->file('image');

            if (!empty($file)) {
                ResourceFunctions::imageChange($id_category, null, $file, 'categories');
            }

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('CategoryController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('CategoryController','store',$e);
        }

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified category.
     *
     * @param  int  $id - (id of the particular category)
     * @return the view(show)
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a CategoryController - show(), del usuario: '.Auth::guard('user')->user()->user_name);

        $category = Category::where('id', $id)->first();

        /*
        *It consult the image of related table
        */
        $image = Media::where(
            [
            'identificator' => $id,
            'table'         => 'categories'
            ])
            ->first();

        return view('admin/categories/show')->with([
            'category'  => $category,
            'image'     => $image
        ]);
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param  int  $id (id of the particular category)
     * @return the view(edit)
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a CategoryController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);

        $category = Category::where('id', $id)->first();

        /*
        *It consult the image of related table
        */
        $image = Media::where([
            'identificator' => $id,
            'table'         => 'categories'
        ])
        ->first();

        return view('admin/categories/edit')->with([
            'category'  => $category,
            'image'     => $image
        ]);
    }

    /**
     * Update the specified category in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @param  int  $id (id of the particular category)
     * @return redirect view(index)
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            /**
             * [Category description] This sentence update the old data by
             * the new data into Category table
             * @var [Category]
             */
            Category::where('id', $id)
            ->update([
                'name'          => $request->name,
                'description'   => $request->description,
                'slug'          => $request->slug,
                'status'        => $request->status,
            ]);

            /**
             * [if description] It verify if the image file is not empty, it
             * update the old image by the new image
             * @var [Request]
             */
            if (!empty($request -> file('image'))) {
                ResourceFunctions::imageChange($id, $request -> id_image, $request -> image, 'categories');
            }

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('CategoryController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('CategoryController','actualizar',$e);
        }

        return redirect()->route('categories.show', $id);
    }

    /**
     * Remove the specified category from storage.
     *
     * @param  int  $id (id of the particular category)
     * @return redirect view(index)
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }
        try {
            DB::beginTransaction();

            Subcategory::where('fk_id_category', $id)->delete();

            Category::destroy($id);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('CategoryController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('CategoryController','eliminar',$e);
        }

        return redirect()->route('categories.index');
    }
}
