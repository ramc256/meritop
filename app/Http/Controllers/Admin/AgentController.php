<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Meritop\Models\Agent;
use Meritop\Http\Assets\ResourceFunctions;

class agentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
       $this->middleware('auth:user');
    }
    
    public function index()
    {
      Log::info('Ingreso exitoso a AgentController - index(), del usuario: '.Auth::guard('user')->user()->user_name);

      $agents = Agent::all();

      return view('admin.agents.index' )->with(['agents'=> $agents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.agents.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
          DB::beginTransaction();

          Agent::create( $request->all() );

          ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

          DB::commit();

          ResourceFunctions::messageSuccess('AgentController','store');

      } catch (\Exception $e) {
          DB::rollBack();
          ResourceFunctions::messageError('AgentController','store',$e);
      }

      return redirect()->route('agents.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      Log::info('Ingreso exitoso a AgentController - show(), del usuario: '.Auth::guard('user')->user()->user_name);

      $agent = Agent::FindOrFail($id);

      return view('admin.agents.show')->with(['agent' => $agent]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

       Log::info('Ingreso exitoso a AgentController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);

       $agent = Agent::FindOrFail($id);

       return view('admin.agents.edit')->with(['agent' => $agent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try {
          DB::beginTransaction();

          Agent::where('id', $id)
          ->update(['name' => $request->name]);

          ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

          DB::commit();

          ResourceFunctions::messageSuccess('AgentController','actualizar');

      } catch (\Exception $e) {
          DB::rollBack();
          ResourceFunctions::messageError('AgentController','actualizar',$e);
      }

      return redirect()->route('agents.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     try {
          DB::beginTransaction();

          Agent::destroy($id);

          ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

          DB::commit();

          ResourceFunctions::messageSuccess('AgentController','eliminar');

      } catch (\Exception $e) {
          DB::rollBack();
          ResourceFunctions::messageError('AgentController','eliminar',$e);
      }

      return redirect()->route('agents.index');
    }
}
