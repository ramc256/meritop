<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Meritop\Models\Order;
use Meritop\Models\ParentProduct;
use Meritop\Models\userClub;
use Meritop\Http\Assets\ResourceFunctions;
use Excel;
use Meritop\Models\Country;
use Meritop\Models\AccountTransaction;
use Meritop\Member;
use Meritop\Models\Memberclub;
use Meritop\Models\MemberGroup;
use Meritop\Models\City;
use Mail;
use Meritop\Mail\NewMemberWelcome;



class DataController extends Controller
{



    public function importExport()
	{
		
		return view('admin.importExport');
	}


	// Method that allows to generate files with extension XLS and CVS
	public function downloadExcel($type,$data,$name_file)
	{
		// dd($name_file);
		// return Excel::create($name_file, function($excel) use ($data) {

		// 	$excel->setTitle("Meritop");

		// 	$excel->sheet('Meritop', function($sheet) use ($data)
	 //        {
	 //        	// Set height for a single row
		// 		$sheet->setHeight(1, 25);

		// 		$sheet->setStyle(array(
		// 		    'font' => array(
		// 		        'name'      =>  'Calibri',
		// 		        'size'      =>  12,
		// 		        'bold'      =>  true
		// 		    )
		// 		));

		// 	$sheet->cells('A1:H1', function($cells){
		// 		$cells->setbackground('#BDBDBD');
		// 	});
		// 		// Sets all borders
		// 		$sheet->setAllBorders('thin');
		// 		$sheet->fromArray($data);

		// 	});
		// })->setCompany('Meritop')->download($type);


		// dd($data);


		$data = Member::all();

		// Excel::create('Filename', function($excel) {

		// })->export('xls');


//dd($data);
		return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);

	        });

			})->export($type);





	}

	//Method that allows to generate files with extension PDF
	public function createPDF($datos, $viewUrl)
	{
		$data = $datos;
		$date = date('y-m-d');
		$view =  \View::make($viewUrl, compact('data', 'date'))->render();
		$pdf = \App::make('dompdf.wrapper');
		$pdf->loadHTML($view);

		return $pdf->download('reporte.pdf');
	}


	public function generateConsult($type,$consult,$id){

		switch ($consult) {
			case 'Orders':

				break;
				
				case 'Transaction':


					// Log::info('Ingreso exitoso a TransactionController - filter(), del usuario: '.Auth::user()->user_name);

			  //       $fechas = ResourceFunctions::getDates($request->date);

			  //       $fecha_inicio = $fechas[0];
			  //       $fecha_fin = $fechas[1];
			  //       // dd($request->all());

			  //       if ($request->operation == null) {
			  //           $operations = [0,1];
			  //       }else {
			  //           $operations = [$request->operation];
			  //       }

			  //       if ($request->fk_id_club == null) {
			  //           $clubs = ResourceFunctions::getClubs();
			  //       }else {
			  //           $clubs = [$request->fk_id_club];
			  //       }

			  //       /*
			  //       *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
			  //       */
			  //       Log::info('Ingreso exitoso a DataController - ExportPDF(), del usuario: '.Auth::user()->user_name);

			  //       $clubs = ResourceFunctions::getClubs();

			  //       $fechas = ResourceFunctions::getDates($request->date);

			  //       $fecha_inicio = $fechas[0];
			  //       $fecha_fin = $fechas[1];
			  //       // dd($request->all());

			  //       if ($request->operation == null) {
			  //           $operations = [0,1];
			  //       }else {
			  //           $operations = [$request->operation];
			  //       }

			  //       if ($request->fk_id_club == null) {
			  //           $clubs = ResourceFunctions::getClubs();
			  //       }else {
			  //           $clubs = [$request->fk_id_club];
			  //       }

			  //       $transactions = AccountTransaction::select('accounts_transactions.*', 'members.dni', 'members.first_name', 'members.last_name')
			  //       ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
			  //       ->join('members', 'members.id', 'accounts.fk_id_member')
			  //       ->whereIn('accounts_transactions.fk_id_club', $clubs)
			  //       ->whereBetween('accounts_transactions.created_at',  [$fecha_inicio, $fecha_fin])
			  //       ->whereIn('accounts_transactions.operation', $operations)
			  //       ->get();

			  //       return $this->downloadExcel($type, $transactions , $consult);

				break;
				default:
				# code...
				break;
		}


		// if (Auth::guard('user')->user()->kind==1) {

		// 	switch ($consult) {
		// 		case 'orders':
		// 			$data = Order::where( 'users_clubs.fk_id_user', Auth::guard('user')->user()->id)->
		// 	            select('orders.id as id','orders.address','orders.status','orders.created_at', 'members.dni','members.first_name','members.last_name')
		// 	            ->join('members','members.id', '=', 'orders.fk_id_member')
		// 	            ->join('members_clubs','members_clubs.fk_id_member', '=', 'members.id')
		// 	            ->join('clubs','clubs.id', '=', 'members_clubs.fk_id_club')
		// 	            ->join('users_clubs','users_clubs.fk_id_club', '=', 'clubs.id')
		// 	            ->get();
		// 	        $name_file = "Orders";
		// 	        $viewUrl = "pdf.reporte_por_pais";
		// 			break;
		// 			case 'members':
		// 				$data = Member::get()->toArray();
		// 			    $name_file = "Members";
		// 			    $viewUrl = "pdf.reporte_por_pais";
		// 			break;

		// 		default:

		// 			$data = Country::get()->toArray();
		// 			$name_file = "Products";
		// 			$viewUrl = "pdf.reporte_por_pais";
		// 		break;
		// 	}

		// }else{

		// 	switch ($consult) {
		// 		case 'orders':
		// 			$data = Order::where( 'clubs.fk_id_country', Auth::guard('user')->user()->fk_id_country)->
		// 	            where( 'members_clubs.fk_id_club', $value->fk_id_club)
		// 	            ->select('orders.id as id','orders.address','orders.status','orders.created_at', 'members.dni','members.first_name','members.last_name')
		// 	            ->join('members','members.id', '=', 'orders.fk_id_member')
		// 	            ->join('members_clubs','members_clubs.fk_id_member', '=', 'members.id')
		// 	            ->join('clubs','clubs.id', '=', 'members_clubs.fk_id_club')
		// 	            ->get();
		// 	        $name_file = "Orders";
		// 	        $viewUrl = "pdf.reporte_por_pais";
		// 			break;

		// 		default:
		// 			$data = Member::get()->toArray();
		// 			$name_file = "Members";
		// 			$viewUrl = "pdf.reporte_por_pais";
		// 			break;
		// 	}





	}

}
