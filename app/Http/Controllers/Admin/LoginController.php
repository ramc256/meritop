<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Meritop\Models\HistoricalLogin;
use Meritop\Models\BannedMember;
use Meritop\Models\RoleModuleAction;
use Meritop\Models\UserClub;
use Meritop\User;
use Session;

class LoginController extends Controller
{

		public function __construct()
		{
			$this->middleware('guest:user',['except' => 'logout']);
		}

		public function showLoginForm()
		{
				return view('admin.auth.login');
		}

		public function login( Request $request){
			/**
			 * Comprobation if the user that try to login is blocked
			 * @var boolean
			 */

			$encontrado = false;
			$banned_members = BannedMember::all();
			foreach ($banned_members as $ip_address_blocked) {
						if ($ip_address_blocked->ip_address == $request->ip()) {
								$encontrado = true;
						}
			}

			if (!$encontrado) {
					// attempt to log the user in
					if (Auth::guard('user')->attempt(['user_name' => $request->user_name, 'password' => $request->password, 'status' => 1, 'deleted' => 0], $request->remember)) {

							Log::info('Ingreso exitoso a LoginControllerAdmin , del usuario: '.Auth::guard('user')->user()->user_name);

							/**
							 * Store a newly created historical of ip address by user in storage.
							 */
							HistoricalLogin::create([
									'ip_address' 	=> $request->ip(),
									'user_name' 	=> Auth::guard('user')->user()->user_name,
							]);

							/*
							*Check the permissions that the user has
							*/
							$role = RoleModuleAction::where('fk_id_role',Auth::guard('user')->user()->fk_id_role)
							->select('*')
							->get();

							/*
							*Consult the clubs related to the user
							*/
							$clubs = UserClub::select('fk_id_club')
							->where('fk_id_user',Auth::guard('user')->user()->id)
							->get();

							/*
							*Saves in the session variable an arrangement with the data consulted, role and clubs related to the user
							*/
							session(['role' => $role, 'clubs'=>$clubs]);

							/*
							*if successful, then redirect to their intdiv(dividend, divisor)tended location
							*/
							
							return redirect()->intended(route('admin.dashboard'));
					}else{
							flash('Usuario  y/o contraseña incorrectos')->error();

						/*
						*if unsuccessful, the rediret back to the login with the form data
						*/
						return redirect()->back()->withInput($request->only('user_name','remember'));
					}

			}else{

				 return back();
			}

		}


		 /**
		 * Log the user out of the application.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @return \Illuminate\Http\Response
		 */


		public function logout()
	    {
	        Auth::guard('user')->logout();
	        return redirect('admin/login');
	    }


		/**
		 * Get the guard to be used during authentication.
		 *
		 * @return \Illuminate\Contracts\Auth\StatefulGuard
		 */
		protected function guard()
		{
			return Auth::guard('user');
		}


}
