<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\AccountTransaction;
use Meritop\Models\Club;
use Meritop\Http\Requests\Admin\Transaction\FilterRequest;

class TransactionController extends Controller
{
    public $module = 1;


    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a TransactionController - index(), del usuario: '.Auth::user()->user_name);

        $id_clubs  = ResourceFunctions::getClubs();

        $accounts_transactions = AccountTransaction::select('accounts_transactions.*','members.id as id_member', 'members.dni', 'members.first_name', 'members.last_name', 'members_clubs.carnet')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->whereIn('accounts_transactions.fk_id_club', $id_clubs)
        ->whereYear('accounts_transactions.created_at', date('Y'))
        ->whereMonth('accounts_transactions.created_at', date('m'))
        ->whereDay('accounts_transactions.created_at', date('d'))
        ->distinct()
        ->orderBy('accounts_transactions.created_at', 'desc')
        ->get();
        
        $clubs = Club::select('clubs.*')->whereIn('id',$id_clubs)->get();

        return view('admin.transactions.index' )
        ->with([
            'accounts_transactions' => $accounts_transactions,
            'clubs'                 => $clubs
        ]);
    }

    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a TransactionController - show(), del usuario: '.Auth::user()->user_name);

        $clubs = ResourceFunctions::getClubs();

        $transaction = AccountTransaction::select('accounts_transactions.*', 'members.dni', 'members.first_name', 'members.last_name')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->whereIn('accounts_transactions.fk_id_club', $clubs)
        ->where('accounts_transactions.id', $id)
        ->distinct()
        ->first();

        return view('admin.transactions.show')->with(['transaction' => $transaction]);
    }

    public function filter(FilterRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a TransactionController - filter(), del usuario: '.Auth::user()->user_name);

        $fechas = ResourceFunctions::getDates($request->date);

        $fecha_inicio = $fechas[0];
        $fecha_fin = $fechas[1];
        // dd($request->all());

        if ($request->operation == null) {
            $operations = [0,1];
        }else {
            $operations = [$request->operation];
        }

        if ($request->fk_id_club == null) {
            $clubs = ResourceFunctions::getClubs();
        }else {
            $clubs = [$request->fk_id_club];
        }

        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $id_clubs = ResourceFunctions::getClubs();

        $accounts_transactions = AccountTransaction::select('accounts_transactions.*', 'members.dni', 'members.first_name', 'members.last_name')
        ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
        ->join('members', 'members.id', 'accounts.fk_id_member')
        ->whereIn('accounts_transactions.fk_id_club', $clubs)
        ->whereBetween('accounts_transactions.created_at',  [$fecha_inicio, $fecha_fin])
        ->whereIn('accounts_transactions.operation', $operations)
        ->get();

        $clubs = Club::select('clubs.*')->whereIn('id',$id_clubs)->get();

        return view('admin.transactions.index' )
        ->with([
            'accounts_transactions' => $accounts_transactions,
            'clubs'                 => $clubs
        ]);
    }

}
