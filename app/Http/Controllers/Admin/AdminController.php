<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\Order;
use Meritop\Models\Media;
use Meritop\Models\ParentProduct;
use Meritop\Member;
use Meritop\User;
use Storage;
use Google\Cloud\Storage\StorageClient;
use League\Flysystem\Filesystem;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;

class AdminController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth:user');
    }


    public function index()
	{
		$clubs = ResourceFunctions::getClubs();

    	$members = Member::select('members.*')
		->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
		->whereIn('members_clubs.fk_id_club', $clubs)
		->get()
		->count();
    	$orders = Order::all()->where('status', 0)->count();
    	$parents_products = ParentProduct::all()->count();

    	return view('admin.index')->with(['members' => $members, 'orders' => $orders, 'parents_products' => $parents_products]);
    }

    public function imageDestroy(Request $request)
    {
		$filesystem = ResourceFunctions::filesystemGoogle();

		$url_cdn = $_ENV["STORAGE_ENV"];
		$id = $request->id;
        try {
            DB::beginTransaction();
            // dd($request -> id);
            $media = Media::FindOrFail($id);

            if (!empty($media -> image) ) {
				if ($filesystem->has($url_cdn . $media -> image)) {
                    $filesystem->delete($url_cdn . $media -> image);
                }
            }

            Media::destroy($id);

            ResourceFunctions::createHistoricalAction('actualizar','Se elimino el registro satisfactoriamente: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('AdminController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('AdminController','actualizar',$e);
        }

        return back();
    }


    public function imageUpdate(Request $request){
        try{
            DB::beginTransaction();

	        $filesystem = ResourceFunctions::filesystemGoogle();

            $id = $request -> id;
            $disk = $request -> disk;
            $identificator = $request -> identificator;

            //Recibimos el archivo enviado mediante el formulario
            $file = $request -> file('image'); // Asignamos el archivo a una valiable
            $file_name = time() . mt_rand() .'.'. $file -> getClientOriginalExtension(); //Asignamos un nuevo nombre al archivo
			$url_cdn = $_ENV["STORAGE_ENV"];
			$url_image = 'image/'. $disk .'/'.$file_name;

            if ($id == null) {

                $result = Media::create([
                    'identificator' => $identificator,
                    'table'         => $disk,
                    'image'         => $url_image,
					'slug'			=> $file_name
                ]);

            }else{

                $media = Media::FindOrFail($id);

				if (!empty($media -> image) ) {
					if ($filesystem->has($url_cdn . $media -> image)) {
	                    $filesystem->delete($url_cdn . $media -> image);
	                }
	            }
                $result = Media::where('id', $id) -> update(['image' => $url_image, 'slug' => $file_name]);
            }

			// write a file
	        $filesystem->write($url_cdn . 'image/'. $disk . '/' . $file_name, file_get_contents($file));

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->slug);

            DB::commit();

            ResourceFunctions::messageSuccess('AdminController','store');

        }catch(\Exception $e){
            DB::rollBack();
            ResourceFunctions::messageError('AdminController','store',$e);
        }

        return back();
    }

}
