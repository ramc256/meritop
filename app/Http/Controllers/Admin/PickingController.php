<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\Order;
use Meritop\Models\Club;
use Meritop\Models\OrderProduct;

class PickingController extends Controller
{
    public function index()
    {
        Log::info('Ingreso exitoso a PickingController - index(), del usuario: '.Auth::user()->user_name);
        /*
         *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
         */
        $clubs = Club::all();

        $orders = DB::table('orders')
        ->select(DB::raw("orders.id as num_order,  orders.status, members.dni,  members.first_name, members.last_name,
        members.cell_phone, branches.*, states.name as state, cities.name as city,  clubs.name as club,
		(select sum(orders_products.quantity)
		from orders_products
		where orders_products.fk_id_order = orders.id
		) as total"))
        ->join('members', 'members.id', 'orders.fk_id_member')
        ->join('branches', 'branches.id', 'orders.fk_id_branch')
        ->join('clubs', 'clubs.id', 'branches.fk_id_club')
        ->join('cities', 'cities.id', 'branches.fk_id_city')
        ->join('states', 'states.id', 'cities.fk_id_state')
        ->where('orders.status', '0')
        ->orderBy('members.dni', 'ASC')
        ->get();

        $orders_products = OrderProduct::select('orders.id as num_order',  'orders.status', 'parents_products.name as product',
        'products.sku', 'products.feature', 'orders_products.quantity')
        ->join('orders', 'orders.id', 'orders_products.fk_id_order')
        ->join('products', 'products.id', 'orders_products.fk_id_product')
        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        ->join('members', 'members.id', 'orders.fk_id_member')
        ->where('orders.status', '0')
        ->orderBy('members.dni', 'ASC')
        ->get();

        return view('admin.picking.index')->with([
            'orders' => $orders,
            'orders_products' => $orders_products,
            'clubs' => $clubs
        ]);
    }
}
