<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Objetive;
use Meritop\Models\Perspective;
use Meritop\Models\Program;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Requests\Admin\Objetive\ObjetiveCreateRequest;
use Meritop\Http\Requests\Admin\Objetive\ObjetiveUpdateRequest;

class ObjetiveController extends Controller
{
    public $module = 6;
    /**
     * Display a listing of the objetives.
     *
     * @return the view(index)
     */
     public function __construct()
    {
       $this->middleware('auth:user');
       $this->middleware('what_kind', ['except' => ['index','show']]);
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new Objetive.
     *
     * @return the view(create)
     */
    public function create(Request $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('programs.show', $request->id);
        }

        Log::info('Ingreso exitoso a ObjetiveController - create(), del usuario: '.Auth::guard('user')->user()->user_name);

        $perspectives = Perspective::all();

        return view('admin/objetives/create')->with(['perspectives' => $perspectives, 'program' => $request -> id]);
    }

    /**
     * Store a newly created Objetive in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @return redirect view(index)
     */
    public function store(ObjetiveCreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.programs.show');
        }


        try {
            DB::beginTransaction();

            $request -> merge(['status' => 0]);

            $objetive = Objetive::create($request -> all());

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ObjetiveController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ObjetiveController','store',$e);
        }

        return redirect()->route('programs.show',$request->fk_id_product);
    }

    /**
     * Display the specified Objetive.
     *
     * @param  int  $id - (id of the particular Objetive)
     * @return the view(show)
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a ObjetiveController - show(), del usuario: '.Auth::guard('user')->user()->user_name);

        $objetive = Objetive::select('objetives.*',  'perspectives.id as id_perspective', 'perspectives.name as perspective', 'programs.id as id_program', 'programs.name as program')
        ->join('perspectives', 'perspectives.id', 'objetives.fk_id_perspective')
        ->join('programs', 'programs.id', 'objetives.fk_id_program')
        ->where('objetives.id', $id)
        ->first();

        return view('admin/objetives/show')->with(['objetive' => $objetive]);
    }

    /**
     * Show the form for editing the specified Objetive.
     *
     * @param  int  $id (id of the particular Objetive)
     * @return the view(edit)
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a ObjetiveController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);

        $objetive = Objetive::select('objetives.*',  'perspectives.id as id_perspective', 'perspectives.name as perspective', 'programs.id as id_program', 'programs.name as program')
        ->join('perspectives', 'perspectives.id', 'objetives.fk_id_perspective')
        ->join('programs', 'programs.id', 'objetives.fk_id_program')
        ->where('objetives.id', $id)
        ->first();

        $perspectives = Perspective::all();

        return view('admin/objetives/edit')->with(['objetive' => $objetive, 'perspectives' => $perspectives]);
    }

    /**
     * Update the specified Objetive in storage.
     *
     * @param  \Illuminate\Http\Request - $request (all parameters from the view)
     * @param  int  $id (id of the particular Objetive)
     * @return redirect view(index)
     */
    public function update(ObjetiveUpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            Objetive::FindOrFail($id)
                 ->update($request->all());

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ObjetiveController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ObjetiveController','actualizar',$e);
        }

     return redirect()->route('objetives.show',$id);
    }

    /**
     * Remove the specified Objetive from storage.
     *
     * @param  int  $id (id of the particular Objetive)
     * @return redirect view(index)
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            $id_program = Objetive::FindOrFail($id);
            Objetive::where('id',$id)
            ->update([
                'status' => false,
                'deleted' => true
            ]);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('ObjetiveController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();

            ResourceFunctions::messageError('ObjetiveController','eliminar',$e);
        }

        return redirect()->route('programs.show', $id_program->fk_id_program);
    }

    /**
     * Show the form for creating a new Objetive.
     *
     * @return the view(create)
     */
    public function newcreate(Request $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a ObjetiveController - create(), del usuario: '.Auth::guard('user')->user()->user_name);
        $perspectives = Perspective::all();

        return view('admin/objetives/create')->with(['perspectives' => $perspectives, 'program' => $id]);
    }
}
