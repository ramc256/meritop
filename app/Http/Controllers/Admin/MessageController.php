<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\Message;
use Meritop\Models\Response;
use Meritop\Member;
use Mail;
use Meritop\Mail\NewAdminResponse;
use Meritop\Http\Requests\Admin\Message\CreateRequest;

class MessageController extends Controller
{
    public $module = 21;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        $clubs = ResourceFunctions::getClubs();

        $open = Message::select('messages.*', 'members.id as id_member', 'members.first_name',
        'members.last_name', 'members.cell_phone', 'members.email', 'clubs.name as club')
        ->join('members', 'members.id', 'messages.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->where('messages.status', 1)
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->orderBy('messages.created_at', 'desc')
        ->paginate(10);

        $close = Message::select('messages.*', 'members.id as id_member', 'members.first_name',
        'members.last_name', 'members.cell_phone', 'members.email', 'clubs.name as club')
        ->join('members', 'members.id', 'messages.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->where('messages.status', 0)
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->orderBy('messages.created_at', 'desc')
        ->paginate(10);

        return view('admin.messages.index')->with(['open' => $open, 'close' => $close]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.index');
        }

        $clubs = ResourceFunctions::getClubs();

        $message = Message::select('messages.*', 'members.id as id_member', 'members.first_name',
        'members.last_name', 'members.cell_phone', 'members.email', 'clubs.name as club')
        ->join('members', 'members.id', 'messages.fk_id_member')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
        ->where('messages.id' , $id)
        ->whereIn('members_clubs.fk_id_club', $clubs)
        ->first();

        $responses = Response::where(['fk_id_message' => $message -> id])->orderBy('created_at', 'asc')->paginate(10);

        return view('admin.messages.show')->with(['message' => $message, 'responses' => $responses]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            Response::where('fk_id_message', $id)->delete();

            Message::destroy($id);

            ResourceFunctions::createHistoricalAction('destroy','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('MessageController','destroy');

        } catch (\Exception $e) {

            DB::rollBack();

            ResourceFunctions::messageError('MessageController','destroy',$e);
        }

        return redirect()->route('messages.index');
    }

    public function response(CreateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            $request->request->add(['fk_id_message' => $id]);
            $request->request->add(['name' => Auth::guard('user')->user()-> first_name . ' ' . Auth::guard('user')->user()-> last_name]);

            Response::create( $request -> all() );

            ResourceFunctions::createHistoricalActionMember('store','Se ha generado una orden por el usuario: '.$request->email);

            DB::commit();

            /*
            *Allows to send an email when the new registered user
            */
            $member = Member::select('members.*')
            ->join('messages', 'messages.fk_id_member', 'members.id')
            ->where('messages.id', $id)
            ->first();

            Mail::to($member -> email)->send(new NewAdminResponse($member -> first_name . ' ' . $member -> last_name));

            ResourceFunctions::messageSuccess('MessageController','store');

        } catch (\Exception $e) {
            DB::rollBack();

            ResourceFunctions::messageError('MessageController','store',$e);
        }

        return redirect()->route('messages.show', $id);
    }

    public function responseDestroy(Request $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            $message = Response::FindOrFail($request -> id_response);
            Response::destroy($request -> id_response);

            ResourceFunctions::createHistoricalAction('destroy','Se ha eliminado el registro: '.$request -> id_response);

            DB::commit();

            ResourceFunctions::messageSuccess('MessageController','destroy');

        } catch (\Exception $e) {

            DB::rollBack();

            ResourceFunctions::messageError('MessageController','destroy',$e);
        }

        return redirect()->route('messages.show', $message->fk_id_message);
    }

    public function updateStatus(Request $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ( (ResourceFunctions::validationReturn($this->module,4)) !=true) {
             return back();
        }

        $message = Message::FindOrFail($request -> id_message);

        $result = ResourceFunctions::changeStatus('messages', $message, $request -> id_message);

        if ($result) {
            ResourceFunctions::createHistoricalAction('change','Se ha eliminado el registro: '.$request -> id_response);

            ResourceFunctions::messageSuccess('MessageController','change');
        }else {
            ResourceFunctions::messageError('MessageController','change',"Ha ocurrido un error al intentar actualizar el status");
        }

        return back();
    }
}
