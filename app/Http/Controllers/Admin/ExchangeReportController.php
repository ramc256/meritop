<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\Order;
use Meritop\User;
use Meritop\Models\Club;
use Meritop\Models\OrderProduct;
use Meritop\Models\Account;
use Meritop\Member;
use Excel;


class ExchangeReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        // if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
        //     return redirect()->route('admin.dashboard');
        // }

        Log::info('Ingreso exitoso a UserController - index(), del usuario: '.Auth::user()->user_name);

        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */




        // $users_clubs = Club::select('clubs.*')
        // ->whereIn( 'clubs.id', $clubs)
        // ->get();


        // $orders = Order::select('orders.*', 'members.dni','members.first_name','members.last_name','clubs.name as club','cities.name as city','states.name as state','branches.name as branch','orders_products.quantity','orders_products.points','parents_products.name as product','products.sku')
        // ->join('members','members.id', 'orders.fk_id_member')
        // ->join('branches','branches.id', 'orders.fk_id_branch')
        // ->join('cities','cities.id', 'branches.fk_id_city')
        // ->join('states','states.id', 'cities.fk_id_state')
        // ->join('clubs','clubs.id', 'branches.fk_id_club')
        // ->join('orders_products','orders_products.fk_id_order', 'orders.id')
        // ->join('products','products.id', 'orders_products.fk_id_product')
        // ->join('parents_products','parents_products.id', 'products.fk_id_parent_product')
        // ->whereIn( 'branches.fk_id_club', $clubs)
        // ->get();

       //  $canjes = DB::table('accounts_transactions')
       // ->select(DB::raw('accounts_transactions.created_at,accounts.fk_id_member,COUNT(accounts_transactions.id),SUM(accounts_transactions.points) as total_trades'))
       // ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
       // ->join('members', 'members.id', 'accounts.fk_id_member')
       // ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
       // ->where('accounts_transactions.operation', 0)
       // ->where('members_clubs.fk_id_club' , 2)
       // ->whereBetween('accounts_transactions.created_at', ['2017-10-02 00:00:00' , '2017-10-29 11:59:59'])
       // ->groupBy('accounts.fk_id_member','accounts_transactions.created_at')
       // ->orderBy('accounts_transactions.created_at', 'DESC')
       // ->get()

        $clubs = ResourceFunctions::getClubs();


        $orders = Order::select('orders.*', 'members.dni','members.first_name','members.last_name','clubs.name as club','cities.name as city','states.name as state','branches.name as branch','orders_products.quantity','orders_products.points','parents_products.name as product','products.sku','brands.name as brand')
        ->join('members','members.id', 'orders.fk_id_member')
        ->join('branches','branches.id', 'orders.fk_id_branch')
        ->join('cities','cities.id', 'branches.fk_id_city')
        ->join('states','states.id', 'cities.fk_id_state')
        ->join('clubs','clubs.id', 'branches.fk_id_club')
        ->join('orders_products','orders_products.fk_id_order', 'orders.id')
        ->join('products','products.id', 'orders_products.fk_id_product')
        ->join('parents_products','parents_products.id', 'products.fk_id_parent_product')
        ->join('brands','brands.id', 'parents_products.fk_id_brand')
        ->whereIn( 'branches.fk_id_club', $clubs)
        ->get();
        // dd($orders);

        foreach ($orders as $key => $value) {
            dd($value);
        }
       
       //  $canjes = DB::table('accounts_transactions')
       //  ->select(DB::raw('COUNT(accounts_transactions.id),accounts.fk_id_member,SUM(accounts_transactions.points) as total_trades'))
       //  ->join('accounts', 'accounts.id', 'accounts_transactions.fk_id_account')
       // ->join('members', 'members.id', 'accounts.fk_id_member')
       // ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
       // ->where('accounts_transactions.operation', 0)
       // ->where('members_clubs.fk_id_club' , 2)
       // ->whereBetween('accounts_transactions.created_at', ['2017-10-02 00:00:00' , '2017-10-29 11:59:59'])
       // ->groupBy('accounts.fk_id_member')
       // ->orderBy('accounts.created_at', 'DESC')
       // ->orderBy('accounts_transactions.created_at', 'DESC')
       // ->get();

         
       //   $data = DB::table('orders_products')
       //  ->select(DB::raw('members.dni,members_clubs.carnet, members.first_name, members.last_name, clubs.name as club, branches.name as branch ,cities.name as city,states.name as state,parents_products.name as product, count(orders_products.quantity) as cantidad, sum(orders_products.points) as puntos'))        
       // ->join('orders', 'orders.id', 'orders_products.fk_id_order')
       // ->join('products', 'products.id', 'orders_products.id')
       // ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
       // ->join('members', 'members.id', 'orders.fk_id_member')
       // ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
       // ->join('clubs', 'clubs.id', 'members_clubs.fk_id_club')
       // ->join('branches','branches.id', 'orders.fk_id_branch')
       // ->join('cities','cities.id', 'branches.fk_id_city')
       //  ->join('states','states.id', 'cities.fk_id_state')
       // ->where('members_clubs.fk_id_club' , 2)
       // ->whereBetween('orders_products.created_at', ['2017-10-02 00:00:00' , '2017-10-29 11:59:59'])
       // ->groupBy('members.dni','members_clubs.carnet', 'members.first_name', 'members.last_name', 'clubs.name','branches.name','cities.name','states.name','parents_products.name')
       // ->orderBy('orders_products.created_at', 'DESC')
       // ->get();
       

       
        // $data = [];
        // $items = OrderProduct::select('members.id','members.dni','members_clubs.carnet', 'members.first_name', 'members.last_name', 'count(orders_products.quantity)', 'count(orders_products.points)')
        
        // ->join('orders', 'orders.id', 'orders_products.fk_id_order')
        // ->join('members', 'members.id', 'orders.fk_id_member')
        // ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        // ->where('members_clubs.fk_id_club' , 2)
        // ->whereBetween('orders_products.created_at', ['2017-10-02 00:00:00' , '2017-10-29 11:59:59'])
        // ->orderBy('orders_products.created_at', 'desc')
        // ->get();
        // dd($items);
       dd($data);
        $i = 0;
        $j = 0; 
        // foreach ($transactions as $key => $value) {

                    
        //     $points=0;
        //     if (in_array($value['dni'], array_column($data, 'dni') ) ==false ) {

        //         foreach ($canjes as $key => $valueCanje) {
        //                 // dd($valueCanje->fk_id_member);
        //             if ( $value['id'] == $valueCanje->fk_id_member ) {
        //                 $points = $valueCanje->total_trades;
        //                 break;
        //             }
        //         }


        //         $data[] = [
        //             'dni'           => $value['dni'],
        //             'carnet'        => $value['carnet'],
        //             'first_name'    => $value['first_name'],
        //             'last_name'     => $value['last_name'],
        //             'canje'         => $points,
        //             'created_at'    => $value['created_at']
        //         ];
        //         // echo 'dni: ' . $value['dni'] . ' carnet: ' . $value['carnet'] . ' nombre: ' . $value['first_name'] . ' '. $value['last_name']. '<br>';  
                               
        //     }    
        // }

        return Excel::create('balance_mensual', function($excel) use ($data) {
        $excel->sheet('mySheet', function($sheet) use ($data)
        {
            $sheet->fromArray($data);

        });

        })->export('csv');



        $members = Member::select('members.dni','members.first_name','members.last_name','members_clubs.carnet','clubs.name','accounts.code')
                ->join('accounts','accounts.fk_id_member','members.id' )
                ->join('members_clubs','members_clubs.fk_id_member','members.id')
                ->join('clubs', 'members_clubs.fk_id_club', 'clubs.id')
                ->where('clubs.id',2)
                ->get();

        $canjes = DB::table('members')
                ->select(DB::raw('members.dni, SUM(accounts_transactions.points) as total'))
                ->join('accounts','accounts.fk_id_member','members.id' )
                ->join('members_clubs','members_clubs.fk_id_member','members.id')
                ->join('clubs', 'members_clubs.fk_id_club', 'clubs.id')
                ->join('accounts_transactions', 'accounts_transactions.fk_id_account','accounts.id')
                ->where('accounts_transactions.operation', 0)
                ->whereBetween('accounts_transactions.created_at', ['2017-10-01 00:00:00' , '2017-11-05 11:59:59'])
                ->groupBy('members.dni')
                ->get()->toArray();

        $aporte = DB::table('members')
                ->select(DB::raw('members.dni,SUM(accounts_transactions.points) as total'))
                ->join('accounts','accounts.fk_id_member','members.id' )
                ->join('members_clubs','members_clubs.fk_id_member','members.id')
                ->join('clubs', 'members_clubs.fk_id_club', 'clubs.id')
                ->join('accounts_transactions', 'accounts_transactions.fk_id_account','accounts.id')
                ->where('accounts_transactions.operation', 1)
                ->whereBetween('accounts_transactions.created_at', ['2017-10-01 00:00:00' , '2017-11-05 11:59:59'])
                ->groupBy('members.dni')
                ->get()->toArray();

        $total_canjes = 0;
        $total_aporte = 0;
        foreach ($members as  $value) {
            $user_canjes = 0;
            $user_aporte = 0;
            if (in_array($value->dni, array_column($canjes, 'dni'))) {
                foreach ($canjes as $key => $val) {
                    if ($val->dni == $value->dni) {
                        $user_canjes = $val->total;
                         break;
                    }

                }

            }
            if (in_array($value->dni, array_column($aporte, 'dni'))) {
                foreach ($aporte as $key => $val) {
                    if ($val->dni == $value->dni) {
                        $user_aporte = $val->total;
                         break;
                    }

                }

            }
            $total_canjes += $user_canjes;
            $total_aporte += $user_aporte;

            $data_aporte [] = [

                'nombres' => $value->first_name,
                'apellidos' => $value->last_name,
                'dni' => $value->dni,
                'carnet' => $value->carnet,
                'club'  => $value->name,
                'n_cuenta' => $value->code,
                'canjes' => $user_canjes,
                'aportes' => $user_aporte
            ];

        }
        // echo $total_aporte.", ".$total_canjes."<br>";
        // dd("");
        return Excel::create('aportes_y_canjes', function($excel) use ($data_aporte) {
           $excel->sheet('mySheet', function($sheet) use ($data_aporte)
           {
               $sheet->fromArray($data_aporte);

           });

           })->export('csv');
          
    }


    public function filter (Request $request){

        /**
         * [$date description] Variable that contains the dates of the form with
         * correct format
         * @var [type] Date Array
         */
        $date = ResourceFunctions::getDates($request->date);

        /**
         * [$date description] Variable that contains the filter start date
         * @var [type] Date
         */
        $start_date = $date[0];

        /**
         * [$due_date description] Variable that contains the filter due date
         * @var [type] Date
         */
        $due_date = $date[1];
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();

        $users_clubs = Club::select('clubs.*')
        ->whereIn( 'clubs.id', $clubs)
        ->get();

        $orders = Order::select('orders.*', 'members.dni','members.first_name','members.last_name','clubs.name as club','cities.name as city','states.name as state','branches.name as branch','orders_products.quantity','orders_products.points','parents_products.name as product','products.sku')
        ->join('members','members.id', 'orders.fk_id_member')
        ->join('branches','branches.id', 'orders.fk_id_branch')
        ->join('cities','cities.id', 'branches.fk_id_city')
        ->join('states','states.id', 'cities.fk_id_state')
        ->join('clubs','clubs.id', 'branches.fk_id_club')
        ->join('orders_products','orders_products.fk_id_order', 'orders.id')
        ->join('products','products.id', 'orders_products.fk_id_product')
        ->join('parents_products','parents_products.id', 'products.fk_id_parent_product')
        ->whereBetween('contributions.created_at',  [$start_date, $due_date])
        ->whereIn( 'branches.fk_id_club', $clubs)
        ->get();

        return view('admin.exchange-report.index')->with([
            'orders'        => $orders,
            'users_clubs'   => $users_clubs,
        ]);

    }
   
}
