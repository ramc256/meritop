<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\Banner;
use Meritop\Models\Club;
use Meritop\Models\Media;
use Meritop\Http\Requests\Admin\Banner\CreateRequest;
use Meritop\Http\Requests\Admin\Banner\UpdateRequest;

class BannerController extends Controller
{
    public $module = 16;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a BannerController - index(), del usuario: '.Auth::guard('user')->user()->user_name);

        $clubs = ResourceFunctions::getClubs();

        $banners = Banner::whereIn('banners.fk_id_club', $clubs)->get();

        return view('admin.banners.index')->with(['banners' => $banners]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('brands.index');
        }

        Log::info('Ingreso exitoso a BannerController - create(), del usuario: '.Auth::guard('user')->user()->user_name);

        return view('admin.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('brands.index');
        }

        try {
            DB::beginTransaction();

            $clubs = ResourceFunctions::getClubs();

            $request->request->add(['fk_id_club' => $clubs[0]]);

            Banner::create( $request->all() );
            $id_banner = Banner::all()->last()->id;

            /**
             * [Store the every images of the product]
             * @var [file]
             */
            $file = $request->file('image');

            if (!empty($file)) {
                ResourceFunctions::imageChange($id_banner, null, $file, 'banners');
            }

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('BannerController','store');

        } catch (\Exception $e) {
            DB::rollBack();

            ResourceFunctions::messageError('BannerController','store',$e);
        }

        return redirect()->route('banners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a BannerController - show(), del usuario: '.Auth::guard('user')->user()->user_name);

        $clubs = ResourceFunctions::getClubs();

        $banner = Banner::where('banners.id' , $id)
        ->whereIn('banners.fk_id_club', $clubs)
        ->first();

        $image = Media::where(['identificator' => $id, 'table' => 'banners'])->first();

        return view('admin.banners.show')->with(['banner' => $banner, 'image' => $image]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a BannerController - edit(), del usuario: '.Auth::guard('user')->user()->user_name);

        $clubs = ResourceFunctions::getClubs();

        $banner = Banner::where('banners.id' , $id)
        ->whereIn('banners.fk_id_club', $clubs)
        ->first();

        $image = Media::where(['identificator' => $id, 'table' => 'banners'])->first();

        return view('admin.banners.edit')->with(['banner' => $banner, 'image' => $image]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            Banner::FindOrFail($id)
            ->update( $request->all() );

            if (!empty($request -> file('image'))) {
                ResourceFunctions::imageChange($id, $request -> id_image, $request -> file('image'), 'banners');
            }

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('BannerController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();

            ResourceFunctions::messageError('BannerController','actualizar',$e);
        }

        return redirect()->route('banners.show',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        try {
            DB::beginTransaction();

            $filesystem = ResourceFunctions::filesystemGoogle();

            $media = Media::where(['table' => 'banners', 'identificator' => $id])->first();

            Media::where(['table' => 'banners', 'identificator' => $id])->delete();

            $filesystem->delete($_ENV["STORAGE_ENV"] . $media -> image);

            Banner::destroy($id);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('BannerController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();

            ResourceFunctions::messageError('BannerController','eliminar',$e);
        }

        return redirect()->route('banners.index');
    }

}
