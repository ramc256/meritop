<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Offer;
use Meritop\Models\Category;
use Meritop\Models\affiliate;
use Meritop\Http\Assets\ResourceFunctions;

class OfferController extends Controller
{
    public $module=6;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
       $this->middleware('auth:user');
    }

    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a OfferController - index(), del usuario: '.Auth::user()->user_name);

        $offers = Offer::select( 'offers.id as id', 'offers.name', 'offers.excerpt','offers.description','offers.image', 'offers.quantity','offers.quantity_per_user', 'offers.frequency','offers.start_date', 'offers.status','affiliates.name as affiliate')
        ->join('time_scales','time_scales.id', '=', 'offers.fk_id_time_scale')
        ->join('affiliates','affiliates.id', '=', 'offers.fk_id_affiliate')
        ->get();

        return view('admin.offers.index' )->with(['offers' => $offers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.offers.index');
        }

        Log::info('Ingreso exitoso a OfferController - create(), del usuario: '.Auth::user()->user_name);
        $categories = Category::all();
        $affiliates = Affiliate::all();

        return view('admin.offers.create')->with(['categories' => $categories, 'affiliates' => $affiliates]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            Offer::create($request->all());

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('OfferController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('OfferController','store',$e);
        }



        return redirect()->route('offers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a offerController - show(), del usuario: '.Auth::user()->user_name);

        $offer = Offer::select( 'offer.*','categories.name as category','affiliates.name as affiliate')
        ->join('time_scales','time_scales.id', '=', 'offers.fk_id_time_scale')
        ->join('affiliates','affiliates.id', '=', 'offers.fk_id_affiliate')->where('offers.id',$id)->get();

        return view('admin.offers.show')->with(['offer'=>$offer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a offerController - show(), del usuario: '.Auth::user()->user_name);

       $offer = Offer::select( 'offer.*','categories.name as category','affiliates.name as affiliate')
       ->join('time_scales','time_scales.id', '=', 'offers.fk_id_time_scale')
       ->join('affiliates','affiliates.id', '=', 'offers.fk_id_affiliate')
       ->where('offers.id',$id)->get();

        return view('admin.offers.show')->with(['offer'=>$offer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try {
            DB::beginTransaction();

            $offers = Offer::where('offers.id',$id)
            ->update(['name'=> $request->name,
                     'except'=>$request->except,
                     'description'=> $request->description,
                     'image' =>$request->image,
                     'quantity' => $request->quantity,
                     'quantity_per_user' => $request->quantity_per_user,
                     'frequency' => $request->frequency,
                     'start_date' => $request->start_date,
                     'due_hour' => $request->due_hour,
                     'status' => $request->status,
                     ]);

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('OfferController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('OfferController','actualizar',$e);
        }

        return redirect()->route('offers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,5)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a destryController - destroy(), del usuario: '.Auth::user()->user_name);

        try {
            DB::beginTransaction();

            $offer = Offer::FindOrFail($id);
            Offer::destroy($id);

            ResourceFunctions::createHistoricalAction('eliminar','Se ha eliminado el registro: '.$id);

            DB::commit();

            ResourceFunctions::messageSuccess('OfferController','eliminar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError($offer->name,'OfferController','eliminar',$e);
        }
        return redirect()->route('offers.index');
    }
}
