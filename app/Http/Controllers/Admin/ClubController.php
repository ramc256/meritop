<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Meritop\Http\Controllers\Controller;
use Meritop\Models\Club;
use Meritop\Models\UserClub;
use Meritop\Models\ClubParentProduct;
use Meritop\Models\ParentProduct;
use Meritop\Models\Country;
use Meritop\Models\Currency;
use Meritop\Models\State;
use Meritop\Models\City;
use Meritop\Models\Media;
use Meritop\Models\Brand;
use Meritop\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Meritop\Http\Assets\ResourceFunctions;
use Storage;
use Meritop\Http\Requests\Admin\Club\ClubCreateRequest;
use Meritop\Http\Requests\Admin\Club\ClubUpdateRequest;

class ClubController extends Controller
{
    public $module = 13;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
         /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a ClubController - index(), del usuario: '.Auth::user()->user_name);
        /*
        *consult the clus Of the country related to the user
        */
        $clubs = Club::select('clubs.*', 'countries.name as country')
        ->join('countries','countries.id','clubs.fk_id_country')->get();
        /*
        *number of clubs
        */
        $num_clubs = $clubs -> count();

        $countries = Country::all();

        return view('admin.clubs.index')->with([
            'clubs'     => $clubs,
            'num_clubs' => $num_clubs,
            'countries' => $countries
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.clubs.index');
        }

        Log::info('Ingreso exitoso a ClubController - create(), del usuario: '.Auth::user()->user_name);
        /*
        *Function that takes from the session variable the ids of the clubs related to the user, and turns them into an array
        */
        $clubs = ResourceFunctions::getClubs();
        /*
        *consult the countries
        */
        $countries = Country::orderBy('name', 'asc')->get();
        $currencies = Currency::orderBy('currency_code', 'asc')->get();

        $parents_products = ParentProduct::select('parents_products.*','categories.name as category')
        ->join('parents_products_subcategories', 'parents_products_subcategories.fk_id_parent_product', 'parents_products.id')
        ->join('subcategories', 'subcategories.id', 'parents_products_subcategories.fk_id_subcategory')
        ->join('categories', 'categories.id', 'subcategories.fk_id_category')
        ->where('parents_products.deleted', false)
        ->where('parents_products.fk_id_country', Auth::user()->fk_id_country)
        ->get();
        /*
        *It consult all brands
        */
        $categories = Category::all();

        return view('admin.clubs.create')->with([
            'countries'         => $countries,
            'currencies'        => $currencies,
            'parents_products'  => $parents_products,
            'categories'        => $categories,
        ]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(ClubCreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        try{
            DB::beginTransaction();

            /*
            *insert a new register of club
            */
            $club = Club::create($request -> all());

            UserClub::create(['fk_id_user' => 1, 'fk_id_club' => $club->id]);

            /**
             * [Store the every images of the product]
             * @var [file]
             */
            $logo_file = $request->file('logo_image');

            if (!empty($logo_file)) {
                ResourceFunctions::imageChange($club->id, null, $logo_file, 'clubs');
            }

            /**
             * [Store the every images of the product]
             * @var [file]
             */
            $login_file = $request->file('login_image');

            if (!empty($login_file)) {
                ResourceFunctions::imageChange($club->id, null, $login_file, 'login');
            }

            /*
            *Code that allows you to relate the registered club to the parents_products
            */

            if (!empty($club) && ( $club==true ) && ( !empty($request->parents_products) )) {
                /*
                *consult the parents_products related to the country of club
                */
                $i=0;
                $clubs_parents_products = null;


                foreach ($request->parents_products as $campo => $dato) {

                    $clubs_parents_products[$i] = [
                        'fk_id_club'            => $club->id,
                        'fk_id_parent_product'  => $dato,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s')
                    ];
                    $i++;
                }//forech close

                DB::table('clubs_parents_products')->insert($clubs_parents_products);

            }

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('ClubController','store');

            } catch (\Exception $e) {
                DB::rollBack();
                ResourceFunctions::messageError('ClubController','store',$e);
            }

        return redirect()->route('clubs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        Log::info('Ingreso exitoso a ClubController - show(), del usuario: '.Auth::user()->user_name);
        /*
        *consult the club
        */
        $club = Club::select('clubs.*','countries.name as country', 'currencies.currency')
        ->join('countries','countries.id','clubs.fk_id_country')
        ->join('currencies', 'currencies.id', 'clubs.fk_id_currency')
        ->where([
            'clubs.id' => $id,
        ])
        ->first();
        /*
        *consult the image related club
        */
        $logo_image = Media::select('image')
        ->where([
            'identificator' => $id,
            'table'         => 'clubs'
        ])
        ->first();

        /*
        *consult the image related club
        */
        $login_image = Media::select('image')
        ->where([
            'identificator' => $id,
            'table'         => 'login'
        ])
        ->first();

        return view('admin.clubs.show')->with([
            'club'          => $club,
            'logo_image'    => $logo_image,
            'login_image'   => $login_image
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a ClubController - edit(), del usuario: '.Auth::user()->user_name);
        /*
        *consult the club
        */
        $club = Club::select('clubs.*','countries.name as country')
        ->join('countries','countries.id','clubs.fk_id_country')
        ->where([
            'clubs.id' => $id,
        ])
        ->first();

        /*
        *consult the image related club
        */
        $logo_image = Media::where([
            'identificator'     => $id,
            'table'             => 'clubs',
        ])
        ->first();

        /*
        *consult the image related club
        */
        $login_image = Media::where([
            'identificator' => $id,
            'table'         => 'login'
        ])
        ->first();
        /*
        *consult the countries
        */
        $countries = Country::orderBy('name', 'asc')->get();
        $currencies = Currency::orderBy('currency_code', 'asc')->get();

        return view('admin.clubs.edit')->with([
            'club'                      => $club,
            'countries'                 => $countries,
            'currencies'                => $currencies,
            'logo_image'                => $logo_image,
            'login_image'               => $login_image,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClubUpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,4)) !=true) {
            return redirect()->route('admin.dashboard');
        }
        // dd($request->all());

        try {
            DB::beginTransaction();
            /*
            *update club data
            */
            // dd($request->all());
            $club=Club::FindOrFail($id)
             ->update($request->all());

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            if (!empty($request -> file('logo_image'))) {

                ResourceFunctions::imageChange($id, $request -> id_logo_image, $request -> file('logo_image'), 'clubs');
            }

            if (!empty($request -> file('login_image'))) {
                ResourceFunctions::imageChange($id, $request -> id_login_image, $request -> file('login_image'), 'login');
            }

            DB::commit();

            ResourceFunctions::messageSuccess('ClubController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('ClubController','actualizar',$e);
        }

        return redirect()->route('clubs.show', $id);
    }

}
