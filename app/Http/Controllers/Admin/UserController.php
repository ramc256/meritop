<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Request\ImageRequest;
use Meritop\Http\Requests\Admin\User\UserCreateRequest;
use Meritop\Http\Requests\Admin\User\UserUpdateRequest;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Http\Assets\ImageClass;
use Meritop\Models\Club;
use Meritop\Models\Role;
use Meritop\Models\Media;
use Meritop\Models\UserClub;
use Meritop\Models\Country;
use Meritop\User;
use Session;
use Storage;
use Google\Cloud\Storage\StorageClient;
use League\Flysystem\Filesystem;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;

class UserController extends Controller
{
    public $module = 17;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,1)) !=true) {
            return redirect()->route('admin.dashboard');
        }

        $users = User::select('users.*', 'roles.name as role','countries.name as country')
        ->join('roles','roles.id', 'users.fk_id_role')
        ->join('countries','countries.id','users.fk_id_country')
        ->where('users.id', '>', 1)
        ->get();

        $clubs = Club::all();
        $roles = Role::all();
        $countries = Country::all();

        return view('admin.users.index' )->with([
            'users'     => $users,
            'clubs'     => $clubs,
            'roles'     => $roles,
            'countries' => $countries
        ]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return back();
        }

        Log::info('Ingreso exitoso a UserController - create(), del usuario: '.Auth::user()->user_name);

        $clubs = Club::all();
        $roles = Role::all();
        $countries = Country::orderBy('name', 'asc')->get();

        return view('admin.users.create')->with([
            'clubs'     => $clubs,
            'roles'     => $roles,
            'countries' => $countries
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,3)) !=true) {
            return redirect()->route('users.index');
        }

        try{
            DB::beginTransaction();

            $request -> merge(['password' => bcrypt($request->password)]);

            $user = User::create($request -> all());

            /*
            *runs the array club passed by request for insert in an array, and later insert in the BD
            */
            foreach ($request->clubs as $key => $value) {
                $data_insert[$key]=[
                    'fk_id_user'    => $user->id,
                    'fk_id_club'    => $value,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s')
                ];
            }

            UserClub::insert($data_insert);

            ResourceFunctions::createHistoricalAction('crear','Ha realizado el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('UserController','store');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('UserController','store',$e);
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ((ResourceFunctions::validationReturn($this->module,2)) !=true) {
            return redirect()->route('admin.index');
        }

        Log::info('Ingreso exitoso a UserController - show(), del usuario: '.Auth::user()->user_name);

        $user = User::select('users.*', 'roles.name as role','countries.name as country')
            ->join('countries', 'countries.id', 'users.fk_id_country')
            ->join('roles','roles.id' , 'users.fk_id_role')
            ->where([
                'users.id' => $id,
            ])
            ->first();

        $users_clubs = UserClub::where('fk_id_user', $id)
        ->join('clubs', 'clubs.id', 'users_clubs.fk_id_club')->get();

        return view('admin.users.show')->with([
            'user'          => $user,
            'users_clubs'   => $users_clubs,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ( (ResourceFunctions::validationReturn($this->module,3)) !=true) {
             return back();
        }

        Log::info('Ingreso exitoso a UserController - edit(), del usuario: '.Auth::user()->user_name);

        $clubs = Club::all();
        $roles = Role::all();
        $user = User::select('users.*', 'roles.name as role','countries.name as country')
            ->join('countries', 'countries.id', '=', 'users.fk_id_country')
            ->join('roles','roles.id' ,'=', 'users.fk_id_role')
            ->where([
                'users.id' => $id,
            ])
            ->first();
        /*
        *consult the image related user
        */
        $image = Media::select('image')
        ->where([
            'identificator' => $id,
            'table'         => 'users'
        ])
        ->first();

        $countries = Country::orderBy('name', 'asc')->get();
        $users_clubs = UserClub::select('users_clubs.fk_id_user as fk_id_user', 'users_clubs.fk_id_Club as fk_id_club', 'clubs.alias as alias')
        ->join('clubs','clubs.id' , '=', 'users_clubs.fk_id_club')
        ->where('users_clubs.fk_id_user', $id)
        ->get()->toArray();

        $clubs_final = ResourceFunctions::checksSeleccionados($clubs, $users_clubs, 'fk_id_club', 'alias');

        return view('admin.users.edit')->with([
            'user'          => $user,
            'clubs'         => $clubs,
            'roles'         => $roles,
            'clubs_final'   => $clubs_final,
            'countries'     => $countries
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        /*
        *Method that allows the validation of privileges
        */
        if ( (ResourceFunctions::validationReturn($this->module,4)) !=true) {
             return back();
        }

        try {
            DB::beginTransaction();

            $user = User::where('users.id',$id)
            ->update([
                'first_name'    => $request -> first_name,
                'last_name'     => $request -> last_name,
                'user_name'     => $request -> user_name,
                'email'         => $request -> email,
                'kind'          => $request -> kind,
                'status'        => $request -> status,
                'fk_id_role'    => $request -> fk_id_role,
                'fk_id_country' => $request -> fk_id_country,
                'updated_at'    => date('Y-m-d H:i:s')
            ]);

            $rma = UserClub::select('*')
                ->where('fk_id_user',  $id);
            $rma -> delete();

             /*
             *runs the array club passed by request for insert in an array, and later insert in the BD the dates updated
             */
            foreach ($request->clubs as $key => $value) {
                $data_insert[$key]=[
                'fk_id_user' => $id,
                'fk_id_club' => $value,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            }

            UserClub::insert($data_insert);

            ResourceFunctions::createHistoricalAction('actualizar','Se ha realizado una actualización sobre el registro: '.$request->name);

            DB::commit();

            ResourceFunctions::messageSuccess('UserController','actualizar');

        } catch (\Exception $e) {
            DB::rollBack();
            ResourceFunctions::messageError('UserController','actualizar',$e);
        }

        return redirect()->route('users.index');

    }


}
