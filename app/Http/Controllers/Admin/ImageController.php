<?php

namespace Meritop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Meritop\Http\Controllers\Controller;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\Media;
use Meritop\Models\Club;
use Meritop\User;
use Meritop\Member;
use Meritop\Http\Requests\ImageRequest;

class ImageController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageRequest $request)
    {
        try{
            DB::beginTransaction();
            //Recibimos el archivo enviado mediante el formulario
            $file = $request -> file('image'); // Asignamos el archivo a una valiable
            $type_file = $file -> getClientOriginalExtension(); //Obtenemos la extención del archivo
            $file_name = time() . mt_rand() . $type_file; //Asignamos un nuevo nombre al archivo

            $file -> move('images/'.$request->disk, $file_name );
            Media::insert([
                'image' => $file_name,
                'slug' => $file_name,
                'identificator' => $request -> identificator,
                'table' => $request -> table,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            DB::commit();

            Log::notice('Proceso exitoso: Image metodo store del usuario: '.Auth::user()->user_name);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::critical('Ha ocurrido un error al intentar realizar el proceso:  para el registro ' . ' -----> [' . $e . ']' . ' || Image-'. $metode.'(), del usuario: '.Auth::user()->user_name);
        }

        return route($request -> table.'.show', $request -> identificator);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {

    //     $image = Media::FindOrFail($id);
        
    // }

  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id,$disk)
    // {
    //     try {

    //         $image = Media::FindOrFail($id);

    //         if (!empty($image -> image)) {
    //             if (Storage::disk($request->disk) -> has($image -> image)) {
    //                 Storage::disk($request->disk) -> delete($image -> image);
    //             }
    //         }

    //         $delete = Media::destroy($id);

    //         Log::notice('Proceso exitoso: Image metodo store del usuario: '.Auth::user()->user_name);
    //     } catch (\Exception $e) {
    //         Log::critical('Ha ocurrido un error al intentar realizar el proceso:  para el registro ' . ' -----> [' . $e . ']' . ' || Image-'. $metode.'(), del usuario: '.Auth::user()->user_name);
    //     }

       
    // }
}
