<?php 

//DATA BASE CONFIG
define('HOST', 'localhost');
define('DATABASE', 'meritop');
define('USER', 'root');
define('PASSWORD', '');
define('CHARSET', 'utf-8');

$db = new mysqli(HOST, USER, PASSWORD, DATABASE) or die('Error al conectar con la Base de Datos') ;
$db -> query('SET NAMES utf-8');

$clubs = $db -> query('SELECT  * FROM clubs;');

foreach ($clubs as $club) {
	echo $club['id'] . ' - ' . $club['name'] . '<br>';

	$members = $db -> query('SELECT fk_id_club as club, fk_id_member as member
	FROM members_clubs
    WHERE fk_id_club = ' . $club["id"] . ';');

	if(!empty($members -> fetch_assoc())){
		foreach ($members as $key) {

			$totals[] = $db -> query('SELECT SUM(accounts_transactions.points) as points, members.id as member 
	        FROM accounts_transactions
	        INNER JOIN accounts ON accounts.id = accounts_transactions.fk_id_account
	        INNER JOIN members ON members.id = accounts.fk_id_member
	        INNER JOIN members_clubs ON members_clubs.fk_id_member = members.id
	        WHERE accounts_transactions.fk_id_club = ' . $key["club"] . ' 
	        AND accounts.fk_id_member = ' . $key["member"] . '
	        AND accounts_transactions.operation = 1;') -> fetch_assoc();       
	   		
	    } 
	    
	    foreach ($totals as $key => $row) {
	    	$order[$key] = $row['points'];
		}
    	$ranking = array_multisort($order, SORT_DESC, $totals);

    	$i = 0;
    	$aux = 0;
    	foreach ($totals as $element) {
            if ($element['points'] != $aux) {
                $i++;       
            } 
            $aux = $element['points']; 
            $db -> query('UPDATE members_clubs
				SET rank = ' . $i . '
				WHERE fk_id_member = ' . $element['member'] . ';') or die ($db -> error);

            // TEMPORAL 
            // $element['rank'] = $i;   
            // echo $element["rank"] . ') ' . $element["member"]  . ' - ' . $element["points"] . ' <br>' ;
	 	} 
	}	
}

