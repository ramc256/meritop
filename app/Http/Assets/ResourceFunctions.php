<?php

namespace Meritop\Http\Assets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Meritop\Http\Assets\ResourceFunctions;
use Meritop\Models\HistoricalAction;
use Meritop\Models\Program;
use Meritop\Models\Account;
use Meritop\Models\Inventory;
use Meritop\Models\Product;
use Meritop\Models\Media;
use Meritop\Models\GroupProgram;
use Meritop\Models\State;
use Meritop\Models\AccountTransaction;
use Meritop\Member;
use Carbon\Carbon;
use Storage;
use Google\Cloud\Storage\StorageClient;
use League\Flysystem\Filesystem;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;
use Illuminate\Pagination\Paginator;

class ResourceFunctions
{

    /**
     * [createHistoricalAction description] This function create a new register
     * of any action realized in the system by the admin
     * @param  [String] $action      [Action realized]
     * @param  [String] $description [Action's description]
     * @return [Void]
     */
    public static function createHistoricalAction($action, $description)
    {
        HistoricalAction::insert([
            'id_user' => Auth::guard('user')->user()->id,
            'action' => $action,
            'description' => $description,
            'ip_address' => \Request::ip(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * [createHistoricalAction description] This function create a new register
     * of any action realized in the system by the member
     * @param  [String] $action      [Action realized]
     * @param  [String] $description [Action's description]
     * @return [Void]
     */
    public static function createHistoricalActionMember($action, $description)
    {
        HistoricalAction::insert([
            'id_user' => Auth::user()->id,
            'action' => $action,
            'description' => $description,
            'ip_address' => \Request::ip(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * [messageSuccess: this function set up a success message with the parameters that it receive]
     * @param  [String] $controller [The controller where the function is called]
     * @param  [String] $method     [The method where the function is called]]
     * @return [void]
     */
    public static function messageSuccess($controller, $method)
    {
        \Log::notice('Proceso exitoso: '.' || '.$controller .'-'. $method .'() del usuario: '.Auth::user()->user_name);
        flash('Se ha realizado la operacion solicidata exitosamente', '¡Operación Exitosa!')->success();
    }

    /**
     * [messageError: this function set up a error message with the parameters that it receive]
     * @param  [String] $controller [The controller where the function is called]
     * @param  [String] $method     [The method where the function is called]]
     * @param  [String] $exception  [the obtained exception ]
     * @return [void]
     */
    public static function messageError($controller, $metode, $exception)
    {
        \Log::critical('Ha ocurrido un error al intentar realizar el proceso: '. $metode . ' para el registro ' . ' -----> [' . $exception . ']' . ' || '.$controller . '-'. $metode.'(), del usuario: '.Auth::user()->user_name);
        flash('Ha ocurrido un error al intentar realizar la operación solicitada', '¡Error!')->error();
    }

    public static function messageErrorExportNull($controller, $metode, $exception)
    {
        \Log::critical('El archivo cargado para importar no posee datos: '. $metode . ' para el registro ' . ' -----> [' . $exception . ']' . ' || '.$controller . '-'. $metode.'(), del usuario: '.Auth::user()->user_name);
        flash('El archivo cargado para importar no posee registros', '¡Error!')->error();
    }

     public static function messageErrorColumnNull($controller, $column)
    {
        \Log::critical('El archivo cargado para importar no posee datos:  || '.$controller . '- (), del usuario: '.Auth::user()->user_name);
        flash('las columna '.$column.' no debe tener campos nulos  ¡Error!')->error();
    }

    public static function messageErrorColumnNumeric($controller, $column)
    {
        \Log::critical('la columna debe poseer datos numericos: '. ' || '.$controller . '- (), del usuario: '.Auth::user()->user_name);
        flash('La Columna '.$column.' debe poseer solo valores numericos', '¡Error!')->error();
    }

    public static function messageStructureHeadboard($controller)
    {
        \Log::critical('El archivo no posee la estructura correcta' . ' -----> ['.$controller . ' del usuario: '.Auth::user()->user_name);
        flash('la estructura del archivo no es la correcta', '¡Alert!')->warning();
    }


    public static function messageNoDeleteclubs($controller)
    {
        \Log::critical('No puede eliminar la sede tiene usuarios relacionados ' . ' -----> ['.$controller . ' del usuario: '.Auth::user()->user_name);
        flash('No puede realizar la operacion, la sede tiene usuarios relacionados', '¡Alert!')->warning();
    }

    /**
     * [createAccount Function
     *  Allows to validate that the logged in user has permissions to access the selected module and action
     * ]
     * @param  [integer] $module       [Identificator to the module]
     * @param  [integer] $action       [Identificator to the action]
     * @return [void]                        [Create success of account]
     */
    public static function validationReturn($module, $action){

        $role = session('role');
        $comprober = false;
        $access = false;
        $redirect = false;

        foreach ($role as $value) {

            if ( (($value->fk_id_module == $module) && ($value->fk_id_action == $action)) ) {
                $comprober=true;
            }

            if ( (($value->fk_id_module == $module) && ($value->fk_id_action == 1)) ) {
                $access= true;
            }

        }

        if ($comprober == true && $access == true) {
            $redirect = true;
        }else{
            \Log::critical('Acceso denegado a: '.Auth::user()->user_name);
            flash('Acceso denegado', '¡Alert!')->warning();
        }

        return $redirect;
    }

    /**
     * [checksSeleccionados: This function return a formatted array with the checked and not checked clubs]
     * @param  [Object Array] $all           [Array with all records]
     * @param  [Object Array] $particular    [Array with selected records]
     * @param  [Integer]      $id_particular [Particular record Id]
     * @param  [String]       $column_name   [Column name to compare ]
     * @return [Array]                       [Formated Array]
     */
    public static function checksSeleccionados($all, $particular, $id_particular, $column_name)
    {
        $final = array();
        if(count($particular)>0) {

            $i=0; $b=0;
            foreach ($all as $value) {
                foreach ($particular as $particular1 ) {
                    if ($value['id'] == $particular1[$id_particular]) {
                        $final[$i] = ['id' => $value['id'], 'name' => $value[$column_name], 'club_check'=>'on'];
                        $i++;
                    }
                }
            }

            $cont=0;$j=0;
            $id_encontrados = array();
            foreach ($all as $value ) {
                foreach ($final as $dato) {
                    if ($value[$column_name] == $dato['name']) {
                        $cont++;
                        $id_encontrados[$j] = $j;
                        break;
                    }else{
                        $id_encontrados[$j] = -1;
                    }
                }
                $j++;
            }

            for ($i=0; $i < count($all); $i++) {
                if (($id_encontrados[$i]==-1)) {
                    $final[$cont]=['id'=>$all[$i]['id'], 'name'=>$all[$i][$column_name], 'club_check'=>'off'];
                    $cont++;
                }
            }
        }

        return $final;
    }

    public static function getGroups($all, $particular, $fk_id_particular)
    {
        $id_aux = '';
        foreach ($all as $pdate) {
            $groups = '';
            foreach ($particular as $gpdate) {
                if ( $pdate -> id == $gpdate -> $fk_id_particular) {
                  $groups = $groups.$gpdate -> name.',';
                }
            }

            if ($id_aux != $pdate -> id) {
                $pdate['groups'] = substr($groups, 0, -1);
                $id_aux = $pdate -> id;
            }
        }

        return $all;
    }

    /**
     * [validateColumnNumeric
     *  Validates that a column has only numerical values ]
     * @param  [arrays] $dataColumn   [Arrangement with the data in column]
     * @param  [arrays] $key   [Column index]
     * @return [boolean] $validate     [Returns true if it has a non-numeric value]
     */
    public static function validateColumnNumeric($dataColumn,$key){
        $validate=false;
        // dd($dataColumn,$key);
        $data = array_column($dataColumn, $key);

        foreach ($data as $k => $value) {

            if (is_numeric($value)==false ) {
                $validate = true;
            }
        }

        return $validate;
    }

    /**
     * [validateSheetExcel
     *  validate that the excel file has a single sheet ]
     * @param  [arrays] $data   [Arrangement with the data ]
     * @return [boolean] $validate   [Returns true if it has a non-numeric value]
     */
    public static function validateSheetExcel($data){
        $validate=false;
            if (sizeOf($data)!=1 ) {
                $validate = true;
            }
        return $validate;
    }

    /**
     * [validateColumnVoid
     *  Validate that a column has no empty values ]
     * @param  [arrays] $array   [Arrangement with the data in column]
     * @return [boolean] $validate     [Returns true if it gets duplicate values]
     */
    public static function  validateColumnVoid($dataColumn, $key){
      $validate=true;
      $data = array_column($dataColumn, $key);
      foreach ($data as $value) {
            if ( strlen( $value ) == 0 )  {
    			$validate = false;
                echo $value;
            }
    	}

      return $validate;
    }

    /**
     * [validateDuplicateValue
     *  Validates that a column has no duplicate values ]
     * @param  [arrays] $array   [Arrangement with the data in column]
     * @return [boolean] $validate     [Returns true if it gets duplicate values]
     */
    public static function validateDuplicateValue($array){
        $validate=false;

        if( (count($array) )!= (count(array_unique($array) ) ) ){
            $validate = true;
        }
        return $validate;

    }

    /**
     * [validateHead
     *  .
     *  Validates the header of the files to import ]
     * @param  [arrays] $keys   [Arrangement with defined keys]
     * @param  [arrays] $data   [Fix with the indexes that the file brings]
     * @return [integet] $i     [Returns in number of valid indexes]
     */
    public static function validateHeadboard($keys, $data){
      $j=0; $i=0;
      $validate=true;

      foreach ($data as $value) {
        	foreach ($value as $key => $value2) {
            	foreach ($keys as $value3) {
					if ($key == $value3) {
						$j++;
					}
                }
			$i++;
			}
		break;
		}

		if(  !((count($keys) == $j) &&  ( count($keys)==$i ))   ){
			$validate = false;

       }

      return $validate;

    }


    /**
     * [validateHead
     *  .
     *  Validates the header of the files to import ]
     * @param  [arrays] $keys   [Arrangement with defined keys]
     * @param  [arrays] $data   [Fix with the indexes that the file brings]
     * @return [integet] $i     [Returns in number of valid indexes]
     */
    public static function validateHead($keys, $data){
        $i=0;
        // dd($keys, $data);
        foreach ($data as $value) {
            foreach ($value as $key => $value2) {
                foreach ($keys as $value3) {

                    if ($key === $value3) {
                        // echo $key .'     ///  '.$value3.'    /////';
                        $i++;
                    }

                }
            }
        break;
        }
        // dd($i);
        return $i;
    }

    /**
     * [createAccount Function
     *  Create a new account for a new member.
     *  The first 4 numbers are the id club.
     *  The next 8 numbers are the id member.
     *  The final 4 numbers are the randon code generated for account.
     * ]
     * @param  [integer] $fk_id_member       [Identificator Club]
     * @param  [integer] $id_club            [Identificator Club]
     * @param  [integer] $fk_id_kind_account [Kind account selected]
     * @return [void]                        [Create success of account]
     */
    public static function createAccount($id_club, $fk_id_member, $fk_id_kind_account, $points)
    {
        $faker = \Faker\Factory::create();

        if (!$fk_id_kind_account) {
            $fk_id_kind_account = 1;
        }

        $id_club = str_pad( $id_club, 4 , 0, STR_PAD_LEFT);

        $fk_id_member = str_pad( $fk_id_member, 8 , 0, STR_PAD_LEFT);

        do {

            $particular = $faker->unique()->regexify('[0-9]{4}');

            $code = $id_club.$fk_id_member.$particular;

            $exist = Account::where('code', $code);

        } while ((count($exist)==0));

        Account::insert([
            'code'                => $code,
            'points'              => $points,
            'fk_id_member'        => $fk_id_member,
            'fk_id_kind_account'  => $fk_id_kind_account,
            'created_at'          => date('Y-m-d H:i:s'),
            'updated_at'          => date('Y-m-d H:i:s')
        ]);
    }

    public static function getDates($dates_string)
    {
        $var = explode(' - ', $dates_string);

        $start_date_separated = explode('/', $var[0]);
        $due_date_separated = explode('/', $var[1]);

        return array(
            Carbon::create($start_date_separated[2], $start_date_separated[1], $start_date_separated[0], 00, 00, 00, 'A'),
            Carbon::create($due_date_separated[2], $due_date_separated[1], $due_date_separated[0], 23, 59, 59, 'P')
        );
    }

    public static function validDates($start_date_incoming, $due_date_incoming)
    {
        $start_date_separated = explode('-', $start_date_incoming[0]);
        $due_date_separated = explode('-', $due_date_incoming[1]);

        $start_date = Carbon::create($start_date_separated[2], $start_date_separated[1], $start_date_separated[0]);
        $due_date = Carbon::create($due_date_separated[2], $due_date_separated[1], $due_date_separated[0]);

        if ( ($start_date->diffInMonths($due_date) <= 6) && (Carbon::now()->diffInYears($start_date) <= 1)) {
            return 1;
        }else{
            if ( !($start_date->diffInMonths($due_date) <= 6) ) {
                return 0;
            }
        }

        return -1;
    }

    public static function threeFirstRanking($club)
    {
        // $members  = Member::select('members.*', 'members_clubs.*', 'departments.name as department', 'accounts.*')
        // ->join('accounts', 'accounts.fk_id_member', 'members.id')
        // ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        // ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        // ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        // ->where('members_clubs.fk_id_club', $club)
        // ->offset(0)
        // ->limit(3)
        // ->get()
        // ->sortByDesc('points');

        $members = Member::select(DB::raw("members.* ,  members_clubs.* ,  accounts.points ,  departments.name as department,
					(select sum(accounts_transactions.points)
					 from accounts_transactions
        			 join accounts on  accounts.id =  accounts_transactions.fk_id_account
        			 join members as me on  me.id =  accounts.fk_id_member
        			 join members_clubs on  members_clubs.fk_id_member =  me.id
        			 where accounts_transactions.fk_id_club = $club
        			 and accounts.fk_id_member = members.id
                     and accounts_transactions.points > 0
                     and accounts_transactions.operation = 1) as total"))
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('accounts', 'accounts.fk_id_member', 'members.id')
        ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->where('members_clubs.fk_id_club', $club)
        ->offset(0)
        ->limit(3)
        ->get()
        ->sortByDesc('total');

        $ranking = [];
        foreach ($members as $key) {
            $ranking[] = $key;
        }

        $pos = 1;
        for ($i=0; $i < count($ranking); $i++) {
            if ( $i > 0) {
                if ($ranking[$i-1]['total'] == $ranking[$i]['total']) {
                    $ranking[$i]['position'] = $pos-1;
                }else {
                    $ranking[$i]['position'] = $pos;
                    $pos++;
                }
            }else {
                $ranking[$i]['position'] = $pos;
                $pos++;
            }
        }

        return $ranking;
    }

    public static function getPositionRanking($ranking, $id)
    {
        foreach ($ranking as $key){
            if ($key->id == $id){
                return $key->position;
            }
        }
    }

    public static function nextBirthday($club)
    {
        $members = Member::select('members.*', 'members_clubs.*', 'departments.name as department')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->where('members_clubs.fk_id_club', $club)
        ->whereMonth('birthdate', date('m'))
        ->get();

        foreach ($members as $key ) {
            $vec = explode('-', $key->birthdate);
            $key['order'] = $vec[2];
        }

        $next_birthday = $members->sortBy('order');

        $i = 1;
        $next_three_birthdate = null;
        foreach ($next_birthday as $key) {

            if ($key->order >=  date('d')) {
                $next_three_birthdate[] = $key;
                if ($i == 3) {
                    break;
                }
                $i++;
            }
        }

        return $next_three_birthdate;
    }

    public static function getClubs()
    {
        $aux = session('clubs');

        $i = 0;
        foreach ($aux as $key) {
            $clubs[$i] = $key->fk_id_club;
            $i++;
        }

        return $clubs;
    }

    public static function getDistinctParentProduct($all)
    {
        $distintos = [];
        $parents_products = [];
        foreach ($all as $key) {
            if (!in_array($key->id, $distintos)) {
                $distintos[] = $key->id;
                $parents_products[] = $key;
            }
        }

        return $parents_products;
    }

    /**
     * Method that allows to validate that the amount of products is equal to or less than the one found in the inventory
     * @param  [array] $products     [Id and quantity for each product]
     * * @param  [array] $inventory     [quantity for each product in the inventory]
     * @return [boolean]                        [return true If quantity exceeds inventory]
     */
    public static function validateQuantityproduct($products,$inventory){

      foreach ($products as $key => $valueQuantities) {
      		foreach ($inventory as  $valueInventory) {

      			if ( ($valueQuantities['quantity'] > $valueInventory->quantity_current ) && ($key == $valueInventory->fk_id_product )  ) {
      					 return true;
			      }

      		}
      }

      return false;
    }

    /**
     * Method that allows to validate that the amount of products is equal to or less than the one found in the inventory
     * @param  [array] $products     [Id and quantity for each product]
     * @param  [int] $points     [quantity points of the members]
     * @return [boolean]                        [true or false]
     */
    public static function validatePointMember($products, $points){

      $content=0;

      $consult_products = product::select('products.id', 'products.points')
      ->whereIn('products.id', $products )
      ->get();

      foreach ($consult_products as  $valueConsult_product) {
          foreach ($products as  $valueproducts){
              if ($valueproducts['fk_id_product'] == $valueConsult_product->id ) {
                $content +=  $valueConsult_product->points * $valueproducts['quantity'];
              }
          }
      }
        
      if ( $content >= $points  ) {
        return true;
      }

      return false;

    }

    public static function arraySimple($array = null){
        $i = 0;
        $valor=[];

        // if (empty($array)) {
        //     $valor = [0=>0];
        //     return $valor;
        // }
        // dd($valor);
        foreach ($array as $key) {
            $valor[$i] = $key;
            $i++;
        }

        return $valor;
    }

    public static function filesystemGoogle()
    {

        // dd($_ENV['GOOGLE_APPLICATION_CREDENTIALS'],strcmp ($_ENV['GOOGLE_APPLICATION_CREDENTIALS'], "desarrollo"),"desarrollo");
        if (strcmp ($_ENV['GOOGLE_CREDENTIALS_PATH'], "desarrollo")) {
            $storageClient = new StorageClient([
                'projectId' => 'friendly-folio-151919',
                'keyFilePath' => $_ENV['GOOGLE_CREDENTIALS_PATH']
            ]);
        }else {
            $storageClient = new StorageClient([
                'projectId' => 'friendly-folio-151919'
            ]);
        }

        $bucket = $storageClient->bucket('friendly-folio-151919.appspot.com');

        $adapter = new GoogleStorageAdapter($storageClient, $bucket);

        $filesystem = new Filesystem($adapter);

        return $filesystem;
    }

    public static function imageChange($identificator = null, $id = null, $image, $disk)
    {
        $filesystem = ResourceFunctions::filesystemGoogle();

        //Recibimos el archivo enviado mediante el formulario
        $file = $image; // Asignamos el archivo a una valiable
        $file_name = time() . mt_rand() .'.'. $file -> getClientOriginalExtension(); //Asignamos un nuevo nombre al archivo
        $url_cdn = $_ENV["STORAGE_ENV"];
        $url_image = 'image/'. $disk .'/'.$file_name;

        if ($id == null) {
            $result = Media::create([
                'identificator' => $identificator,
                'table'         => $disk,
                'image'         => $url_image,
                'slug'         => $file_name
            ]);

        }else{

            $media = Media::FindOrFail($id);

            if (!empty($media -> image) ) {
                if ($filesystem->has($url_cdn . $media -> image)) {
                    $filesystem->delete($url_cdn . $media -> image);
                }
            }

            $result = Media::where('id', $id) -> update(['image' => $url_image, 'slug' => $file_name]);

        }

        // write a file
        $filesystem->write($url_cdn . 'image/'. $disk .'/' . $file_name, file_get_contents($file));
        // $file -> move('storage/'.$disk, $file_name );

        Log::notice('Proceso exitoso: Image metodo store del usuario: '.Auth::user()->user_name);

        return $result;
    }

    public static function changeStatus($table, $model, $id)
    {
        $result = true;

        try {
            DB::beginTransaction();

            $status = false;
            if (!$model->status) {
                $status = true;
            }

            DB::table($table)
            ->where('id', $id)
            ->update(['status' => $status]);

            DB::commit();

        } catch (\Exception $e) {
            $result = false;

            DB::rollBack();

            ResourceFunctions::messageError('ResourceFunction','change',$e);
        }

        return $result;
    }
}
