<?php

namespace Meritop\Http\Requests\Admin\Transaction;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->redirect = route('transactions.index');
        return [
            'operation' => 'nullable|integer',
            'date' => 'required|string|size:23'
        ];

    }

    public function messages()
    {
        return [
            'date.required' => 'Debe seleccionar una fecha de inicio y una de fin',
            'date.size' => 'Debe seleccionar una fecha de inicio y una de fin',
            'operation.integer' => 'Debe seleccionar uno de los tipos de operación listados',
        ];
    }
}
