<?php

namespace Meritop\Http\Requests\Admin\ExchangeReport;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|string|size:23'
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'Debe seleccionar una fecha de inicio y una de fin',
            'date.size' => 'Debe seleccionar una fecha de inicio y una de fin',
        ];
    }
}
