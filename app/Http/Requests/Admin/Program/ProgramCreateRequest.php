<?php

namespace Meritop\Http\Requests\Admin\Program;

use Illuminate\Foundation\Http\FormRequest;

class ProgramCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'                          => 'required|max:40',
            'description'                   => 'required|max:65535',
            'image'                         => 'nullable|image',
            'fk_id_group'                   => 'required|array',
            'objetive_name'                 => 'required|max:40',
            'code'                          => 'required|min:1|max:6|unique:objetives,code',
            'objetive_description'          => 'required|max:65535',
            'start_date'                    => 'required|date|date_format:Y-m-d|before_or_equal:due_date',
            'due_date'                      => 'required|date|date_format:Y-m-d',
            'fk_id_perspective'             => 'required|integer|exists:perspectives,id'
        ];

        if (!empty($this->request->get('fk_id_group'))) {
            foreach($this->request->get('fk_id_group') as $key => $val)
            {
                $rules['fk_id_group.'.$key] = 'required|integer|exists:groups,id';
            }
        }

        return $rules;

    }

    public function messages()
    {
        return [
            'fk_id_group.required' => 'Debe seleccionar al menos un grupo',
            'objetive_name.required' => 'El nombre del objetivo es obligatorio.',
            'objetive_name.max' => 'El nombre del objetivo no debe ser mayor que :max caracteres.',
            'objetive_description.required' => 'La descripción del objetivo es obligatorio.',
            'objetive_description.max' => 'La descripción del objetivo no debe ser mayor que :max caracteres.',
            'fk_id_perspective.required' => 'Debe seleccionar al menos una perspectiva',
            'start_date.required' => 'El campo Inicio es obligatorio.',
            'start_date.date' => 'Inicio no es una fecha válida.',
            'start_date.date_format' => 'Inicio no corresponde al formato :format.',
            'start_date.before_or_equal' => 'Inicio debe ser una fecha anterior o igual a Fin.',
            'due_date.required' => 'El campo Fin es obligatorio.',
            'due_date.date' => 'Fin no es una fecha válida.',
            '   due_date.date_format' => 'Fin no corresponde al formato :format.'
        ];
    }
}
