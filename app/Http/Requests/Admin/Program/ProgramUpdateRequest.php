<?php

namespace Meritop\Http\Requests\Admin\Program;

use Illuminate\Foundation\Http\FormRequest;

class ProgramUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name' => 'required|max:40',
            'description' => 'required|max:65535',
            'fk_id_group' => 'required|array',
        ];

        if (!empty($this->request->get('fk_id_group'))) {
            foreach($this->request->get('fk_id_group') as $key => $val)
            {
                $rules['fk_id_group.'.$key] = 'required|integer|exists:groups,id';
            }
        }

        return $rules;

    }
    public function messages()
    {
        return [
            'fk_id_group.required' => 'Debe seleccionar al menos un grupo'
        ];
    }
}
