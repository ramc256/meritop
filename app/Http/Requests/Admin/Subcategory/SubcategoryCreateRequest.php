<?php

namespace Meritop\Http\Requests\Admin\Subcategory;

use Illuminate\Foundation\Http\FormRequest;

class SubcategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:40|unique:subcategories,name',
            'description' => 'required|max:65535',
            'slug' => 'required|max:120|unique:subcategories,slug',
            'status' => 'required|integer|digits_between:0,1',
            'fk_id_category' => 'required|integer|exists:categories,id',
        ];
    }

    public function messages()
    {
        return [
            'fk_id_category.required' => 'Debe seleccionar al menos una categoría',
        ];
    }
}
