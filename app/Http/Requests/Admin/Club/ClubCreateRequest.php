<?php

namespace Meritop\Http\Requests\Admin\Club;

use Illuminate\Foundation\Http\FormRequest;

class ClubCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|min:3|max:200',
            'alias'                 => 'required|min:3|max:100|unique:clubs,alias',
            'fiscal'                => 'required|min:9|max:20|unique:clubs,fiscal',
            'address'               => 'required|min:10|max:200',
            'phone'                 => 'required|string|min:10',
            'logo_image'            => 'required|image|mimes:jpeg,jpg,png,svg',
            'login_image'           => 'required|image|mimes:jpeg,jpg,png,svg',
            'slug'                  => 'required|min:3|max:120',
            'color'                 => 'required|string',
            'status'                => 'required|boolean',
            'fk_id_country'         => 'required|integer|exists:countries,id',
            'state'                 => 'required',
            'city'                  => 'required',
            'member_format_import'  => 'required|integer|min:0|max:1',
            'fk_id_currency'        => 'required|integer|exists:currencies,id',
            'system_currency'       => 'required|string|min:3',
            'convertion_rate'       => 'required|numeric|min:0'
        ];
    }

    public function messages()
    {
     return [
            'fk_id_country.required'    => 'Debe seleccionar un pais',
            'fk_id_currency.required'    => 'Debe seleccionar una moneda',
            'state.required'            => 'Debe agregar un estado',
            'city.required'             => 'Debe agregar una ciudad',
            'fiscal.unique'             => 'El numero de identificacion fiscal ya se encuentra registrado en la base de datos del sistema',
            'logo_image.required'       => 'El campo de Imagen del logo es obligatorio.',
            'logo_image.image'          => 'El archivo seleccionado debe ser una imagen.',
            'logo_image.mimes'          => 'El campo Imagen del logo debe ser un archivo con formato: :values.',
            'login_image.required'      => 'El campo Imagen del login es obligatorio.',
            'login_image.image'         => 'El archivo seleccionado debe ser una imagen.',
            'login_image.mimes'         => 'El campo Imagen del login debe ser un archivo con formato: :values.',
        ];
    }
}
