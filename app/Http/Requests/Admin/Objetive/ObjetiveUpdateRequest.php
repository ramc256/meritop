<?php

namespace Meritop\Http\Requests\Admin\Objetive;

use Illuminate\Foundation\Http\FormRequest;

class ObjetiveUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:40',
            'description' => 'required|max:65535',
            'start_date' => 'required|date|date_format:Y-m-d|before_or_equal:due_date',
            'due_date' => 'required|date|date_format:Y-m-d',
            'fk_id_perspective' => 'required|integer|exists:perspectives,id',
        ];
    }

    public function messages()
    {
        return [
            'fk_id_perspective.required' => 'Debe seleccionar al menos una perspectiva',
            'start_date.required' => 'El campo Inicio es obligatorio.',
            'start_date.date' => 'Inicio no es una fecha válida.',
            'start_date.date_format' => 'Inicio no corresponde al formato :format.',
            'start_date.before_or_equal' => 'Inicio debe ser una fecha anterior o igual a Fin.',
            'due_date.required' => 'El campo Fin es obligatorio.',
            'due_date.date' => 'Fin no es una fecha válida.',
            'due_date.date_format' => 'Fin no corresponde al formato :format.',
        ];
    }
}
