<?php

namespace Meritop\Http\Requests\Admin\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class RoleUpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:40|unique:roles,name,'.$this->route->parameter('role'),
            'description' => 'required|max:65535',
            'privileges' => 'required|array',
            'fk_id_kind_role' => 'required|integer|exists:kind_roles,id'
        ];
    }

    public function messages()
    {
        return [
            'privileges.required' => 'El campo Privilegios es obligatorio.',
            'fk_id_kind_role.required' => 'Debe seleccionar un tipo de role de la lista',
        ];
    }
}
