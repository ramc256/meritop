<?php

namespace Meritop\Http\Requests\Admin\Banner;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|string|min:6|max:250',
            'subtitle'  => 'required|string|min:6|max:250',
            'image'     => 'nullable|image|mimes:jpeg,jpg,png'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El campo Título es obligatorio.',
            'title.string' => 'El campo Título debe ser una cadena de caracteres.',
            'title.min' => 'Título debe contener al menos :min caracteres.',
            'title.max' => 'Título no debe ser mayor que :max caracteres.',
            'subtitle.required' => 'El campo Subtítulo es obligatorio.',
            'subtitle.string' => 'El campo subtitle debe ser una cadena de caracteres.',
            'subtitle.min' => 'Subtítulo debe contener al menos :min caracteres.',
            'subtitle.max' => 'Subtítulo no debe ser mayor que :max caracteres.',
            'subtitle.max' => 'Subtítulo no debe ser mayor que :max caracteres.',
            'image.image' => 'El archivo seleccionado debe ser una imagen.',
            'image.mimes' => 'El campo Imagen debe ser un archivo con formato: :values.',
        ];
    }
}
