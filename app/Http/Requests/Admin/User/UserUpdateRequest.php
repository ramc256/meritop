<?php

namespace Meritop\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
       return [
            'first_name'                => 'required|min:3|max:40',
            'last_name'                 => 'required|min:3|max:40',
            'user_name'                 => 'required|min:3|max:255|unique:users,user_name,'.$this->route->parameter('user'),
            'image'                     => 'nullable|mimes:jpeg,png,jpg|image',
            'kind'                      => 'required|boolean',
            'email'                     => 'required|string|min:5|max:50|email',
            'status'                    => 'required|boolean',
            'fk_id_role'                => 'required|integer|exists:roles,id',
            'fk_id_country'             => 'required|integer|exists:countries,id',
            'clubs'                     => 'required',
        ];
    }

    public function messages()
    {
        return [
            'kind.required' => 'Debe seleccionar al menos un tipo',
            'clubs.required' => 'Debe seleccionar al menos un club',
            'fk_id_country.required' => 'Debe seleccionar al menos un pais',
            'fk_id_role.required' => 'Debe seleccionar al menos un rol',
        ];
    }
}
