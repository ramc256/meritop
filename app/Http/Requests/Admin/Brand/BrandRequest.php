<?php

namespace Meritop\Http\Requests\Admin\Brand;

use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   if ($this->id) $this->redirect = url('objetives.create')->with('fk_id_program', \Request::fk_id_program);
        return [
            'name' => 'required|max:40|unique:brands,name'
        ];
    }
}
