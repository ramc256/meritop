<?php

namespace Meritop\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class CategoryUpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * For it to can update the fields with validation  "unique",
         * it will must get the parameter with name of the module
         */
        return [
            'name' => 'required|max:40|unique:categories,name,'.$this->route->parameter('category'),
            'description' => 'required|max:65535',
            'slug' => 'required|max:120|unique:categories,slug,'.$this->route->parameter('category'),
            'status' => 'required|integer|digits_between:0,1'
        ];
    }
}
