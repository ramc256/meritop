<?php

namespace Meritop\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:40|unique:categories,name',
            'description' => 'required|max:65535',
            'slug' => 'required|max:120|unique:categories,slug',
            'status' => 'required|integer|digits_between:0,1',
            'image' => 'nullable|image|mimes:jpeg,jpg,png',
        ];
    }
}
