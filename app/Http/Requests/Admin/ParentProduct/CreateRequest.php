<?php

namespace Meritop\Http\Requests\Admin\ParentProduct;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:200',
            'model' => 'required|max:100',
            'description' => 'required|string|max:65535',
            'warranty_day' => 'nullable|integer|digits_between:1,730',
            'warranty_conditions' => 'nullable|string|min:5',
            'delivery_conditions' => 'required|string|min:5',
            'fk_id_market' => 'required|integer|exists:markets,id',
            'fk_id_brand' => 'required|integer|exists:brands,id',
            'fk_id_subcategory' => 'required|array',
            'fk_id_club' => 'required|array',
            'product_image' => 'nullable|array',
            'parent_image' => 'nullable|image|mimes:jpeg,jpg,png',
            'feature' => 'required|max:200',
            'sku' => 'required|alpha_num|max:15|unique:products,sku',
            'barcode' => 'required|string|min:0|unique:products,barcode',
            'size' => 'required|numeric|min:0',
            'color' => 'required|string|between:4,7',
            'measure' => 'required|numeric|min:0',
            'weight' => 'required|numeric|min:0',
            'width' => 'required|numeric|min:0',
            'heigth' => 'required|numeric|min:0',
            'depth' => 'required|numeric|min:0',
            'volume' => 'required|numeric|min:0',
            'points' => 'required|numeric|min:0',
            'status' => 'required|integer|digits_between:0,1',
            'fk_id_country' => 'required|integer|exists:countries,id',
        ];

        if (!empty($this->files->get('product_image'))) {
            foreach($this->files->get('product_image') as $key => $val)
            {
                $rules['product_image.'.$key] = 'required|image|mimes:jpeg,jpg,png';
            }
        }

        if (!empty($this->request->get('fk_id_subcategory'))) {
            foreach($this->request->get('fk_id_subcategory') as $key => $val)
            {
                $rules['fk_id_subcategory.'.$key] = 'required|integer|exists:subcategories,id';
            }
        }

        if (!empty($this->request->get('fk_id_club'))) {
            foreach($this->request->get('fk_id_club') as $key => $val)
            {
                $rules['fk_id_club.'.$key] = 'required|integer|exists:clubs,id';
            }
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'fk_id_subcategory.required' => 'Debe seleccionar al menos una subcategoria',
            'fk_id_club.required' => 'Debe seleccionar al menos un club',
            'size.min' => 'El campo Tamaño debe ser al menos 0',
            'barcode.integer' => 'El código de barras debe ser un número entero',
            'measure.min' => 'El campo Medida debe ser al menos 0',
            'weight.min' => 'El campo Peso debe ser al menos 0',
            'width.min' => 'El campo Ancho debe ser al menos 0',
            'heigth.min' => 'El campo Alto debe ser al menos 0',
            'depth.min' => 'El campo Profundidad debe ser al menos 0',
            'volume.min' => 'El campo Volumen debe ser al menos 0',
            'points.min' => 'El campo Puntos debe ser al menos 0',
        ];
    }
}
