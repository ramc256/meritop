<?php

namespace Meritop\Http\Requests\Admin\ParentProduct;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:200',
            'model' => 'required|max:100',
            'description' => 'required|string|max:65535',
            'parent_image' => 'nullable|image|mimes:jpeg,jpg,png',
            'warranty_day' => 'nullable|integer|digits_between:1,730',
            'warranty_conditions' => 'nullable|between:1,250',
            'delivery_conditions' => 'required|max:250',
            'fk_id_market' => 'required|integer|exists:markets,id',
            'fk_id_brand' => 'required|integer|exists:brands,id',
            'fk_id_subcategory' => 'required|array',
            'fk_id_club' => 'required|array',
        ];

        if (!empty($this->files->get('fk_id_subcategory'))) {
            foreach($this->files->get('fk_id_subcategory') as $key => $val)
            {
                $rules['fk_id_subcategory.'.$key] = 'required|integer|exists:subcategories,id';
            }
        }

        if (!empty($this->request->get('fk_id_club'))) {
            foreach($this->request->get('fk_id_club') as $key => $val)
            {
                $rules['fk_id_club.'.$key] = 'required|integer|exists:clubs,id';
            }
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'fk_id_subcategory.required' => 'Debe seleccionar al menos una subcategoria',
            'fk_id_club.required' => 'Debe seleccionar al menos un club',
            'fk_id_brand.required' => 'Debe seleccionar al menos una marca',
        ];
    }
}
