<?php

namespace Meritop\Http\Requests\Admin\Inventory;

use Illuminate\Foundation\Http\FormRequest;

class InventoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'quantities' => 'required|array',
            'variants' => 'array'
        ];

        if (!empty($this->request->get('quantities'))) {
            foreach($this->request->get('quantities') as $key => $val)
            {
                $rules['quantities.'.$key] = 'required|integer';
            }
        }

        if (!empty($this->request->get('variants'))) {
            foreach($this->request->get('variants') as $key => $val)
            {
                $rules['variants.'.$key] = 'required|integer|exists:variants,id';
            }
        }

        return $rules;
    }
}
