<?php

namespace Meritop\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'feature' => 'required|max:200',
            'sku' => 'required|alpha_num|max:15|unique:products,sku,'.$this->route->parameter('product'),
            'barcode' => 'required|string|min:0|unique:products,barcode,'.$this->route->parameter('product'),
            'excerpt' => 'required|string|max:250',
            'size' => 'required|numeric|min:0',
            'color' => 'required|string',
            'measure' => 'required|numeric|min:0',
            'weight' => 'required|numeric|min:0',
            'width' => 'required|numeric|min:0',
            'heigth' => 'required|numeric|min:0',
            'depth' => 'required|numeric|min:0',
            'volume' => 'required|numeric|min:0',
            'points' => 'required|numeric|min:0',
            'status' => 'required|integer|digits_between:0,1',
        ];
    }

    public function messages()
    {
        return [
            'size.min' => 'El campo Tamaño debe ser al menos 0',
            'barcode.integer' => 'El código de barras debe ser un número entero',
            'measure.min' => 'El campo Medida debe ser al menos 0',
            'weight.min' => 'El campo Peso debe ser al menos 0',
            'width.min' => 'El campo Ancho debe ser al menos 0',
            'heigth.min' => 'El campo Alto debe ser al menos 0',
            'depth.min' => 'El campo Profundidad debe ser al menos 0',
            'volume.min' => 'El campo Volumen debe ser al menos 0',
            'points.min' => 'El campo Puntos debe ser al menos 0',
        ];
    }
}
