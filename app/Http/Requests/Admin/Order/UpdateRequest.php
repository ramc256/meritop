<?php

namespace Meritop\Http\Requests\Admin\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'note'          => 'max:200',
            'internal_note' => 'max:1000',
            'fk_id_branch'  => 'required|integer|exists:branches,id',
        ];
    }

     public function messages()
    {
    return [
            'fk_id_branch.required' => 'Debe seleccionar una sede',
            
        ];
    }
}
