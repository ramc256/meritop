<?php

namespace Meritop\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class ProfileRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name'    => 'required|min:3|max:40',
            'last_name'     => 'required|min:3|max:40',
            'user_name'     => 'required|min:3|max:255|unique:users,user_name,'.$this->route->parameter('profile'),
            'email'         => 'required|string|min:5|max:50|email'
        ];
    }

    public function messages()
    {
     return [
            'first_name.required'   => 'El campo Nombre es obligatorio.',
            'first_name.min'        => 'Nombre debe contener al menos :min caracteres.',
            'first_name.max'        => 'Nombre no debe ser mayor que :max caracteres.',
            'last_name.required'    => 'El campo Apellido es obligatorio.',
            'last_name.min'         => 'Apellido debe contener al menos :min caracteres.',
            'last_name.max'         => 'Apellido no debe ser mayor que :max caracteres.',
            'user_name.required'    => 'El campo Nombre de Usuario es obligatorio.',
            'user_name.min'         => 'Nombre de Usuario debe contener al menos :min caracteres.',
            'user_name.max'         => 'Nombre de Usuario no debe ser mayor que :max caracteres.',
            'user_name.unique'      => 'Ese Nombre de Usuario ya ha sido registrado.',
            'email.required'        => 'El campo Correo es obligatorio.',
            'email.string'          => 'El campo Correo debe ser una cadena de caracteres.',
            'email.min'             => 'Correo debe contener al menos :min caracteres.',
            'email.max'             => 'Correo no debe ser mayor que :max caracteres.',
            'email.email'           => 'Correo no es un correo válido',
        ];
    }
}
