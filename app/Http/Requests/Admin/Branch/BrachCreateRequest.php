<?php

namespace Meritop\Http\Requests\Admin\Branch;

use Illuminate\Foundation\Http\FormRequest;

class BrachCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            => 'required|min:3|max:200',
            'address'         => 'required|min:10|max:200',
            'postal_code'     => 'required|min:3|max:10',
            'dispatch_route'  => 'required|min:3|max:200',
            'phone'           => 'required|string|min:10',
            'secondary_phone' => 'required|string|min:10',
            'fk_id_city'      => 'required|integer|exists:cities,id',
        ];
    }
    /*
    *custon messages
    */
    public function messages()
    {
     return [
            'fk_id_city.required' => 'Debe seleccionar una ciudad',
            'name.required|min:3' => 'El campo nombre es obligatorio',
            'name.min:3'          => 'El campo nombre debe tener un minimo de 3 caracterres',
            'address.required'    => 'El campo direccion es obligatorio',
            'name.min:3'          => 'El campo direccion debe tener un minimo de 3 caracterres',
        ];
    }
}
