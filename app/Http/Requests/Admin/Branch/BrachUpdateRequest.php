<?php

namespace Meritop\Http\Requests\Admin\Branch;

use Illuminate\Foundation\Http\FormRequest;

class BrachUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|min:3|max:200',
            'address' => 'required|min:3|max:200',
            'postal_code'   => 'required|min:3|max:10',
            'dispatch_route'    => 'required|min:3|max:200',
            'phone'           => 'required|string|min:10',
            'secondary_phone'   => 'required|string|min:10',
            'fk_id_city' => 'required|integer|exists:cities,id',
        ];
    }
}
