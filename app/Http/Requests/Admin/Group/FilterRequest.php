<?php

namespace Meritop\Http\Requests\Admin\Group;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department' => 'required',
        ];
    }

    public function messages()
    {
     return [
        
        'department.required'    => 'Debe seleccionar un departamento',
    ];
    }


}
