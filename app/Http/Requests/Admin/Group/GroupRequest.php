<?php

namespace Meritop\Http\Requests\Admin\Group;

use Illuminate\Foundation\Http\FormRequest;

class GroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name' => 'required|max:40',
            'description' => 'required|max:65535',
            'members' => 'nullable|array'
        ];

        if (!empty($this->request->get('members'))) {
            foreach($this->request->get('members') as $key => $val)
            {
                $rules['members.'.$key] = 'required|integer|exists:members,id';
            }
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'members.required' => 'Debe seleccionar al menos un miembro de los listados',
        ];
    }
}
