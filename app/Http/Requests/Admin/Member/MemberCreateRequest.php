<?php

namespace Meritop\Http\Requests\Admin\Member;

use Illuminate\Foundation\Http\FormRequest;

class MemberCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dni'             => 'required|string',
            'first_name'      => 'required|min:3|max:40',
            'last_name'       => 'required|min:3|max:40',
            'birthdate'       => 'required|date|date_format:Y-m-d',
            'email'           => 'required|string|min:5|max:100|email|unique:members,email',
            'cell_phone'      => 'required|string|min:10|max:22|unique:members,cell_phone',
            'gender'          => 'required|integer|min:0|max:1',
            'status'          => 'required|integer|min:0|max:1',
            'image'           => 'nullable|image|mimes:jpeg,jpg,png',
            'carnet'          => 'nullable|string|min:1',
            'email_corporate' => 'nullable|string|min:5|max:100|email',
            'fk_id_department' => 'required|integer|exists:departments,id',
            'supervisor_code' => 'nullable|string|min:1',
            'fk_id_branch'        => 'required|integer|exists:branches,id',
        ];
    }

    public function messages()
    {
     return [
            'cell_phone.required' => 'El campo Teléfono es obligatorio',
            'cell_phone.min' => 'El Teléfono debe tener al menos 10 dígitos',
            'dni.required' => 'El campo DNI es obligatorio',
            'email.required' => 'El campo Correo es obligatorio',
            'email_corporate.required' => 'El campo Correo es obligatorio',
            'email.email' => 'El campo Correo no contiene un correo valido',
            'email.unique' => 'El Correo ya esta registrado',
            'email_corporate.email' => 'El campo Correo no contiene un correo valido',
            'image.required' => 'Debe seleccionar una Imagen',
            'image.image' => 'El archivo seleccionado debe ser una Imagen',
            'image.mimes' => 'Las extensiones permitidas para la imagen son jpeg,jpg y png',
        ];
    }
}
