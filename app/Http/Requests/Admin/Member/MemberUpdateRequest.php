<?php

namespace Meritop\Http\Requests\Admin\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class MemberUpdateRequest extends FormRequest
{
    /**
     * [__construct to get the route]
     * @param Route $route [The route of the petition]
     */
    public  function __construct(Route $route)
    {
        $this->route = $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dni'               => 'required|string|min:7|max:20|unique:members,dni,'.$this->route->parameter('member'),
            'first_name'        => 'required|min:3|max:40',
            'last_name'         => 'required|min:3|max:40',
            'birthdate'         => 'required|date|date_format:Y-m-d',
            'email'             => 'required|string|min:5|max:100|email|unique:members,email,'.$this->route->parameter('member'),
            'cell_phone'        => 'required|string|min:10|max:22|unique:members,cell_phone,'.$this->route->parameter('member'),
            'gender'            => 'required|integer|min:0|max:1',
            'status'            => 'required|integer|min:0|max:1',
            'image'             => 'nullable|image|mimes:jpeg,jpg,png',
            'carnet'            => 'required|string|min:1',
            'email_corporate'   => 'required|string|min:5|max:100|email',
            'branch'            => 'required|integer|exists:branches,id',

        ];
    }

    public function messages()
    {
     return [
            'cell_phone.required'       => 'El campo Teléfono es obligatorio',
            'cell_phone.min'            => 'El Teléfono debe tener al menos 10 dígitos',
            'dni.required'              => 'El campo dni es obligatorio',
            'email.required'            => 'El campo Correo es obligatorio',
            'email.unique'              => 'El correo ya existe en la base de datos',
            'email.email'               => 'El campo Correo no contiene un correo valido',
            'email_corporate.required'  => 'El campo Correo corporativo es obligatorio',
            'email_corporate.email'     => 'El campo Correo corporativo no contiene un correo valido',
        ];
    }
}
