<?php

namespace Meritop\Http\Requests\Web\Message;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject'           => 'required|string|max:250',
            'message'           => 'required|string|max:65535',
        ];
    }

    public function messages()
    {
        return [
            'subject.required' => 'El campo Asunto es obligatorio.',
            'subject.string' => 'El campo Asunto debe ser una cadena de caracteres.',
            'subject.max' => 'El Asunto no debe ser mayor que :max caracteres.',
            'message.required' => 'El campo Mensaje es obligatorio.',
            'message.string' => 'El campo Mensaje debe ser una cadena de caracteres.',
            'message.max' => 'El Mensaje no debe ser mayor que :max caracteres.',
        ];
    }
}
