<?php

namespace Meritop\Http\Requests\Web\Store;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'market' => 'nullable|array',
            'category' => 'nullable|array',
            'brand' => 'nullable|array'
        ];

        if (!empty($this->request->get('market'))) {
            foreach($this->request->get('market') as $key => $val)
            {
                $rules['market.'.$key] = 'required|integer|min:0|max:2';
            }
        }

        if (!empty($this->request->get('category'))) {
            foreach($this->request->get('category') as $key => $val)
            {
                $rules['category.'.$key] = 'required|string|exists:categories,name';
            }
        }

        if (!empty($this->request->get('brand'))) {
            foreach($this->request->get('brand') as $key => $val)
            {
                $rules['brand.'.$key] = 'required|string|exists:brands,name';
            }
        }

        return $rules;
    }
}
