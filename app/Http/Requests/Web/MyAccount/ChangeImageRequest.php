<?php

namespace Meritop\Http\Requests\Web\MyAccount;

use Illuminate\Foundation\Http\FormRequest;

class ChangeImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|image|mimes:jpeg,jpg,png'
        ];
    }

    public function messages()
    {
     return [
            'image.required' => 'Debe seleccionar una Imagen',
            'image.image' => 'El archivo seleccionado debe ser una Imagen',
            'image.mimes' => 'Las extensiones permitidas para la imagen son jpeg,jpg y png'
        ];
    }
}
