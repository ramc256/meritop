<?php

namespace Meritop\Http\Requests\Web\MyAccount;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|string|min:8|max:18|',
            'new_password' => 'required|string|min:8|max:18|',
            're_passowrd'  => 'required|string|min:8|max:18|same:new_password'
        ];
    }

    public function messages()
    {
        return [
            'old_password.required' => 'El campo Contraseña Anterior es obligatorio.',
            'old_password.string'   => 'La Contraseña Anterior sólo debe contener letras y números.',
            'old_password.min'      => 'El tamaño de la Contraseña Anterior debe ser de al menos :min.',
            'old_password.max'      => 'La Contraseña Anterior no debe ser mayor a :max.',
            'new_password.required' => 'El campo Nueva Contraseña es obligatorio.',
            'new_password.string'   => 'La Nueva Contraseña sólo debe contener letras y números.',
            'new_password.min'      => 'El tamaño de la Nueva Contraseña debe ser de al menos :min.',
            'new_password.max'      => 'La Nueva Contraseña no debe ser mayor a :max.',
            're_passowrd.required'  => 'El campo Repetir Contraseña es obligatorio.',
            're_passowrd.string'    => 'El campo Repetir Contraseña sólo debe contener letras y números.',
            're_passowrd.min'       => 'El tamaño del campo Repetir Contraseña debe ser de al menos :min.',
            're_passowrd.max'       => 'El campo Repetir Contraseña no debe ser mayor a :max.',
            're_passowrd.same'      => 'El campo Repetir Contraseña y el campo Nueva Contraseña deben coincidir.'
        ];
    }
}
