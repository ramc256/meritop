<?php

namespace Meritop\Http\Requests\Web\MyAccount;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'fk_id_branch' => 'required|array'
        ];

        if (!empty($this->request->get('fk_id_branch'))) {
            foreach($this->request->get('fk_id_branch') as $key => $val)
            {
                $rules['fk_id_branch.'.$key] = 'required|integer|exists:branches,id';
            }
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        foreach($this->request->get('fk_id_branch') as $key => $val)
        {
            $messages['fk_id_branch.'.$key.'.required'] = 'Debe seleccionar al menos una sede.';
            $messages['fk_id_branch.'.$key.'.integer'] = 'Debe seleccionar una sede de la lista';
            $messages['fk_id_branch.'.$key.'.exists'] = 'Debe seleccionar una sede de la lista';
        }

        return $messages;
        // return [
        //     'fk_id_branch.required' => 'Debe seleccionar al menos una sede.',
        //     'fk_id_branch.integer' => 'Debe seleccionar una sede de la lista',
        //     'fk_id_branch.exists' => 'Debe seleccionar una sede de la lista',
        // ];
    }
}
