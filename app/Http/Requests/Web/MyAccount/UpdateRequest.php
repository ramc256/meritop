<?php

namespace Meritop\Http\Requests\Web\MyAccount;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->redirect = route('my-account.configuration');

        return [
            'first_name'    => 'required|min:3|max:40',
            'last_name'     => 'required|min:3|max:40',
            'birthdate'     => 'required|date_format:Y-m-d',
            'biography'     => 'nullable|max:250',
            'email'         => 'required|string|min:5|max:100|email',
            'cell_phone'    => 'required|string|min:10|max:22|unique:members,cell_phone,'.\Auth::user()->id,
            'gender'        => 'required|integer|min:0|max:1',
        ];
    }

    public function messages()
    {
        return [
            'cell_phone.required' => 'El campo Teléfono es obligatorio',
            'cell_phone.min' => 'El Teléfono debe tener al menos 10 dígitos',
            'dni.required' => 'El campo dni es obligatorio',
            'biography.max' => 'El campo Biografía no debe ser mayor que :max caracteres.',
            'email.required' => 'El campo Correo es obligatorio',
            'email.email' => 'El campo Correo no contiene un correo valido',
            'email_corporate.required' => 'El campo Correo corporativo es obligatorio',
            'email_corporate.email' => 'El campo Correo corporativo no contiene un correo valido',
        ];
    }
}
