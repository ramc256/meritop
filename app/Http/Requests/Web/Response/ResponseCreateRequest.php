<?php

namespace Meritop\Http\Requests\Web\Response;

use Illuminate\Foundation\Http\FormRequest;

class ResponseCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message'           => 'required|string|max:65535',
        ];
    }

    public function messages()
    {
        return [
            'message.required' => 'El campo Mensaje es obligatorio.',
            'message.string' => 'El campo Mensaje debe ser una cadena de caracteres.',
            'message.max' => 'El Mensaje no debe ser mayor que :max caracteres.',
        ];
    }
}
