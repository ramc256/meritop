<?php

namespace Meritop\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            case 'user':
                if (Auth::guard($guard)->check()) {
                    return redirect()->route('admin.dashboard');                    
                }
                break;
            case 'web':
                if (Auth::guard($guard)->check()) {
                    return redirect('/');
                }
            break;

            default:
            if (Auth::guard($guard)->check()) {
                   flash('Ha ocurrido un error diganle a crhistian', '¡Error!')->error();
                    return redirect('/');
                }
                
            break;
        }
        return $next($request);
    }
}
