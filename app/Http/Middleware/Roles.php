<?php

namespace Meritop\Http\Middleware;

use Closure;

class Roles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $rr = 'as';
        if (! $request->$rr == $role){
            redirect('/error/404.php');
        }
        return $next($request);
    }
}
