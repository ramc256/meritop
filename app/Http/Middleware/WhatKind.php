<?php

namespace Meritop\Http\Middleware;

use Closure;

class WhatKind
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (\Auth::guard('user')->user()->kind == 1) {
                \Log::critical('Acceso denegado a: '.\Auth::guard('user')->user()->user_name);
                flash('Acceso denegado')->warning();
                return redirect()->route('admin.dashboard');
        }

        return $next($request);
    }
}
