<?php

namespace Meritop\Http\Middleware;
use Meritop\Http\Assets\ResourceFunctions;
use Illuminate\Support\Facades\Auth;
use Closure;

class PrivilegeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module)
    {

        $role = session('role');
        $comprober=false;
         $module = explode('/', $module);
         //dd($module);
         foreach ($role as $value) {
                if (  (($value->fk_id_module ==$module[0]) && ($value->fk_id_action ==$module[1])) ) {
                      //$comprober = true;
                      //dd("hola");
                    return $next($request);

                }


            }

             ResourceFunctions::messageError(Auth::guard('user')->user()->user_name,'UserController','store',null);
                return redirect()->route('admin.dashboard');

            // return $comprober;


    //     function privilegies($module,$action){
    //     $role = session('role');
    //     $comprober=false;

    //      foreach ($role as $value) {
    //             if (  (($value->fk_id_module ==$module) && ($value->fk_id_action ==$action)) ) {
    //                  $comprober = true;
    //                     // dd($value);
    //             }


    //         }

    //         return $comprober;
    // }
        // return $next($request);
    }
}
