<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class KindRole extends Model
{
    protected $table = 'kind_roles';

    protected $fillable = [
        'id', 'name', 'description'
    ];

    public function role()
    {
  	 return $this->hasmany(Role::class);
    }
}
