<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model
{
    protected $table= 'affiliates';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'id','alias','name','fiscal','address','status','latitude','longitude','phone','contact_person','email','open_hours','facebook','twiter','instagram','google_plus','slug','fk_id_city',
    	];


        public function city(){
            //relacion de de uno a muchos entre pais y estado
            return $this->hasmay(City::class);
        }
}
