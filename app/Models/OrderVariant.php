<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class OrderVariant extends Model
{
    protected $table = 'orders_variants';

    protected $fillable = [
        'id', 'quantity', 'points', 'fk_id_order', 'fk_id_variant',
    ];
}


