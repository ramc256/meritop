<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InventoryTransaction extends Model
{
    protected $table = 'inventory_transactions';

    protected $fillable = [
        'id', 'quantity_update', 'description', 'fk_id_inventory',
    ];

    public function scopeInventoryOutputQuery($query)
	{
		return $query->select(DB::raw('products.sku, parents_products.name, parents_products.model, brands.name as brand,
         clubs.name as club, SUM(inventory_transactions.quantity_update) as quantity'))
        ->join('inventory', 'inventory.id', 'inventory_transactions.fk_id_inventory')
        ->join('products', 'products.id', 'inventory.fk_id_product')
        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('clubs', 'clubs.id', 'clubs_parents_products.fk_id_club')
        ->groupBy('products.sku', 'parents_products.name', 'parents_products.model', 'brands.name', 'clubs.name')
        ->orderBy('parents_products.name');
	}

	public function scopeInventoryOutputIndex($query)
	{
		return $query->inventoryOutputQuery()
        ->where('inventory_transactions.description', 'Compra de artículos')
        ->whereMonth('inventory_transactions.created_at', date('m'));
	}

	public function scopeInventoryOutputFilter($query, $id_club, $fecha_inicio, $fecha_fin)
	{
		return $query->inventoryOutputQuery()
        ->where([
            'inventory_transactions.description' => 'Compra de artículos',
            'clubs.id' => $id_club
        ])
        ->whereBetween('inventory_transactions.created_at',  [$fecha_inicio, $fecha_fin]);
	}

    public function scopeInventoryOutputCQuery($query, $id_club, $fecha_inicio, $fecha_fin)
	{
		return $query->inventoryOutputQuery()
        ->where([
            'inventory_transactions.description' => 'devolucion de productos',
            'clubs.id' => $id_club
        ])
        ->whereBetween('inventory_transactions.created_at',  [$fecha_inicio, $fecha_fin]);
	}

}
