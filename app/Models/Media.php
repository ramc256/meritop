<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';

    protected $fillable = [
        'id', 'image', 'table', 'slug', 'identificator'
    ];

}
