<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class HistoricalLogin extends Model
{
    protected $table = 'historical_logins';

    protected $fillable = [
        'id', 'user_name', 'ip_address', 
    ];
}
