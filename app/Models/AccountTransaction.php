<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class AccountTransaction extends Model
{
    protected $table = 'accounts_transactions';

    protected $fillable = [
        'id', 'description', 'operation', 'points', 'balance', 'fk_id_account', 'fk_id_club'
    ];

}
