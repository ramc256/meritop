<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class ParentProduct extends Model
{
    protected $table = 'parents_products';

    protected $fillable = [
        'id', 'name', 'model', 'description', 'warranty', 'warranty_day', 'warranty_conditions', 'delivery_conditions', 'tag',
        'deleted', 'fk_id_brand', 'fk_id_country', 'fk_id_market'
    ];

    public function  clubParentProduct()
    {
      return $this->hasmany(ClubParentProduct::class);
    }

    public function  parentProductSubcategory()
    {
      return $this->hasmany(ParentProductSubcategory::class);
    }

    public function  product()
    {
      return $this->hasmany(Product::class);
    }

    public function scopeParentProductStore()
    {
        $club = session('club')->id;

        $parents_products = ParentProduct::select('parents_products.*', 'products.id as id_product', 'products.points',
        'media.image', 'media.slug', 'brands.name as brand')
        ->join('media', 'media.identificator', 'parents_products.id')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('products', 'products.fk_id_parent_product', 'parents_products.id')
        ->join('inventory', 'inventory.fk_id_product', 'products.id')
        ->where([
            'media.table' => 'parents_products',
            'clubs_parents_products.fk_id_club' => $club,
            ['inventory.quantity_current', '>', 0],
            'parents_products.deleted' => false,
            'products.status' => true
        ])->get();
        // ->paginate(15);

        $parents_products = $this->getDistinctParentProduct($parents_products, "id");

        return $parents_products;
    }

    public function scopeParentProductFilter($query, $club, $market, $brand, $category)
    {
        if (!empty($market) && empty($brand) && empty($category)) {
            $query->where([
                'media.table' => 'parents_products',
                'clubs_parents_products.fk_id_club' => $club,
                ['inventory.quantity_current', '>', 0],
                'products.status' => true,
                'parents_products.deleted' => false
            ])
            ->whereIn('markets.name', $market);
        }

        if (empty($market) && !empty($brand) && empty($category)) {
            $query->where([
                'media.table' => 'parents_products',
                'clubs_parents_products.fk_id_club' => $club,
                ['inventory.quantity_current', '>', 0],
                'products.status' => true,
                'parents_products.deleted' => false
            ])
            ->whereIn('brands.name', $brand);
        }

        if (empty($market) && empty($brand) && !empty($category)) {
            $query->where([
                'media.table' => 'parents_products',
                'clubs_parents_products.fk_id_club' => $club,
                ['inventory.quantity_current', '>', 0],
                'products.status' => true,
                'parents_products.deleted' => false
            ])
            ->whereIn('categories.name', $category);
        }

        if (!empty($market) && !empty($brand) && empty($category)) {
            $query->where([
                'media.table' => 'parents_products',
                'clubs_parents_products.fk_id_club' => $club,
                ['inventory.quantity_current', '>', 0],
                'products.status' => true,
                'parents_products.deleted' => false
            ])
            ->whereIn('markets.name', $market)
            ->whereIn('brands.name', $brand);
        }

        if (!empty($market) && empty($brand) && !empty($category)) {
            $query->where([
                'media.table' => 'parents_products',
                'clubs_parents_products.fk_id_club' => $club,
                ['inventory.quantity_current', '>', 0],
                'products.status' => true,
                'parents_products.deleted' => false
            ])
            ->whereIn('markets.name', $market)
            ->whereIn('categories.name', $category);
        }

        if (empty($market) && !empty($brand) && !empty($category)) {
            $query->where([
                'media.table' => 'parents_products',
                'clubs_parents_products.fk_id_club' => $club,
                ['inventory.quantity_current', '>', 0],
                'products.status' => true,
                'parents_products.deleted' => false
            ])
            ->whereIn('brands.name', $brand)
            ->whereIn('categories.name', $category);
        }

        if (!empty($market) && !empty($brand) && !empty($category)) {
            $query->where([
                'media.table' => 'parents_products',
                'clubs_parents_products.fk_id_club' => $club,
                ['inventory.quantity_current', '>', 0],
                'products.status' => true,
                'parents_products.deleted' => false
            ])
            ->whereIn('markets.name', $market)
            ->whereIn('brands.name', $brand)
            ->whereIn('categories.name', $category);
        }

        if (empty($market) && empty($brand) && empty($category)) {
            $query->where([
                'media.table' => 'parents_products',
                'clubs_parents_products.fk_id_club' => $club,
                ['inventory.quantity_current', '>', 0],
                'products.status' => true,
                'parents_products.deleted' => false
            ]);
        }

    }

    public function scopeParentProductCategory($query, $club, $category)
    {
        $query->where([
            'media.table' => 'parents_products',
            'categories.slug' => $category,
            ['inventory.quantity_current', '>', 0],
            'products.status' => true,
            'parents_products.deleted' => false
        ])
        ->where('clubs_parents_products.fk_id_club', $club);
    }

    public function scopeParentProductSubcategory($query, $club, $subcategoy)
    {
        $query->where([
            'media.table' => 'parents_products',
            'subcategories.slug' => $subcategoy,
            ['inventory.quantity_current', '>', 0],
            'products.status' => true,
            'parents_products.deleted' => false
        ])
        ->where('clubs_parents_products.fk_id_club', $club);
    }

    public function scopeParentProductMarket($query, $club, $market)
    {
        $filter = null;
        if ($market == 'male') {
            $filter = 1;
        }elseif ($market == 'female') {
            $filter = 2;
        }else {
            $filter = 0;
        }

        $query->where([
            'media.table' => 'parents_products',
            'parents_products.market' => $filter,
            ['inventory.quantity_current', '>', 0],
            'parents_products.deleted' => false
        ])
        ->where('clubs_parents_products.fk_id_club', $club);
    }

    public static function getDistinctParentProduct($all, $column)
    {
        $distintos = [];
        $parents_products = [];
        foreach ($all as $key) {
            if (!in_array($key->$column, $distintos)) {
                $distintos[] = $key->$column;
                $parents_products[] = $key;
            }
        }

        return $parents_products;
    }
}
