<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;
use Meritop\Models\ParentProduct;

class Market extends Model
{
    protected $table = 'markets';

   protected $fillable = [
       'id', 'name', 'description'
   ];

   public function parentProduct()
   {
     return $this->hasmany(ParentProduct::class);
   }
}
