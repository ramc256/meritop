<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class GroupProgram extends Model
{
    protected $table = 'groups_programs';

    protected $fillable = [
        'id', 'fk_id_group', 'fk_id_program',
    ];
}
