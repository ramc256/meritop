<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModuleAction extends Model
{
    protected $table = 'roles_modules_actions';

    protected $fillable = [
        'id', 'fk_id_role', 'fk_id_module', 'fk_id_action',
    ];

}
