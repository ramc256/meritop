<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    protected $fillable = [
        'id', 'name',
    ];

    public function parentProduct()
    {
      return $this->hasmany(ParentProduct::class);
    }
}
