<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Inventory extends Model
{
    protected $table = 'inventory';

    protected $fillable = [
        'id', 'quantity_current', 'fk_id_product',
    ];

    public function inventoryTransaction()
    {
      return $this->hasmany(InventoryTransaction::class);
    }

    public function scopeInventoryOrder($query)
    {
        return $query->select(DB::raw('distinct products.sku, parents_products.name, parents_products.model, brands.name as brand,
         clubs.name as club, SUM(orders_products.quantity) as quantity'))
        ->join('products', 'products.id', 'inventory.fk_id_product')
        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('clubs', 'clubs.id', 'clubs_parents_products.fk_id_club')
        ->join('orders_products', 'orders_products.fk_id_product', 'products.id')
        ->join('orders', 'orders.id', 'orders_products.fk_id_order')
        ->groupBy('products.sku', 'parents_products.name', 'parents_products.model', 'brands.name', 'clubs.name');
    }

    public function scopeInventoryOrderFilter($query, $id_club, $fecha_inicio, $fecha_fin)
	{
		return $query->inventoryOrder()
        ->where([
            'clubs.id' => $id_club
        ])
        ->whereNotIn('orders.status', [3,4])
        ->whereBetween('orders.created_at',  [$fecha_inicio, $fecha_fin]);
	}
}
