<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class MemberBranch extends Model
{
    protected $table = 'members_branches';

    protected $fillable = [
        'id','fk_id_member', 'fk_id_branch',
    ];
}
