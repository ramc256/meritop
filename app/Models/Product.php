<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'id', 'feature', 'sku', 'barcode', 'excerpt', 'size', 'color', 'measure',  'weight', 'width', 'heigth', 'depth', 'volume',
         'sell_out_stock', 'points', 'status', 'deleted', 'fk_id_parent_product'
    ];

    public function  inventory()
    {
      return $this->hasmany(Inventory::class);
    }

    public function  orderProduct()
    {
      return $this->hasmany(OrderProduct::class);
    }
}
