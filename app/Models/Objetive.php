<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Objetive extends Model
{
     protected $table = 'objetives';

    protected $fillable = [
        'id', 'code', 'name', 'description', 'start_date', 'due_date', 'status', 'fk_id_perspective', 'fk_id_program', 'deleted'
    ];
}
