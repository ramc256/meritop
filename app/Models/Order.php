<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'id', 'status', 'note', 'internal_note', 'id_order_reference','discount', 'fk_id_member', 'fk_id_branch'
    ];

    public function guide()
    {
      return $this->hasmany(Guide::class);
    }

    public function orderVariant()
    {
      return $this->hasmany(OrderVariant::class);
    }

    public function scopeInventoryOrder($query)
    {
        return $query->select(DB::raw('products.sku, parents_products.name as product, parents_products.model, brands.name as brand, clubs.name as club, SUM(orders_products.quantity) as quantity'))
        ->join('orders_products', 'orders_products.fk_id_order', 'orders.id')
        ->join('products', 'products.id', 'orders_products.fk_id_product')
        ->join('parents_products', 'parents_products.id', 'products.fk_id_parent_product')
        ->join('brands', 'brands.id', 'parents_products.fk_id_brand')
        ->join('clubs_parents_products', 'clubs_parents_products.fk_id_parent_product', 'parents_products.id')
        ->join('clubs', 'clubs.id', 'clubs_parents_products.fk_id_club')
        ->groupBy('products.sku', 'parents_products.name', 'parents_products.model', 'brands.name', 'clubs.name');
    }

    public function scopeInventoryOrderQuery($query)
    {
        return $query->InventoryOrder()
        ->whereNotIn('orders.status', [3,4])
        ->whereMonth('orders.created_at', date('m'));
    }

    public function scopeInventoryOrderFilter($query, $id_club, $fecha_inicio, $fecha_fin)
    {
        return $query->InventoryOrder()
        ->whereNotIn('orders.status', [3,4])
        ->where('clubs.id', $id_club)
        ->whereBetween('orders.created_at', [$fecha_inicio , $fecha_fin]);
    }

}
