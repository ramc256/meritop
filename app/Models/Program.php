<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
  protected $table = 'programs';

  protected $fillable = [
      'id', 'name', 'description', 'status', 'fk_id_club', 'deleted'
  ];

  public function objetive()
  {
    return $this->hasmany(Objetive::class);
  }
}
