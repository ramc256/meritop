<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class MemberClub extends Model
{
    protected $table = 'members_clubs';

    protected $fillable = [
        'id', 'carnet', 'email', 'title', 'supervisor_code','status','fk_id_member', 'fk_id_club'
    ];

}
