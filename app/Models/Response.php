<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $table = 'responses';

    protected $fillable = [
        'id', 'name', 'message', 'fk_id_message'
    ];
}
