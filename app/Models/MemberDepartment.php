<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class MemberDepartment extends Model
{
    protected $table = 'members_departments';

    protected $fillable = [
        'id','fk_id_member', 'fk_id_department',
    ];
}
