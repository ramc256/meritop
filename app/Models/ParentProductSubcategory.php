<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class ParentProductSubcategory extends Model
{
    protected $table = 'parents_products_subcategories';

    protected $fillable = [
        'id', 'fk_id_subcategory', 'fk_id_parent_product',
    ];
}
