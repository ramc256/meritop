<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;
use Meritop\Models\Contribution;

class Club extends Model
{
    protected $table= 'clubs';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'id','name','alias','fiscal','status','address','fk_id_country','phone','slug','color','member_format_import','state','city',
        'fk_id_currency', 'system_currency', 'convertion_rate'
    	];

    	public function User_club(){
            //relacion de uno a muchos entre clubs y userClubs
            return $this->belongsto(Users_Club::class);
        }

        public function country(){
            //relacion de de uno a muchos entre city y clubs
            return $this->belongsto(Country::class);
        }

        public function objetives()
        {
            return $this->hasmany(Objetive::class);
        }

        public function branch()
        {
            return $this->hasmany(Branch::class);
        }

        public function contribution()
        {
            return $this->hasmany(Contribution::class);
        }

        public function departments()
        {
          return $this->hasmany(Department::class);
        }

        public function banner()
        {
          return $this->hasmany(Banner::class);
        }

        public function bannedMember()
        {
          return $this->hasmany(BannedMember::class);
        }
}
