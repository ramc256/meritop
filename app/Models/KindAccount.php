<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class KindAccount extends Model
{
    protected $table = 'kind_accounts';

    protected $fillable = [
        'id', 'name', 'description'
    ];

    public function account()
    {
      return $this->hasmany(Account::class);
    }
}
