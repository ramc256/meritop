<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class UserClub extends Model
{
    protected $table= 'users_clubs';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'id','fk_id_user','fk_id_club',
    	];


    public function club(){
        //relacion de de uno a muchos entre clubs y users_clubs
        return $this->hasmay(Club::class);
    }


    public function user(){
        //relacion de de uno a muchos entre users y users_clubs
        return $this->hasmay(User::class);
    }

    public function scopeGetUserClubs($query,$id_user)
    {
        return $query->select('clubs.id', 'clubs.name')
        ->join('clubs', 'clubs.id', 'users_clubs.fk_id_club')
        ->where('fk_id_user', $id_user);
    }
}
