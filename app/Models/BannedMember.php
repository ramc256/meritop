<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class BannedMember extends Model
{
    protected $table = 'banned_members';

    protected $fillable = [
        'id', 'fk_id_club', 'fk_id_member'
    ];
}
