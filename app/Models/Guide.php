<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
    protected $table = 'guides';

    protected $fillable = [
        'id', 'num_guide', 'note', 'internal_note', 'fk_id_order', 'fk_id_agent'
    ];

}
