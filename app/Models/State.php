<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table= 'states';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'id','name', 'iso_3166-2', 'fk_id_state',
    	];


	public function city(){
        //relacion de uno a muchos entre ciudades y estados
        return $this->hasmay(City::class);
    }

    

}
