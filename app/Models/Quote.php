<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $table = 'quotes';

    protected $fillable = [
        'id', 'author', 'content'
    ];
}
