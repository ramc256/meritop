<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $fillable = [
        'id', 'subject', 'message', 'fk_id_member', 'status'
    ];

    public function response()
    {
      return $this->hasmany(Response::class);
    }
}
