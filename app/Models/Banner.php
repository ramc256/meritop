<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';

    protected $fillable = [
        'id', 'title', 'subtitle', 'fk_id_club'
    ];
}
