<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';

    protected $fillable = [
        'id', 'code', 'name', 'address', 'postal_code', 'dispatch_route', 'phone', 'seconday_phone', 'fk_id_club', 'fk_id_city'
    ];

    public function memberBranch()
    {
        return $this->hasmany(MemberBranch::class);
    }
}
