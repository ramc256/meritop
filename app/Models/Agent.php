<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = 'agents';

    protected $fillable = [
        'id', 'name',
    ];

    public function guide()
    {
      return $this->hasmany(Guide::class);
    }
}
