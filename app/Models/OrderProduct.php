<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'orders_products';

    protected $fillable = [
        'id', 'quantity', 'points', 'fk_id_order', 'fk_id_product',
    ];
}
