<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
  protected $table = 'actions';

  protected $fillable = [
      'id', 'name',
  ];

  public function role_module_action()
  {
    return $this->hasmany(Role_Module_Action::class);
  }
}
