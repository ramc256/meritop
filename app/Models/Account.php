<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'accounts';

    protected $fillable = [
        'id', 'code', 'points', 'fk_id_member', 'fk_id_kind_account'
    ];

    public function accountTransaction()
    {
      return $this->hasmany(AccountTransaction::class);
    }
}
