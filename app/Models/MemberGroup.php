<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class MemberGroup extends Model
{
    protected $table = 'members_groups';

    protected $fillable = [
        'id', 'fk_id_member', 'fk_id_group'
    ];
}
