<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table= 'countries';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'id','country_code', 'iso_3166_2', 'name', 'full_name',
    	];

    	public function states(){
            //relacion de uno a muchos entre roles y administradores
            return $this->hasmany(State::class);
        }

        public function club()
        {
            return $this->hasmany(Club::class);
        }

        public function parentProduct()
        {
            return $this->hasmany(ParentProduct::class);
        }

}
