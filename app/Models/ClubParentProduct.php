<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class ClubParentProduct extends Model
{
    protected $table= 'clubs_parents_products';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'id','fk_id_parent_product','fk_id_club',
    	];

    public function club(){
        //relacion de de uno a muchos entre clubs y clubs_products
        return $this->hasmay(Club::class);
    }

    public function parentProduct(){
        //relacion de de uno a muchos entre product y clubs_products
        return $this->hasmay(ParentProduct::class);
    }
}
