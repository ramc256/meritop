<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';

    protected $fillable = [
        'id', 'code', 'name', 'fk_id_club'
    ];

    public function memberDepartment()
    {
        return $this->hasmany(MemberDepartment::class);
    }

}
