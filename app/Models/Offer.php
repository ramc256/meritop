<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    
    protected $table= 'Offers';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
      'id','name','excerpt','description','image','quantity','quantity_per_user','frequency','start_date','due_date','status','fk_id_afiliate','fk_id_time_scale',
      ];


        public function affiliate(){
            //relacion de de uno a muchos entre pais y estado
            return $this->hasmay(Affiliate::class);
        }
        
}
