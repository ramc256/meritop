<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class HistoricalAction extends Model
{
    protected $table = 'historical_actions';

    protected $fillable = [
        'id', 'user_name', 'action', 'description', 'ip_address'
    ];
}
