<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'id', 'name', 'description', 'slug', 'status',
    ];

    public function  subcategory()
    {
      return $this->hasmany(Subcategory::class);
    }

    public function  offer()
    {
      return $this->hasmany(Offer::class);
    }
}
