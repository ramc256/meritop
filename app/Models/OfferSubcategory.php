<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class OfferSubcategory extends Model
{
    protected $table= 'Offers_subcategories';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
      'id','fk_id_subcategory','fk_id_offer',
      ];

    public function offer(){
        //relacion de de uno a muchos entre clubs y users_clubs
        return $this->hasmay(Offer::class);
    }


    public function subcategory(){
        //relacion de de uno a muchos entre users y users_clubs
        return $this->hasmay(Subcategory::class);
    }
}
