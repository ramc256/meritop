<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'id', 'name', 'description', 'fk_id_club',
    ];

    public function memberGroup()
    {
      return $this->hasmany(MemberGroup::class);
    }

    public function objetive()
    {
      return $this->hasmany(Objetive::class);
    }
}
