<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class FailedRecord extends Model
{
    protected $table = 'failed_records';

    protected $fillable = [
        'id', 'dni', 'carnet', 'objetive_code', 'points', 'status', 'fk_id_contribution'
    ];
}
