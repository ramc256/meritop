<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Perspective extends Model
{
     protected $table = 'perspectives';

    protected $fillable = [
        'id', 'name', 'description',
    ];

    public function objetives()
	{
	  return $this->hasmany(Objetive::class);
	}

}
