<?php

namespace Meritop;

use Illuminate\Database\Eloquent\Model;

class TimeScale extends Model
{
    protected $table = 'time_scales';

    protected $fillable = [
        'id', 'day', 'description',
    ];

    public function offers()
    {
      return $this->hasmany(Offer::class);
    }
}
