<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
  protected $table = 'modules';

  protected $fillable = [
      'id', 'name', 'description', 'order'
  ];

  public function role_module_action()
  {
    return $this->hasmany(RoleModuleAction::class);
  }
}
