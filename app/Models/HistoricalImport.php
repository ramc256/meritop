<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class HistoricalImport extends Model
{
    protected $table = 'historical_imports';

    protected $fillable = [
        'id', 'file_name', 'status', 'ip_address', 'total_records', 'success_records', 'fail_records','kind', 'fk_id_user'
    ];

}
