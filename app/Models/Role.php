<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;
use Meritop\Models\Action;
use Meritop\Models\Module;
use Meritop\Models\RoleModuleAction;

class Role extends Model
{
  protected $table = 'roles';

  protected $fillable = [
      'id', 'name', 'description', 'fk_id_kind_role'
  ];

  public function user()
  {
	 return $this->hasmany(User::class);
  }

  public function roleModuleAction()
  {
	 return $this->hasmany(RoleModuleAction::class);
  }

}
