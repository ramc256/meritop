<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'subcategories';

    protected $fillable = [
        'id', 'name', 'description', 'slug', 'status', 'fk_id_category',
    ];

    public function  productSubcategory()
    {
      return $this->hasmany(ProductSubcategory::class);
    }

}
