<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    protected $table= 'cities';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'id', 'name', 'fk_id_state',
    ];

    public function Memberclub()
    {
        return $this->hasmany(Memberclub::class);
    }

    public function city($id){
        
        return City::where('fk_id_state', $id)
        ->get();

    }
}
