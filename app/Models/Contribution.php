<?php

namespace Meritop\Models;

use Illuminate\Database\Eloquent\Model;

class Contribution extends Model
{
    protected $table = 'contributions';

    protected $fillable = [
        'id', 'total_users', 'processed_users', 'rejected_users', 'total_points',
        'processed_points', 'rejected_points', 'file_name', 'status', 'ip_address',
        'fk_id_user', 'fk_id_club'
    ];

    public function failedRecord()
    {
        return $this->hasmany(FailedRecord::class);
    }
}
