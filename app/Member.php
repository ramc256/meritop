<?php

namespace Meritop;
use Illuminate\Notifications\Notifiable;
use Meritop\Notifications\MemberResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Meritop\Models\Media;

use Illuminate\Database\Eloquent\Model;

class Member extends Authenticatable
{
	use Notifiable;



	protected $table = 'members';

	protected $fillable = [
			'id', 'dni', 'first_name', 'last_name', 'biography', 'birthdate','password','email','cell_phone','gender','status'
	];

	public function order()
	{
		return $this->hasmany(Order::class);
	}

	public function memberGroup()
	{
		return $this->hasmany(MemberGroup::class);
	}

	public function memberClub()
	{
		return $this->hasmany(MemberClub::class);
	}

	public function account()
	{
		return $this->hasmany(Account::class);
	}

	public function message()
	{
		return $this->hasmany(Message::class);
	}

	public function bannedMember()
	{
		return $this->hasmany(BannedMember::class);
	}

	 /**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
			'password', 'remember_token',
	];

	/**
	 * Send the password reset notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendPasswordResetNotification($token)
	{
			$this->notify(new MemberResetPassword($token));
	}

	public function scopeGetMembers($query)
	{
		return $query->select('members.*' ,  'members_clubs.carnet', 'members_clubs.email',
        'members_clubs.title', 'members_clubs.supervisor_code', 'members_clubs.status',  'accounts.points' ,  'departments.name as department')
        ->join('members_clubs', 'members_clubs.fk_id_member', 'members.id')
        ->join('accounts', 'accounts.fk_id_member', 'members.id')
        ->join('members_departments', 'members_departments.fk_id_member', 'members.id')
        ->join('departments', 'departments.id', 'members_departments.fk_id_department')
        ->where('members_clubs.fk_id_club', session('club')->id)
        ->orderBy('accounts.points', 'DESC');
	}

	public function scopeDepartmentMembers($query,$department)
	{
		return $query->getMembers()->where('departments.name', $department);
	}

	public function scopeSearchName($query, $search)
	{
		return $query->getMembers()->where('first_name', 'LIKE', "%$search%")->orWhere('last_name', 'LIKE', "%$search%");
	}


}
