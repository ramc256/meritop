<?php

namespace Meritop\Brokers;

use \Meritop\Brokers\PasswordBroker;

class PasswordBrokerManager extends \Illuminate\Auth\Passwords\PasswordBrokerManager
{
	/**
	 * @see Illuminate\Auth\Passwords\PasswordBrokerManager#resolve(string)
	 */
	protected function resolve($p_name)
    {
        $v_config = $this->getConfig($p_name);

        if (is_null($v_config)) {
            throw new InvalidArgumentException("Password resetter [{$p_name}] is not defined.");
        }

        return new PasswordBroker(
            $this->createTokenRepository($v_config),
            $this->app['auth']->createUserProvider($v_config['provider'])
        );
    }
}