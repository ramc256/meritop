<?php

namespace Meritop\Brokers;

use Illuminate\Support\Arr;
use UnexpectedValueException;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Meritop\Models\Club;

class PasswordBroker extends \Illuminate\Auth\Passwords\PasswordBroker
{
	/**
	 * @see Illuminate\Auth\Passwords\PasswordBroker#getUser(array)
	 */
	public function getUser(array $p_credentials)
    {
    	// no se recibió el club al que pertenece el miembro, entonces no es un miembro
    	if(empty($p_credentials['gateway']))
    	{
    		// quita el indice del arreglo para evitar que se busque esa columna en la tabla
    		unset($p_credentials['gateway']);

    		return parent::getUser($p_credentials);
    	}
    	// dd($p_credentials['gateway']);
	    	// $data_club = Club::select('clubs.*')
	     //        ->where('slug', $p_credentials['gateway'] )
	     //        ->first();
        // dd($data_club);    


        
        $aux = $p_credentials['gateway'];
        $p_credentials = Arr::except($p_credentials, ['token','gateway']);
        $p_credentials = Arr::except($p_credentials, ['token']);

        $user = $this->users->retrieveByCredentials($p_credentials);

        if ($user && ! $user instanceof CanResetPasswordContract) {
            throw new UnexpectedValueException('User must implement CanResetPassword interface.');
        }


        if(empty($user)){

            return parent::getUser($p_credentials);
        }

        $user['gateway'] = $aux;
        

        return $user;
    }
}