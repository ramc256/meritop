<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware('auth')->group(function(){
    // Rutas Web
	Route::get('/',                 			'WebController@index');
    Route::get('/directory',					'Web\DirectoryController@index')->name('directory');
    Route::post('/directory',					'Web\DirectoryController@search')->name('directory.search');
    Route::post('/directory/filter',		'Web\DirectoryController@filter')->name('directory.filter');
    Route::get('/ranking',						'Web\RankingController@index')->name('ranking');
    Route::get('/programs',						'Web\ProgramController@index');
    Route::get('/programs/{id}',				'Web\ProgramController@show');
    Route::get('/contact',						'WebController@contact');
    Route::get('/mall/product/{id}',          	'Web\StoreController@product') -> name('store.product');
    Route::get('/mall/market/{market}',         'Web\StoreController@market') -> name('store.market');
    Route::get('/mall/{category}',             	'Web\StoreController@category') -> name('store.category');
    Route::get('/mall/{category}/{subcategory}','Web\StoreController@subcategory') -> name('store.subcategory');
	Route::get('/mall',             			'Web\StoreController@index') -> name('store.index');
	Route::post('/mall',             			'Web\StoreController@filter') -> name('store.filter');
	Route::get('/shopping/branches/{branch_id}','WebController@branchCart');

	//Others routes
	Route::get('/politics', function() {
	    return view('web.others.politics');
	});
	Route::get('/terms', function() {
	    return view('web.others.terms');
	});
	Route::get('/faq', function() {
	    return view('web.others.faq');
	});

	//My account routes
	Route::get('/my-account/orders',      'Web\MyAccountController@orders')->name('my-account.orders');
    Route::get('/my-account/orders/{id}',      'Web\MyAccountController@order')->name('my-account.order');
    Route::post('/my-account/change-image/{id}',      'Web\MyAccountController@changeImage')->name('my-account.change-image');
    Route::get('/my-account/configuration',      'Web\MyAccountController@configuration')->name('my-account.configuration');
    Route::post('/my-account/change-password',      'Web\MyAccountController@changePassword')->name('my-account.change-password');
	Route::post('/my-account/edit-branch',      'Web\MyAccountController@updateBranch')->name('my-account.edit-branch');
    Route::get('/my-account',      'Web\MyAccountController@index')->name('my-account.index');
	Route::post('/my-account',      'Web\MyAccountController@update')->name('my-account.update');
	Route::post('/my-account/imessages/response/destroy',      'Web\MessageController@responseDestroy')->name('response.destroy');
	Route::post('/my-account/imessages/response/{id}',      'Web\MessageController@response')->name('imessage.response');
	Route::resource('/my-account/imessages',      'Web\MessageController');


    //Cart routes
    Route::get('/shopping',        	'Web\CartController@show')->name('cart.show');
    Route::post('/shopping/add-product/{id}',        	'Web\CartController@add')->name('cart.add');
    Route::get('/shopping/delete-product/{id}',        	'Web\CartController@delete')->name('cart.delete');
    Route::post('/shopping/update-products',        	'Web\CartController@update')->name('cart.update');
    Route::post('/shopping/destroy',        	'Web\CartController@destroy')->name('cart.destroy');
    Route::post('/shopping/order',        	'Web\CartController@generateOrder')->name('cart.order');

});


Route::prefix('admin')->group( function(){
	Route::post('/images/destroy', 						'Admin\AdminController@imageDestroy')->name('images.destroy');
	Route::post('/images/update', 						'Admin\AdminController@imageUpdate')->name('images.update');
	Route::get('/login',					'Admin\LoginController@showLoginForm')->name('admin.login');
	Route::post('/login',					'Admin\LoginController@login')->name('admin.login.submit');
	Route::post('/logout',					'Admin\LoginController@logout')->name('admin.logout');
	Route::post('password/email',					'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('password/reset',					'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	Route::post('password/reset',					'Admin\ResetPasswordController@reset');
	Route::get('password/reset/{token}',	'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

	Route::middleware('auth:user')->group(function(){

		Route::post('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.change-password');
		Route::get('/lists/state/{id}', 		'Admin\ListController@getState');
		Route::get('/lists/city/{id}', 			'Admin\ListController@getCity');
		Route::post('guides/store/{id}',     	'Admin\GuideController@store')->name('guides.store');
		Route::get('/', 						'Admin\AdminController@index')->name('admin.dashboard');
		Route::get('/logs', 					'\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
		Route::get('/objetives/create/{id}',    'Admin\ObjetiveController@newcreate');
		Route::get('/products/create/{id}',     'Admin\ProductController@newCreate');
		Route::post('/members/import',          'Admin\MemberController@import')->name('members.import');
		Route::get('/members/filter',      		'Admin\MemberController@filter')->name('members.filter');
		Route::post('/departments/import',		'Admin\DepartmentController@import')->name('departments.import');
		Route::post('/branches/import',     	'Admin\BranchController@import')->name('branches.import');
		Route::get('/transactions/filter',      'Admin\TransactionController@filter')->name('transactions.filter');
		Route::post('/payload/insert-file',      	'Admin\PayloadController@InsertFile')->name('payload.insert_file');
        Route::get('/payload/filter',      		'Admin\PayloadController@filter')->name('payload.filter');
        Route::post('/groups/filter',      		'Admin\GroupController@filter')->name('groups.filter');
		Route::get('/orders/filter',      		'Admin\OrderController@filter')->name('orders.filter');
		Route::put('/orders/status/{id}',       'Admin\OrderController@status')->name('orders.status');
		Route::get('/orders/refund-create/{id}','Admin\OrderController@refund_create')->name('orders.refund-create');
		Route::post('/orders/refund-store/{id}','Admin\OrderController@refund_store')->name('orders.refund-store');
		Route::get('/accounts/filter/{id}',     'Admin\AccountController@filter')->name('accounts.filter');
		Route::get('/programs/filter',          'Admin\ProgramController@filter')->name('programs.filter');
		Route::get('/picking',          'Admin\PickingController@index');
		Route::get('/reports',          'Admin\ReportController@index');
		Route::get('/reports/inventory-output',          'Admin\ReportController@inventoryOutput');
		Route::get('/reports/exchange-item',          'Admin\ReportController@exchangeItem');
		Route::get('/reports/exchange-user',     'Admin\ReportController@exchangeUser');
		Route::get('/reports/exchange-item/filter',   'Admin\ReportController@exchangeFilter')->name('exchange-item.filter');
		Route::get('/reports/exchange-user/filter',   'Admin\ReportController@exchangeUserFilter')->name('exchange-user.filter');
		Route::get('/reports/contribution-exchange',   'Admin\ReportController@reportContributionAndExchange');
		Route::get('/reports/contribution-exchange/filter',   'Admin\ReportController@reportContributionAndExchangeFilter')->name('contribution-exchange.filter');
		Route::get('/reports/inventory-output/filter',          'Admin\ReportController@inventoryOutputFilter')->name('reports.inventory-output.filter');
		Route::resource('/consolidated',        'Admin\AccountController');
		Route::resource('/transactions',        'Admin\TransactionController');
		Route::resource('/payload',             'Admin\PayloadController');
		Route::resource('/accounts',            'Admin\AccountController');
		Route::post('/profile/image-change/{id}',		'Admin\ProfileController@imageChange');
		Route::resource('/profile',				'Admin\ProfileController');
		Route::resource('/users', 				'Admin\UserController');
		Route::resource('/roles', 				'Admin\RoleController');
		Route::resource('/clubs', 				'Admin\ClubController');
		Route::resource('/brands', 		  		'Admin\BrandController');
		Route::resource('/categories', 			'Admin\CategoryController');
		Route::resource('/subcategories', 		'Admin\SubcategoryController');
		Route::resource('/offers', 				'Admin\OfferController');
		Route::resource('/affiliates', 			'Admin\AffiliateController');
		Route::resource('/parents-products', 	'Admin\ParentProductController');
		Route::resource('/inventory', 			'Admin\InventoryController');
		Route::resource('/products', 			'Admin\ProductController');
		Route::resource('/agents', 				'Admin\AgentController');
		Route::resource('/orders', 				'Admin\OrderController');
		Route::resource('/programs', 			'Admin\ProgramController');
		Route::resource('/groups', 				'Admin\GroupController');
		Route::resource('/objetives', 			'Admin\ObjetiveController');
		Route::resource('/members', 			'Admin\MemberController');
		Route::resource('/branches',     		'Admin\BranchController');
		Route::resource('departments',     		'Admin\DepartmentController');
		Route::post('/messages/change-status',      'Admin\MessageController@updateStatus')->name('message.change-status');
		Route::post('/messages/response/destroy',      'Admin\MessageController@responseDestroy')->name('responses.destroy');
		Route::post('/messages/response/{id}',      'Admin\MessageController@response')->name('message.response');
		Route::resource('/messages',      'Admin\MessageController');
	    Route::resource('/banners', 		  		'Admin\BannerController');
	    Route::resource('/markets',                   'Admin\MarketController');
		// RUTAS DE PRUEBA MODULO EXPORTAR IMPORTAR
		// Route::get('/download/{operation}/{club}/{fecha}', 'Admin\DataController@generateConsult');


	});

});






Route::middleware('guest')->group(function(){
	Route::get('{club}/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('member.password.email');
	Route::get('password/reset/{club}/{token}',	'Auth\ResetPasswordController@showResetForm')->name('members.password.reset');
	Route::post('{club}/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::post('password/reset/{club}', 'Auth\ResetPasswordController@reset');
	//Rutas de autentificacion
});

Auth::routes();

Route::middleware('guest')->group(function(){
	//Rutas de autentificacion
    Route::get('{club}', 'Auth\LoginController@showLoginForm')->name('member.login');
});
//Rutas para las vista de errores
Route::any('admin/{catchall}', function() {
   return Response::view('errors.admin.404', [], 404);
   return Response::view('errors.admin.500', [], 500);
});
Route::any('{catchall}', function() {
   return Response::view('errors.404', [], 404);
   return Response::view('errors.500', [], 500);
});
