"use strict";

$(document).ready(function() {
    $('.datatable-asc').dataTable({
        "responsive": true,
        "order": [ 0, 'asc']
    });
    $('.datatable-desc').dataTable({
        "responsive": true,
        "order": [ 0, 'desc']
    });
    $('#marcas').dataTable({
        "responsive": true,
        "order": [ 0, 'desc']
    });
    $('#countries_table').dataTable({
        "responsive": true,
        "order": [ 0, 'desc']
    });
    $('button.toggle-vis').on('click', function(e) {
        e.preventDefault();

        // Get the column API object
        var column = table.column($(this).attr('data-column'));

        // Toggle the visibility
        column.visible(!column.visible());
    });

    
});
