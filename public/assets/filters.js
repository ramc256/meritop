$(document).ready(function(){

	$("#departments").change(function(){
	   
	    var value = $(this).val();

	   	$('#tablet tr').each(function(){
	   		
	   		if($(this).find('.department').text() == value){   	
	   			$(this).removeClass('hidden');   
	   		}else{						
	   			$(this).addClass('hidden');
	   		}
	   	});
	});

});