/*
 	Opciones por default:

 	options_default({
   		table : '#filter-table',
   		column : '.filter-column'
 	})

 	Para el correcto funcionamiento de plugin se debe colocar el id #filterInput al filtro que se desea aplicar, el id #filterTable a la tabla
 	y la clase .filterColumn.

 	En caso de que se desee aplicar el filtro a otra tabla se deberá especificar el nuevo nombre de la tabla y la columna 
 	de la siguiente manera

 	$('nombre_del_filtro').filterTables({
		table : 'nombre_de_la_tabla',
		column : 'nombre_de_la_columna'
 	});

 	Esto se deberá colocar entre etiquetas "<scripts>" en el archivo donde se encuentra la tabla
 	o agregarlo en un archivo "custom" externo.

 	Se pueden usar diversos filtros para una tabla de la siguiente forma

 	$('razas').filterTables({
		table : 'perritos',					//Establece el nombre de la tabla como por ejemplo "perritos"
		column : 'raza'					//Columna por la cual se efectuará el filtrado, ejemplo "raza"
 	});
 	$('estaturas').filterTables({
		table : 'perritos',					//Seguimos usando el mismo nombre de tabla como por ejemplo "perritos"
		column : 'estatura'					//Columna por la cual se efectuará el filtrado, ejemplo "estatura"
 	});

 	También se puede hacer uso del filtro para distintas tablas pero filtrando la misma columna
 	$('razas_perros').filterTables({
		table : 'perros',				
		column : 'raza'					
 	});
 	$('razas_gatos').filterTables({
		table : 'gatos',					
		column : 'raza'					
 	});
*/


(function($) {
	$.fn.filterTables = function(options_user){
		$(this).change(function(){
		    var value = $(this).val();

		   	$(options.table + ' tbody tr').each(function(){		   		
		   		if(($(this).find(options.column).text() == value) || (value == 'all') ){   	
		   			$(this).removeClass('hidden');   
		   		}else{						
		   			$(this).addClass('hidden');
		   		}
		   	});
		});
		options_default = {
			table 	: '#filter-table',
			column 	: '.filter-column'
		};
		var options = $.extend( options_default, options_user);
	};	
	$("#filterInput").filterTables();
})(jQuery);



