<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'users' => [
            'driver' => 'local',
            'root' => public_path('storage/users'),
            'url' => env('APP_URL').'/storage/users',
            'visibility' => 'public',
        ],

        'clubs' => [
            'driver' => 'local',
            'root' => public_path('storage/clubs'),
            'url' => env('APP_URL').'/storage/clubs',
            'visibility' => 'public',
        ],

        'members' => [
            'driver' => 'local',
            'root' => public_path('storage/members'),
            'url' => env('APP_URL').'/storage/members',
            'visibility' => 'public',
        ],

        'programs' => [
            'driver' => 'local',
            'root' => public_path('storage/programs'),
            'url' => env('APP_URL').'/storage/programs',
            'visibility' => 'public',
        ],

        'products' => [
            'driver' => 'local',
            'root' => public_path('storage/products'),
            'url' => env('APP_URL').'/storage/products',
            'visibility' => 'public',
        ],

        'variants' => [
            'driver' => 'local',
            'root' => public_path('storage/variants'),
            'url' => env('APP_URL').'/storage/variants',
            'visibility' => 'public',
        ],

        'banners' => [
            'driver' => 'local',
            'root' => public_path('storage/banners'),
            'url' => env('APP_URL').'/storage/banners',
            'visibility' => 'public',
        ],

        'login' => [
            'driver' => 'local',
            'root' => public_path('storage/login'),
            'url' => env('APP_URL').'/storage/login',
            'visibility' => 'public',
        ],

        'media' => [
            'driver' => 'local',
            'root' => public_path('storage/media'),
            'url' => env('APP_URL').'/storage/media',
            'visibility' => 'public',
        ],

        'categories' => [
            'driver' => 'local',
            'root' => public_path('storage/categories'),
            'url' => env('APP_URL').'/storage/categories',
            'visibility' => 'public',
        ],

        'images' => [
            'driver' => 'local',
            'root' => public_path('images'),
            'url' => env('APP_URL').'/images',
            'visibility' => 'public',
        ],

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],

    ],

];
