<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
        DB::table('roles')->insert([
            'name' => 'Super Admin',
            'description' => 'Super Administrator',
            'fk_id_kind_role' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('roles')->insert([
           'name' => 'Admin',
           'description' => 'Administrator',
           'fk_id_kind_role' => 2,
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
        ]);
     }
}
