<?php

use Illuminate\Database\Seeder;
use Meritop\Models\Club;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
        DB::table('users')->insert([
            'first_name' => 'administrator',
            'last_name' => 'del sistema',
            'user_name' => 'admin',
            'email' => 'dev@meritop.com',
            'password' => bcrypt('admin1234'),
            'kind' => '1',
            'status' => '1',
            'fk_id_role' => '1',
            'fk_id_country' => '245',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $clubs = Club::all();

        for ($i = 1; $i <= count($clubs); $i++) {
            DB::table('users_clubs')->insert([
                'fk_id_user' => 1,
                'fk_id_club' => $i,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        DB::table('users')->insert([
            'first_name' => 'administrator',
            'last_name' => 'del club meritop',
            'user_name' => 'adminmeritop',
            'email' => 'dev@meritop.com',
            'password' => bcrypt('admin1234'),
            'kind' => '0',
            'status' => '1',
            'fk_id_role' => '2',
            'fk_id_country' => '245',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('users_clubs')->insert([
            'fk_id_user' => 2,
            'fk_id_club' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
     }
}
