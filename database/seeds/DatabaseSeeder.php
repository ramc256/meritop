<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(ActionsTableSeeder::class);
        // $this->call(ModulesTableSeeder::class);
        // $this->call(KindRolesTableSeeder::class);
        // $this->call(RolesTableSeeder::class);
        // $this->call(RolesModulesActionsTableSeeder::class);
        // $this->call('CountriesSeeder');
        // $this->call(StatesTableSeeder::class);
        // $this->call(CitiesTableSeeder::class);
        $this->call(Illuminate\Database\Seeds\GeoData\Seeders\PanamaSeeder::class);
        $this->call(Illuminate\Database\Seeds\GeoData\Seeders\GuatemalaSeeder::class);
        $this->call(Illuminate\Database\Seeds\GeoData\Seeders\SpainSeeder::class);
        $this->call(Illuminate\Database\Seeds\GeoData\Seeders\UnitedStatesSeeder::class);
        // $this->call(ClubsTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        // $this->call(CategoriesTableSeeder::class);
        // $this->call(SubcategoriesTableSeeder::class);
        // $this->call(TimeScalesTableSeeder::class);
        // $this->call(PerspectivesTableSeeder::class);
        // $this->call(QuotesTableSeeder::class);
        // $this->call(KindAccountsTableSeeder::class);
        // $this->call(AgentsTableSeeder::class);
        // $this->call(MarketsTableSeeder::class);
        // $this->call(DepartmentsTableSeeder::class);
        // $this->call(BranchesTableSeeder::class);
        // $this->call(MembersTableSeeder::class);
        // $this->call(MembersClubsTableSeeder::class);
        // $this->call(AccountsTableSeeder::class);
        // $this->call(GroupsTableSeeder::class);
        // $this->call(MembersGroupsTableSeeder::class);
        // $this->call(MembersDepartmentsTableSeeder::class);
        // $this->call(MembersBranchesTableSeeder::class);

        //-------------------------------Data de prueba-------------------------------//

        // $this->command->info('--------------Data de prueba--------------');
        // $this->call(BrandsTableSeeder::class);
        // $this->call(ProductsTableSeeder::class);
        // $this->call(VariantsTableSeeder::class);
        // $this->call(ProductsSubcategoriesTableSeeder::class);
        // $this->call(ClubsProductsTableSeeder::class);
        // $this->call(GroupsTableSeeder::class);
        // $this->call(ProgramsTableSeeder::class);
        // $this->call(ObjetivesTableSeeder::class);
        // $this->call(GroupsProgramsTableSeeder::class);
        // $this->call(HistoricalActionsTableSeeder::class);
        // $this->call(MembersTableSeeder::class);
        // $this->call(DepartmentsTableSeeder::class);
        // $this->call(BranchesTableSeeder::class);
        // $this->call(MembersClubsTableSeeder::class);
        // $this->call(MembersDepartmentsTableSeeder::class);
        // Model::unguard();
        // factory('Meritop\Models\MemberGroup', 50)->create();
        // factory('Meritop\Models\MemberBranch', 50)->create();
        // factory('Meritop\Models\Account', 50)->create();
        // factory('Meritop\Models\AccountTransaction', 50)->create();
        // factory('Meritop\Models\HistoricalImport', 50)->create();
        // factory('Meritop\Models\Order', 50)->create();
        // factory('Meritop\Models\OrderVariant', 50)->create();
        // factory('Meritop\Models\Guide', 50)->create();
        // Model::reguard();
        // $this->command->info('--------------Fin de data de prueba--------------');

        //-------------------------------Fin de data de prueba-------------------------------//
    }
}
