<?php

use Illuminate\Database\Seeder;

class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $actions = array("listar", "ver", "crear", "editar", "eliminar", "importar", "exportar");

      for ($i = 0; $i < count($actions); $i++) {
        DB::table('actions')->insert([
           'name' => $actions[$i],
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
       ]);
      }

    }
}
