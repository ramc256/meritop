<?php

use Illuminate\Database\Seeder;

class ClubsProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $fk_id_products = array("1", "2", "2", "3", "3", "1", "2", "3", "4", "5", "5", "6", "4", "6", "4", "5");

      $fk_id_clubs = array("1", "1", "2", "2", "3", "4", "4", "4", "1", "1", "2", "2", "3", "3", "4", "4");

      for ($i=0; $i < count($fk_id_products); $i++) {
          DB::table('clubs_products')->insert([
            'fk_id_product' => $fk_id_products[$i],
            'fk_id_club' => $fk_id_clubs[$i],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
      }
  }
}
