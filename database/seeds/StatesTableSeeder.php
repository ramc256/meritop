<?php

use Illuminate\Database\Seeder;
use Meritop\Models\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::create(array( 'id' => 1, 'name' => 'Amazonas', 'iso_3166-2'=> 'VE-X', 'fk_id_country' => '245'));
		State::create(array( 'id' => 2, 'name' => 'Anzoátegui', 'iso_3166-2'=> 'VE-B', 'fk_id_country' => '245'));
		State::create(array( 'id' => 3, 'name' => 'Apure', 'iso_3166-2'=> 'VE-C', 'fk_id_country' => '245'));
		State::create(array( 'id' => 4, 'name' => 'Aragua', 'iso_3166-2'=> 'VE-D', 'fk_id_country' => '245'));
		State::create(array( 'id' => 5, 'name' => 'Barinas', 'iso_3166-2'=> 'VE-E', 'fk_id_country' => '245'));
		State::create(array( 'id' => 6, 'name' => 'Bolívar', 'iso_3166-2'=> 'VE-F', 'fk_id_country' => '245'));
		State::create(array( 'id' => 7, 'name' => 'Carabobo', 'iso_3166-2'=> 'VE-G', 'fk_id_country' => '245'));
		State::create(array( 'id' => 8, 'name' => 'Cojedes', 'iso_3166-2'=> 'VE-H', 'fk_id_country' => '245'));
		State::create(array( 'id' => 9, 'name' => 'Delta Amacuro', 'iso_3166-2'=> 'VE-Y', 'fk_id_country' => '245'));
		State::create(array( 'id' => 10, 'name' => 'Falcón', 'iso_3166-2'=> 'VE-I', 'fk_id_country' => '245'));
		State::create(array( 'id' => 11, 'name' => 'Guárico', 'iso_3166-2'=> 'VE-J', 'fk_id_country' => '245'));
		State::create(array( 'id' => 12, 'name' => 'Lara', 'iso_3166-2'=> 'VE-K', 'fk_id_country' => '245'));
		State::create(array( 'id' => 13, 'name' => 'Mérida', 'iso_3166-2'=> 'VE-L', 'fk_id_country' => '245'));
		State::create(array( 'id' => 14, 'name' => 'Miranda', 'iso_3166-2'=> 'VE-M', 'fk_id_country' => '245'));
		State::create(array( 'id' => 15, 'name' => 'Monagas', 'iso_3166-2'=> 'VE-N', 'fk_id_country' => '245'));
		State::create(array( 'id' => 16, 'name' => 'Nueva Esparta', 'iso_3166-2'=> 'VE-O', 'fk_id_country' => '245'));
		State::create(array( 'id' => 17, 'name' => 'Portuguesa', 'iso_3166-2'=> 'VE-P', 'fk_id_country' => '245'));
		State::create(array( 'id' => 18, 'name' => 'Sucre', 'iso_3166-2'=> 'VE-R', 'fk_id_country' => '245'));
		State::create(array( 'id' => 19, 'name' => 'Táchira', 'iso_3166-2'=> 'VE-S', 'fk_id_country' => '245'));
		State::create(array( 'id' => 20, 'name' => 'Trujillo', 'iso_3166-2'=> 'VE-T', 'fk_id_country' => '245'));
		State::create(array( 'id' => 21, 'name' => 'Vargas', 'iso_3166-2'=> 'VE-W', 'fk_id_country' => '245'));
		State::create(array( 'id' => 22, 'name' => 'Yaracuy', 'iso_3166-2'=> 'VE-U', 'fk_id_country' => '245'));
		State::create(array( 'id' => 23, 'name' => 'Zulia', 'iso_3166-2'=> 'VE-V', 'fk_id_country' => '245'));
		State::create(array( 'id' => 24, 'name' => 'Distrito Capital', 'iso_3166-2'=> 'VE-A', 'fk_id_country' => '245'));
		State::create(array( 'id' => 25, 'name' => 'Dependencias Federales', 'iso_3166-2'=> 'VE-Z', 'fk_id_country' => '245'));
    }
}
