<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $names = array("automotríz", "bienestar", "establecimientos", "eventos", "gastronomia", "hogar", "moda", 'servicios', "tecnología",
       "viajes");

      $descriptions = array('categoría automotríz', 'categoría miembros', 'categoría configuración', 'categoría productos', 'categoría afilaidos',
                            'categoría ofertas', 'categoría categorías', 'categoría subcategorías', 'categoría usuarios', 'categoría roles');

      $slug = array("automotríz", "bienestar", "establecimientos", "eventos", "gastronomia", "hogar", "moda", 'servicios', "tecnología",
       "viajes");

      $status = array("1", "1", "1", "1", "1", "1", "1", '1', "1","1");

      for ($i = 0; $i < count($names); $i++) {
        DB::table('categories')->insert([
           'name' => $names[$i],
           'description' => $descriptions[$i],
           'slug' => $slug[$i],
           'status' => $status[$i],
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
       ]);
      }

      $faker = \Faker\Factory::create();

      for ($i = 0; $i < count($slug); $i++) {
         DB::table('media')->insert([
            'image' => $names[$i] . '.jpg',
            'slug' => $slug[$i],
            'identificator' => $i + 1,
            'table' => 'categories',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
       }
    }
}
