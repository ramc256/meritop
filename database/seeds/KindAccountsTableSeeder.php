<?php

use Illuminate\Database\Seeder;

class KindAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = array('puntos');

        $descriptions = array('cuenta de tipo puntos');

        for ($i=0; $i < count($names); $i++) {
            DB::table('kind_accounts')->insert([
               'name' => $names[$i],
               'description' => $descriptions[$i],
               'created_at' => date('Y-m-d H:i:s'),
               'updated_at' => date('Y-m-d H:i:s')
           ]);
        }
    }
}
