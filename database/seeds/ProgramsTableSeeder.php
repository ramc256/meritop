<?php

use Illuminate\Database\Seeder;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

            for ($i = 0; $i < 50; $i++) {
              DB::table('programs')->insert([
                 'name' => $faker->name,
                 'description' => $faker->text($maxNbChars = 200),
                 'status' => $faker->numberBetween($min = 0, $max = 1),
                 'fk_id_club' => $faker->numberBetween($min = 1, $max = 3),
                 'created_at' => date('Y-m-d H:i:s'),
                 'updated_at' => date('Y-m-d H:i:s')
              ]);

              $image =  $faker->randomElement(['img-default-1.jpg', 'img-default-2.jpg', 'img-default-3.jpg', 'img-default-4.jpg']);

              DB::table('media')->insert([
                 'image' => $image,
                 'slug' => $image,
                 'identificator' => $i + 1,
                 'table' => 'programs',
                 'created_at' => date('Y-m-d H:i:s'),
                 'updated_at' => date('Y-m-d H:i:s')
             ]);
            }
    }
}
