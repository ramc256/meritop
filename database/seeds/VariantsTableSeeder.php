<?php

use Illuminate\Database\Seeder;

class VariantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //-----------------------------------Dates of variants-----------------------------------//
      $skus = array("a1b2c3d4", "q1w2e3r", "p9l6i5o7", "b2c3d4f5", "x2d5e8r9", "q7s4d5e8", "l09oi80", "5f9e73f", "2b5d8er", "2b5d8es");

      $barcodes = array("124578", "134679", "141516", "794613", "976431", "172839", "461973", "769435", "287319" , "287320");

      $size = array('5', '5', '6', '3', '36', '41',  '15', "45", "20", "20");

      $clors = array("azul", "blanco", "negro", "negro", "blanco", "negro", "cristal", "azul", "negro", "blanco");

      $measures = array("12", "12", "12", "12", "12", "12", "12", "12", "12", "12");

      $weights = array("21", "21", "21", "21", "21", "21", "21", "12", "21", "21");

      $withs = array("13", "13", "13", "13", "13", "13", "13", "12", "13", "13");

      $heigths = array("31", "31", "31", "31", "31", "31", "31", "12", "31", "31");

      $depths = array("14", "14", "14", "14", "14", "14", "14", "12", "14", "14");

      $volumes = array("41", "41", "41", "41", "41", "41", "41", "12", "41", "41");

      $sell_out_stocks = array(true, true, false, false, true, false, true, false, false, false);

      $points = array("1500", "1650", "1650", "900", "1200", "1200", "800", "400", "100", "100");

      $status = array("1", "1", "0", "0", "1", "1", "1", "1", "1", "1");

      $fk_id_products = array("1", "1", "1", "2", "3", "3", "4", "5", "6", "6");

      $fk_id_countries = array("245", "245", "245", "245", "245", "245", "245", "245", "245", "245");

     for ($i = 0; $i < count($skus); $i++) {
        DB::table('variants')->insert([
           'sku' => $skus[$i],
           'barcode' => $barcodes[$i],
           'size' => $size[$i],
           'color' => $clors[$i],
           'measure' => $measures[$i],
           'weight' => $weights[$i],
           'width' => $withs[$i],
           'heigth' => $heigths[$i],
           'depth' => $depths[$i],
           'volume' => $volumes[$i],
           'sell_out_stock' => $sell_out_stocks[$i],
           'points' => $points[$i],
           'status' => $status[$i],
           'fk_id_product' => $fk_id_products[$i],
           'fk_id_country' => $fk_id_countries[$i],
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
       ]);
      }

        $faker = \Faker\Factory::create();

        for ($i = 1; $i <=  count($skus); $i++) {
          //-----------------------------------Dates of inventory-----------------------------------//
            DB::table('inventory')->insert([
                'quantity_current' => 0,
                'fk_id_variant' => $i,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
           ]);


            //-----------------------------------Dates of hisotrical-----------------------------------//
            DB::table('inventory_transactions')->insert([
               'quantity_update' => 0,
               'description' => 'cantidad inicial',
               'fk_id_inventory' => $i,
               'created_at' => date('Y-m-d H:i:s'),
               'updated_at' => date('Y-m-d H:i:s')
           ]);

           DB::table('prices')->insert([
              'amount' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = NULL),
              'fk_id_variant' => $i,
              'fk_id_kind_account' => 1,
              'created_at' => date('Y-m-d H:i:s'),
              'updated_at' => date('Y-m-d H:i:s')
          ]);
        }

        $faker = \Faker\Factory::create();

        $slugs = array("teléfono-pixi-4-blanco", "teléfono-pixi-4-azul", "teléfono-pixi-4-rojo", "teléfono-bold-azul", "botas-square-rosadas", "botas-square-azules", "vajilla-cristal", "bankpower-energy", "boxers-machomen-negros", "boxers-machomen-blancos");

        $identificatos = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");

        $tables = array('variants','variants','variants', 'variants','variants','variants','variants','variants','variants','variants');

        for ($i = 0; $i < count($slugs); $i++) {
           DB::table('media')->insert([
              'image' => $faker->randomElement(['img-default-1.jpg', 'img-default-2.jpg', 'img-default-3.jpg', 'img-default-4.jpg']),
              'slug' => $slugs[$i],
              'identificator' => $identificatos[$i],
              'table' => $tables[$i],
              'created_at' => date('Y-m-d H:i:s'),
              'updated_at' => date('Y-m-d H:i:s')
          ]);
         }

       }
}
