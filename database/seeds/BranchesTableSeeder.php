<?php

use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
          'code' => '1',
          'name' => 'Boleita Sur',
          'address' => 'calle República Dominicana, Edif. Feltre, Piso 3, Local 3 - B, Urb. Boleita',
          'postal_code' => '1070',
          'dispatch_route' => 'las de ZOOM',
          'phone' => '+582122398820',
          'secondary_phone' => '+582122398820',
          'fk_id_club' => 1,
          'fk_id_city' => 137,
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
      ]);
    }
}
