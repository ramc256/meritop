<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = \Faker\Factory::create();

      $names = array("teléfono pixi 4", "teléfono bold", "botas square", "vajilla de cristal", "bankpower energy", "boxers machomen");

      $excerpts = array("dispositio móvil", "dispositio móvil", "botas deportivas", "vajilla de exceletne calidad", "cargador portable", "ropa interior");

      $models = array('5045A', 'Bold', 'square', "cristal", "5RE", "extensible");

      $descriptions = array("teléfono celular de gama media", "teléfono celular de gama baja", "zapatos deportivos aerodinámicos", "vajilla de alta cocina y presencia", "cargador portable para celulares genérico", "ropa interior para hombre");

      $markets = array("0", "0", "0", "1", "0", "2");

      $warranties = array(true, false, true, true, false, true);

      $warranty_days = array("240", null, "30", "30", null, "30");

      $warranty_conditions = array("únicamente defectos de fabrica", null, "únicamente defectos de fabrica", "únicamente defectos de fabrica", null, "únicamente defectos de fabrica");

      $delivery_conditions = array("únicamente por zoom", "por zoom y mrw", "únicamente por zoom", "únicamente por zoom", "por zoom y mrw", "únicamente por zoom");

      $deleteds = array(false, false, false, false, false, false);

      $fk_id_brands = array("1", "2", "3", "4", "5", "3");

      $tags = array("pixi,spmartphone,android", null, "deporte,atletismo", "vajilla", "bankpower", "boxers,ropa-interior-hombre");

     for ($i = 0; $i < count($names); $i++) {
        DB::table('products')->insert([
           'name' => $names[$i],
           'excerpt' => $excerpts[$i],
           'model' => $models[$i],
           'description' => $descriptions[$i],
           'market' => $markets[$i],
           'warranty' => $warranties[$i],
           'warranty_day' => $warranty_days[$i],
           'warranty_conditions' => $warranty_conditions[$i],
           'delivery_conditions' => $delivery_conditions[$i],
           'deleted' => $deleteds[$i],
           'fk_id_brand' => $fk_id_brands[$i],
           'tags' => $tags[$i],
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
        ]);
      }

      $slugs = array("teléfono-pixi-4-1", "teléfono-pixi-4-2", "teléfono-pixi-4-3", "teléfono-pixi-4-4", "teléfono-pixi-4-5", "teléfono-pixi-4-6",
                    "teléfono-bold-1", "botas-square-1", "botas-square-2", "vajilla-cristal", "bankpower-energy",
                "boxers-machomen");

      $identificatos = array("1", "1", "1", "1", "1", "1", "2", "3", "3", "4", "5", "6", "6");

      $tables = array('products','products','products', 'products','products','products', 'products','products','products','products','products','products','products');

      for ($i = 0; $i < count($slugs); $i++) {
         DB::table('media')->insert([
            'image' => $faker->randomElement(['img-default-1.jpg', 'img-default-2.jpg', 'img-default-3.jpg', 'img-default-4.jpg']),
            'slug' => $slugs[$i],
            'identificator' => $identificatos[$i],
            'table' => $tables[$i],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
       }
    }
}
