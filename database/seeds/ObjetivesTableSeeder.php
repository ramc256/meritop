<?php

use Illuminate\Database\Seeder;

class ObjetivesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

            for ($i = 0; $i < 50; $i++) {
              DB::table('objetives')->insert([
                 'code' => $faker->unique()->regexify('[A-Z0-9]{6}'),
                 'name' => $faker->name,
                 'description' => $faker->text($maxNbChars = 200),
                 'start_date' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now'),
                 'due_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+2 years'),
                 'status' => $faker->boolean($chanceOfGettingTrue = 50),
                 'fk_id_perspective' => $faker->numberBetween($min = 1, $max = 5),
                 'fk_id_program' => $faker->numberBetween($min = 1, $max = 50),
                 'created_at' => date('Y-m-d H:i:s'),
                 'updated_at' => date('Y-m-d H:i:s')
              ]);
            }
    }
}
