<?php

use Illuminate\Database\Seeder;

class GroupsProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

            for ($i = 0; $i < 5; $i++) {
              DB::table('groups_programs')->insert([
                 'fk_id_group' => $faker->numberBetween($min = 1, $max = 5),
                 'fk_id_program' => $faker->numberBetween($min = 1, $max = 5),
                 'created_at' => date('Y-m-d H:i:s'),
                 'updated_at' => date('Y-m-d H:i:s')
              ]);
            }
    }
}
