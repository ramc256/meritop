<?php

use Illuminate\Database\Seeder;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $names = array("zoom");

      for ($i = 0; $i < count($names); $i++) {
        DB::table('agents')->insert([
           'name' => $names[$i],
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
       ]);
      }
    }
}
