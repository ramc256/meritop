<?php

use Illuminate\Database\Seeder;

class MarketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $names = array("todos", "hombres", "mujeres", "niños", "niñas");

      $descriptions = array('todos los mercados', 'mercado de hombres', 'mercado de mujeres', 'mercado de niños', 'mercado de niñas');

      for ($i = 0; $i < count($names); $i++) {
        DB::table('markets')->insert([
           'name' => $names[$i],
           'description' => $descriptions[$i],
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
       ]);
      }

    }
}
