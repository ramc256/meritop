<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
            ["name"=>"consolidado","description"=>"módulo consolidado","order"=>1,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"estado de cuenta","description"=>"módulo estado de cuenta","order"=>2,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"aporte de puntos","description"=>"módulo aporte de puntos","order"=>3,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"miembros","description"=>"módulo miembros","order"=>4,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"grupos","description"=>"módulo grupos","order"=>5,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"programas","description"=>"módulo programas","order"=>6,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"productos","description"=>"módulo productos","order"=>7,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"marcas","description"=>"módulo marcas","order"=>8,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"inventario","description"=>"módulo inventario","order"=>10,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"categorías","description"=>"módulo categorías","order"=>11,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"subcategorías","description"=>"módulo subcategorías","order"=>12,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"ordenes","description"=>"módulo ordenes","order"=>13,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"clubs","description"=>"módulo clubs","order"=>14,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"sedes","description"=>"módulo sedes","order"=>15,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"departamentos","description"=>"módulo departamentos","order"=>16,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"banners","description"=>"módulo banners","order"=>17,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"usuarios","description"=>"módulo usuarios","order"=>18,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"roles","description"=>"módulo roles","order"=>19,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"logs","description"=>"módulo logs","order"=>20,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"tipos de cuentas","description"=>"módulo tipos de cuentas","order"=>21,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"mensajes","description"=>"módulo mensajes","order"=>22,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')],
            ["name"=>"mercados","description"=>"módulo mercados","order"=>9,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s')]
        ];

        DB::table('modules')->insert($modules);

    }
}
