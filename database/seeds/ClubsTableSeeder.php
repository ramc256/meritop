<?php

use Illuminate\Database\Seeder;

class ClubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    //   $faker = \Faker\Factory::create();
      //
    //   $names = array("meritop", "digitel empresa", "telefónica movistar", "mercantl seguros");
      //
    //   $aliases = array("meritop", "digitel", "telefónica", "mercantl seguros");
      //
    //   $fiscals = array("8523697", '124578', '784512', '134679');
      //
    //   $states = array("Miranda", "Miranda", "Miranda", "Miranda");
      //
    //   $addresses = array("Boleita Sur", "la castellana", "altamira", "chacaito");
      //
    //   $cities = array("Caracas", "Caracas", "Caracas", "Caracas");
      //
    //   $phones = array("798946", "875421", "215487", "316497");
      //
    //   $slugs = array("meritop", "digitel", "telefónica", "mercantl-seguros");
      //
    //   $colors = array("naranja", "red", "verde", "azul");
      //
    //   $status = array("1", "1", "1", "1");
      //
    //   $fk_id_countries = array("245", "245", "245", "245");
      //
    //   for ($i = 0; $i < count($names); $i++) {
    //     DB::table('clubs')->insert([
    //        'name' => $names[$i],
    //        'alias' => $aliases[$i],
    //        'fiscal' => $fiscals[$i],
    //        'state' => $states[$i],
    //        'address' => $addresses[$i],
    //        'city' => $cities[$i],
    //        'phone' => $phones[$i],
    //        'slug' => $slugs[$i],
    //        'color' => $colors[$i],
    //        'status' => $status[$i],
    //        'fk_id_country' => $fk_id_countries[$i],
    //        'created_at' => date('Y-m-d H:i:s'),
    //        'updated_at' => date('Y-m-d H:i:s')
    //    ]);
      //
    //    $image =  $faker->randomElement(['img-default-1.jpg', 'img-default-2.jpg', 'img-default-3.jpg', 'img-default-4.jpg']);
      //
    //    DB::table('media')->insert([
    //       'image' => $image,
    //       'slug' => $image,
    //       'identificator' => $i + 1,
    //       'table' => 'clubs',
    //       'created_at' => date('Y-m-d H:i:s'),
    //       'updated_at' => date('Y-m-d H:i:s')
    //   ]);
    //   }

        DB::table('clubs')->insert([
         'name' => 'meritop',
         'alias' => 'meritop',
         'fiscal' => 'J-407981994',
         'state' => 'Miranda',
         'address' => 'Boleita Sur, entre el canal Tves y la Alcaldía del Municipio Sucre',
         'city' => 'Caracas',
         'phone' => '0212-2398820',
         'slug' =>'meritop',
         'color' => '#18445F',
         'member_format_import' => true,
         'system_currency' => 'puntos',
         'convertion_rate' => '1.7',
         'status' => true,
         'fk_id_country' => 245,
         'fk_id_currency' => 157,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('media')->insert([
                'slug' => 'img-default-1.jpg',
                'identificator' => 1,
                'table' => 'clubs',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
