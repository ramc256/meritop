<?php

use Illuminate\Database\Seeder;

class ProductsSubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $fk_id_products = array("1", "2", "3", "3", "3", "4", "5", "5", "6", "6");

      $fk_id_subcategories = array("28", "28", "22", "23", "24", "24", "28", "29", "22", "26");

      for ($i=0; $i < count($fk_id_products); $i++) {
          DB::table('products_subcategories')->insert([
            'fk_id_product' => $fk_id_products[$i],
            'fk_id_subcategory' => $fk_id_subcategories[$i],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
      }
  }
}

/*
select  categories.*, (select count(products_subcategories.fk_id_product)
										from products_subcategories
										join subcategories on (subcategories.id = products_subcategories.fk_id_subcategory)
										join categories as cate on (cate.id = subcategories.fk_id_category)
										where cate.id = categories.id) as total
from categories
 */

/*
select distinct categories.name, (select count(products_subcategories.fk_id_product)
                                       from products_subcategories
                                       join subcategories on (subcategories.id = products_subcategories.fk_id_subcategory)
                                       join categories as cate on (cate.id = subcategories.fk_id_category)
                                       where cate.id = categories.id)  as total
from products_subcategories
join subcategories on (subcategories.id = products_subcategories.fk_id_subcategory)
join categories on (categories.id = subcategories.fk_id_category)

 */
