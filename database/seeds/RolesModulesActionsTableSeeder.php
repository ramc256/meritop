<?php

use Illuminate\Database\Seeder;
use Meritop\Models\Module;

class RolesModulesActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = array("ver", "crear", "editar", "eliminar", "importar", "exportar", "listar");

        $modules = Module::orderBy('order')->get();

        foreach ($modules as $module) {
            for ($j = 1; $j <= count($actions); $j++) {
                DB::table('roles_modules_actions')->insert([
                    'fk_id_role' => '1',
                    'fk_id_module' => $module->id,
                    'fk_id_action' => $j,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }

        $club_modules = array("programas", "miembros", "consolidado", "estado de cuenta", "aporte de puntos",
        "grupos", "sedes", "departamentos", "banners", "mensajes");

        $club_modules_id  = Module::whereIn('name', $club_modules)->orderBy('order')->get();

        foreach ($club_modules_id as $module) {
            for ($j = 1; $j <= count($actions); $j++) {
                DB::table('roles_modules_actions')->insert([
                    'fk_id_role' => '2',
                    'fk_id_module' => $module->id,
                    'fk_id_action' => $j,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
  }
}
