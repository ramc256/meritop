<?php

use Illuminate\Database\Seeder;

class MembersClubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members_clubs')->insert([
            'carnet' => null,
            'email' => 'dev@meritop.com',
            'title' => 'usuario de desarrollo',
            'supervisor_code' => null,
            'status' => 1,
            'fk_id_member' => 1,
            'fk_id_club' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
