<?php

use Illuminate\Database\Seeder;

class PerspectivesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = array("Financiera", "Clientes", "Procesos", "Conocimiento", "Social");

        $descriptions = array('Objetivo enfocado hacia las finanzas', 'Objetivo enfocado hacia los clientes', 'Objetivo enfocado hacia los procesos',
        'Objetivo enfocado hacia los conocimientos', 'Objetivo enfocado hacia lo social');

        for ($i = 0; $i < count($names); $i++) {
          DB::table('perspectives')->insert([
             'name' => $names[$i],
             'description' => $descriptions[$i],
             'created_at' => date('Y-m-d H:i:s'),
             'updated_at' => date('Y-m-d H:i:s')
         ]);
        }
    }
}
