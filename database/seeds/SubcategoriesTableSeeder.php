<?php

use Illuminate\Database\Seeder;

class SubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $names = array("repuestos", "servicios", "belleza", "estética", "medicina", "odontología", "spa", "alimentos", "bebidas", "deportes", "entretenimiento", "frigoríficos", "piñaterías", "bar", "bodegón", "cafés", "comida rápida", "heladería", "panaderías", "restaurantes", "ferretería", "caballero", "calzado", "dama", "joyería", "ropa", "delivery", "celulares", "electrónicos", "destinos", "hoteles", "nacionales", "paquetes");

      $descriptions = array("subcategoría automotríz", "subcategoría automotríz", "subcategoría bienestar", "subcategoría bienestar", "subcategoría bienestar",
                            "subcategoría bienestar", "subcategoría bienestar", "subcategoría establecimientos", "subcategoría establecimientos", "subcategoría establecimientos", "subcategoría establecimientos", "subcategoría establecimientos", "subcategoría establecimientos",
                            "subcategoría gastronomía", "subcategoría gastronomía", "subcategoría gastronomía", "subcategoría gastronomía", "subcategoría gastronomía", "subcategoría gastronomía", "subcategoría gastronomía", "subcategoría hogar", "subcategoría moda", "subcategoría moda", "subcategoría moda", "subcategoría moda", "subcategoría moda", "subcategoría servicios", "subcategoría tecnología", "subcategoría tecnología", "subcategoría viajes", "subcategoría viajes", "subcategoría viajes", "subcategoría viajes");

      $slug = array("repuestos", "servicios", "belleza", "estética", "medicina", "odontología", "spa", "alimentos", "bebidas", "deportes", "entretenimiento", "frigoríficos", "piñaterías", "bar", "bodegón", "cafés", "comida rápida", "heladería", "panaderías", "restaurantes", "ferretería", "caballero", "calzado", "dama", "joyería", "ropa", "delivery", "celulares", "electrónicos", "destinos", "hoteles", "nacionales", "paquetes");

      $status = array("1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
                      "1", "1", "1", "1", "1", "1", "1", "1");

      $fk_id_categories = array("1", "1", "2", "2", "2", "2", "2", "3", "3", "3", "3", "3", "3", "5", "5", "5", "5", "5", "5", "5", "6", "7", "7",
                                "7", "7", "7", "8", "9", "9", "10", "10", "10", "10");

      for ($i = 0; $i < count($names); $i++) {
        DB::table("subcategories")->insert([
           "name" => $names[$i],
           "description" => $descriptions[$i],
           "slug" => $slug[$i],
           "status" => $status[$i],
           "created_at" => date("Y-m-d H:m:s"),
           "updated_at" => date("Y-m-d H:m:s"),
           "fk_id_category" => $fk_id_categories[$i]
       ]);
      }
    }
}
