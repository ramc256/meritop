<?php

use Illuminate\Database\Seeder;

class KindRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kind_roles')->insert([
            'name' => 'meritop',
            'description' => 'administrador meritop',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('kind_roles')->insert([
            'name' => 'club',
            'description' => 'administrador club',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
