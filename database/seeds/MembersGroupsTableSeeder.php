<?php

use Illuminate\Database\Seeder;

class MembersGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members_groups')->insert([
            'fk_id_member' => 1,
            'fk_id_group' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
