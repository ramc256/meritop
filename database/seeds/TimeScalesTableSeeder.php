<?php

use Illuminate\Database\Seeder;

class TimeScalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $day= array("1", "7", "30", "365");

        $descriptions = array('un día', 'una semana', 'un mes', 'un año');

        for ($i = 0; $i < count($day); $i++) {
          DB::table('time_scales')->insert([
             'day' => $day[$i],
             'description' => $descriptions[$i],
             'created_at' => date('Y-m-d H:i:s'),
             'updated_at' => date('Y-m-d H:i:s')
         ]);
        }
    }
}
