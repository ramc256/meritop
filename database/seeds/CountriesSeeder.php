<?php

use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        //Empty the countries table
        DB::table(\Config::get('countries.table_name'))->delete();
        $country_code = array("068", "180", "364", "408", "410", "498", "807", '834', "862");
        $correct_names = array("068" => "Bolivia", "180" => "Congo, the Democratic Republic", "364" => "Iran", "408" => "Korea, Democratic People's Republic",
        "410" => "Korea, Republic", "498" => "Moldova", "807" => "Macedonia", "834" => 'Tanzania', "862" => "Venezuela");
        //Get all of the countries
        $countries = Countries::getList();
        $i=1;
        $distinct_currency = [];
        foreach ($countries as $countryId => $country){
            if (in_array($country['country-code'], $country_code)) {
                $country['name'] = $correct_names[$country['country-code']];
            }
            DB::table(\Config::get('countries.table_name'))->insert(array(
                'id' => $i,
                'country_code' => $country['country-code'],
                'iso_3166_2' => $country['iso_3166_2'],
                'name' => $country['name'],
                'full_name' => ((isset($country['full_name'])) ? $country['full_name'] : null),
                // 'capital' => ((isset($country['capital'])) ? $country['capital'] : null),
                // 'citizenship' => ((isset($country['citizenship'])) ? $country['citizenship'] : null),
                // 'currency' => ((isset($country['currency'])) ? $country['currency'] : null),
                // 'currency_code' => ((isset($country['currency_code'])) ? $country['currency_code'] : null),
                // 'currency_sub_unit' => ((isset($country['currency_sub_unit'])) ? $country['currency_sub_unit'] : null),
                // 'currency_decimals' => ((isset($country['currency_decimals'])) ? $country['currency_decimals'] : null),
                // 'iso_3166_3' => $country['iso_3166_3'],
                // 'region_code' => $country['region-code'],
                // 'sub_region_code' => $country['sub-region-code'],
                // 'eea' => (bool)$country['eea'],
                // 'calling_code' => $country['calling_code'],
                // 'currency_symbol' => ((isset($country['currency_symbol'])) ? $country['currency_symbol'] : null),
                // 'flag' =>((isset($country['flag'])) ? $country['flag'] : null),
            ));

            // $this->command->info('currency: '.$country['currency'].' '. 'in_array: '. in_array($country['currency'], $distinct_currency));
            if ( !empty($country['currency']) && !in_array($country['currency'], $distinct_currency) ) {
                $distinct_currency[] = $country['currency'];
                DB::table('currencies')->insert(array(
                    'currency' => ((isset($country['currency'])) ? $country['currency'] : null),
                    'currency_code' => ((isset($country['currency_code'])) ? $country['currency_code'] : null),
                    'currency_symbol' => ((isset($country['currency_symbol'])) ? $country['currency_symbol'] : null),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ));
            }
            $i++;
        }
    }
}
