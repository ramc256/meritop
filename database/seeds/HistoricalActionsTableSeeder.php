<?php

use Illuminate\Database\Seeder;

class HistoricalActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $actions = array("crear", "actualizar", "eliminar", "importar", "exportar");

        for ($i = 0; $i < count($actions); $i++) {
          DB::table('historical_actions')->insert([
             'id_user' => 1,
             'action' => $actions[$i],
             'description' => $faker->text($maxNbChars = 200),
             'ip_address' => "127.0.0.1",
             'created_at' => date('Y-m-d H:i:s'),
             'updated_at' => date('Y-m-d H:i:s')
         ]);
        }
    }
}
