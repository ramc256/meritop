<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannedMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banned_members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fk_id_club');
            $table->unsignedInteger('fk_id_member');
            $table->timestamps();

            $table->foreign('fk_id_club')->references('id')->on('clubs');
            $table->foreign('fk_id_member')->references('id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banned_members');
    }
}
