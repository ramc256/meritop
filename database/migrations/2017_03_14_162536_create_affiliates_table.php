<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliates', function (Blueprint $table) {
           $table->increments('id');
            $table->string('alias',40)->nullable()->unique();
            $table->string('name',40)->nullable()->unique();
            $table->string('fiscal',20)->nullable()->unique();
            $table->string('address',200)->nullable();
            $table->boolean('status')->nullable();
            $table->integer('latitude')->nullable();
            $table->integer('longitude')->nullable();
            $table->string('phone',85)->nullable();
            $table->string('contact_person',100)->nullable();
            $table->string('email',50)->nullable();
            $table->string('open_hours',250)->nullable();
            $table->string('facebook',40);
            $table->string('twitter',40);
            $table->string('instagram',40);
            $table->string('google_plus',40);
            $table->string('slug',120)->nullable()->unique();
            $table->integer('fk_id_city')->nullable()->unsigned();

            $table->foreign('fk_id_city')->references('id')->on('cities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliates');
    }
}
