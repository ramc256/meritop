<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contributions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('total_users');
            $table->integer('processed_users');
            $table->integer('rejected_users');
            $table->double('total_points');
            $table->double('processed_points');
            $table->double('rejected_points');
            $table->string('file_name',40);
            $table->boolean('status');
            $table->ipAddress('ip_address');
            $table->unsignedInteger('fk_id_user');
            $table->unsignedInteger('fk_id_club');
            $table->timestamps();

            $table->foreign('fk_id_user')->references('id')->on('users');
            $table->foreign('fk_id_club')->references('id')->on('clubs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contributions');
    }
}
