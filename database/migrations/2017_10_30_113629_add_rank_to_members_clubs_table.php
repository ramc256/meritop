<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRankToMembersClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members_clubs', function (Blueprint $table) {
            $table->integer('rank')->nullable()->default(0)->after('supervisor_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members_clubs', function (Blueprint $table) {
            $table->dropColumn('rank');
        });
    }
}
