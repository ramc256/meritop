<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers_subcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_id_subcategory')->nullable()->unsigned();
            $table->integer('fk_id_offer')->nullable()->unsigned();
            $table->timestamps();

            //permite relacionar
            $table->foreign('fk_id_subcategory')->references('id')->on('categories');
            $table->foreign('fk_id_offer')->references('id')->on('offers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers_subcategories');
    }
}
