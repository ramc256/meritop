<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',40)->nullable();
            $table->string('excerpt',250)->nullable();
            $table->text('description')->nullable();
            $table->string('image',100)->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('quantity_per_user')->nullable();
            $table->integer('frequency')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->boolean('status')->nullable();
            $table->string('slug',120)->unique()->nullable();
            $table->boolean('delete')->unique()->nullable();
            $table->integer('fk_id_affiliate')->nullable()->unsigned();
            $table->integer('fk_id_time_scale')->nullable()->unsigned();

            $table->foreign('fk_id_time_scale')->references('id')->on('time_scales');
            $table->foreign('fk_id_affiliate')->references('id')->on('affiliates');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
