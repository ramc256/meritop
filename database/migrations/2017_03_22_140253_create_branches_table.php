<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',20);
            $table->string('name',200);
            $table->text('address');
            $table->string('postal_code',10);
            $table->string('dispatch_route',200)->nullable();
            $table->string('phone',85)->nullable();
            $table->string('secondary_phone',85)->nullable();
            $table->unsignedInteger('fk_id_club');
            $table->unsignedInteger('fk_id_city');
            $table->timestamps();

            $table->foreign('fk_id_club')->references('id')->on('clubs');
            $table->foreign('fk_id_city')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
