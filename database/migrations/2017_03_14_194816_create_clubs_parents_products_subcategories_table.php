<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsParentsProductsSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parents_products_subcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fk_id_parent_product');
            $table->unsignedInteger('fk_id_subcategory');
            $table->timestamps();

            $table->foreign('fk_id_parent_product')->references('id')->on('parents_products');
            $table->foreign('fk_id_subcategory')->references('id')->on('subcategories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parents_products_subcategories');
    }
}
