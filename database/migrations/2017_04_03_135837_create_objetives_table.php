<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjetivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',6)->unique();
            $table->string('name',40);
            $table->text('description');
            $table->date('start_date');
            $table->date('due_date');
            $table->boolean('status');
            $table->boolean('deleted')->default(false);
            $table->integer('fk_id_perspective')->unsigned();
            $table->integer('fk_id_program')->unsigned();
            $table->timestamps();

           $table->foreign('fk_id_perspective')->references('id')->on('perspectives');
           $table->foreign('fk_id_program')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objetives');
    }
}
