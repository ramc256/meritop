<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesModulesActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_modules_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fk_id_role');
            $table->unsignedInteger('fk_id_module');
            $table->unsignedInteger('fk_id_action');
            $table->timestamps();

            $table->foreign('fk_id_role')->references('id')->on('roles');
            $table->foreign('fk_id_module')->references('id')->on('modules');
            $table->foreign('fk_id_action')->references('id')->on('actions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles_modules_actions');
    }
}
