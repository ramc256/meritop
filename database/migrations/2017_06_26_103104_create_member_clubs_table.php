<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members_clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('carnet',20)->nullable();
            $table->string('email',50)->nullable();
            $table->string('title',250)->nullable();
            $table->string('supervisor_code',20)->nullable();
            $table->boolean('status');
            $table->unsignedInteger('fk_id_club');
            $table->unsignedInteger('fk_id_member');
            $table->timestamps();

            $table->foreign('fk_id_club')->references('id')->on('clubs');
            $table->foreign('fk_id_member')->references('id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members_clubs');
    }
}
