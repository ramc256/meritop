<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('operation');
            $table->double('points');
            $table->double('balance');
            $table->unsignedInteger('fk_id_account');
            $table->unsignedInteger('fk_id_club');
            $table->timestamps();

            $table->foreign('fk_id_account')->references('id')->on('accounts');
            $table->foreign('fk_id_club')->references('id')->on('clubs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts_transactions');
    }
}
