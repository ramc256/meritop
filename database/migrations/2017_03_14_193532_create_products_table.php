<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('feature', 200);
            $table->string('sku', 15)->unique();
            $table->string('barcode', 15)->unique();
            $table->string('excerpt', 250);
            $table->string('size', 20);
            $table->string('color', 30);
            $table->float('measure');
            $table->float('weight');
            $table->float('width');
            $table->float('heigth');
            $table->float('depth');
            $table->float('volume');
            $table->boolean('sell_out_stock')->default('0');
            $table->double('points', 10, 2);
            $table->boolean('status');
            $table->boolean('deleted')->default('0');
            $table->unsignedInteger('fk_id_parent_product');
            $table->timestamps();

            $table->foreign('fk_id_parent_product')->references('id')->on('parents_products');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
