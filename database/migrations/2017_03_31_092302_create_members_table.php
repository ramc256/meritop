 <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dni',40)->unique();
            $table->string('first_name',40)->nullable();
            $table->string('last_name',40)->nullable();
            $table->string('biography',250)->nullable();
            $table->date('birthdate')->nullable();
            $table->string('password',255);
            $table->string('email',100);
            $table->string('cell_phone',85)->nullable();
            $table->boolean('gender')->nullable();
            $table->string('image',100)->nullable();
            $table->boolean('status');
            $table->boolean('deleted')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
