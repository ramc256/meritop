<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups_programs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fk_id_group');
            $table->unsignedInteger('fk_id_program');
            $table->timestamps();

            $table->foreign('fk_id_group')->references('id')->on('groups');
            $table->foreign('fk_id_program')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups_programs');
    }
}
