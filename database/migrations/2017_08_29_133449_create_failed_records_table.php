<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFailedRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dni',45);
            $table->string('carnet',20);
            $table->string('objetive_code',6);
            $table->double('points');
            $table->boolean('status');
            $table->unsignedInteger('fk_id_contribution');
            $table->timestamps();

            $table->foreign('fk_id_contribution')->references('id')->on('contributions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_records');
    }
}
