<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guides', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_guide',20);
            $table->string('note',200)->nullable();
            $table->string('internal_note',200)->nullable();
            $table->unsignedInteger('fk_id_order');
            $table->unsignedInteger('fk_id_agent');
            $table->timestamps();

            $table->foreign('fk_id_order')->references('id')->on('orders');
            $table->foreign('fk_id_agent')->references('id')->on('agents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guides');
    }
}
