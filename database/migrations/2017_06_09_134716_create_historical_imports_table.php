<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricalImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name',40);
            $table->boolean('status');
            $table->ipAddress('ip_address');
            $table->integer('total_records');
            $table->integer('success_records');
            $table->integer('fail_records');
            $table->string('kind',40);
            $table->unsignedInteger('fk_id_user');
            $table->timestamps();

            $table->foreign('fk_id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_imports');
    }
}
