<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',16)->unique();
            $table->double('points');
            $table->unsignedInteger('fk_id_member');
            $table->unsignedInteger('fk_id_kind_account');
            $table->timestamps();

            $table->foreign('fk_id_member')->references('id')->on('members');
            $table->foreign('fk_id_kind_account')->references('id')->on('kind_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
