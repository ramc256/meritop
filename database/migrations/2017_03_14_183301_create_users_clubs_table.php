<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fk_id_user')->nullable();
            $table->unsignedInteger('fk_id_club')->nullable();
            $table->timestamps();

            //permite relacionar
            $table->foreign('fk_id_user')->references('id')->on('users');
            $table->foreign('fk_id_club')->references('id')->on('clubs');
        });
    }

    /**
     * Reverse the migrations.|
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_clubs');
    }
}
