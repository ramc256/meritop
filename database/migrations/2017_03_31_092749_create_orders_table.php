<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->string('note',200)->nullable();
            $table->string('internal_note',200)->nullable();
            $table->unsignedInteger('id_order_reference')->nullable();
            $table->integer('discount')->nullable();
            $table->unsignedInteger('fk_id_member');
            $table->unsignedInteger('fk_id_branch');
            $table->timestamps();

            $table->foreign('fk_id_member')->references('id')->on('members');
            $table->foreign('fk_id_branch')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
