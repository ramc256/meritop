<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsParentsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs_parents_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fk_id_club');
            $table->unsignedInteger('fk_id_parent_product');
            $table->timestamps();

            //permite relacionar
            $table->foreign('fk_id_club')->references('id')->on('clubs');
            $table->foreign('fk_id_parent_product')->references('id')->on('parents_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs_parents_products');
    }
}
