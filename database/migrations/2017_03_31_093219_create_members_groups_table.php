<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fk_id_member');
            $table->unsignedInteger('fk_id_group');
            $table->timestamps();

            $table->foreign('fk_id_member')->references('id')->on('members');
            $table->foreign('fk_id_group')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members_groups');
    }
}
