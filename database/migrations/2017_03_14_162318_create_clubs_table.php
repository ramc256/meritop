<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',200);
            $table->string('alias',100)->unique();
            $table->string('fiscal',20)->unique();
            $table->string('state',80);
            $table->string('city',80);
            $table->string('address',200);
            $table->string('phone',85);
            $table->string('slug',120)->unique();
            $table->string('color',30);
            $table->boolean('member_format_import');
            $table->string('system_currency',40);
            $table->float('convertion_rate');
            $table->boolean('status');
            $table->unsignedInteger('fk_id_country');
            $table->unsignedInteger('fk_id_currency');
            $table->timestamps();

            $table->foreign('fk_id_country')->references('id')->on('countries');
            $table->foreign('fk_id_currency')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
