<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parents_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',200);
            $table->string('model',100);
            $table->text('description');
            $table->boolean('warranty')->nullable()->default('0');
            $table->integer('warranty_day')->nullable();
            $table->text('warranty_conditions')->nullable();
            $table->text('delivery_conditions');
            $table->longText('tags')->nullable();
            $table->boolean('deleted')->default('0');
            $table->unsignedInteger('fk_id_brand');
            $table->unsignedInteger('fk_id_country');
            $table->unsignedInteger('fk_id_market');
            $table->timestamps();

            $table->foreign('fk_id_brand')->references('id')->on('brands');
            $table->foreign('fk_id_country')->references('id')->on('countries');
            $table->foreign('fk_id_market')->references('id')->on('markets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parents_products');
    }
}
