<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name',40);
            $table->string('last_name',40);
            $table->string('user_name')->unique();
            $table->string('image',100)->nullable();
            $table->string('email',50);
            $table->string('password',80);
            $table->boolean('kind');
            $table->boolean('status');
            $table->boolean('deleted')->default(false);
            $table->integer('fk_id_country')->unsigned();
            $table->integer('fk_id_role')->unsigned();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('fk_id_country')->references('id')->on('countries');
            $table->foreign('fk_id_role')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
