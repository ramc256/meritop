<?php
use Meritop\Models\MemberDepartment;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
// $factory->define(Meritop\User::class, function (Faker\Generator $faker) {
//     static $password;
//
//     return [
//         'name' => $faker->name,
//         'email' => $faker->unique()->safeEmail,
//         'password' => $password ?: $password = bcrypt('secret'),
//         'remember_token' => str_random(10),
//     ];
// });

$factory->define(Meritop\Models\MemberGroup::class, function (Faker\Generator $faker) {

    return [
        'fk_id_member' => $faker->numberBetween($min = 1, $max = 50),
        'fk_id_group' => $faker->numberBetween($min = 1, $max = 5),
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(Meritop\Models\MemberBranch::class, function (Faker\Generator $faker) {

    return [
        'fk_id_member' => $faker->numberBetween($min = 1, $max = 50),
        'fk_id_branch' => $faker->numberBetween($min = 1, $max = 50),
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(Meritop\Models\KindAccount::class, function (Faker\Generator $faker) {

    return [
        'name' => 'meritop',
        'description' => 'cuenta de tipo puntos',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(Meritop\Models\Account::class, function (Faker\Generator $faker) {

    return [
        'code' => $faker->regexify('[0-9]{16}'),
        'points' => $faker->randomNumber(),
        'fk_id_member' => $faker->numberBetween($min = 1, $max = 50),
        'fk_id_kind_account' => 1,
        'created_at' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = '+1 years'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(Meritop\Models\AccountTransaction::class, function (Faker\Generator $faker) {

    return [
        'description' => $faker->text($maxNbChars = 200),
        'operation' => $faker->numberBetween($min = 0, $max = 1),
        'points' => $faker->randomNumber(),
        'fk_id_club' => $faker->numberBetween($min = 1, $max = 4),
        'fk_id_account' => $faker->numberBetween($min = 1, $max = 50),
        'created_at' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = '+1 years'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(Meritop\Models\HistoricalImport::class, function (Faker\Generator $faker) {

    return [
        'file_name' => $faker->text($maxNbChars = 40),
        'status' => $faker->boolean($chanceOfGettingTrue = 50),
        'ip_address' => $faker->ipv4,
        'total_records' => $faker->randomNumber(),
        'success_records' => $faker->randomNumber(),
        'fail_records' => $faker->randomNumber(),
        'kind' => $faker->text($maxNbChars = 40),
        'fk_id_user' => $faker->numberBetween($min = 1, $max = 1),
        'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(Meritop\Models\Order::class, function (Faker\Generator $faker) {

    return [
        'status' => $faker->boolean($chanceOfGettingTrue = 50),
        'note' => $faker->text($maxNbChars = 200),
        'internal_note' => $faker->text($maxNbChars = 200),
        'discount' => $faker->numberBetween($min = 1, $max = 8),
        'fk_id_member' => $faker->numberBetween($min = 1, $max = 50),
        'fk_id_branch' => $faker->numberBetween($min = 1, $max = 50),
        'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(Meritop\Models\OrderVariant::class, function (Faker\Generator $faker) {

    return [
        'quantity' => $faker->numberBetween($min = 1, $max = 150),
        'points' => $faker->numberBetween($min = 15, $max = 900000),
        'fk_id_variant' => $faker->numberBetween($min = 1, $max = 6),
        'fk_id_order' => $faker->numberBetween($min = 1, $max = 50),
        'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(Meritop\Models\Guide::class, function (Faker\Generator $faker) {

    return [
        'num_guide' => $faker->numberBetween($min = 100000, $max = 500000),
        'note' => $faker->text($maxNbChars = 200),
        'fk_id_agent' => $faker->numberBetween($min = 1, $max = 3),
        'fk_id_order' => $faker->numberBetween($min = 1, $max = 50),
        'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});
